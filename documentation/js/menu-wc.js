'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">cobranzas-v2 documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/ActividadesConClientesPageModule.html" data-type="entity-link">ActividadesConClientesPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ActividadesConClientesPageModule-c47e25656f2acad832696b64bdf851bd"' : 'data-target="#xs-components-links-module-ActividadesConClientesPageModule-c47e25656f2acad832696b64bdf851bd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ActividadesConClientesPageModule-c47e25656f2acad832696b64bdf851bd"' :
                                            'id="xs-components-links-module-ActividadesConClientesPageModule-c47e25656f2acad832696b64bdf851bd"' }>
                                            <li class="link">
                                                <a href="components/ActividadesConClientesPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ActividadesConClientesPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-0f68a344afb6f6e73a5be9932d3baf8b"' : 'data-target="#xs-components-links-module-AppModule-0f68a344afb6f6e73a5be9932d3baf8b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-0f68a344afb6f6e73a5be9932d3baf8b"' :
                                            'id="xs-components-links-module-AppModule-0f68a344afb6f6e73a5be9932d3baf8b"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AsignacionPageModule.html" data-type="entity-link">AsignacionPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AsignacionPageModule-390900eb87f9ba37240f44251475d242"' : 'data-target="#xs-components-links-module-AsignacionPageModule-390900eb87f9ba37240f44251475d242"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AsignacionPageModule-390900eb87f9ba37240f44251475d242"' :
                                            'id="xs-components-links-module-AsignacionPageModule-390900eb87f9ba37240f44251475d242"' }>
                                            <li class="link">
                                                <a href="components/AsignacionPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AsignacionPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CambiarPassPageModule.html" data-type="entity-link">CambiarPassPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CambiarPassPageModule-ac6e269c2624f6bc8a2412ad87c3d2a6"' : 'data-target="#xs-components-links-module-CambiarPassPageModule-ac6e269c2624f6bc8a2412ad87c3d2a6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CambiarPassPageModule-ac6e269c2624f6bc8a2412ad87c3d2a6"' :
                                            'id="xs-components-links-module-CambiarPassPageModule-ac6e269c2624f6bc8a2412ad87c3d2a6"' }>
                                            <li class="link">
                                                <a href="components/CambiarPassPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CambiarPassPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CrearReciboPageModule.html" data-type="entity-link">CrearReciboPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CrearReciboPageModule-6e3f2d0f9c94339f159259409b0ad850"' : 'data-target="#xs-components-links-module-CrearReciboPageModule-6e3f2d0f9c94339f159259409b0ad850"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CrearReciboPageModule-6e3f2d0f9c94339f159259409b0ad850"' :
                                            'id="xs-components-links-module-CrearReciboPageModule-6e3f2d0f9c94339f159259409b0ad850"' }>
                                            <li class="link">
                                                <a href="components/CrearReciboPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CrearReciboPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DocumentosPageModule.html" data-type="entity-link">DocumentosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DocumentosPageModule-b882b01a090abe304ec3b8d22f72ac92"' : 'data-target="#xs-components-links-module-DocumentosPageModule-b882b01a090abe304ec3b8d22f72ac92"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DocumentosPageModule-b882b01a090abe304ec3b8d22f72ac92"' :
                                            'id="xs-components-links-module-DocumentosPageModule-b882b01a090abe304ec3b8d22f72ac92"' }>
                                            <li class="link">
                                                <a href="components/DocumentosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DocumentosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FacturasPageModule.html" data-type="entity-link">FacturasPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FacturasPageModule-5ed98cfdc958c5db4e5de954bba2d34c"' : 'data-target="#xs-components-links-module-FacturasPageModule-5ed98cfdc958c5db4e5de954bba2d34c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FacturasPageModule-5ed98cfdc958c5db4e5de954bba2d34c"' :
                                            'id="xs-components-links-module-FacturasPageModule-5ed98cfdc958c5db4e5de954bba2d34c"' }>
                                            <li class="link">
                                                <a href="components/FacturasPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FacturasPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomePageModule.html" data-type="entity-link">HomePageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomePageModule-05f8e3bca7e728edb1768ce5a2163b7f"' : 'data-target="#xs-components-links-module-HomePageModule-05f8e3bca7e728edb1768ce5a2163b7f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomePageModule-05f8e3bca7e728edb1768ce5a2163b7f"' :
                                            'id="xs-components-links-module-HomePageModule-05f8e3bca7e728edb1768ce5a2163b7f"' }>
                                            <li class="link">
                                                <a href="components/HomePage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomePage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginPageModule.html" data-type="entity-link">LoginPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' : 'data-target="#xs-components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' :
                                            'id="xs-components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' }>
                                            <li class="link">
                                                <a href="components/LoginPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotasPageModule.html" data-type="entity-link">NotasPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotasPageModule-fd3a3e588b4b36f381f52222d308ed49"' : 'data-target="#xs-components-links-module-NotasPageModule-fd3a3e588b4b36f381f52222d308ed49"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotasPageModule-fd3a3e588b4b36f381f52222d308ed49"' :
                                            'id="xs-components-links-module-NotasPageModule-fd3a3e588b4b36f381f52222d308ed49"' }>
                                            <li class="link">
                                                <a href="components/NotasPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotasPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PipesModuleModule.html" data-type="entity-link">PipesModuleModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-PipesModuleModule-4f38985c684138d5239e315352b8717e"' : 'data-target="#xs-pipes-links-module-PipesModuleModule-4f38985c684138d5239e315352b8717e"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-PipesModuleModule-4f38985c684138d5239e315352b8717e"' :
                                            'id="xs-pipes-links-module-PipesModuleModule-4f38985c684138d5239e315352b8717e"' }>
                                            <li class="link">
                                                <a href="pipes/FilterPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FilterPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/MomentPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MomentPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/MonedaPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MonedaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RecibosCreadosPageModule.html" data-type="entity-link">RecibosCreadosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RecibosCreadosPageModule-d76ba9491f8e6673801ecb36e2ca0725"' : 'data-target="#xs-components-links-module-RecibosCreadosPageModule-d76ba9491f8e6673801ecb36e2ca0725"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RecibosCreadosPageModule-d76ba9491f8e6673801ecb36e2ca0725"' :
                                            'id="xs-components-links-module-RecibosCreadosPageModule-d76ba9491f8e6673801ecb36e2ca0725"' }>
                                            <li class="link">
                                                <a href="components/RecibosCreadosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RecibosCreadosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ResumenCobroPageModule.html" data-type="entity-link">ResumenCobroPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ResumenCobroPageModule-169478181784a68911e8fd4147231af8"' : 'data-target="#xs-components-links-module-ResumenCobroPageModule-169478181784a68911e8fd4147231af8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ResumenCobroPageModule-169478181784a68911e8fd4147231af8"' :
                                            'id="xs-components-links-module-ResumenCobroPageModule-169478181784a68911e8fd4147231af8"' }>
                                            <li class="link">
                                                <a href="components/ResumenCobroPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ResumenCobroPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SeleccionarPuntoUbicacionPageModule.html" data-type="entity-link">SeleccionarPuntoUbicacionPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SeleccionarPuntoUbicacionPageModule-83cec2f73d66b4a14dc2cea4fe79202f"' : 'data-target="#xs-components-links-module-SeleccionarPuntoUbicacionPageModule-83cec2f73d66b4a14dc2cea4fe79202f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SeleccionarPuntoUbicacionPageModule-83cec2f73d66b4a14dc2cea4fe79202f"' :
                                            'id="xs-components-links-module-SeleccionarPuntoUbicacionPageModule-83cec2f73d66b4a14dc2cea4fe79202f"' }>
                                            <li class="link">
                                                <a href="components/SeleccionarPuntoUbicacionPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SeleccionarPuntoUbicacionPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Asignacion.html" data-type="entity-link">Asignacion</a>
                            </li>
                            <li class="link">
                                <a href="classes/Cliente.html" data-type="entity-link">Cliente</a>
                            </li>
                            <li class="link">
                                <a href="classes/Concepto.html" data-type="entity-link">Concepto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateReceiptData.html" data-type="entity-link">CreateReceiptData</a>
                            </li>
                            <li class="link">
                                <a href="classes/Cuenta.html" data-type="entity-link">Cuenta</a>
                            </li>
                            <li class="link">
                                <a href="classes/DatosUsuario.html" data-type="entity-link">DatosUsuario</a>
                            </li>
                            <li class="link">
                                <a href="classes/DocumentData.html" data-type="entity-link">DocumentData</a>
                            </li>
                            <li class="link">
                                <a href="classes/Documentos.html" data-type="entity-link">Documentos</a>
                            </li>
                            <li class="link">
                                <a href="classes/DocumentWithCheck.html" data-type="entity-link">DocumentWithCheck</a>
                            </li>
                            <li class="link">
                                <a href="classes/Entidad.html" data-type="entity-link">Entidad</a>
                            </li>
                            <li class="link">
                                <a href="classes/Factura.html" data-type="entity-link">Factura</a>
                            </li>
                            <li class="link">
                                <a href="classes/FacturaWithCheck.html" data-type="entity-link">FacturaWithCheck</a>
                            </li>
                            <li class="link">
                                <a href="classes/Impresora.html" data-type="entity-link">Impresora</a>
                            </li>
                            <li class="link">
                                <a href="classes/InvoiceWithACobrar.html" data-type="entity-link">InvoiceWithACobrar</a>
                            </li>
                            <li class="link">
                                <a href="classes/MensajeResponse.html" data-type="entity-link">MensajeResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/Moneda.html" data-type="entity-link">Moneda</a>
                            </li>
                            <li class="link">
                                <a href="classes/NewCobroRequest.html" data-type="entity-link">NewCobroRequest</a>
                            </li>
                            <li class="link">
                                <a href="classes/NewDocumentResponse.html" data-type="entity-link">NewDocumentResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/NewNotasResponse.html" data-type="entity-link">NewNotasResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/NewReceiptResponse.html" data-type="entity-link">NewReceiptResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/Notas.html" data-type="entity-link">Notas</a>
                            </li>
                            <li class="link">
                                <a href="classes/NuevaNota.html" data-type="entity-link">NuevaNota</a>
                            </li>
                            <li class="link">
                                <a href="classes/NumHabilitacion.html" data-type="entity-link">NumHabilitacion</a>
                            </li>
                            <li class="link">
                                <a href="classes/Recibo.html" data-type="entity-link">Recibo</a>
                            </li>
                            <li class="link">
                                <a href="classes/ReciboDTO.html" data-type="entity-link">ReciboDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValorRecibo.html" data-type="entity-link">ValorRecibo</a>
                            </li>
                            <li class="link">
                                <a href="classes/Value.html" data-type="entity-link">Value</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValueType.html" data-type="entity-link">ValueType</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ActividadesLocalesService.html" data-type="entity-link">ActividadesLocalesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AlertControllerService.html" data-type="entity-link">AlertControllerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BDManagementService.html" data-type="entity-link">BDManagementService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ClientesBDService.html" data-type="entity-link">ClientesBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ClientesLogicaService.html" data-type="entity-link">ClientesLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConceptoLogicaService.html" data-type="entity-link">ConceptoLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConceptosBDService.html" data-type="entity-link">ConceptosBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfigService.html" data-type="entity-link">ConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CuentaLogicaService.html" data-type="entity-link">CuentaLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CuentasBDService.html" data-type="entity-link">CuentasBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DatosOnlineService.html" data-type="entity-link">DatosOnlineService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DocumentosLogicaService.html" data-type="entity-link">DocumentosLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EntidadesBDService.html" data-type="entity-link">EntidadesBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EntidadLogicaService.html" data-type="entity-link">EntidadLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EntregasDocBDService.html" data-type="entity-link">EntregasDocBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacturasBDService.html" data-type="entity-link">FacturasBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FacturasLogicaService.html" data-type="entity-link">FacturasLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GeolocalizacionLogicaService.html" data-type="entity-link">GeolocalizacionLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HabilitacionesBDService.html" data-type="entity-link">HabilitacionesBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HttpService.html" data-type="entity-link">HttpService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ImpresoraLogicaService.html" data-type="entity-link">ImpresoraLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoadingControllerService.html" data-type="entity-link">LoadingControllerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MonedaLogicaService.html" data-type="entity-link">MonedaLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MonedasBDService.html" data-type="entity-link">MonedasBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NetworkControllerService.html" data-type="entity-link">NetworkControllerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotasBDService.html" data-type="entity-link">NotasBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotasLogicaService.html" data-type="entity-link">NotasLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotAuthGuardService.html" data-type="entity-link">NotAuthGuardService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ReciboRequestService.html" data-type="entity-link">ReciboRequestService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RecibosBDService.html" data-type="entity-link">RecibosBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RecibosLogicaService.html" data-type="entity-link">RecibosLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SesionService.html" data-type="entity-link">SesionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SincronizacionService.html" data-type="entity-link">SincronizacionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TablesManageService.html" data-type="entity-link">TablesManageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ToastControllerService.html" data-type="entity-link">ToastControllerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsuariosBDService.html" data-type="entity-link">UsuariosBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsuarioService.html" data-type="entity-link">UsuarioService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UtilitariosService.html" data-type="entity-link">UtilitariosService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValoresBDService.html" data-type="entity-link">ValoresBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValoresLogicaService.html" data-type="entity-link">ValoresLogicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValorInteresService.html" data-type="entity-link">ValorInteresService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WriteFileService.html" data-type="entity-link">WriteFileService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise-inverted.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});