import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonedaPipe } from './moneda/moneda.pipe';
import { MomentPipe } from './moment/moment.pipe';
import { FilterPipe } from './filtrado/filter.pipe';

@NgModule({
  declarations: [MonedaPipe,MomentPipe,FilterPipe],
  imports: [
    CommonModule
  ],
  exports:[MonedaPipe,MomentPipe,FilterPipe],
})
export class PipesModuleModule { }
