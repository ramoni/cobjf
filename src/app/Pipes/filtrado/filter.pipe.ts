import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], value1: string, value2: string): any[] {

    let filter = { id_entidad: value1, id_moneda: value2 };
    let itemes = [];
    console.log('El filtro es ', filter);
    console.log('Los items son ', items);
    if (!items) return [];
    itemes = items.filter(item => {
      return item.id_entidad === filter.id_entidad && item.id_moneda === filter.id_moneda;
    })
    /*return items.filter(item =>{
      let notMatchingField = Object.keys(filter)
        .find(key => item[key] !== filter[key]);
      return !notMatchingField; // true if matches all field
  })*/
    console.log('el retorno', itemes)
    return itemes;
  }

}
