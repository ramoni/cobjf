import { Pipe, PipeTransform } from '@angular/core';
import { MonedaLogicaService } from 'src/app/Servicios/monedaService/moneda-logica.service';

@Pipe({
  name: 'moneda'
})
export class MonedaPipe implements PipeTransform {
  constructor(private logicaMoneda: MonedaLogicaService) {
  }
  transform(value: any,
    currencySign: string = '',
    decimalLength: number = 0, 
    chunkDelimiter: string = '.', 
    decimalDelimiter:string = ',',
    chunkLength: number = 3): string {
    console.log('moneda pipe value', value, 'currencySign ', currencySign );
    if (!currencySign) currencySign = 'GUARANIES';

    let moneda = this.logicaMoneda.getByIdOrName(currencySign);

    if (moneda) {
        currencySign = moneda.simbolo;
        decimalLength = moneda.cant_decimales;
    } else {
        if (currencySign === 'DOLARES') {
            currencySign = 'USD. ';
            decimalLength = 2;
        } else {
            currencySign = 'Gs. ';
            decimalLength = 0;
        }
    }

    var result = '\\d(?=(\\d{' + chunkLength + '})+' + (decimalLength > 0 ? '\\D' : '$') + ')'
    var num = value.toFixed(Math.max(0, ~~decimalLength));

    return (decimalDelimiter ? num.replace('.', decimalDelimiter) : num).replace(new RegExp(result, 'g'), '$&' + chunkDelimiter) +' '+ currencySign;
}

}
