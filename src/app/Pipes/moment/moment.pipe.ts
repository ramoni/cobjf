import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'
@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {

  transform(value: string, format: string = 'DD/MM/YYYY'): any {
    if (!value) return '';
    return moment(value, 'DD-MMM-YYYY').format(format);
}

}
