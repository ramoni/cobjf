import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NavParams, NavController, ModalController, Platform } from '@ionic/angular';

declare var google;
@Component({
  selector: 'app-seleccionar-punto-ubicacion',
  templateUrl: './seleccionar-punto-ubicacion.page.html',
  styleUrls: ['./seleccionar-punto-ubicacion.page.scss'],
})

export class SeleccionarPuntoUbicacionPage implements OnInit {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  startPos: any;
  marker: any;
  autocomplete: any;
  autocompleteItems = [];
  GoogleAutocomplete: any;
  geocoder: any;

  constructor(public navCtrl: NavController,
    private navParams: NavParams,
    private modal: ModalController,
    private geo: Geolocation,
    private plat: Platform,
    public zone: NgZone) {

    this.startPos = this.navParams.data.position;

    if (!this.startPos.lat || !this.startPos.lng || this.startPos.lat === 0 || this.startPos.l === 0)
      this.startPos = null;

    console.log("la ubicacion ", this.startPos);

    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
    this.geocoder = new google.maps.Geocoder;
    this.initializeBackButtonCustomHandler()
  }

  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      this.dismiss();
    })
  }

  ngOnInit() {
    if (this.startPos) {
      console.log('tiene ubicacion')
      this._buildMap(this.startPos.lat, this.startPos.lng);
      this._showPoint(this.startPos.lat, this.startPos.lng);
    } else {
      this.geo.getCurrentPosition().then(pos => {
        this._buildMap(pos.coords.latitude, pos.coords.longitude);
        this._showPoint(pos.coords.latitude, pos.coords.longitude);
      });
    }

  }

  _buildMap(lat: number, lng: number) {
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: { lat: lat, lng: lng },
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
  }

  _showPoint(lat: number, lng: number) {
    console.log('direccion en showpoint', lat, lng)
    this.marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: {
        lat: lat,
        lng: lng
      },
      draggable: true,

    });

    var infoWindow = new google.maps.InfoWindow({
      content: "Ubicacion!"
    });

    google.maps.event.addListener(this.marker, 'click', function () {
      infoWindow.open(this.map, this.marker);

    });
  
  }

  aceptar() {
    console.log('el marker position', this.marker.position)
    let lat = this.marker.position.lat();
    let long = this.marker.position.lng();
    let ubicacion = { lat: lat, lng: long }
    this.dismissData(ubicacion);

  }
  updateSearchResults() {
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
      (predictions) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
        });
      });
  }

  selectSearchResult(item) {
    //this.clearMarkers();
    this.geocoder.geocode({ 'placeId': item.place_id }, (results, status) => {
      if (status === 'OK' && results[0]) {
        console.log('results', results)
        let position = {
          lat: results[0].geometry.location.lat,
          lng: results[0].geometry.location.lng
        };
        /*let marker = new google.maps.Marker({
          position: results[0].geometry.location,
          map: this.map,
        });*/
        //this.markers.push(marker);
        //this.map.setCenter(results[0].geometry.location);
        console.log('position ', position)
        this.autocomplete.input = '';
        this.autocompleteItems = [];
        this.map.setCenter(results[0].geometry.location);
        this._showPoint(Number(position.lat()), Number(position.lng()))
      }
    }, error => {
      console.log('error', error)
    })
  }
  async dismissData(ubicacion: any) {
    await this.modal.dismiss(ubicacion)
  }
  async dismiss() {
    await this.modal.dismiss();
  }

}

