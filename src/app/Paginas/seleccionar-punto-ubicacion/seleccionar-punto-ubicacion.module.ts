import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SeleccionarPuntoUbicacionPage } from './seleccionar-punto-ubicacion.page';

const routes: Routes = [
  {
    path: '',
    component: SeleccionarPuntoUbicacionPage
  }
];

@NgModule({
  entryComponents:[SeleccionarPuntoUbicacionPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SeleccionarPuntoUbicacionPage]
})
export class SeleccionarPuntoUbicacionPageModule {}
