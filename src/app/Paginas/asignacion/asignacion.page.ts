import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/Clases/cliente';
import { Platform, NavController } from '@ionic/angular';
import { ClientesLogicaService } from 'src/app/Servicios/clienteService/clientes-logica.service';
import {  WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';

@Component({
  selector: 'app-asignacion',
  templateUrl: './asignacion.page.html',
  styleUrls: ['./asignacion.page.scss'],
})
export class AsignacionPage implements OnInit {
  
  /** Dice si hay o no mas registros que traer */
  noMoreData: boolean = false;
  /** Dice si hay o no infinite scroll */
  public infinite: boolean = true;
  /** Representa desde que posición se traeran los registros */
  nextOffset: number;
  /** La cantidad de registros que se traerán por cada llamada */
  limit: number = 10;
  /** Registros (Clientes con sus respectivos datos) */
  clientes: Array<Cliente> = [];
  /** Tipo de registros que se traerá */
  tipoFiltro: string;
  /** Filtro de los registros de un cliente */
  cliente: string = null;
  /** El número de habilitacion actual */
  habilitacion: number;
  /** Dice si el boton esta habilitado o no dependiendo de la habilitacion */
  buttonCondition: boolean = false;
 
  constructor(private toast: ToastControllerService,
    private loadingService: LoadingControllerService,
    private logic: ClientesLogicaService,
    private plat: Platform,
    private navCtrl: NavController,
    private alertCtrl: AlertControllerService,
    private writeLog:  WriteFileService,
  ) {
    
  }

  /**
   * Inicializa los parámetros y carga los primeros registros
   */
  ngOnInit() {
    this.tipoFiltro = 'N';
    this.habilitacion = Number(JSON.parse(localStorage.getItem('habilitacion')));
    if (this.habilitacion > 0) {
      this.buttonCondition = false
    }else {
      this.buttonCondition = true
    }
    this.nextOffset = 0;
    this.noMoreData = false;
    this._doLoad(false);
  }
  
  /**
   * Inicializar acciones que solo pueden ejecutarse cada vez que se entra a la página
   */
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
   }

  /**
   * Inicializa el boton de atras, para que al ejecutarse se envie el home
   */
  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0,() => {
      this.navCtrl.navigateRoot(['home']);
    })
  }

  /**
   * Se ejecuta al realizar un refresh, prepara los parametros necesarios para filtrar los registros y luego llama al método que se
   * encarga de obtenerlos
   * @param {any} refresher Evento refresh
   */
  loadData(refresher: any = null) {

    this.noMoreData = false;
    this.nextOffset = 0;
    this.limit = 10;
    this.clientes = [];
    this.infinite = true;
    this._doLoad(false).then( () => {
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
    }).catch(err=>{
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
      console.log('error en refresher ',err)
    });
  }

  /**
   * Se ejecuta al llamar al infinite scroll, si ya no hay mas registros que traer lo muestra en un toast
   * @param {any} event El evento del infinite scroll
   */
  doInfinite(event) {
    this._doLoad(true).then( () => {
      event.target.complete();
      if (this.noMoreData) {
        this.infinite = false;
        event.target.disabled = true;
        this.toast.presentToastDanger("No hay mas clientes")
        
      }
    }).catch(err=>{
      console.log('error en infinite scroll',err)
      event.target.complete();
      this.toast.presentToastDanger(JSON.parse(err.error).error)
    })
  }

  /**
   * Muestra la pantalla de carga y llama al método para traer los registros de la base de datos
   * @param {boolean} append Decide si va a añadir mas registros a lo existente o no
   */
  _doLoad(append: boolean = false): Promise<Array<Cliente>> {
   
    this.loadingService.present("Obteniendo clientes")
    let ob = this._buildPromise();
    ob.then(value => {
      this.loadingService.dismiss();
      if (append) {
        this.clientes = this.clientes.concat(value);

      }else {
        this.clientes = value;

      }
      this.noMoreData = value.length < 10;

      this.nextOffset += value.length;
      if (this.nextOffset <= 10) {
        this.nextOffset += 1;
      }
      this.limit += value.length;

    }).catch( error => {
      this.loadingService.dismiss();
      this.toast.presentToastDanger(JSON.parse(error.error).error);
      //this.presentToastSimple("El servidor no esta disponible, intente mas tarde");
    });
    return ob;
  }

  /**
   * Trae los registros de la api con los parametros requeridos
   */
  _buildPromise(): Promise<Array<Cliente>> {
    return this.logic.getVisitasOnline(this.nextOffset, this.limit, this.cliente, this.tipoFiltro);
  }

  /**
   * Se ejecuta al realizar una busqueda en el cuadro de búsqueda, prepara los parámetros básicos 
   * y llama al método para cargar los registros
   */
  buscarClientes() {
    this.nextOffset = 0;
    this.noMoreData = false;
    this.limit = 10;
    this._doLoad(false);
  }

  /**
   * Limpia todos los campos para cancelar la busqueda
   */
  cancelarBusqueda() {
    this.cliente = null;
    this.nextOffset = 0;
    this.noMoreData = false;
    this.limit = 10;
    this._doLoad(false);
  }

  /**
  * Asigna el cliente al usuario logueado
  * @param {Cliente} cliente Cliente al cual desea autoasignarse
  */
  asignarCliente(cliente: Cliente) {
    let mensaje = 'Desea asignarse el cliente ' + cliente.razonSocial + ' ?';
    this.alertCtrl.alertMessageWithHandler("Asignacion de Cliente", mensaje, async d => {
    
      await this.writeLog.escribirLog(new Date() + 'Peticion: Asignacion de Cliente ' + cliente.razonSocial + '\n')

      this.loadingService.present('Asignando ...')
      this.logic.guardarCliente(cliente).then(async response => {
     
        this.loadingService.dismiss();
        await this.writeLog.escribirLog(new Date() + 'Respuesta con exito: ' + JSON.stringify(response) + '\n');
        
        this.toast.presentToastSuccess(response.exito)
        
      }).catch(async error => {
          this.loadingService.dismiss();
          await this.writeLog.escribirLog(new Date()+ 'Respuesta con error: ' + JSON.stringify(error) + '\n');
          this.toast.presentToastDanger(JSON.parse(error.error).error);
        })

    }, true);

  }
}
