import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { Notas } from 'src/app/Clases/notas';
import { NotasLogicaService } from 'src/app/Servicios/notaService/notas-logica.service';
import { ActivatedRoute } from '@angular/router';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.page.html',
  styleUrls: ['./notas.page.scss'],
})
export class NotasPage implements OnInit {

  nota: string = null;
  notaCobrador: Notas;
  id_cliente: string;
  marca: string;
  title: string = '¿Guardar nota?';
  constructor(
    private logic: NotasLogicaService,
    private loadingService: LoadingControllerService,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private ruta: ActivatedRoute,
    private plat:Platform,
    private writeLog:  WriteFileService,
  ) {
    this.initializeBackButtonCustomHandler();
  }
  
  ngOnInit() {
    let datos=JSON.parse(this.ruta.snapshot.params.datos)
    console.log('nota recibida en notas',datos)
    this.notaCobrador=datos.nota;
    this.marca=datos.marca;
    this.id_cliente=datos.idCliente;
    //console.log(this.notaCobrador);
    if (!this.notaCobrador.id) {
      this.notaCobrador.tipo = this.marca;
      this.notaCobrador.id_cliente = this.id_cliente;
      console.log('Escribir nueva nota');
    }else {
      this.notaCobrador.tipo = this.marca;
      this.nota = this.notaCobrador.nota;
      console.log('Editar nota');
    }
  }
  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      this.navCtrl.pop();
    })
  }
  async guardarNota() {
    this.notaCobrador.nota = this.nota;
    this.notaCobrador.habilitacion_caja = Number(JSON.parse(localStorage.getItem('habilitacion')));
  
    console.log("nota ", this.notaCobrador);
    await this.writeLog.escribirLog(new Date() + ' Peticion: Creación o edicion de nota \n '+JSON.stringify(this.notaCobrador));
      
    this.loadingService.present('Guardando...')

    this.logic.addOrUpdateNotas(this.notaCobrador).subscribe(async response => {
      await this.writeLog.escribirLog(new Date() + ' Respuesta con exito: Creacion o edicion de nota \n '+JSON.stringify(response));
      
     this.loadingService.dismiss();
      this.mostrarPopUp(response.message.mensaje, data => {
        this.navCtrl.pop();
      });
      
    },async error => {
      await this.writeLog.escribirLog(new Date() + ' Respuesta con error: \n '+JSON.stringify(error));
      this.mostrarPopUp(error);
      this.loadingService.dismiss();
    });
  }
  mostrarPopUp( mensaje: string, handler: (data: any) => any = null, conCancelar: boolean = false) {
    let buttons =
      [{
        text: 'Aceptar',
        handler: data => { if (handler) handler(data); }
      }]
    if (conCancelar) {
      buttons.unshift({
        text: 'Cancelar',
        handler: null
      });
    }
    this.simpleAlert(mensaje,buttons)
   
  }
  async simpleAlert( texto,buttons) {
    const alert = await this.alertCtrl.create({
      message: texto,
      buttons: buttons,
    });

    await alert.present();
  }

}
