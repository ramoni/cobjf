import { Component, OnInit } from '@angular/core';
import { Platform, ModalController, NavController } from '@ionic/angular';
import { Cliente } from 'src/app/Clases/cliente';
import { ActivatedRoute } from '@angular/router';
import { OverlayEventDetail } from '@ionic/core';
import { SeleccionarPuntoUbicacionPage } from '../seleccionar-punto-ubicacion/seleccionar-punto-ubicacion.page';
import { ClientesLogicaService } from 'src/app/Servicios/clienteService/clientes-logica.service';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ClientesBDService } from 'src/app/Servicios/almacenamientoSQLITE/clientesBD/clientes-bd.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';

@Component({
  selector: 'app-actividades-con-clientes',
  templateUrl: './actividades-con-clientes.page.html',
  styleUrls: ['./actividades-con-clientes.page.scss'],
})

export class ActividadesConClientesPage implements OnInit {

  /** Registros (Es un listado de clientes con sus respectivos datos y cantidades de entregas, visitas y todos) */
  clientes: Array<Cliente> = [];
  /** Representa desde que posición se traeran los registros */
  nextOffset: number = 0;
  /** La cantidad de registros que se traeran por cada llamada */
  limit: number = 10;
  /** Dice si hay mas registros que traer o no */
  noMoreData: boolean = false;
  /** Dice si aun hay o no infinite scroll */
  public infinite: boolean = true;
  /** Filtro para traer los registros (RUC o RAZON SOCIAL del cliente buscado) */
  cliente: string = null;
  /** Representa que opción esta seleccionada en la pestaña (Todos, Entregas o Visitas) para mostrar los mensajes*/
  public opciones: string = null;
  /** Representa que opción esta seleccionada en la pestaña (Todos, Entregas o Visitas) para 
   * filtrar de la base de datos  */
  tipoFiltro: string = null;

  /**
   * Inicializa los parámetros del tipo de registro que se va a traer
   */
  constructor(
    protected navCtrl: NavController,
    protected modalController: ModalController,
    protected platform: Platform,
    protected logicCliente: ClientesLogicaService,
    protected loadingService: LoadingControllerService,
    protected toast: ToastControllerService,
    public plat: Platform,
    private ruta: ActivatedRoute,
    private clientesBDService: ClientesBDService,
  ) {
    this.opciones = JSON.parse(this.ruta.snapshot.params.datos).marca;
    console.log("llego a clientes la marca= ", this.opciones);
  }

  /**
   * Inicializa los parámetros y carga los primeros registros
   */
  ngOnInit() {
    ///////////////////////////////////////////
    const hosts = document.querySelectorAll('.is-list-condensed ion-item-option');
    Array.from(hosts).forEach((host) => {
      const style = document.createElement('style');
      style.textContent = `
    button.button-native {
      font-weight: var(--is-button-native-font-weight);
    }`;
      host.shadowRoot.appendChild(style);
    });
    ///////////////////////////////////////////
    this.nextOffset = 0;
    this.noMoreData = false;
    this._doLoad(false);
  }

  /**
   * Inicializar acciones que solo pueden ejecutarse cada vez que se entra a la página
   */
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  /**
   * Inicializa el boton de ir atras para ir al home
   */
  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      this.navCtrl.navigateRoot(['home']);
    })
  }

  /**
   * Se ejecuta al cambiar de pestaña (Todos, Entregas o Visitas), resetea los registros y parámetros
   * @param {event} event Evento de cambio, actualmente no tiene uso
   */
  cambiarEvento(event: any) {
    this.nextOffset = 0;
    this.noMoreData = false;
    this.limit = 10;
    this.clientes = [];
    this._doLoad(false);
    this.infinite = true;
  }

  /**
   * Trae los registros de la base de datos con los parametros requeridos
   */
  _buildPromise(): Promise<Array<Cliente>> {
    return this.clientesBDService.getListLocal(this.nextOffset, this.limit, this.cliente, this.tipoFiltro);
  }

  /**
   * Muestra la pantalla de carga y llama al método para traer los registros de la base de datos
   * @param {boolean} append Decide si va a añadir mas registros a lo existente o no
   */
  _doLoad(append: boolean = false): Promise<Array<Cliente>> {
    let mensaje = null;
    if (this.opciones === "visitas") {
      this.tipoFiltro = 'F';
      mensaje = 'Obteniendo visitas';
    }
    if (this.opciones === "documentos") {
      this.tipoFiltro = 'E';
      mensaje = 'Obteniendo entregas';
    }
    if (this.opciones === "todos") {
      this.tipoFiltro = 'T';
      mensaje = "Obteniendo visitas y entregas";
    }
    //console.log(this.tipoFiltro);
    this.loadingService.present(mensaje)
    let ob = this._buildPromise();
    ob.then(value => {
      this.loadingService.dismiss();
      //console.log("respuesta _doLoad ", value)
      if (append) {
        this.clientes = this.clientes.concat(value);

      } else {
        this.clientes = value;

      }
      this.noMoreData = value.length < 10;

      this.nextOffset += value.length;
      if (this.nextOffset <= 10) {
        this.nextOffset += 1;
      }
      this.limit += value.length;

    }).catch(error => {
      this.loadingService.dismiss();
      this.toast.presentToastDanger(JSON.parse(error.error).error);
    });
    return ob;
  }

  /**
   * Se ejecuta al realizar una busqueda en el cuadro de búsqueda, prepara los parámetros básicos y llama al método para cargar los registros
   * @param {string} cliente  Filtro para buscar cliente
   */
  buscarVisitas(cliente: string) {
    console.log("cliente ", cliente);
    this.nextOffset = 0;
    this.noMoreData = false;
    this.limit = 10;
    this._doLoad(false);
  }

  /**
   * Se ejecuta al realizar un refresh, prepara los parametros necesarios para filtrar los registros y luego llama al método que se
   * encarga de obtenerlos
   * @param {any} refresher El evento que refresca los registros
   */
  loadData(refresher) {

    this.noMoreData = false;
    this.nextOffset = 0;
    this.limit = 10;
    this.clientes = [];
    this.infinite = true;
    this._doLoad(false).then(() => {
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);

    }).catch(error => {
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
      console.log('error en refresher ', error)
      this.toast.presentToastDanger(JSON.parse(error.error).error);

    });;
  }


  /**
   * Se ejecuta al llamar al infinite scroll, si ya no hay mas registros que traer lo muestra en un toast
   * @param {any} event El evento del infinite scroll
   */
  doInfinite(event) {

    this._doLoad(true).then(() => {
      event.target.complete();
      if (this.noMoreData) {
        event.target.disabled = true;
        this.infinite = false;

        if (this.opciones === "visitas") {
          this.toast.presentToastDanger("No hay mas visitas")

        }
        if (this.opciones === "documentos") {
          this.toast.presentToastDanger('No hay mas entregas')

        }
        if (this.opciones === "todos") {
          this.toast.presentToastDanger("No hay mas visitas y entregas")

        }
      }
    }).catch(error => {
      event.target.complete();
      this.toast.presentToastDanger(JSON.parse(error.error).error);
     
    })
  }

  /**
   * Limpia todos los campos para cancelar la busqueda
   */
  cancelarBusqueda() {
    this.cliente = null;
    this.nextOffset = 0;
    this.noMoreData = false;
    this.limit = 10;
    this._doLoad(false);
  }

  /**
   * Navega a la vista de facturas para manipular las facturas del cliente seleccionado de los registros
   * @param {string} cliente  Cliente del cual se va a manipular las facturas
   */
  cargarFacturas(cliente) {
    this.navCtrl.navigateForward(['facturas', {
      datos: JSON.stringify(
        { cliente: cliente })
    }])
  }

  /**
   * Navega a la vista de documentos para manipular los documentos del cliente seleccionado de los registros
   * @param {string} cliente  Cliente del cual se va a manipular los documentos
   */
  cargarDocumentos(cliente) {
    this.navCtrl.navigateForward(['documentos', {
      datos: JSON.stringify(
        { cliente: cliente })
    }])
  }

  /**
   * Abre el discador del telefono con el número de telefono del cliente
   * @param {string} nro_telefono Número de telefono del cliente al que se desea llamar
   */
  llamar(nro_telefono) {
    window.open('tel:'+nro_telefono, '_system');
  }

  /**
   * Abre en google maps la ubicación del cliente que recibe como parámetro
   * @param {Cliente} item Cliente del cual se verá su ubicación en google maps
   */
  async abrirDir(item: Cliente) {
    let toEdit = false;
    if (!item.latitud || !item.longitud || item.latitud === 0 || item.longitud === 0) {

      this.toast.presentToastOptions('Necesita Ubicacion',
        'No hay ubicacion, por favor elija una y presione guardar',
        ['Aceptar']);
      toEdit = true;
    }
    const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: SeleccionarPuntoUbicacionPage,
        componentProps: {

          position: {
            lat: Number(item.latitud),
            lng: Number(item.longitud)
          },
          startEdit: toEdit

        }
      });

    modal.onDidDismiss().then((ubicacion: OverlayEventDetail) => {
      if (ubicacion !== null && ubicacion.data !== undefined) {
        console.log('The result data:', ubicacion);
        item.latitud = parseFloat(ubicacion.data.lat);
        item.longitud = parseFloat(ubicacion.data.lng);
        console.log('item', item)
        this.loadingService.present('Actualizando Ubicacion del cliente...');
        this.logicCliente.actualizarCliente(item).then(resp => {
          this.clientesBDService.updateCliente(item);
          console.log('actualizado ', resp)
          this.loadingService.dismiss();
          this.toast.presentToastSuccess("Ubicacion actualizada")
        }).catch(error => {
          this.loadingService.dismiss();
          console.log('error al actualizar la lat y long del cliente', error)
          this.toast.presentToastDanger("No pudimos actualizar la ubicacion del cliente")
        });

      } else {
        this.toast.presentToastDanger("Ubicacion no seleccionada")
      }
    });

    await modal.present();
  }

}
