import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ActividadesConClientesPage } from './actividades-con-clientes.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadesConClientesPage
  }
];

@NgModule({
 
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActividadesConClientesPage]
})
export class ActividadesConClientesPageModule {}
