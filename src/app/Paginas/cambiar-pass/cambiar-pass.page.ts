import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { HttpService } from 'src/app/Servicios/httpFunctions/http.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';

@Component({
  selector: 'app-cambiar-pass',
  templateUrl: './cambiar-pass.page.html',
  styleUrls: ['./cambiar-pass.page.scss'],
})
export class CambiarPassPage implements OnInit {

  /** Password Actual del usuario */
  public passwordActual: string;
  /** Password Nuevo del usuario */
  public passwordNuevo: string;
  /** Confirmacion de contraseña */
  public passwordNuevo1: string;
  /** Indica si se muestra el mensaje 'Ingrese la misma contraseña dos veces para confirmarla, ingrese nuevamente!' */
  public mostrarMensaje: boolean = false;
  /** Formulario con los datos del cambio de contraseña */
  public passwordForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    private form: FormBuilder,
    private helper: HttpService,
    private loadingService: LoadingControllerService,
    private plat: Platform,
    private toast: ToastControllerService,

  ) {
    this.inicializarFormularioDePassword();
  }

  ngOnInit() {
  }

  /**
   * Inicializar acciones que solo pueden ejecutarse cada vez que se entra a la página
   */
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  /**
   * Inicializa el formulario de cambio de contraseña
   */
  inicializarFormularioDePassword(){
    this.passwordForm = this.form.group({
      passwordActual: [null, Validators.required],
      passwordNuevo: [null, [Validators.required, Validators.minLength(6)]],
      passwordNuevo1: [null, [Validators.required, Validators.minLength(6)]]
    })
  }

  /**
   * Inicializa el boton de atras, para que al ejecutarse se envie al home
   */
  initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribe(() => {
      this.navCtrl.navigateRoot(['home']);
    })
  }
  
  /**
   * Actualiza la contraseña validando los parametros
   */
  guardar() {

    if (this.verificarPass() === false) {
      //los passwords no son iguales//
      this.passwordForm.controls['passwordNuevo'].setValue(null);
      this.passwordForm.controls['passwordNuevo1'].setValue(null);
    }
    else {
      this.cambiarContraseña(this.passwordForm.controls['passwordActual'].value, this.passwordForm.controls['passwordNuevo'].value, this.passwordForm.controls['passwordNuevo1'].value)
    }
  }

  /**
   * Verifica que las contraseñas coincidan (contraseña nueva y confirmación) y muestra el mensaje
   * @returns Retorna si coinciden las contraseñas
   */
  verificarPass(): boolean {
    if (this.passwordForm.controls['passwordNuevo'].value !== this.passwordForm.controls['passwordNuevo1'].value) {
      this.mostrarMensaje = true;
      return false;
    }
  }

  /**
   * Llama a la api para cambiar la contraseña del usuario logueado y muestra los mensajes respectivos
   * @param {string} passActual Contraseña actual del usuario
   * @param {string} passNuevo Contraseña nueva
   * @param {string} passConfir Confirmación de contraseña nueva
   * @returns Retorna la función asincrona del POST a la api
   */
  cambiarContraseña(passActual: string, passNuevo: string, passConfir: string) {
    this.loadingService.present('Cambiando contraseña...')
    return this.helper.doPost('cambiar_password?', {
      password_actual: passActual,
      password_nuevo: passNuevo,
      password_nuevo1: passConfir
    }, {}).then(response => {
      let data = JSON.parse(response.data);
      console.log('cambio contrasenha ', data);
      this.toast.presentToastSuccess(data.exito);
      this.loadingService.dismiss();
      this.navCtrl.navigateRoot(['login']);

    }).catch(
      error => {
        this.loadingService.dismiss();
        if (error.status === 401) {
          this.toast.presentToastDanger("La contraseña actual no corresponde a este usuario");
        }
        else if (error.status === 400) {
          this.toast.presentToastDanger("Debes ingresar la misma contraseña dos veces para confirmarla");
        }
        else {
          this.toast.presentToastDanger("El servidor no se encuentra disponible, verifique su conexion e intente nuevamente");
        }

      });
  }

  /**
   * Oculta o muestra la contraseña actual
   */
  togglePasswordTypePA() {
    this.passwordActual = this.passwordActual || 'password';
    this.passwordActual = (this.passwordActual === 'password') ? 'text' : 'password';
  }

  /**
   * Oculta o muestra la contraseña nueva
   */
  togglePasswordTypePN() {
    this.passwordNuevo = this.passwordNuevo || 'password';
    this.passwordNuevo = (this.passwordNuevo === 'password') ? 'text' : 'password';
  }

  /**
   * Oculta o muestra la confirmación de contraseña
   */
  togglePasswordTypePN1() {
    this.passwordNuevo1 = this.passwordNuevo1 || 'password';
    this.passwordNuevo1 = (this.passwordNuevo1 === 'password') ? 'text' : 'password';
  }


}


