import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { Cliente } from 'src/app/Clases/cliente';
import { DatosUsuario } from 'src/app/Clases/datos-usuario';
import { Notas } from 'src/app/Clases/notas';
import { Documentos } from 'src/app/Clases/documentos';
import { DocumentWithCheck } from 'src/app/Clases/document-with-check';
import { DocumentosLogicaService } from 'src/app/Servicios/documentosService/documentos-logica.service';
import { ActivatedRoute } from '@angular/router';
import { NotasLogicaService } from 'src/app/Servicios/notaService/notas-logica.service';
import { LaunchNavigator } from 'plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/uk.co.workingedge.phonegap.plugin.launchnavigator';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
declare var launchnavigator: LaunchNavigator;
@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.page.html',
  styleUrls: ['./documentos.page.scss'],
})
export class DocumentosPage implements OnInit {

  nextOffset: number = 0;
  limit: number = 10;
  noMoreData: boolean = false;
  cliente: Cliente;
  title: string;
  user: DatosUsuario;
  buttonCondition: boolean = false;
  habilitacion: number;
  notasCobrador: Notas;
  mensajeNotasCobrador: string = "No hay notas del cobrador para mostrar";
  documents: Array<DocumentWithCheck> = [];

  constructor(

    protected navCtrl: NavController,
    protected loadingService: LoadingControllerService,
    protected platform: Platform,
    protected logic: DocumentosLogicaService,
    public plat: Platform,
    private writeLog: WriteFileService,
    private ruta: ActivatedRoute,
    private logicaNotas: NotasLogicaService,
    private toast: ToastControllerService,
    private alert: AlertControllerService,

  ) {

  }
  ngOnInit() {
    this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.habilitacion = Number(JSON.parse(localStorage.getItem('habilitacion')));
    if (this.habilitacion > 0) {
      this.buttonCondition = false
    } else {
      this.buttonCondition = true
    }
    this.nextOffset = 0;
    this.noMoreData = false;
    this._doLoad(false);
    this.notasDocumentosCobrador();
  }

  async abrirDir() {
    let noTieneUbicacion = false;
    let end;
    if (!this.cliente.latitud || !this.cliente.longitud) {
      console.log('no tiene ubicacion el cliente')
      noTieneUbicacion = true;
    }
    if (noTieneUbicacion) {
      end = this.cliente.dir;
    } else {
      end = [this.cliente.latitud, this.cliente.longitud]
    }
    launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function (isAvailable) {
      var app;
      //console.log('esta disponible',isAvailable)
      if (isAvailable) {
        console.log('esta disponible', isAvailable)
        app = launchnavigator.APP.GOOGLE_MAPS;
      } else {
        console.log("Google Maps not available - falling back to user selection");
        app = launchnavigator.APP.USER_SELECT;
      }
      launchnavigator.navigate(end, {
        app: app
      });
    });

  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler()
  }
  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0,() => {
      this.navCtrl.pop();
    })
  }
  loadData(refresher) {

    this.noMoreData = false;
    this.notasDocumentosCobrador()
    this._doLoad(true).then(() => {
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
    }).catch(error => {
      console.error('error en obtener los datos de entregas localmente', error)
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
      this.toast.presentToastDanger('No pudo obtenerse los documentos del cliente')

    });
  }
  doInfinite(event) {

    this._doLoad(true).then(() => {
      event.target.complete();
      if (this.noMoreData) {
        event.target.disabled = true;
        this.toast.presentToastDanger('No hay mas documentos');
      }
    }).catch(error => {
      event.target.complete();
      console.error('no puede obtenerse las entregas del cliente', error)
      this.toast.presentToastDanger("El servidor no esta disponible, intente mas tarde")

    })
  }
  _doLoad(append: boolean = false): Promise<Array<DocumentWithCheck>> {
    this.loadingService.present('Obteniendo documentos...')

    let ob = this._buildPromise();
    ob.then(value => {
      this.loadingService.dismiss();
      if (append) {
        this.documents = this.documents.concat(value);


      } else {
        this.documents = value;
      }
      this.noMoreData = value.length < 10;
      this.nextOffset += value.length;
      if (this.nextOffset <= 10) {
        this.nextOffset += 1;
      }

      this.limit += value.length;
    }).catch(error => {
      console.log('error al cargar documentos ', error)
      this.loadingService.dismiss();
      this.toast.presentToastDanger('No pudo obtenerse los documentos del cliente');

    });
    return ob;
  }

  _buildPromise(): Promise<Array<DocumentWithCheck>> {
    return this.logic.obtenerDocumentosLocal(this.cliente, this.nextOffset, this.limit);
  }

  async guardar() {
    let aGuardar: Array<Documentos> = [];
    let aGuardar1: Array<Documentos> = [];
    for (let row of this.documents) {
      //let toDocument = new Documentos(row.data);
      //console.log('los documentos ', toDocument);
      if (row.check === true) {
        //console.log('Marcado con 1');
        row.data.guardado = 1;
        row.data.imei = localStorage.getItem('imei');
        //console.log('se va a guardar ', row.data);
        aGuardar1.push(row.data)
      } else {
        //console.log('Marcado con 0');
        row.data.guardado = 0;
        row.data.imei = localStorage.getItem('imei');
        //console.log('no se va a guardar ', row.data);
      }
      aGuardar.push(row.data);
    }
    console.log('aGuardar ', aGuardar);
    /*if (aGuardar1.length < 1) {
      this.toast.presentToastDanger('Seleccione Documentos')
      return;
    }*/

    await this.writeLog.escribirLog(new Date() + ' Peticion: Guardado de documentos ' + JSON.stringify(aGuardar) + '\n')
    this.alert.alertMessageWithHandler(this.title, "Esta seguro que desea entregar los documentos?", d => {

      this.loadingService.present('Guardando...')
      this.logic.addOrUpdate(aGuardar).subscribe(async response => {
        console.log('el mensaje documentos', response.message);
        await this.writeLog.escribirLog(new Date() + ' Respuesta con exito: ' + JSON.stringify(response.message) + '\n');

        this.loadingService.dismiss();
        this.toast.presentToastSuccess(response.message.mensaje)

      }, async error => {
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error: ' + JSON.stringify(error) + '\n');
        this.toast.presentToastDanger(error);
        this.loadingService.dismiss();
      });
    }, true);

  }

  notasDocumentosCobrador() {

    this.logicaNotas.loadNotaCobrador(Number(this.cliente.id), Number(JSON.parse(localStorage.getItem('habilitacion'))), 'E')
      .then(notasCob => {

        this.notasCobrador = notasCob;
        console.log('notas Cobrador entregas', this.notasCobrador)

        if (this.notasCobrador.id !== null && this.notasCobrador.nota !== null) {
          this.mensajeNotasCobrador = ""
          this.mensajeNotasCobrador = this.mensajeNotasCobrador.concat(this.notasCobrador.nota);

        } else {
          this.mensajeNotasCobrador = "No hay notas de cobrador que mostrar"
        }
      })

  }

  mostrarNotas() {

    let mensaje = this.mensajeNotasCobrador;
    let buttons =
      [{
        text: 'Editar',
        handler: () => {
          this.navCtrl.navigateForward(['notas', {
            datos: JSON.stringify({
              idCliente: this.cliente.id,
              nota: this.notasCobrador,
              marca: 'E',
            })
          }])
        }
      },
      {
        text: 'Aceptar',
        handler: null
      }]

    this.alert.simpleAlert('Notas', mensaje, buttons)
  }


}


