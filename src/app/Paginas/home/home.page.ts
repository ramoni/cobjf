import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { GeolocalizacionLogicaService } from 'src/app/Servicios/geolocalizacionService/geolocalizacion-logica.service';
import { DatosUsuario } from 'src/app/Clases/datos-usuario';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { UsuarioService } from 'src/app/Servicios/usuarioService/usuario.service';
import { DatosOnlineService } from 'src/app/Servicios/onlineServices/datos-online.service';
import { HabilitacionesBDService } from 'src/app/Servicios/AlmacenamientoSQLITE/habilitacionesBD/habilitaciones-bd.service';
import { ActividadesLocalesService } from 'src/app/Servicios/actividadesLocales/actividades-locales.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
import { SincronizacionService } from 'src/app/Servicios/sincronizacionService/sincronizacion.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  v: any;
  timeStarts: any;
  lugar: string;
  public userData: DatosUsuario;
  title: string;
  public habilitacion_caja: number;
  public cantidad_datos: number;
  datosSubida: number = 0;
  datosBajada: number = 0;
  recibos: number = 0;
  entregas: number = 0;
  notasVisitas: number = 0;
  notasEntregas: number = 0
  cargando: boolean = true

  constructor(
    public navCtrl: NavController,
    private geo: GeolocalizacionLogicaService,
    private loadingService: LoadingControllerService,
    private userService: UsuarioService,
    private plat: Platform,
    private writeLog: WriteFileService,
    private toastService: ToastControllerService,
    private alertService: AlertControllerService,
    private onlineMethods: DatosOnlineService,
    private actividadesLocalesService: ActividadesLocalesService,
    private habilitacionService: HabilitacionesBDService,
    private syncro:SincronizacionService ) {

  }
  ngOnInit() {
    this.syncro.sincronizarTodoManual();
    this.v = JSON.parse(localStorage.getItem('version'));
    this.userData = JSON.parse(localStorage.getItem('user'))
    this.habilitacion_caja = Number(JSON.parse(localStorage.getItem('habilitacion')));
    this.getGeo();
    this.bajandoDatosOnline();
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    this.initActividadesLocales();
    this.mostrarUsoDatosApp();
  }

  bajandoDatosOnline() {

    let intervaloDatosOnline = setInterval(() => {
      let bajandoDatos = JSON.parse(localStorage.getItem("bajandoDatosOnline"));
      console.log("VALOR DE BAJANDO DATOS ONLINE ", bajandoDatos);
      if (bajandoDatos == true) {
        this.cargando = true;
      } else {
        //Se deja de realizar el intervalo
        clearInterval(intervaloDatosOnline);
        this.cargando = false;
      }
    }, 1000);

  }

  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      this.alertService.confirmExitAPP();
    })
  }

  getGeo() {
    this.geo.findCurrentCiudad().subscribe(ciudad => {
      this.lugar = ciudad;
      console.log(this.lugar);
    }, error => {
      console.log("error en la ubicacion ", error)
      this.lugar = 'No se puede obtener la ubicacion';
    });
  }

  openDocumentos() {
    this.navCtrl.navigateForward(['actividades', {
      datos: JSON.stringify({
        marca: "documentos"
      })
    }])
  }

  openCobros() {
    this.navCtrl.navigateForward(['actividades', {
      datos: JSON.stringify({
        marca: "visitas"
      })
    }]);
  }
  async cerrarCaja(habilitacion: number) {

    await this.writeLog.escribirLog(new Date() + ' Peticion: Cierre de habilitacion  ' + habilitacion + '\n')
    this.alertService.alertMessageWithHandler(this.title, "Esta seguro que desea cerrar su caja?", d => {
      this.loadingService.present('Verificando datos...');
      this.userService.existenDatosHabilitacion(Number(JSON.parse(localStorage.getItem('habilitacion')))).then(async resp => {
        this.loadingService.dismiss();
        if (resp === true) {
          await this.writeLog.escribirLog(new Date() + " Respuesta con error: Existen datos que faltan ser subidos \n");
          this.toastService.presentToastDanger("Existen datos que faltan ser subidos, aguarde tres minutos, verifique su conexion a internet e intente nuevamente!");
          return;
        } else {
          this.loadingService.present('Cerrando...');
          this.userService.cambiarHabilitacion(habilitacion).then(async response => {
            await this.writeLog.escribirLog(new Date() + ' Respuesta con exito: ' + JSON.stringify(response) + '\n');
            this.toastService.presentToastSuccess(response.message);
            this.habilitacion_caja = JSON.parse(localStorage.getItem('habilitacion'));
            this.datosSubida = 0;
            this.datosBajada = 0;
            await this.writeLog.escribirLog(new Date() + " Petición: Cierre de caja local \n");
            this.habilitacionService.cerrarHabilitacionLocal(habilitacion).then(async data => {
              await this.writeLog.escribirLog(new Date() + " Respuesta con exito: " + JSON.stringify(data) + " \n");
              console.log('Cierre de caja local ', data);
              this.loadingService.dismiss();
              return this.habilitacion_caja;
            }).catch(async error => {
              this.toastService.presentToastDanger("Error en cerrar caja localmente");
              console.error("Error en cierre local", error);
              await this.writeLog.escribirLog(new Date() + " Cierre de caja local con errores: " + JSON.stringify(error) + " \n");
              this.loadingService.dismiss();
            });
            this.cantidad_datos = 0;

          }).catch(async error => {
            this.loadingService.dismiss();
            await this.writeLog.escribirLog(new Date() + ' Respuesta con error: ' + JSON.stringify(error) + '\n');
            this.toastService.presentToastDanger(JSON.parse(error.error).error);
            if (error.status === 401) {
              this.navCtrl.navigateRoot('login');
            }
          });
        }
      })

    }, true);

  }

  initActividadesLocales() {
    this.habilitacion_caja = Number(JSON.parse(localStorage.getItem('habilitacion')));
    this.actividadesLocalesService.mostrarActividadesLocales(this.habilitacion_caja).then((data) => {
      let respuesta = data;
      console.log('La respuesta de actividades locales ', respuesta);
      this.recibos = respuesta.RECIBOS;
      this.entregas = respuesta.ENTREGAS;
      this.notasVisitas = respuesta.NOTAS_VISITAS;
      this.notasEntregas = respuesta.NOTAS_ENTREGAS;
    });
  }

  mostrarUsoDatosApp() {
    let subida = Number(localStorage.getItem('subida'));
    let bajada = Number(localStorage.getItem('bajada'));
    let calculo1 = (subida / 10000).toFixed(1);
    let calculo2 = (bajada / 10000).toFixed(1);
    this.datosSubida = Number(calculo1)
    this.datosBajada = Number(calculo2)
    this.cantidad_datos = Number(calculo1) + Number(calculo2);
    localStorage.setItem('datos', JSON.stringify(subida + bajada));
    console.log('subida: ', subida, 'bajada: ', bajada);
  }

  async refrescarHome(refresher) {

    await this.writeLog.escribirLog(new Date() + ' Peticion: Refrescar Datos App \n');
    this.onlineMethods.bajarDatosOnline().then(async () => {
      await this.writeLog.escribirLog(new Date() + ' Respuesta con exito: DATOS DESCARGADOS CON EXITO \n');
      await this.writeLog.escribirLog(new Date() + ' Peticion: Actualizar Datos de usuario \n');
      this.onlineMethods.obtenerDatosInicio().then(async data => {
        await this.writeLog.escribirLog(new Date() + ' Respuesta con exito: Datos de usuario actualizados \n');
        console.log('data refresh user', data)
        this.userData = <DatosUsuario>data;
        console.log('user Data', this.userData);
        localStorage.setItem('habilitacion', JSON.stringify(this.userData.HABILITACION));
        localStorage.setItem('expiredToken', this.userData.EXPIRED)
        this.habilitacion_caja = this.userData.HABILITACION;
        localStorage.setItem('user', JSON.stringify(data));
        refresher.target.disabled = true;
        refresher.target.complete();
        setTimeout(() => {
          refresher.target.disabled = false;
        }, 100);
        this.initActividadesLocales();
        this.mostrarUsoDatosApp();
        return this.userData, this.getGeo(), this.habilitacion_caja;
      }).catch(async error => {
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error: Datos de usuario no actualizados \n');
        this.toastService.presentToastDanger('Los datos de su usuario no pudieron ser descargados')
        console.error('error al obtener datos del user', error)
        refresher.target.disabled = true;
        refresher.target.complete();
        setTimeout(() => {
          refresher.target.disabled = false;
        }, 100);
        this.mostrarUsoDatosApp()
      });
    }).catch(async () => {
      await this.writeLog.escribirLog(new Date() + ' Respuesta con error: NO SE PUDIERON DESCARGAR TODOS LOS DATOS \n');
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
      this.toastService.presentToastDanger('Los datos de su usuario no pudieron ser descargados')
      this.mostrarUsoDatosApp();
    });
  }

}
