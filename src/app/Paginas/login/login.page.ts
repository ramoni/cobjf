import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { SesionService } from 'src/app/Servicios/sesionValidation/sesion.service';
import { ConfigService } from 'src/app/Servicios/configuracion/config.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { TablesManageService } from 'src/app/Servicios/almacenamientoSQLITE/tablesManagment/tables-manage.service';
import { UsuarioService } from 'src/app/Servicios/usuarioService/usuario.service';
import { DatosUsuario } from 'src/app/Clases/datos-usuario';
import { DatosOnlineService } from 'src/app/Servicios/onlineServices/datos-online.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
import { SincronizacionService } from 'src/app/Servicios/sincronizacionService/sincronizacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public passwordType: string;
  usuario: string;
  password: string;
  version: string;
  datosConsulta: any;
  public tokenExpirado: boolean = false;
  usuarioParse: DatosUsuario;
  constructor(
    private loadingService: LoadingControllerService,
    private writeLog: WriteFileService,
    private userService: UsuarioService,
    private navControl: NavController,
    private validarSesion: SesionService,
    private config: ConfigService,
    private tablesManage: TablesManageService,
    private toast: ToastControllerService,
    private onlineMethods: DatosOnlineService,
    private syncro:SincronizacionService) { }

  ngOnInit() {
    if (localStorage.getItem('version')) {
      this.version = JSON.parse(localStorage.getItem('version'));
    }
    this.syncro.sincronizarTodoManual();
  }

  togglePasswordType() {
    this.passwordType = this.passwordType || 'password';
    this.passwordType = (this.passwordType === 'password') ? 'text' : 'password';
  }

  async bajadaDeDatos() {
    if (!this.config.IS_OFFLINE) {
      console.log('Modo online, hacer peticiones de conceptos, entidades, cuentas, etc');
      let bajada: boolean;
      localStorage.setItem("bajandoDatosOnline", JSON.stringify(true));
      bajada = await this.onlineMethods.bajarDatosOnline();

      if (bajada === false) {
        this.toast.presentToastDanger('No se pudieron descargar todos los datos, se hara un nuevo intento')
        let timerId = setInterval(async () => {
          bajada = await this.onlineMethods.bajarDatosOnline();
          if (bajada) {
            console.log('limpiar el intervalo de llamada');
            localStorage.setItem("bajandoDatosOnline", JSON.stringify(false));
            clearInterval(timerId);
          } else {
            this.toast.presentToastDanger('No se pudieron descargar todos los datos, verifique su conexion. Se hara un nuevo intento')
          }
        }, 30000);

      } else {
        localStorage.setItem("bajandoDatosOnline", JSON.stringify(false));
      }
    }
  }
  guardarIdEIrAMenu() {
    this.bajadaDeDatos()
    let w: any = window;
    w.nav = this.navControl;
    console.log('Guardar e ir al MENU');
    this.navControl.navigateRoot('home');

  }
  async iniciarSesion() {

    await this.writeLog.escribirLog(new Date() + ' Peticion: Inicio de sesion ONLINE usuario ' + this.usuario + ' password: ' + this.password + '\n')

    this.userService.iniciarSesion(this.usuario, this.password, JSON.parse(localStorage.getItem('version')), localStorage.getItem('imei'))
      .then(async data => {
        let usuario = JSON.parse(localStorage.getItem('user'));
        this.usuarioParse = usuario;

        await this.writeLog.escribirLog(new Date() + ' Respuesta con exito: ' + JSON.stringify(data) + '\n');

        if (this.tokenExpirado) {// expiro el token
          this.userService.existeHabilitacionUsuario(Number(data.HABILITACION), this.usuario.toLowerCase(),
            this.password).then(async data_resp => {
              let resp = data_resp;
              console.log('habilitacion ', resp)
              if (data_resp === true) {//inserta un nuevo usuario
                await this.writeLog.escribirLog(new Date() + ' Expiro el token, existe la habilitacion del usuario ' + '\n');
                this.userService.addUsuario(this.usuario, this.password, this.usuarioParse,
                  localStorage.getItem(this.config.AUTHORIZATION_STRING), localStorage.getItem('expiredToken'))
                  .then(async () => {
                    this.loadingService.dismiss();
                    localStorage.setItem('habilitacion', JSON.stringify(data.HABILITACION));
                    await this.writeLog.escribirLog(new Date() + ' Actualizo el usuario en la tabla local: ' + JSON.stringify(data) + '\n');

                    this.guardarIdEIrAMenu();

                  }).catch(error => {
                    this.loadingService.dismiss();
                    console.log('no se pudo insertar un nuevo usuario', error)
                    this.toast.presentToastDanger('No pudo actualizar su usuario en la tabla local')
                  })

              } else {
                //verificar que esta caja tenga datos que no se hayan sincronizado todavia y entonces pasar
                await this.writeLog.escribirLog(new Date() + ' Expiro el token, NO existe la habilitacion del usuario' + '\n');

                this.userService.existenDatosHabilitacion(Number(data.HABILITACION)).then(async resp => {
                  if (resp === true) {//retorno true porque hay datos
                    localStorage.setItem('expiredToken', '');
                    this.loadingService.dismiss();
                    await this.writeLog.escribirLog(new Date() + ' Existen datos que faltan ser sincronizados con la habilitacion anterior ' + '\n');
                    this.toast.presentToastDanger("Existen datos que faltan ser sincronizados con la habilitacion anterior, sincronice e intente nuevamente!");
                    return;
                  } else {//no hay datos para sincronizar
                    await this.writeLog.escribirLog(new Date() + ' NO existen datos que faltan ser sincronizados con la habilitacion anterior ' + '\n');
                    this.tablesManage.deleteAllTables().then(resp => {
                      if (resp === true) {//se eliminaron todas las tablas
                        this.tablesManage.createTables().then(() => {
                          this.userService.addUsuario(this.usuario, this.password, this.usuarioParse,
                            localStorage.getItem(this.config.AUTHORIZATION_STRING), localStorage.getItem('expiredToken'))
                            .then(async () => {
                              console.log('se inserto un new user')
                              this.loadingService.dismiss();
                              localStorage.setItem('habilitacion', JSON.stringify(data.HABILITACION));

                              await this.writeLog.escribirLog(new Date() + ' Se eliminaron las tablas y se agrego el usuario: ' + JSON.stringify(data) + '\n');

                              this.guardarIdEIrAMenu();

                            }).catch(error => {
                              this.loadingService.dismiss();
                              console.log('Error al agregar nuevo usuario', error)
                              this.toast.presentToastDanger('No se pudo agregar su usuario a la tabla local')
                            })
                        }).catch(error => {
                          this.loadingService.dismiss();
                          console.log('No se crearon todas las tablas', error)
                          this.toast.presentToastDanger('Error al eliminar las tablas locales');
                        })
                      } else {
                        this.loadingService.dismiss();
                        console.log('No se pudieron eliminar todas las tablas')
                        this.toast.presentToastDanger('No se pudo eliminar las tablas locales');
                      }
                    }).catch(err => {
                      this.loadingService.dismiss();
                      console.log('error al eliminar todas las tablas ', err)
                      this.toast.presentToastDanger('Error intentando eliminar las tablas locales');
                    })
                  }
                }).catch(error => {
                  this.loadingService.dismiss();
                  console.log('No se pudieron obtener los datos de la habilitacion', error)
                  this.toast.presentToastDanger('No se pudo obtener los datos de la habilitacion de las tablas locales');
                })
              }
            }).catch(error => {
              this.loadingService.dismiss();
              console.log('No se pudo obtener la habilitacion del usuario', error)
              this.toast.presentToastDanger('No se pudo acceder a la tabla local de Habilitaciones');
            });

        } else {//no expiro el token
          /*
            Falta la sincronizacion
 
          */
          this.tablesManage.deleteAllTables().then(resp => {
            if (resp === true) {//se eliminaron todas las tablas
              this.tablesManage.createTables().then(() => {
                this.userService.addUsuario(this.usuario, this.password, this.usuarioParse,
                  localStorage.getItem(this.config.AUTHORIZATION_STRING), localStorage.getItem('expiredToken'))
                  .then(async () => {
                    console.log('se inserto un new user');

                    this.loadingService.dismiss();
                    localStorage.setItem('habilitacion', JSON.stringify(data.HABILITACION));

                    await this.writeLog.escribirLog(new Date() + ' No expiro el token, se eliminaron todas las tablas ' + '\n');

                    this.guardarIdEIrAMenu();

                  }).catch(error => {

                    this.loadingService.dismiss();
                    console.log('no se pudo insertar un nuevo usuario', error)
                    this.toast.presentToastDanger('Error al insertar su usuario en la tabla local');
                  })
              }).catch(error => {
                this.loadingService.dismiss();
                console.log('no se pudo crear las tablas', error)
                this.toast.presentToastDanger('No se pudo crear las tablas localmente');
              })
            } else {//errores al eliminar las tablas
              this.loadingService.dismiss();
              this.toast.presentToastDanger('No se puede iniciar sesion, error al eliminar las tablas')
            }
          }).catch(err => {
            this.loadingService.dismiss();
            console.error("No se eliminaron las tablas", err);
            this.toast.presentToastDanger('Error al eliminar tablas locales');

          })
        }
      }).catch(async error => {

        this.loadingService.dismiss();
        let messageError;
        console.log("ocurrio un error en iniciar sesion ", error)

        if (error.status === 401) {
          messageError = JSON.parse(error.error).error;
          await this.writeLog.escribirLog(new Date() + 'Respuesta con error 401: ' + JSON.stringify(messageError) + '\n');
          this.toast.presentToastDanger(messageError);
        } else {
          if(error.status<0){
            await this.writeLog.escribirLog(new Date() + 'Respuesta con error: ' + ' El servidor no se encuentra disponible, verifique su conexion e intente nuevamente' + '\n');
            this.toast.presentToastDanger('El servidor no se encuentra disponible, verifique su conexion e intente nuevamente')
          }else{
            messageError = JSON.parse(error.error).error;
            await this.writeLog.escribirLog(new Date() + 'Respuesta con error 401: ' + JSON.stringify(messageError) + '\n');
            this.toast.presentToastDanger(messageError)
          }
          
        }

      });
  }

  validarDatosLocales() {
    if (!this.usuario || !this.password) {
      this.toast.presentToastDanger("Usuario o contraseña con campos vacios");
      return;
    }
    this.loadingService.present("Iniciando Sesion...");

    this.tablesManage.createTables().then(() => {

      this.userService.existeUsuario(this.usuario, this.password).then(async data => {
        console.log('data de la BD', data)
        if (data !== null) {//encontro un usuario
          let user = data;
          console.log('data validarDatosLocales', user)
          let expired: string;
          if (user.TOKEN_EXPIRED !== null) {
            expired = user.TOKEN_EXPIRED;
          } else {
            expired = null;
          }
          if (this.validarSesion.validarSesion(user.TOKEN, user.TOKEN_EXPIRED)) {
            localStorage.setItem(this.config.AUTHORIZATION_STRING, user.TOKEN);
            localStorage.setItem('expiredToken', user.TOKEN_EXPIRED);
            this.datosConsulta = user;
            let userData = user
            this.config.IS_OFFLINE = true;
            this.userService.setDatosUsuario(userData);
            this.loadingService.dismiss();

            await this.writeLog.escribirLog(new Date() + ' Existe el usuario y el token aun no expira: ' + JSON.stringify(data) + '\n');

            this.guardarIdEIrAMenu();
          } else {
            console.log('Token expirado, LOGIN ONLINE');
            this.tokenExpirado = true;
            this.config.IS_OFFLINE = false;

            //aca tambien el mismo control
            this.iniciarSesion();
          }
        } else {
          console.log('No hay datos en la tabla o el usuario ingresado no es correcto!, preguntar en la API, LOGIN ONLINE');

          //tomar la habilitacion actual si es que la hay y ver si existen datos que tienen que ser subidos con esa habilitacion
          if (localStorage.getItem('habilitacion')) {
            let habilitacion: number = Number(JSON.parse(localStorage.getItem('habilitacion')));
            if (habilitacion !== 0) {
              console.log('Consultar si es que hay datos con esa habilitacion para ver si se sincronizo todo');
              this.userService.existenDatosHabilitacion(Number(JSON.parse(localStorage.getItem('habilitacion'))))
              .then(async resp => {//true tiene datos para sincronizar
                if (resp === true) {
                  this.loadingService.dismiss();
                  let usuario = JSON.parse(localStorage.getItem('user'));
                  this.usuarioParse = usuario;
                  await this.writeLog.escribirLog(new Date() + ' Peticion: Inicio de sesion, existen datos que faltan ser subidos con el cobrador  ' + this.usuarioParse.USUARIO.NOMBRE + '\n');
                  this.toast.presentToastDanger("Existen datos que faltan ser subidos con el cobrador " + this.usuarioParse.USUARIO.NOMBRE + ", inicie sesión nuevamente y aguarde unos minutos la sincronización!!");
                  return;
                } else {//no hay datos que sincronizar
                  this.config.IS_OFFLINE = false;
                  this.iniciarSesion();
                }
              }).catch(err => {
                this.loadingService.dismiss();
                this.toast.presentToastDanger('Problemas para consultar la Base de Datos local')
                console.log('No se puede consultar si exiten datos para la habilitacion', err)
              })
            } else {//debe 
              this.config.IS_OFFLINE = false;
              this.iniciarSesion();
            }

          } else {//no hay guardado de habilitacion
            this.config.IS_OFFLINE = false;
            this.iniciarSesion();
          }
        }
      }).catch(error => {
        this.loadingService.dismiss();
        this.toast.presentToastDanger('No pudo obtenerse el usuario de la BD local')
        console.log('no se pudo obtener los datos de la bd', error);
      })
    }).catch(error => {
      this.loadingService.dismiss();
      this.toast.presentToastDanger('Problemas para crear las tablas en la bd local')
      console.log('Problemas para crear las tablas en la bd local', error);
    })
  }
}
