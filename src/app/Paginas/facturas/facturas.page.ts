import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/Clases/cliente';
import { Notas } from 'src/app/Clases/notas';
import { Factura } from 'src/app/Clases/factura';
import { DatosUsuario } from 'src/app/Clases/datos-usuario';
import { NavController, Platform } from '@ionic/angular';
import { FacturasLogicaService } from 'src/app/Servicios/facturasService/facturas-logica.service';
import { FacturaWithCheck } from 'src/app/Clases/factura-with-check';
import { ActivatedRoute } from '@angular/router';
import { NotasLogicaService } from 'src/app/Servicios/notaService/notas-logica.service';
import { LaunchNavigator } from 'plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/uk.co.workingedge.phonegap.plugin.launchnavigator';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { Moneda } from 'src/app/Clases/moneda';
import { MonedaLogicaService } from 'src/app/Servicios/monedaService/moneda-logica.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
declare var launchnavigator: LaunchNavigator;

@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.page.html',
  styleUrls: ['./facturas.page.scss'],
})

export class FacturasPage implements OnInit {

  cliente: Cliente;
  public mensajeNotas: string = "No hay notas para mostrar";
  public mensajeNotasCobrador: string = "No hay notas del cobrador para mostrar";
  nextOffset: number = 0;
  facturas: Array<FacturaWithCheck> = [];
  factura: Factura;
  noMoreData: boolean = false;
  limit: number = 10;
  limitChecks: number = 10;
  countChecks: number = 0;
  public notas: Notas;
  notasCobrador: Notas;
  user: DatosUsuario;
  habilitacion: number;
  buttonCondition: boolean = false;
  public infinite: boolean = true;
  public orden: string = 'nroFactura';
  moneda: Moneda;
  constructor(
    public navCtrl: NavController,
    private logic: FacturasLogicaService,
    private alertService: AlertControllerService,
    private plat: Platform,
    private ruta: ActivatedRoute,
    private notasLogica: NotasLogicaService,
    private loadingService: LoadingControllerService,
    private toastControl: ToastControllerService,
    private monedaLogic: MonedaLogicaService,

  ) { }
  ngOnInit() {

    this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente;
    console.log("cliente en facturas ", this.cliente)
    this.user = JSON.parse(localStorage.getItem('user'));
    this.habilitacion = Number(JSON.parse(localStorage.getItem('habilitacion')));
    if (this.habilitacion > 0) {
      this.buttonCondition = false
    } else {
      this.buttonCondition = true
    }

    this.cargarNotasGenerales();

  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    this.nextOffset = 0;
    this.noMoreData = false;
    /////
    this.infinite = true;
    //////
    this.limit = 10;
    this._doLoad(false);
    
  }
  
  cargarNotasGenerales() {

    if (this.cliente.notas === "null") {
      this.mensajeNotas = "No hay notas para mostrar";
    } else {
      this.mensajeNotas = this.cliente.notas;
    }
    this.notasVisitasCobrador()
  }
  async abrirDir() {
    let noTieneUbicacion = false;
    let end;
    if (!this.cliente.latitud || !this.cliente.longitud) {
      console.log('no tiene ubicacion el cliente')
      noTieneUbicacion = true;
    }
    if (noTieneUbicacion) {
      end = this.cliente.dir;
    } else {
      end = [this.cliente.latitud, this.cliente.longitud]
    }
    launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function (isAvailable) {
      var app;
      //console.log('esta disponible',isAvailable)
      if (isAvailable) {
        console.log('esta disponible', isAvailable)
        app = launchnavigator.APP.GOOGLE_MAPS;
      } else {
        console.log("Google Maps not available - falling back to user selection");
        app = launchnavigator.APP.USER_SELECT;
      }
      launchnavigator.navigate(end, {
        app: app
      });
    });

  }
  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      this.navCtrl.pop();
    })
  }

  cambiarFiltro() {
    console.log(this.orden);
    this.nextOffset = 0;
    this.noMoreData = false;
    this._doLoad(false);

  }

  cargarRecibos(fact: Factura) {
    this.navCtrl.navigateForward(['recibos-creados',
      { datos: JSON.stringify({ factura: fact }) }]);
  }

  async cobrar() {
    let aCobrar: Array<Factura>
    aCobrar = this.facturas
      .filter(Factura => Factura.check)
      .map(Factura => Factura.data);

    if (aCobrar.length < 1) {
      this.toastControl.presentToastDanger('Seleccione facturas')
      return;
    }
    let factura_fisrt = aCobrar[0].moneda;
    await this.monedaLogic.getMonedasBD();
    this.moneda = this.monedaLogic.getByIdOrName(factura_fisrt);
    this.navCtrl.navigateForward(['crear-recibo',
      {
        datos: JSON.stringify({
          facturas: aCobrar,
          cliente: this.cliente,
          monedaRecibo: this.moneda
        })
      }]);

  }

  loadData(refresher: any = null) {
    this.noMoreData = false;
    this.nextOffset = 0;
    this.limit = 10;
    this.facturas = [];
    this.infinite = true;
    this.cargarNotasGenerales()
    this._doLoad(false).then(() => {
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
    }).catch(error => {
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
      this.toastControl.presentToastDanger(JSON.parse(error.error).error)

    });
  }
  _doLoad(append: boolean = false): Promise<Array<FacturaWithCheck>> {

    this.loadingService.present('Obteniendo facturas...')

    let ob = this._buildPromise();
    ob.then(value => {
      this.loadingService.dismiss();
      if (append) {
        this.facturas = this.facturas.concat(value)
      } else {
        this.facturas = value;
      }
      this.noMoreData = value.length < 10;

      this.nextOffset += value.length;
      if (this.nextOffset <= 10) {
        this.nextOffset += 1;
      }
      this.limit += value.length;
    }).catch(error => {
      this.loadingService.dismiss();
      console.log('No se pudo obtener localmente las facturas', error)
      this.toastControl.presentToastDanger('No se pudo obtener las facturas de la tabla local')

    });
    return ob;
  }
  _buildPromise(): Promise<Array<FacturaWithCheck>> {
    return this.logic.obtenerFacturasLocal(this.cliente, this.nextOffset, this.limit, this.orden);
    //return this.logic.getList(this.cliente, this.nextOffset, this.limit, this.orden);
  }

  ver(Factura: FacturaWithCheck) {
    this.cargarRecibos(Factura.data);
  }

  doInfinite(event) {
    this._doLoad(true).then(() => {
      event.target.complete();
      if (this.noMoreData) {
        this.infinite = false;
        event.target.disabled = true;
        this.toastControl.presentToastDanger("No hay mas facturas")
      }
    }).catch(error => {
      event.target.complete();
      console.error('no pudo obtenerse las facturas del cliente', error)
      this.toastControl.presentToastDanger('Error al obtener las facturas del cliente');

    });
  }

  notasVisitasCobrador() {

    this.notasLogica.loadNotaCobrador(Number(this.cliente.id), Number(JSON.parse(localStorage.getItem('habilitacion'))), 'F').then(notasCob => {
      this.notasCobrador = notasCob;
      console.log('notas Cobrador facturas', this.notasCobrador)

      if (this.notasCobrador.id !== null && this.notasCobrador.nota !== null) {
        this.mensajeNotasCobrador = ""
        this.mensajeNotasCobrador = this.mensajeNotasCobrador.concat(this.notasCobrador.nota);

      } else {
        this.mensajeNotasCobrador = "No hay notas de cobrador que mostrar"
      }
    })

  }

  mostrarNotas(opcion: string) {
    let buttons;
    let mensaje;
    if (opcion === '1') {
      mensaje = this.mensajeNotas;
      buttons =
        [
          {
            text: 'Aceptar',
            handler: null
          }
        ]
    } else {
      mensaje = this.mensajeNotasCobrador;
      buttons =
        [{
          text: 'Editar',
          handler: () => {
            this.navCtrl.navigateForward(['notas', {
              datos: JSON.stringify({
                idCliente: this.cliente.id,
                nota: this.notasCobrador,
                marca: 'F',
              })
            }])
          }
        },
        {
          text: 'Aceptar',
          handler: null
        }]

    }

    this.alertService.simpleAlert('Notas', mensaje, buttons)
  }


}