import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FacturasPage } from './facturas.page';
import { PipesModuleModule } from 'src/app/Pipes/pipes-module.module';


const routes: Routes = [
  {
    path: '',
    component: FacturasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    PipesModuleModule,
    
  ],
  declarations: [FacturasPage]
})
export class FacturasPageModule {}
