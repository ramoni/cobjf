import { Component, OnInit } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { Recibo } from 'src/app/Clases/recibo';
import { Factura } from 'src/app/Clases/factura';
import { RecibosLogicaService } from 'src/app/Servicios/reciboService/recibos-logica.service';
import { ActivatedRoute } from '@angular/router';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';

@Component({
  selector: 'app-recibos-creados',
  templateUrl: './recibos-creados.page.html',
  styleUrls: ['./recibos-creados.page.scss'],
})
export class RecibosCreadosPage implements OnInit {
  recibos: Array<Recibo>;
  factura: Factura;
  totalFactura: number;
  totalRecibo: number;
  totalSaldo: number;
  title: string;

  constructor(
    public navCtrl: NavController,
    private logicaRecibo: RecibosLogicaService,
    private loadingService: LoadingControllerService,
    private alertCtrl: AlertControllerService,
    public plat: Platform,
    private toast: ToastControllerService,
    public parametros: ActivatedRoute) {

    this.initializeBackButtonCustomHandler();
  }
  ngOnInit() {
    this.factura = JSON.parse(this.parametros.snapshot.params.datos).factura;
    this.totalFactura = this.factura.total;
    this.totalSaldo = this.factura.saldo;
    this.loadingService.present('Obteniendo Recibos...')
    this.logicaRecibo.getRecibosPorFactura(this.factura).then(data => {
      this.loadingService.dismiss()
      this.recibos = data;
      console.log('recibos', this.recibos)
    }).catch(error => {
      this.loadingService.dismiss()
      console.log('No se pudo obtener los recibos de las facturas', error)
      this.toast.presentToastDanger('No se pudo obtener los recibos de las facturas')
    });
  }

  loadData(refresher: any = null) {
    this.logicaRecibo.getRecibosPorFactura(this.factura).then(data => {
      this.recibos = data;
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
    }).catch(error => {
      refresher.target.disabled = true;
      refresher.target.complete();
      setTimeout(() => {
        refresher.target.disabled = false;
      }, 100);
      console.log('No se pudo obtener los recibos de las facturas', error)
      this.toast.presentToastDanger('No se pudo obtener los recibos de las facturas')
    });;
  }
  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      this.navCtrl.pop();
    })
  }

  anular(recibo: Recibo) {
    this.alertCtrl.alertMessageWithHandler(this.title, "Esta seguro que desea anular el recibo?", d => {

      this.loadingService.present("Anulando...")
      this.logicaRecibo.anular(recibo).then(response => {
        recibo.estado = 'ANULADO';
        this.loadingService.dismiss();

        this.toast.presentToastSuccess(response.exito)
      }).catch(error => {
        this.loadingService.dismiss();
        console.log('No se pudo anular el recibo', error)
        this.toast.presentToastDanger(error.error)
      });
    }, true);
  }

  imprimir(recibo: Recibo, tipo: string) {
    this.loadingService.present("Imprimiendo...")
    this.logicaRecibo.print(recibo, tipo).subscribe(response => {
      this.loadingService.dismiss();
      this.toast.presentToastSuccess(response.data.message)
    }, error => {
      this.loadingService.dismiss();
      console.log('error en impresion de recibo', error);
      this.toast.presentToastDanger(error);

    });
  }

}
