import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosCreadosPage } from './recibos-creados.page';

describe('RecibosCreadosPage', () => {
  let component: RecibosCreadosPage;
  let fixture: ComponentFixture<RecibosCreadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosCreadosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosCreadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
