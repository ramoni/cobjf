import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { Factura } from 'src/app/Clases/factura';
import { RecibosLogicaService } from 'src/app/Servicios/reciboService/recibos-logica.service';
import { Recibo } from 'src/app/Clases/recibo';
import { ActivatedRoute, Router } from '@angular/router';
import { ValueType } from 'src/app/Clases/value-type';
import { Concepto } from 'src/app/Clases/concepto';
import { Entidad } from 'src/app/Clases/entidad';
import { Moneda } from 'src/app/Clases/moneda';
import { Cuenta } from 'src/app/Clases/cuenta';
import { ConceptoLogicaService } from 'src/app/Servicios/conceptoService/concepto-logica.service';
import { CuentaLogicaService } from 'src/app/Servicios/cuentaService/cuenta-logica.service';
import { EntidadLogicaService } from 'src/app/Servicios/entidadService/entidad-logica.service';
import { MonedaLogicaService } from 'src/app/Servicios/monedaService/moneda-logica.service';
import { ValoresLogicaService } from 'src/app/Servicios/valorService/valores-logica.service';
import { Value } from 'src/app/Clases/value';
import * as moment from 'moment';
import { Cliente } from 'src/app/Clases/cliente';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';

@Component({
  selector: 'app-crear-recibo',
  templateUrl: './crear-recibo.page.html',
  styleUrls: ['./crear-recibo.page.scss'],
})
export class CrearReciboPage implements OnInit {

  cliente: Cliente;
  totalFacturas: number = 0;
  totalValores: number = 0;
  recibo: Recibo;
  conceptos: Array<Concepto> = [];
  tipoValores: Array<ValueType> = [];
  cuentas: Array<Cuenta> = [];
  entidades: Array<Entidad> = [];
  entidadesAux: Array<Entidad> = [];
  fecha: string;
  tab: string = 'datos';
  monedas: Array<Moneda>;
  moneda: Moneda;
  monedaAux: Moneda;
  title: string;
  isChecked: boolean = false;
  interesAux: number = 0;
  totalInteres: number = 0;
  monedaValor: Moneda;
  montoFac: string;
  facturaInteres: Array<Factura>;
  facturaCobrar: Array<Factura>;
  value: string;
  monedaEnable: boolean = false;
  dataSync: boolean = false;;
  facturas: Array<Factura> = [];

  constructor(
    public navCtrl: NavController,
    private logic: RecibosLogicaService,
    private monedaLogic: MonedaLogicaService,
    private valorLogic: ValoresLogicaService,
    private entidadLogic: EntidadLogicaService,
    private cuentaLogic: CuentaLogicaService,
    private plat: Platform,
    private conceptoLogic: ConceptoLogicaService,
    private ruta: ActivatedRoute,
    private alertService: AlertControllerService
  ) {


    this.initializeBackButtonCustomHandler();
  }

  ngOnInit() {
    this.dataSync = false;
    this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente;
    this.facturaCobrar = JSON.parse(this.ruta.snapshot.params.datos).facturas;
    this.moneda = JSON.parse(this.ruta.snapshot.params.datos).monedaRecibo;
    console.log('moneda en crear_recibo ', this.moneda)
    this.facturas = this.facturaCobrar;
    this.recibo = null;

    console.log('facturas a cobrar en recibos ', this.facturaCobrar)
    //console.log('recibo en recibos ', this.recibo)
    this.utiles().then(() => {
      if (!this.recibo) {
        this.recibo = this.logic.getDefaultReceipt(this.cliente, this.facturas);
        this.recibo.id_moneda = this.moneda.id;
        this.recibo.cotizacion = this.valorLogic.getCotizacion();
        this.facturaInteres = this.facturas;

        this.addDefaultValor();

        this.title = 'Carga';
      } else {
        this.moneda = this.monedaLogic.getByIdOrName(this.recibo.id_moneda.toString());
        this.title = 'Edicion';
      }
      console.log(this.recibo);

      this.fecha = moment(this.recibo.fecha).format('YYYY-MM-DD')
      console.log('fecha recibo', this.fecha)
      this.recalcularTotalFacturas();
      this.recalcularTotalValores();
      this.dataSync = true;
    })

  }
  ionViewDidEnter() {

  }
  async utiles() {
    this.monedas = await this.monedaLogic.getMonedasBD();
    console.log('monedas en crear-recibo', this.monedas)
    this.tipoValores = await this.valorLogic.getValoresBD();
    console.log('valores en crear-recibo', this.tipoValores)
    this.entidades = await this.entidadLogic.getEntidadesBD();
    console.log('entidades en crear-recibo', this.entidades)
    this.cuentas = await this.cuentaLogic.getCuentasBD();
    console.log('cuentas en crear-recibo', this.cuentas)
    this.conceptos = await this.conceptoLogic.getConceptosBD();
    console.log('conceptos en crear-recibo', this.conceptos)
    this.filterEntidades()
    return this.monedas, this.tipoValores, this.entidades, this.cuentas, this.conceptos;

  }

  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      this.navCtrl.pop();
    })
  }

  public addDefaultValor() {
    console.log('tipoValores', this.tipoValores[0])
    let valor = this.tipoValores[0];

    for (let mon of this.monedas) {
      //console.log('mon; ', mon, 'monedas', this.monedas)
      if (mon.id === valor.id_moneda) {
        this.monedaValor = mon;
        break;
      }
    }
    console.log('monedaValor addDefaultValor', this.monedaValor)
    this.recibo.valores.push(this.logic.getDefaultValor(this.monedaValor));

  }


  public removeValor(valor: Value) {
    if (this.recibo.valores.length === 1) {
      this.alertService.alertMessageWithHandler('Error', 'Debe haber al menos un valor');
      return;
    }
    this.recibo.valores = this.recibo.valores.filter(val => val !== valor);
    this.recalcularTotalValores();
  }

  public onMonedaChange() {
    console.log('onMonedaChange ', this.moneda.id, this.monedas)
    this.monedaAux = this.monedaLogic.getByIdOrName(this.moneda.id);
    this.moneda = this.monedaAux;
    this.recibo.id_moneda = this.moneda.id;
    this.recalcularTotalFacturas();
    this.recalcularTotalValores();
  }

  setearMonedaValor(i: number) {
    console.log('setearMonedaValor')
    this.recibo.valores[i].tipo.id_moneda = this.recibo.valores[i].moneda.id;
    this.recalcularTotalValores();
  }

  public calcularInteres(facturas: Array<Factura>) {
    for (let i = 0; i < facturas.length; i++) {

      let interes = facturas[i].interes;
      this.sumarInteres(interes, facturas[i].moneda)
    }
  }

  public sumarInteres(data: number, moneda: string): number {

    this.monedaAux = this.monedaLogic.getByIdOrName(moneda);
    if (this.moneda.descripcion === moneda) {
      this.totalInteres += parseFloat(data.toFixed(this.moneda.cant_decimales));
      this.totalFacturas += parseFloat(data.toFixed(this.moneda.cant_decimales));
      console.log('es igual a las monedas ', this.totalInteres, this.totalFacturas)
    } else {
      if (data > 0) {
        this.interesAux = 0;
        this.interesAux = this.monedaLogic.convert(data, this.monedaAux, this.moneda, this.recibo.cotizacion);
        console.log('interes auxiliar ', this.interesAux)
        this.totalFacturas += parseFloat(this.interesAux.toFixed(this.moneda.cant_decimales));
        this.totalInteres += parseFloat(this.interesAux.toFixed(this.moneda.cant_decimales));
        console.log('total interes ', this.totalInteres)
        console.log('total facturas', this.totalFacturas)

      }
    }
    this.recibo.interes = this.totalInteres;

    return this.recibo.interes;
  }

  public recalcularTotalFacturas() {
    console.log('recalcularTotalFacturas')
    this.totalFacturas = 0;
    for (let f of this.recibo.facturas) {
      if (f.a_cobrar > f.data.saldo) {
        this.title = 'carga';
        //return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'El monto a cobrar no debe superar el saldo de la factura!');
        return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'No puede realizarse mas de un recibo en el dia, a una misma factura!', d=>{
          /*self.navCtrl.navigateForward(['facturas', {
            datos: JSON.stringify(
              { cliente: self.cliente })
          }])*/
          this.navCtrl.pop();
        });
      } else {
        f.a_cobrar = +f.a_cobrar;
        if (this.moneda.descripcion === f.data.moneda) {
          f.valorizado = f.a_cobrar;
        } else {
          this.value = this.monedaLogic.convertByDesc(f.a_cobrar, f.data.moneda, this.moneda.descripcion, this.recibo.cotizacion)
            .toFixed(this.moneda.cant_decimales);
          f.valorizado = Number(this.value);
        }

        this.totalFacturas += f.valorizado;
      }

    }
    let redondeoFac = this.totalFacturas.toFixed(this.moneda.cant_decimales);
    this.totalFacturas = Number(redondeoFac);
    if (this.isChecked) {
      this.totalInteres = 0;
      this.recibo.interes = 0;
      this.calcularInteres(this.facturaInteres);
    } else {
      this.totalInteres = 0;
      this.recibo.interes = 0;
      this.totalFacturas = this.totalFacturas;
    }
    let total = this.totalFacturas.toFixed(this.moneda.cant_decimales);
    this.totalFacturas = Number(total);

  }


  public recalcularTotalValores() {
    console.log('recalcularTotalValores')
    this.totalValores = 0;
    for (let f of this.recibo.valores) {
      f.total = +f.total;
      if (this.moneda.id === f.moneda.id) {
        f.valorizado = f.total;
      } else {
        f.valorizado = Number(this.monedaLogic.convert(f.total, f.moneda, this.moneda, this.recibo.cotizacion)
          .toFixed(this.moneda.cant_decimales));
      }

      this.totalValores += f.valorizado;
    }

  }

  public limpiarValor(i: number) {
    console.log('limpiarValores ');
    this.recibo.valores[i].banco = null;
    this.recibo.valores[i].cuenta = null;
    this.recibo.valores[i].numero = "0";
    let valorSeleccionado = this.valorLogic.getByIdOrName((this.recibo.valores[i].tipo.id).toString());
    console.log('valor seleccionado', valorSeleccionado)
    this.recibo.valores[i].tipo.id = valorSeleccionado.id;
    this.recibo.valores[i].tipo.descripcion = valorSeleccionado.descripcion;
    this.recibo.valores[i].tipo.has_banco = valorSeleccionado.has_banco;
    this.recibo.valores[i].tipo.has_cuenta = valorSeleccionado.has_cuenta;
    this.recibo.valores[i].tipo.has_fecha = valorSeleccionado.has_fecha;
    this.recibo.valores[i].tipo.has_number = valorSeleccionado.has_number;
    this.recibo.valores[i].tipo.abreviacion = valorSeleccionado.abreviacion;
    if (parseInt(valorSeleccionado.id_moneda) === 0) {
      console.log('No tiene moneda por defecto true');
      this.recibo.valores[i].tipo.id_moneda = this.moneda.id;
      this.recibo.valores[i].modificarMoneda = 1;
    } else {
      console.log('Tiene moneda por defecto false')
      this.recibo.valores[i].tipo.id_moneda = valorSeleccionado.id_moneda;
      this.recibo.valores[i].modificarMoneda = 0;
    }
    if (this.recibo.valores[i].tipo.has_fecha === 1) {
      console.log('fecha de recibo', this.recibo.fecha)

      this.recibo.valores[i].fecha = moment(this.recibo.fecha).format('YYYY-MM-DD');
      console.log('fecha de recibo valor moment', this.recibo.valores[i].fecha)
    } else {
      this.recibo.valores[i].fecha = null;
    }

    if (this.recibo.valores[i].tipo.has_cuenta === 1) {
      console.log('maneja cuenta', this.entidadesAux);
    }
    for (let moneda of this.monedas) {
      if (moneda.id === this.recibo.valores[i].tipo.id_moneda) {
        this.monedaEnable = false;
        console.log('Encuentra la moneda del valor');
        this.recibo.valores[i].moneda = moneda;
      }
    }
  }

  /*public changeCuenta(i: number) {

    for (let cuenta of this.cuentas) {
      if (this.recibo.valores[i].cuenta === cuenta.id) {
        for (let moneda of this.monedas) {
          if (moneda.id === cuenta.id_moneda) {

            this.recibo.valores[i].moneda = moneda;
            break;
          }
        }
      }
    }
  }*/

  public finish() {
    //para controlar valores con numeros que sean requeridos el numero de cheque, fecha y banco
    for (let i = 0; i < this.recibo.valores.length; i++) {
      if (this.recibo.valores[i].tipo.has_number === 1) {
        if (this.recibo.valores[i].numero === "0" || this.recibo.valores[i].numero === "") {
          return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese el numero de cheque/ cuenta o retencion!');
        }
      }
      if (this.recibo.valores[i].tipo.has_fecha === 1) {
        if (this.recibo.valores[i].fecha === null || this.recibo.valores[i].fecha === "") {
          return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Seleccione la fecha del cheque/ cuenta o retencion!');
        }
      }
      if (this.recibo.valores[i].tipo.has_banco === 1) {
        if (this.recibo.valores[i].banco === null) {
          return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Seleccione el banco correspondiente!');
        }
      }
      //para acreditacion en cuenta//
      if (this.recibo.valores[i].tipo.has_cuenta === 1) {

        if (this.recibo.valores[i].cuenta === null) {
          return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Seleccione una cuenta!');
        }
      }
    }
    if (this.totalFacturas <= 0) {
      return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese al menos un monto para una factura mayor a 0');
    }
    if (this.recibo.cotizacion <= 0) {
      return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese una cotizacion valida, al menos la cotizacion del dia');
    }
    if (this.totalValores <= 0) {
      return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese al menos un valor');
    }
    if (!this.isChecked) {
      this.recibo.interes = 0;
    }
    if (this.totalFacturas - this.totalValores > 0.0001) {
      return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'El total de los valores es menor al total a cobrar');
    } else {
      this.recibo.anticipo = Number((this.totalValores - this.totalFacturas).toFixed(this.moneda.cant_decimales));
    }


    this.recibo.total = this.totalValores;
    this.navCtrl.navigateForward(['resumen-cobro', {
      datos: JSON.stringify({
        recibo: this.recibo,
        cliente: this.cliente,
        decimalesMoneda: this.moneda.cant_decimales,
        moneda: this.moneda,
        bancos: this.entidades,
        valores: this.tipoValores

      })
    }]);

  }
  filterEntidades() {
    this.entidadesAux = this.entidades.filter(enti => {
      return this.cuentas.find(cuenta => {
        return cuenta.id_entidad === enti.id
      })
    })
  }
  
  vaciarCuentas(i: number){
    console.log('VaciarCuentas')
    this.recibo.valores[i].cuenta = null;
  }
}
