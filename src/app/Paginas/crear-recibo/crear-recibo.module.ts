import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CrearReciboPage} from './crear-recibo.page';
import { PipesModuleModule } from 'src/app/Pipes/pipes-module.module';

const routes: Routes = [
  {
    path: '',
    component: CrearReciboPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    PipesModuleModule,
  ],
  declarations: [CrearReciboPage]
})
export class CrearReciboPageModule {}
