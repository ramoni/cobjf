import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/Clases/cliente';
import { InvoiceWithACobrar } from 'src/app/Clases/invoice-with-acobrar';
import { Recibo } from 'src/app/Clases/recibo';
import { Moneda } from 'src/app/Clases/moneda';
import { ValueType } from 'src/app/Clases/value-type';
import { Entidad } from 'src/app/Clases/entidad';
import { NavController, Platform, } from '@ionic/angular';
import { RecibosLogicaService } from 'src/app/Servicios/reciboService/recibos-logica.service';
import { CreateReceiptData } from 'src/app/Clases/create-receipt-data';
import { ActivatedRoute } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
import { SincronizacionService } from 'src/app/Servicios/sincronizacionService/sincronizacion.service';
@Component({
  selector: 'app-resumen-cobro',
  templateUrl: './resumen-cobro.page.html',
  styleUrls: ['./resumen-cobro.page.scss'],
})
export class ResumenCobroPage implements OnInit {

  cliente: Cliente;
  recibo: Recibo;
  facturas: Array<InvoiceWithACobrar>;
  totalRecibo: number = 0;
  totalFCO: number = 0;
  totalFCR: number = 0;
  fcoCheck: boolean = false;
  reciboCheck: boolean = false;
  interesCheck: boolean = false;
  anticipoCheck: boolean = false;
  todosFCO: boolean = false;
  todosFCR: boolean = false;
  moneda: Moneda;
  title: string;
  bancos: Array<Entidad>;
  valores: Array<ValueType>;
  public printOK: boolean = false;
  public imprimio: boolean = false;
  public imprimioDuplicado=false;
  public marcaFin: Array<any> = [];
  public marca: string;
  constructor(
    public navCtrl: NavController,
    private logicaRecibos: RecibosLogicaService,
    private alertCtrl: AlertControllerService,
    private loadingService: LoadingControllerService,
    public plat: Platform,
    private writeLog: WriteFileService,
    private ruta: ActivatedRoute,
    private toast: ToastControllerService,
    private sincronizacion:SincronizacionService ) {

    this.initializeBackButtonCustomHandler();
  }
  ngOnInit() {
    this.recibo = JSON.parse(this.ruta.snapshot.params.datos).recibo
    this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente
    this.moneda = JSON.parse(this.ruta.snapshot.params.datos).moneda;
    this.valores = JSON.parse(this.ruta.snapshot.params.datos).valores
    this.bancos = JSON.parse(this.ruta.snapshot.params.datos).bancos

    this.facturas = this.recibo.facturas;

    console.log(this.moneda)
    this._sumatoriaFacturas(this.facturas);
    let date = new Date();
    let time = date.getTime();
    let hash = Md5.hashStr(String(time));
    console.log('hash', hash);
    this.marcaFin.push(hash);
    this.marca = this.marcaFin[0];

    console.log('marca', this.marca);
  }
  atras() {
    this.navCtrl.pop();
  }
  public initializeBackButtonCustomHandler() {
    this.plat.backButton.subscribeWithPriority(0, () => {
      (this.navCtrl.pop()).then(() => {
        this.navCtrl.pop();
      });
    })
  }
  _sumatoriaFacturas(facturas: Array<InvoiceWithACobrar>) {
    for (let f of this.facturas) {
      if (f.data.tipo === 'FCO') {
        this.totalFCO += f.valorizado;
      } else {
        this.totalFCR += f.valorizado;
      }
    }
    if (this.totalFCO > 0) {
      this.fcoCheck = true;
      this.totalRecibo += this.recibo.interes + this.totalFCO;
      //this.totalFCO +=this.recibo.interes;

    }
    if (this.totalFCR > 0) {
      this.reciboCheck = true;
      this.totalRecibo += + this.totalFCR;
    }
    if (this.recibo.interes > 0) {
      this.interesCheck = true;
      this.totalRecibo += this.recibo.interes;

    }
    if (this.recibo.anticipo > 0) {
      this.anticipoCheck = true;
      this.totalRecibo += this.recibo.anticipo;
    }
  }
  finish() {
    for (let f of this.facturas) {
      if (f.data.tipo === 'FCO') {
        this.todosFCO = true;
      } else {
        this.todosFCO = false;
        break;
      }
    }
    for (let f of this.facturas) {
      if (f.data.tipo === 'FCR') {
        this.todosFCR = true;
      } else {
        this.todosFCR = false;
        break;
      }
    }

    if ((this.todosFCO === true && this.todosFCR === true) ||
      (this.todosFCO === false && this.todosFCR === true)) {
      this.alertCtrl.alertMessageWithHandler(this.title,
        "Esta seguro de guardar e imprimir el recibo?", async d => {
          this.loadingService.present('Guardando ...')
          this.recibo.marca = this.marca;
          await this.writeLog.escribirLog(new Date()+ ' Peticion: Guardado de recibo, facturas FCR e impresion original' + JSON.stringify(this.recibo) + '\n')
          this.logicaRecibos.addOrUpdate(this.recibo).subscribe(async response => {
            this.sincronizacion.sincronizarRecibos(Number(JSON.parse(localStorage.getItem('habilitacion'))));
            this.toast.presentToastAlert('Sincronizando...')
            console.log('retorno de addOrUpdate ', response)
            this.loadingService.dismiss();
            await this.writeLog.escribirLog(new Date() + 'Respuesta con exito: ' + JSON.stringify(response.data) + '\n')
            this.toast.presentToastSuccess(response.data.message)
            this.imprimio = true;
            this.printOK=true;
            localStorage.removeItem('reciboGenerado');

          }, async error => {

            this.imprimio = false;
            this.printOK=false;
            this.loadingService.dismiss();
            console.log('error en crear/imprimir el recibo original', error)
            let recibo = new CreateReceiptData();
            recibo.id = JSON.parse(localStorage.getItem('reciboGenerado'));
            //console.log('recibo en error de impresiones ', recibo);

            if (recibo.id !== null) {
              this.verificarRecibo();
              console.log('Tiene que eliminar el recibo generado');
              localStorage.removeItem('reciboGenerado');
            }
            await this.writeLog.escribirLog(new Date() + 'Respuesta con error: ' + error + '\n')
            this.toast.presentToastDanger(error)
          });

        }, true);

    } else {
      this.alertCtrl.alertMessageWithHandler(this.title,
        "Esta seguro que desea guardar el cobro?", async d => {

          this.loadingService.present('Guardando ...')
        
          await this.writeLog.escribirLog(new Date() + ' Peticion: Guardado de recibo, facturas FCO' + JSON.stringify(this.recibo) + '\n')
          this.recibo.marca = this.marca;
          console.log(this.recibo);
          this.logicaRecibos.addOrUpdateFCO(this.recibo).subscribe(async response => {
            console.log('retorno de addOrUpdateFCO', response)
            await this.writeLog.escribirLog(new Date() + ' Respuesta con exito: ' + JSON.stringify(response.data) + '\n')
            this.loadingService.dismiss();
            this.imprimio = true;
            this.printOK = true;
            this.imprimioDuplicado=true;
          }, async error => {
            console.log('ocurrio un error para factura FCO', error)
            this.imprimio = false;
            this.printOK = false;
            this.imprimioDuplicado=false;
            await this.writeLog.escribirLog(new Date() + ' Respuesta con error: ' + error + '\n')
            this.toast.presentToastDanger(error)
            this.loadingService.dismiss();
          });

        }, true);

    }
  }
  imprimirDuplicado() {

    this.loadingService.present('Imprimiendo...')
    setTimeout(async () => {
      await this.writeLog.escribirLog(new Date()+ ' Peticion: Guardado de recibo, facturas FCR e impresion duplicado' + JSON.stringify(localStorage.getItem('recibo')) + '\n')
      this.logicaRecibos.printDuplicado().subscribe(async response => {
        this.loadingService.dismiss();
        await this.writeLog.escribirLog(new Date()+ ' Respuesta con exito: ' + JSON.stringify(response) + '\n')
        this.imprimioDuplicado=true;
        this.toast.presentToastSuccess('Recibo impreso correctamente')

      }, async error => {
        console.log('error en imprimiendo')
        this.loadingService.dismiss();
        console.log('error en imprimir el recibo ', error);
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error: ' + error + '\n')
        this.toast.presentToastDanger(error)
      });
    }, 2000)
  }

  verificarRecibo() {
    this.imprimio = true;
    this.printOK = true;

  }

  volverFacturas() {
    this.navCtrl.navigateBack(['facturas',
      this.cliente
    ])
  }
  esCero(total: number): boolean {
    if (total > 0) {
      return false;
    } else {
      return true;
    }

  }

}
