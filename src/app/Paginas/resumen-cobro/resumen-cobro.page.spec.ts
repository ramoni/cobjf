import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenCobroPage } from './resumen-cobro.page';

describe('ResumenCobroPage', () => {
  let component: ResumenCobroPage;
  let fixture: ComponentFixture<ResumenCobroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenCobroPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenCobroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
