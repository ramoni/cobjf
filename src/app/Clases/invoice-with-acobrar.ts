import { Factura } from './factura';

/**
  * Modela los datos de una factura y el monto que el cliente abonara por la factura
  */
export class InvoiceWithACobrar {
  /** Monto que se debe cobrar de la factura */
  public a_cobrar: number;

  constructor(public data: Factura, public valorizado: number) {
    this.a_cobrar = valorizado;
  }
}