export class NuevaNota {
    constructor(public id: number,
                public id_cliente: string,
                public nota: string,
                public latitud: string,
                public longitud: string,
                public tipo: string,
                public habilitacion_caja: number,
                public imei: string) {
    }
}