import { DocumentData } from './document-data';
import { MensajeResponse } from './mensaje-response';

export class NewDocumentResponse {

    public message: MensajeResponse;
    public document: Array<DocumentData>;

    constructor() {

    }
}
