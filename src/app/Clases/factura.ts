import { Cliente } from './cliente';

export class Factura {

    public id: string;
    public cliente: Cliente;
    public numero: string;
    public moneda: string;
    public total: number;
    public monto_factura: number;
    public saldo: number;
    public saldo_cuota: number;
    public cuota_minima: number;
    public cuota_maxima: number;
    public fecha_emision: string;
    public fecha_vencimiento: string;
    public tipo: string;
    public id_moneda: string;
    public interes:number;

    constructor(dat: any, cliente: any) {
      this.id = dat.ID_FACTURA;
      this.numero = dat.NO_FACTURA;
      this.moneda = dat.DESCRIPCION_MONEDA;
      this.fecha_emision = dat.FECHA;
      this.monto_factura= parseFloat(dat.MONTO_FACTURA);
      this.cuota_minima= dat.CUOTA_MINIMA;
      this.cuota_maxima= dat.CUOTA_MAXIMA;
      this.total = parseFloat(dat.TOTAL);
      this.saldo = parseFloat(dat.SALDO);
      this.saldo_cuota = parseFloat(dat.SALDO_CUOTA);
      this.cliente = cliente;
      this.fecha_vencimiento=dat.VENCIMIENTO;
      this.tipo=dat.TIPO;
      this.id_moneda= dat.ID_MONEDA;
      this.interes= dat.INTERES;
  
    }
  
  
  }
  