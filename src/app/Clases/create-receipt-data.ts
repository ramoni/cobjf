import { NewReceiptResponse } from './new-receipt-response';
import { ReciboDTO } from './recibo-DTO';
import { Impresora } from './impresora';

export class CreateReceiptData {

    public id : number;
    public data : NewReceiptResponse;
    public dto : ReciboDTO;
    public printer : Impresora;
    public print : string;
  
    constructor() {};
  }