export class NewReceiptResponse {
    public message: string;
    public id: number;
    public totalHabilitacion: number;
    public reciboOriginal:string;
    public reciboDuplicado:string;
    constructor(data: any) {
        this.message = data.exito;
        this.id =Number(data.ID_RECIBO);
        this.totalHabilitacion = Number(data.totalHabilitacion);
        this.reciboOriginal= data.RECIBO_ORIGINAL;
        this.reciboDuplicado=data.RECIBO_DUPLICADO;
    }
}
