import { Documentos } from "./documentos";

export class DocumentWithCheck {
    constructor(
        public data: Documentos, 
        public check: boolean) { }
  }