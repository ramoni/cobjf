class NewCobroRequest {

    id_cliente: string;
    notas: string;
    latitud: string;
    longitud: string;
  
    constructor(idCliente: string, notas: string, latitud: string, longitud : string) {
      this.id_cliente = idCliente;
      this.notas = notas;
      this.latitud = latitud;
      this.longitud = longitud;
    }
  
  }