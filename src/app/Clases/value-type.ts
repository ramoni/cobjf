export class ValueType {
    public id: string;
    public descripcion: string;
    public abreviacion: string;
    public has_number: number;
    public has_cuenta: number;
    public has_banco: number;
    public has_fecha: number;
    public id_moneda: string;

    constructor(data: any) {
        this.id = data.ID_VALOR.toString();
        this.descripcion = data.DESCRIPCION;
        this.abreviacion = data.ABREVIACION;
        this.has_number = parseInt(data.IND_NUMERO);
        this.has_cuenta = parseInt(data.IND_CUENTA);
        this.has_banco = parseInt(data.IND_BANCO);
        this.has_fecha = parseInt(data.IND_FECHA);
        this.id_moneda = data.ID_MONEDA;
    }
}