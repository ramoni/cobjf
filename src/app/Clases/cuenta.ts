export class Cuenta {
    public id: string;
    public descripcion: string;
    public id_entidad: string;
    public id_moneda: string;

    constructor(data: any) {
        this.id = data.ID_CUENTA.toString();
        this.descripcion = data.DESCRIPCION;
        this.id_entidad = data.ID_ENTIDAD;
        this.id_moneda = data.ID_MONEDA;
    }
}