export class Impresora {
    /**
   * Modelado de datos de una impresora StarIO
   * @param {string} portName puerto de comunicacion de la impresiora
   * @param {string} macAddress direccion Mac de comunicacion de la impresora
   * @param {string} modelName nombre del modelo de la impresora  
   */
    constructor(public portName: string, public macAddress:string, public modelName: string) { }

}