import { Factura } from "./factura";

export class FacturaWithCheck {
    constructor(public data: Factura, public check: boolean) { }
  }
  