/**
 * Modelado del concepto con el que un recibo puede ser asociado
 * 
 */
export class Concepto {
  /** Identificador unico del concepto asociado a un recibo */
  id_concepto: string;
  /** Concepto con el que un recibo puede ser expedido */
  descripcion: string;

  constructor(data: any) {
    this.id_concepto = data.ID_CONCEPTO.toString();
    this.descripcion = data.DESCRIPCION;
  }

}