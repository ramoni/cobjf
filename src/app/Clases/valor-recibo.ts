import { ValueType } from './value-type';
import { Moneda } from './moneda';

export class ValorRecibo {

    public id: string;
  
    constructor(
      public tipo: ValueType,
      public moneda: Moneda,
      public numero: string,
      public total: number,
      public valorizado: number,
      public banco: number,
      public cuenta: string,
      public fecha: string,
      public modificarMoneda:number,
    ) {}
  }