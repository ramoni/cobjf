export class ReciboDTO {
    constructor(
  
      public fecha: string,
      public id_cliente: number,
      public id_moneda: number,
      public cotizacion: number,
      public interes: number,
      public total_recibo: number,
      public anticipo: number,
      public version: string,
      public facturas: Array<{id_factura:number,monto:number}>,
      public valores: Array<{id_tipo_valor:number, numero_valor: string, 
        id_moneda: number, monto: number,
        id_entidad: number, id_cuenta_bancaria:string, 
        fecha_emision: string,valorizado: number}>,
      public latitud: string,
      public longitud: string,
      public marca: string,
      public id_concepto: number,
      public imei: string,
    ) {}
  }