/**
 * Modelado de un objeto de tipo cliente
 */
export class Cliente {
  /** Identificador unico del cliente */
  id: string;
  /** Nombre de la empresa unipersonal o juridica */
  razonSocial: string;
  /** El ruc del cliente */
  ruc: string;
  /** La direccion del cliente */
  dir: string;
  /** Ubicacion de la direccion en un mapa */
  latitud: number;
  longitud: number;
  /** La cantidad de documentos o facturas que deben ser entregadas */
  entregas: number;
  /** La cantidad de facturas que el cliente tiene asociado y que puede cobrar en el dia */
  facturas: number;
  /** Cantidad de entregas que realizo con el cliente */
  entregas_doc: number;
  /** Cantidad de recibos hechos al cliente */
  recibos_fac: number;
  /** Notas de las facturas a cobrar del cliente */
  notas_fac: number;
  /** Notas del cobrador para los documentos a entregar del cliente */
  notas_ent: number;
  /** Notas generales para el cliente */
  notas: string;

  constructor(data: any) {
    this.id = data.ID_CLIENTE;
    this.razonSocial = data.RAZON_SOCIAL;
    this.ruc = data.RUC_CLIENTE;
    this.dir = data.DIR_CLIENTE;
    this.entregas = parseInt(data.ENTREGAS);
    this.facturas = parseInt(data.FACTURAS);
    this.latitud = data.LATITUD ? parseFloat(data.LATITUD) : 0;
    this.longitud = data.LONGITUD ? parseFloat(data.LONGITUD) : 0;
    this.entregas_doc = parseInt(data.ENTREGAS_DOC);
    this.recibos_fac = parseInt(data.RECIBOS_FAC);
    this.notas_fac = parseInt(data.NOTAS_FACTURAS);
    this.notas_ent = parseInt(data.NOTAS_ENTREGAS);
    this.notas = data.NOTAS;
  }
}