export class NumHabilitacion {
    habilitacion: number;
    datos: number;
    imei: string;
    logs: string;
    constructor(habilitacion: number, datos: number, imei: string) {
        this.habilitacion = habilitacion;
        this.datos = datos;
        this.imei = imei;
        this.logs = null;
    }
}
