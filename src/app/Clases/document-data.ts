export class DocumentData {
    constructor(
        public entregas: Array<{
            id_entrega: number,
            id_cobrador: string,
            id_cliente: string,
            id_entrega_det: number,
            id_documento: number,
            no_documento: string,
            id_moneda: string,
            importe: number,
            guardado: number,
            fecha_documento: string,
            latitud: string,
            longitud: string,
            imei: string,
            habilitacion_caja: number
        }>
    ) { }
}
