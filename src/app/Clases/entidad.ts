
export class Entidad {
    public id: string;
    public descripcion: string;

    constructor(data: any) {
        this.id = data.ID_ENTIDAD.toString();
        this.descripcion = data.DESCRIPCION;
    }
}
