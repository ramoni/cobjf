import { MensajeResponse } from './mensaje-response';
import { NuevaNota } from './nueva-nota';

export class NewNotasResponse {
    public message: MensajeResponse;
    public notas: NuevaNota;
  
    constructor() {
  
    }
}