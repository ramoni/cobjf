export class Moneda {
    public id: string;
    public descripcion: string;
    public activo: string;
    public cant_decimales: number;
    public simbolo: string;
    public plural: string;
    public singular: string;

    constructor(data: any) {
        this.id = data.ID_MONEDA.toString();
        this.descripcion = data.DESCRIPCION_MONEDA;
        this.activo = data.ACTIVO.toString();
        this.cant_decimales = parseInt(data.CANT_DECIMALES);
        this.simbolo = data.SIMBOLO;
        this.plural = data.MONEDA_PLURAL;
        this.singular = data.MONEDA_SINGULAR;
    }
}