
export class DatosUsuario {
    CAJA: string;
    DOCUMENTOS: number;
    EXPIRED: string;
    FACTURAS: number;
    HABILITACION: number;
    USUARIO: {
        ID_COBRADOR: string;
        NOMBRE: string;
        ROL: string;
        TALONARIO: string;
        ULTIMO_RECIBO: string;
    }
    DATOS_SINCRONIZADOS: {
        RECIBOS: string;
        ENTREGAS: string;
        NOTAS_VISITAS: string;
        NOTAS_ENTREGAS: string;
    }

    constructor(data:any){
        this.CAJA=data.CAJA;
        this.DOCUMENTOS=parseInt(data.DOCUMENTOS);
        this.EXPIRED=data.EXPIRED;
        this.FACTURAS=parseInt(data.FACTURAS);
        this.HABILITACION=parseInt(data.HABILITACION);
        this.DATOS_SINCRONIZADOS.RECIBOS=data.DATOS_SINCRONIZADOS.RECIBOS;
        this.DATOS_SINCRONIZADOS.ENTREGAS=data.DATOS_SINCRONIZADOS.ENTREGAS;
        this.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS=data.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS;
        this.DATOS_SINCRONIZADOS.NOTAS_VISITAS=data.DATOS_SINCRONIZADOS.NOTAS_VISITAS;

    }

    /*public totalDocumentos: number;
    public totalFacturas: number;
    public totalHabilitacion: number;
    public caja: string;
    public nombre: string;

    constructor(data: any) {
        this.totalDocumentos = parseInt(data.DOCUMENTOS);
        this.totalFacturas = parseInt(data.FACTURAS);
        this.totalHabilitacion = parseInt(data.HABILITACION);
        this.caja = data.CAJA;
        if (typeof (data.USUARIO) !== "undefined") {
            this.nombre = data.USUARIO.NOMBRE;
        }else {
            this.nombre = data.NOMBRE;
        }
      
    }*/


}
