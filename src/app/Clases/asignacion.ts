/**
 * Modelado de datos para la asignacion de un cliente a un cobrador
 */
export class Asignacion {
  /** Indica si tiene o no facturas agendadas para ese dia */
  visitas: boolean;
  /** Indica si tiene documentos que entregar*/
  entregas: boolean;
  /** Identificador unico del cliente a asignarse al cobrador */
  id_cliente: number;
  /** Imei del telefono que realiza la asignacion del cliente al cobrador */
  imei: string;

  constructor(data: any) {
    this.visitas = data.VISITAS;
    this.entregas = data.ENTREGAS;
    this.id_cliente = data.ID_CLIENTE;
    this.imei = localStorage.getItem('imei');
  }
}