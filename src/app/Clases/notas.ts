export class Notas {
  id: number;
  nota: string;
  fecha: string;
  id_cliente: string;
  latitud: string;
  longitud: string;
  tipo: string;
  habilitacion_caja: number;
  sincronizacion: number;
  constructor(data: any = null) {
    if(data != null){
      this.id = parseInt(data.ID);
      this.nota = data.NOTAS;
      this.fecha = data.FEC_VISITA;
      this.id_cliente = data.ID_CLIENTE;
      this.latitud = data.LATITUD;
      this.longitud = data.LONGITUD;
      this.tipo = data.TIPO;
      this.habilitacion_caja = parseInt(data.HABILITACION);
      this.sincronizacion = data.REQUIERE_SINCRONIZACION;
    } else {
      this.id = null;
      this.nota = null;
      this.fecha = null;
      this.id_cliente = null;
      this.latitud = null;
      this.longitud = null;
      this.tipo = null;
      this.habilitacion_caja = null;
      this.sincronizacion = null;
    }
  }
}