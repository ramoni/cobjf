import { Component } from '@angular/core';
import { Platform, NavController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Uid } from '@ionic-native/uid/ngx';
import { AlertControllerService } from './Servicios/alertController/alert-controller.service';
import { SincronizacionService } from './Servicios/sincronizacionService/sincronizacion.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ToastControllerService } from './Servicios/toastController/toast-controller.service';
import { BDManagementService } from './Servicios/almacenamientoSQLITE/bdManagement/bdmanagement.service';
import { WriteFileService } from './Servicios/writeFileService/logWrite.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'ios-home'
    },
    {
      title: 'Actividades',
      url: '/actividades',
      icon: 'ios-navigate'

    },
    {
      title: 'Asignar',
      url: '/asignacion',
      icon: 'ios-clipboard'

    },
    {
      title: 'Contraseña',
      url: '/cambiar-pass',
      icon: 'key'
    },

  ];

  constructor(
    private platform: Platform,
    public bd:BDManagementService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navControl: NavController,
    private gpsRequest: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    private appVersion: AppVersion,
    private toast:ToastControllerService,
    private uid: Uid,
    private alertService: AlertControllerService,
    private screenOrientation: ScreenOrientation,
    public menuCtrl: MenuController,
    public syncro:SincronizacionService 
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#C9113C')
      this.statusBar.show();
      this.splashScreen.hide();
      
      this.versionImei();
      this.syncro.sincronizarTodo();
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      
      /*(<any>window).plugins.preventscreenshot.disable((a) => 
      this.successCallback(a), 
      (b) => this.errorCallback(b));*/
      
    });

  }
  /*successCallback(result) {
    console.log(result); // true - enabled, false - disabled
  }

  errorCallback(error) {
    console.log(error);
  }*/

  openActividades() {
    this.navControl.navigateForward(['actividades', { datos: JSON.stringify({ marca: "todos" }) }]);
  }

  logout() {
    this.alertService.mostrarConfirmacionLogout();
  }
  closeMenu(){
    this.menuCtrl.close()
  }
  async checkGPSPermission() {
    await this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
          //If having permission show 'Turn On GPS' dialogue
          this.encenderGPS();
        } else {
          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }
  async encenderGPS() {
    await this.gpsRequest.request(this.gpsRequest.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        console.log('Encendio el gps')
      },
      //error => alert('Error al solicitar encendido de GPS ' + JSON.stringify(error))
      error => console.log('Error al solicitar encendido de GPS ' + JSON.stringify(error))
    );

  }

  async requestGPSPermission() {
    await this.gpsRequest.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.encenderGPS();
            }, error => {
              //Show alert if user click on 'No Thanks'
              //alert('Error al preguntar los permisos de acceso a la ubicacion ' + error)
              console.log('Error al preguntar los permisos de acceso a la ubicacion ' + error)
            }
          );
      }
    });
  }
  async versionImei() {
    await this.appVersion.getVersionNumber().then(dat => {
      localStorage.setItem('version', JSON.stringify(dat));
    }).catch(err => {
      console.log('error al obtener la version de la App ', err)
    })
    await this.getIMEI().then(resp => {
      console.log("imei del telefono en app-compo getImei", resp)

      localStorage.setItem('imei', resp);
    }).catch(err => {
      console.log('No se obtine el imei del telefono ', err);
    });

  }
  async getIMEI() {
    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    );

    if (!hasPermission) {
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );

      if (!result.hasPermission) {
        console.error('Se necesita permisos')
        throw new Error('Se necesita permisos ');
      }
      console.log('se dio los permisos')
      this.toast.presentToastSuccess('En el proximo reinicio obtendremos la configuracion de su telefono');
      //this.checkGPSPermission();
      setTimeout(() => {
        navigator['app'].exitApp();
      }, 3000)

      // ok, a user gave us permission, we can get him identifiers after restart app
      return;
    }
    this.checkGPSPermission();
    return this.uid.IMEI;
  }
}
