import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
@Injectable({
  providedIn: 'root'
})
export class ToastControllerService {

  constructor(private toast: Toast, private toastControl:ToastController) { }

  presentToastSuccess(mensaje) {

    this.toast.showWithOptions({
      message: mensaje,
      duration: 3000, // 2000 ms
      position: "bottom",
      styling: {
        opacity: 0.75, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
        backgroundColor: '#22bb33', // make sure you use #RRGGBB. Default #333333
        textColor: '#FFFFFF', // Ditto. Default #FFFFFF
        cornerRadius: 16, // minimum is 0 (square). iOS default 20, Android default 100
        horizontalPadding: 20, // iOS default 16, Android default 50
        verticalPadding: 16 // iOS default 12, Android default 30
      }
    }).subscribe(
      toast => {
        console.log(toast);
      }
    );

  }
  /*async presentToastSuccess(mensaje) {
    const toast = await this.toastControl.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom',
      animated: true,
      cssClass: "toast-success",
    });
    toast.present();
  }*/

   presentToastAlert(mensaje) {

    this.toast.showWithOptions({
      message: mensaje,
      duration: 2000, // 2000 ms
      position: "top",
      styling: {
        opacity: 0.75, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
        backgroundColor: '#292b2c', // make sure you use #RRGGBB. Default #333333
        textColor: '#FFFFFF', // Ditto. Default #FFFFFF
        cornerRadius: 16, // minimum is 0 (square). iOS default 20, Android default 100
        horizontalPadding: 20, // iOS default 16, Android default 50
        verticalPadding: 16 // iOS default 12, Android default 30
      }
    }).subscribe(
      toast => {
        console.log(toast);
      }
    );

  }
  /*async presentToastAlert(mensaje) {
    const toast = await this.toastControl.create({
      message: mensaje,
      duration: 2000,
      position: 'top',
      animated: true,
      cssClass: "toast-alert",
    });
    toast.present();
  }*/
  presentToastDanger(mensaje) {

    this.toast.showWithOptions({
      message: mensaje,
      duration: 3000, // 2000 ms
      position: "bottom",
      styling: {
        opacity: 0.75, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
        backgroundColor: '#bb2124', // make sure you use #RRGGBB. Default #333333
        textColor: '#FFFFFF', // Ditto. Default #FFFFFF
        cornerRadius: 16, // minimum is 0 (square). iOS default 20, Android default 100
        horizontalPadding: 50, // iOS default 16, Android default 50
        verticalPadding: 30// iOS default 12, Android default 30
      }
    }).subscribe(
      toast => {
        console.log(toast);
      }
    );

  }
  /*async presentToastDanger(mensaje) {
    const toast = await this.toastControl.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom',
      animated: true,
      cssClass: "toast-danger",
    });
    toast.present();
  }*/
  
  async presentToastOptions(title, texto, buttons) {
    const t = await this.toastControl.create({
      header: title,
      duration: 2000,
      position: 'bottom',
      animated: true,
      message: texto,
      buttons: buttons,
      cssClass: "toast-alert",
    });

    await t.present();
  }
}
