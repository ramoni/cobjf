import { Injectable } from '@angular/core';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class BDManagementService {
  public bd: SQLiteObject;
  constructor(
    private platform: Platform,
    private sqlitedb: SQLite
  ) {
    this.platform.ready().then(async () => {
      await this.sqlitedb.create({
        name: 'cobranzasMovil.db',
        location: "default", // the location field is required
        createFromLocation: 1
      }).then((db: SQLiteObject) => {
        console.log("bd traida");
        this.bd = db;
      }).catch(error => {
        console.log('error al crear o traer la base de datos ', error);
      });
    });
  }

  public getDataBase() {

    return this.bd;

  }

  public generateQueryToInsert(table: string, values) {
    let query = "INSERT INTO " + table + " (";

    let valuesToInsert = "";
    let parameters = [];

    Object.keys(values).forEach(function (key) {
      query = query + key + ",";
      parameters.push(values[key]);
      valuesToInsert = valuesToInsert + "?,";
    });

    query = this.erase_last_element(query) + ")";
    valuesToInsert = " VALUES(" + this.erase_last_element(valuesToInsert) + ")";

    query = query + valuesToInsert;

    console.log(query, parameters);

    return {
      query: query,
      params: parameters
    }
  }

  public generateQueryToUpdate(table: string, values, conditions: Array<any> = null) {
    let query = "UPDATE " + table + " SET ";

    let parameters = [];

    //Sets
    Object.keys(values).forEach(function (key) {
      query = query + key + "=?,";
      parameters.push(values[key]);
    });
    query = this.erase_last_element(query);

    //Wheres
    if (conditions != null) {
      if (conditions.length > 0) {
        query = query + " WHERE ";
        let n = conditions.length;
        for (let i = 0; i < n; i++) {
          query = query + conditions[i][0] + " " + conditions[i][1] + " ? ";
          if (i < n - 1) {
            query = query + " AND ";
          }
          parameters.push(conditions[i][2]);
        }
      }
    }

    console.log(query, parameters);

    return {
      query: query,
      params: parameters
    }
  }

  public update(table: string, values, conditions = null) {

    let builder = this.generateQueryToUpdate(table, values, conditions);

    return this.getDataBase().executeSql(builder.query, builder.params).then(data => {
      console.log("Update ejecutado exitosamente en " + table, data);
      return data;
    }).catch(error => {
      console.error("Error en update en " + table, error);
      throw (error);
    });
  }

  public insert(table: string, values) {

    let builder = this.generateQueryToInsert(table, values);

    return this.getDataBase().executeSql(builder.query, builder.params).then(data => {
      console.log("Insert ejecutado exitosamente", data);
      return data;
    }).catch(error => {
      console.error("Error en inserción en " + table);
      throw (error);
    });
  }

  erase_last_element(x) {
    var pos = x.length - 1;
    if (pos > 0) {
      return x.substring(0, pos);
    }
    return "";
  }

  public find(table: string, id, id_key = "ID") {
    let query = "SELECT * FROM " + table + " WHERE " + id_key + "=? LIMIT 1";
    return this.getDataBase().executeSql(query, [id]).then(data => {
      console.log('Dato encontrado de ', table, data, data.rows.item(0));
      return data.rows.item(0);
    }).catch(error => {
      console.log('Dato no pude ser encontrado de ' + table, error);
      return null;
    })
  }

  public delete(table: string, id, id_key = "ID") {
    let query = "DELETE FROM " + table + " WHERE " + id_key + "=?";
    return this.getDataBase().executeSql(query, [id]).then(data => {
      console.log('Dato eliminado de' + table + ' con ' + id_key + " = " + id);
      return true;
    }).catch(error => {
      console.log('Dato no pude ser eliminado de ' + table, error);
      return false;
    })
  }
  
}
