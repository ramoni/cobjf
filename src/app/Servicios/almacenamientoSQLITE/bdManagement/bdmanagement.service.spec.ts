import { TestBed } from '@angular/core/testing';

import { BDManagementService } from './bdmanagement.service';

describe('BDManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BDManagementService = TestBed.get(BDManagementService);
    expect(service).toBeTruthy();
  });
});
