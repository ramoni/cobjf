import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Cliente } from 'src/app/Clases/cliente';
import moment from 'moment';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})
export class ClientesBDService {

  private TABLE_CLIENTES = "CBV_CLIENTES";
  private KEY_ID = "ID_CLIENTE";
  private KEY_RAZON_SOCIAL = "RAZON_SOCIAL";
  private KEY_RUC_CLIENTE = "RUC_CLIENTE";
  private KEY_DIR_CLIENTE = "DIR_CLIENTE";
  private KEY_ENTREGAS = "ENTREGAS";
  private KEY_FACTURAS = "FACTURAS";
  private KEY_LATITUD = "LATITUD";
  private KEY_LONGITUD = "LONGITUD";
  private KEY_ENTREGAS_DOC = "ENTREGAS_DOC";
  private KEY_RECIBOS_FAC = "RECIBOS_FAC";
  private KEY_NOTAS_FACTURAS = "NOTAS_FACTURAS";
  private KEY_NOTAS_ENTREGAS = "NOTAS_ENTREGAS";
  private KEY_FECHA_VISITA = "FEC_VISITA";
  private KEY_NOTAS = "NOTAS";
  constructor(private db: BDManagementService) { }
  
  public createTableClientes(): Promise<any> {

    let CREATE_CLIENTES_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_CLIENTES + " (ID INTEGER " +
      "PRIMARY KEY AUTOINCREMENT, ID_CLIENTE INT, RAZON_SOCIAL TEXT, RUC_CLIENTE TEXT, " +
      "DIR_CLIENTE TEXT, ENTREGAS INTEGER, FACTURAS INTEGER, LATITUD REAL, LONGITUD REAL, " +
      "ENTREGAS_DOC INTEGER, RECIBOS_FAC INTEGER, NOTAS_FACTURAS INTEGER, FEC_VISITA TEXT, " +
      "NOTAS_ENTREGAS INTEGER, NOTAS TEXT)";

    return this.db.getDataBase().executeSql(CREATE_CLIENTES_TABLE, []).then(clientes => {
      console.log('se creo la tabla clientes', clientes);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla clientes', error);
      return false;
    });
  }
  public addCliente(cliente: Cliente) {
    let sql = "INSERT INTO " + this.TABLE_CLIENTES + " (ID_CLIENTE, RAZON_SOCIAL, RUC_CLIENTE, DIR_CLIENTE, " +
      "ENTREGAS, FACTURAS, LATITUD, LONGITUD, ENTREGAS_DOC, RECIBOS_FAC, NOTAS_FACTURAS, " +
      "NOTAS_ENTREGAS, NOTAS, FEC_VISITA) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    let params = [cliente.id, cliente.razonSocial, cliente.ruc, cliente.dir, cliente.entregas,
    cliente.facturas, cliente.latitud, cliente.longitud, cliente.entregas_doc, cliente.recibos_fac,
    cliente.notas_fac, cliente.notas_ent, cliente.notas, moment(new Date()).format("DD-MM-YYYY")];
    //console.log(params);
    this.db.getDataBase().executeSql(sql, params).then(data => {
      console.log('se inserto un cliente en la bd', data)
    }).catch(error => {
      console.error('error al insertar el cliente', error)
    })
  }
  public selectClientes(tipo){
    let facturas = "FACTURAS";
    let entregas = "ENTREGAS";
    let notas_facturas = "(NOTAS_FACTURAS + (select COUNT (nv.ID_CLIENTE) FROM CBV_COB_VISITAS nv WHERE nv.ID_CLIENTE=c.ID_CLIENTE AND nv.TIPO='F' )) AS NOTAS_FACTURAS";
    let recibos_fac = "(RECIBOS_FAC + (select COUNT(rc.ID_RECIBO_CAB) FROM CBV_RECIBOS_CAB rc WHERE rc.ID_CLIENTE=c.ID_CLIENTE and rc.ESTADO !='ANULADO' )) AS RECIBOS_FAC ";
    let notas_entregas = "(NOTAS_ENTREGAS + (select COUNT (nv.ID_CLIENTE) FROM CBV_COB_VISITAS nv WHERE nv.ID_CLIENTE=c.ID_CLIENTE AND nv.TIPO='E' )) AS NOTAS_ENTREGAS";
    let entregas_doc = "(ENTREGAS_DOC + (select COUNT(e.ID_DOCUMENTO) FROM CBV_ENTREGAS e WHERE e.ID_CLIENTE=c.ID_CLIENTE and e.GUARDADO=1) ) AS ENTREGAS_DOC";

    switch(tipo) {
      case "F":
        entregas = "0 ENTREGAS";
        entregas_doc = "0 ENTREGAS_DOC";
        notas_entregas = "0 NOTAS_ENTREGAS";
        break;
      case "E":
        facturas = "0 FACTURAS";
        recibos_fac = "0 RECIBOS_FAC";
        notas_facturas = "0 NOTAS_FACTURAS";
        break;
    }

    return this.KEY_ID+","+this.KEY_RAZON_SOCIAL+","+this.KEY_RUC_CLIENTE+","+this.KEY_DIR_CLIENTE+","+entregas+","+facturas+","+this.KEY_LATITUD+","+this.KEY_LONGITUD+","+entregas_doc+","+recibos_fac+","+notas_facturas+","+notas_entregas+","+this.KEY_FECHA_VISITA+","+this.KEY_NOTAS;
  }
  
  deleteAllClientes() {
    let query = "DELETE FROM " + this.TABLE_CLIENTES;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla clientes', data);
      return true;
    }).catch(error => {
      console.error('no se eliminaron los datos de tabla clientes', error);
      return false;
    })
  }

  getListLocal(offset: number, limit: number, filter: string, tipo: string): Promise<Array<Cliente>> {
    console.log("offset: " + offset + " limit: " + limit);
    offset = offset > 0 ? offset - 1 : 0;
    let query = "";
    let today = moment(new Date()).format("DD-MM-YYYY");
    let params = [];
    let select = "SELECT "+this.selectClientes(tipo)+" FROM "+this.TABLE_CLIENTES+" c ";
    if (filter != "" && filter != null) {
      filter = "%"+filter.toLowerCase()+"%";
      console.log("Filtro de cliente no es vacio " + filter);
      params = [today, filter, filter, limit, offset];
      if (tipo == "F") {
        query = select + " WHERE c.FACTURAS > 0 AND c.FEC_VISITA=? AND lower(c.RAZON_SOCIAL) like ? OR c.RUC_CLIENTE like ? ORDER BY c.FACTURAS DESC,c.RAZON_SOCIAL LIMIT ? OFFSET ?";
      }
      if (tipo == "E") {
        query = select + " WHERE c.ENTREGAS > 0 AND c.FEC_VISITA=? AND lower(c.RAZON_SOCIAL) like ? OR c.RUC_CLIENTE like ? ORDER BY c.ENTREGAS DESC,c.RAZON_SOCIAL LIMIT ? OFFSET ?";
      }
      if (tipo == "T") {
        query = select + " WHERE c.FEC_VISITA=? AND lower(c.RAZON_SOCIAL) like ? OR c.RUC_CLIENTE like ? ORDER BY c.RECIBOS_FAC DESC,c.ENTREGAS_DOC DESC,c.FACTURAS DESC,c.ENTREGAS ASC,c.RAZON_SOCIAL LIMIT ? OFFSET ?";
      }
    } else {
      console.log("Filtro de cliente es vacio");
      params = [today, limit, offset];
      if (tipo == "F") {
        query = select + " WHERE c.FACTURAS > 0 AND c.FEC_VISITA=? ORDER BY C.FACTURAS DESC,C.RAZON_SOCIAL LIMIT ? OFFSET ?";
      }
      if (tipo == "E") {
        query = select + " WHERE c.ENTREGAS > 0 AND c.FEC_VISITA=? ORDER BY c.ENTREGAS DESC,c.RAZON_SOCIAL LIMIT ? OFFSET ?";
      }
      if (tipo == "T") {
        query = select + " WHERE c.FEC_VISITA=? ORDER BY c.RECIBOS_FAC DESC,c.ENTREGAS_DOC DESC,c.FACTURAS DESC,c.ENTREGAS ASC,c.RAZON_SOCIAL LIMIT ? OFFSET ?";
      }
    }
    console.log("SELECT CBV_CLIENTES",query);
    return this.db.getDataBase().executeSql(query, params).then(data => {
      //console.log("CLIENTES TIPO " + tipo, query, params, filter);
      let list: Array<Cliente> = [];
      for (let i = 0; i < data.rows.length; i++) {
        //console.log(data.rows.item(i));
        list.push(new Cliente(data.rows.item(i)));
      }
      //console.log("LISTA DE CLIENTES", list);
      return list;
    }).catch(error => {
      console.error("Error trayendo clientes", error);
      let list: Array<Cliente> = [];
      return list;
    });
  }
  getCliente(id_cliente): Promise<Cliente> {
    let query_cliente = "SELECT RAZON_SOCIAL, RUC_CLIENTE FROM CBV_CLIENTES WHERE ID_CLIENTE=?";
    let params = [id_cliente]
    return this.db.getDataBase().executeSql(query_cliente, params).then(data => {
      if (data.rows.length > 0) {
        let cliente: Cliente = new Cliente(data.rows.item(0));
        return cliente;
      } else {
        return null
      }
    }).catch(error => {
      console.error("Error trayendo clientes", error);
      return null;
    });
  }
  deleteTableCliente() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_CLIENTES;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla cliente', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla cliente', error);
      return false;
    })
  }
  updateCliente(cliente:Cliente){
    let query ="UPDATE "+this.TABLE_CLIENTES+" SET LATITUD=?, LONGITUD=? WHERE ID_CLIENTE=?"
    return this.db.getDataBase().executeSql(query, [cliente.latitud,cliente.longitud,cliente.id]).then(data => {
      console.log('se actualizo la ubicacion del cliente', data);
      return true;
    }).catch(error => {
      console.log('NO se actualizo la ubicacion del cliente', error);
      return false;
    })
  }
}
