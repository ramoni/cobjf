import { TestBed } from '@angular/core/testing';

import { ClientesBDService } from './clientes-bd.service';

describe('ClientesBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientesBDService = TestBed.get(ClientesBDService);
    expect(service).toBeTruthy();
  });
});
