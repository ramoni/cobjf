import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Concepto } from 'src/app/Clases/concepto';

@Injectable({
  providedIn: 'root'
})
export class ConceptosBDService {

  private TABLE_CONCEPTOS = "CBV_CONCEPTOS";
  private KEY_ID_CONCEPTO = "ID_CONCEPTO";
  private KEY_DESCRIPCION = "DESCRIPCION";

  constructor(private db: BDManagementService) { }

  createTableConcepto(): Promise<boolean> {
    let CREATE_CONCEPTOS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_CONCEPTOS +
      " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_CONCEPTO INTEGER, DESCRIPCION TEXT)";
    return this.db.getDataBase().executeSql(CREATE_CONCEPTOS_TABLE, []).then(concepto => {
      console.log('se creo la tabla concepto', concepto);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla concepto', error);
      return false;
    });
  }
  addConcepto(concepto: Concepto) {
    let addLine = "INSERT INTO " + this.TABLE_CONCEPTOS + " (ID_CONCEPTO, DESCRIPCION) VALUES (?,?)";
    return this.db.getDataBase().executeSql(addLine, [concepto.id_concepto, concepto.descripcion]).then(exito => {
      console.log('se agrego el concepto', exito)
      return true;
    }).catch(error => {
      console.log('no se puede agregar el concepto', error)
      return false;
    })
  }
  getConceptos(): Promise<Array<Concepto>> {
    let query = "SELECT * FROM " + this.TABLE_CONCEPTOS;
    let conceptos: Array<Concepto> = [];
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log("DATOS DE CONCEPTOS TRAIDA ", data.rows.length, data.rows.item(0), data)
      if (data.rows.length > 0) {
        for (let index = 0; index < data.rows.length; index++) {
          conceptos.push(new Concepto(data.rows.item(index)));
        }
      }
      return conceptos;
    }).catch(err => {
      console.log('no pudo obtenerse las conceptos localmente', err);
      return err;
    });
  }
  getConcepto(id_concepto): Promise<Concepto> {
    let query = "SELECT * FROM " + this.TABLE_CONCEPTOS + " WHERE ID_CONCEPTO=?";
    return this.db.getDataBase().executeSql(query, [id_concepto]).then(data => {
      if (data.rows.length > 0) {
        return new Concepto(data.rows.item(0));
      } else {
        return null;
      }
    }).catch(error => {
      console.log("error al traer la concepto por el id", error)
      return null;
    })
  }
  deleteAllConceptos() {
    let query = "DELETE FROM " + this.TABLE_CONCEPTOS;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla conceptos', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla conceptos', error);
      return false;
    })
  }
  deleteTableConcepto() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_CONCEPTOS;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla concepto', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla concepto', error);
      return false;
    })
  }
}


