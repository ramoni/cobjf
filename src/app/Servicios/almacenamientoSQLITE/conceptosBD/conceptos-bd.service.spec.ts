import { TestBed } from '@angular/core/testing';

import { ConceptosBDService } from './conceptos-bd.service';

describe('ConceptosBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConceptosBDService = TestBed.get(ConceptosBDService);
    expect(service).toBeTruthy();
  });
});
