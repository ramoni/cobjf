import { TestBed } from '@angular/core/testing';

import { ValoresBDService } from './valores-bd.service';

describe('ValoresBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValoresBDService = TestBed.get(ValoresBDService);
    expect(service).toBeTruthy();
  });
});
