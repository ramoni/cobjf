import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { ValueType } from 'src/app/Clases/value-type';

@Injectable({
  providedIn: 'root'
})
export class ValoresBDService {
  private TABLE_VALORES = "CBV_VALORES";
  private KEY_ID_VALOR = "ID_VALOR";
  private KEY_DESCRIPCION = "DESCRIPCION";
  private KEY_ABREVIACION = "ABREVIACION";
  private KEY_IND_NUMERO = "IND_NUMERO";
  private KEY_IND_CUENTA = "IND_CUENTA";
  private KEY_IND_BANCO = "IND_BANCO";
  private KEY_IND_FECHA = "IND_FECHA";
  private KEY_ID_MONEDA = "ID_MONEDA";

  constructor(private db: BDManagementService) { }


  createTableValores(): Promise<boolean> {
    let CREATE_VALORES_TABLE = " CREATE TABLE IF NOT EXISTS " + this.TABLE_VALORES +
      " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_VALOR INTEGER, DESCRIPCION TEXT, ABREVIACION TEXT, "
      + "IND_NUMERO INTEGER, IND_CUENTA INTEGER, IND_BANCO INTEGER, IND_FECHA INTEGER, ID_MONEDA TEXT)";

    return this.db.getDataBase().executeSql(CREATE_VALORES_TABLE, []).then(valores => {
      console.log('se creo la table valores', valores);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla valores', error);
      return false;
    });
  }
  addValor(valor: ValueType): Promise<boolean> {
    let addLine = "INSERT INTO " + this.TABLE_VALORES + " (ID_VALOR, DESCRIPCION, ABREVIACION, " +
      "IND_NUMERO, IND_CUENTA, IND_BANCO, IND_FECHA, ID_MONEDA) " +
      "VALUES (?,?,?,?,?,?,?,?)";
    return this.db.getDataBase().executeSql(addLine, [valor.id, valor.descripcion, valor.abreviacion,
    valor.has_number, valor.has_cuenta, valor.has_banco, valor.has_fecha, valor.id_moneda])
      .then(exito => {
        console.log('se agrego el valor', exito)
        return true;
      }).catch(error => {
        console.log('no se puede agregar el valor', error)
        return false;
      })
  }
  getValores(): Promise<Array<ValueType>> {

    let query = "SELECT * FROM " + this.TABLE_VALORES;
    let valores: Array<ValueType> = [];
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log("DATOS DE VALORES TRAIDA ", data.rows.length, data.rows.item(0), data)
      if (data.rows.length > 0) {
        for (let index = 0; index < data.rows.length; index++) {
    
          valores.push(new ValueType(data.rows.item(index)));
        }
      }
      return valores;
    }).catch(err => {
      console.log('no pudo obtenerse las valores localmente', err);
      return err;
    });

  }
  deleteAllValores() {
    let query = "DELETE FROM " + this.TABLE_VALORES;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla valores', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla valores', error);
      return false;
    })
  }
  deleteTableCValores() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_VALORES;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla valores', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla valores', error);
      return false;
    })
  }
}
