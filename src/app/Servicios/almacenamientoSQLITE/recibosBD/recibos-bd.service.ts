import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';
import { MonedaLogicaService } from '../../monedaService/moneda-logica.service';
import * as moment from 'moment';
import { WriteFileService } from '../../writeFileService/logWrite.service';

@Injectable({
  providedIn: 'root'
})
export class RecibosBDService {
  private TABLE_RECIBOS_CAB = "CBV_RECIBOS_CAB";
  private KEY_ID_COBRADOR = "ID_COBRADOR";//cabecera
  private KEY_FECHA = "FECHA";//cabecera
  private KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION";//cabecera, detalle, valores
  private KEY_HABILITACION_CAJA = "HABILITACION_CAJA";//cabecera
  private KEY_ESTADO = "ESTADO";//cabecera
  private KEY_ID_MONEDA = "ID_MONEDA";//cabecera, detalle
  private KEY_COTIZACION = "COTIZACION";//cabecera, detalle, valores
  private KEY_TOTAL_RECIBO = "TOTAL_RECIBO";//cabecera
  private KEY_INTERES = "INTERES";//cabecera
  private KEY_ANTICIPO = "ANTICIPO";//cabecera
  private KEY_LATITUD = "LATITUD";//cabecera
  private KEY_LONGITUD = "LONGITUD";//cabecera
  private KEY_ID_CLIENTE = "ID_CLIENTE";//cabecera
  private KEY_NRO_RECIBO = "NRO_RECIBO";//cabecera
  private KEY_IMEI = "IMEI";//cabecera
  private KEY_VERSION_APP = "VERSION";//cabecera
  private KEY_USUARIO = "USUARIO";//cabecera
  private KEY_ID_CONCEPTO = "ID_CONCEPTO";//cabecera
  private KEY_MARCA = "MARCA";//cabecera

  private KEY_FECHA_ANULACION = "FECHA_ANULACION";//cabecera

  private TABLE_RECIBOS_DETALLE = "CBV_RECIBOS_DET";
  private KEY_ID_RECIBO_CAB = "ID_RECIBO_CAB";//detalle, valores
  private KEY_ID_FACTURA = "ID_FACTURA";
  private KEY_MONTO = "MONTO";//detalle, valores
  private KEY_NRO_CUOTA = "NRO_CUOTA";//detalle
  private KEY_MONTO_VALORIZADO = "MONTO_VALORIZADO";//detalle
  private KEY_NO_FACTURA = "NO_FACTURA";//detalle

  private TABLE_RECIBOS_VALORES = "CBV_RECIBOS_VALORES";
  private KEY_ID_TIPO_VALOR = "ID_TIPO_VALOR";//valores
  private KEY_NUMERO_VALOR = "NUMERO_VALOR";//valores
  private KEY_ID_ENTIDAD = "ID_ENTIDAD";//valores
  private KEY_ID_CUENTA_BANCARIA = "ID_CUENTA_BANCARIA";//valores
  private KEY_FECHA_EMISION = "FECHA_EMISION";//valores
  private KEY_FECHA_VENCIMIENTO = "FECHA_VENCIMIENTO";//valores





  constructor(
    private bd: BDManagementService,
    private fileService: File,
    private platform: Platform,
    private monedaService: MonedaLogicaService,
    private writeLog:WriteFileService,
  ) { }

  createTableRecibos() {
    let CREATE_RECIBOS_CAB_TABLE = "CREATE TABLE IF NOT EXISTS CBV_RECIBOS_CAB (ID_RECIBO_CAB INTEGER PRIMARY KEY AUTOINCREMENT, "
      + "ID_COBRADOR INTEGER, FECHA TEXT, REQUIERE_SINCRONIZACION INTEGER, HABILITACION_CAJA INTEGER, "
      + "ESTADO TEXT, ID_MONEDA INTEGER, COTIZACION REAL(12,4), TOTAL_RECIBO REAL, INTERES REAL, "
      + "ANTICIPO REAL, LATITUD TEXT, LONGITUD TEXT, NRO_RECIBO INTEGER, ID_CLIENTE INTEGER, "
      + "FECHA_ANULACION TEXT, USUARIO TEXT, IMEI TEXT,VERSION TEXT,ULTIMO_TIME_IMPRESION_DUPLICADO TEXT, "
      + "ULTIMO_TIME_IMPRESION_ORIGINAL TEXT, ID_CONCEPTO INTEGER,MARCA TEXT, "
      + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA),"
      + "FOREIGN KEY (ID_CONCEPTO) REFERENCES CBV_CONCEPTOS (ID_CONCEPTO))";
    let CREATE_RECIBOS_DET_TABLE = "CREATE TABLE IF NOT EXISTS CBV_RECIBOS_DET (ID_RECIBO_DET INTEGER PRIMARY KEY AUTOINCREMENT, "
      + "ID_RECIBO_CAB INTEGER, ID_FACTURA INTEGER, REQUIERE_SINCRONIZACION INTEGER , "
      + "MONTO REAL(20,4), NRO_CUOTA INTEGER, COTIZACION REAL(12,4), MONTO_VALORIZADO REAL, "
      + "NO_FACTURA TEXT, ID_MONEDA INTEGER, "
      + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA), "
      + "FOREIGN KEY (ID_RECIBO_CAB) REFERENCES CBV_RECIBOS_CAB (ID_RECIBO_CAB))";
    let CREATE_RECIBOS_VALORES_TABLE = "CREATE TABLE IF NOT EXISTS CBV_RECIBOS_VALORES (ID_RECIBO_VALOR INTEGER PRIMARY KEY AUTOINCREMENT, "
      + "ID_RECIBO_CAB INTEGER, ID_TIPO_VALOR INTEGER, NUMERO_VALOR TEXT, "
      + "REQUIERE_SINCRONIZACION INTEGER, MONTO REAL(18,2), "
      + "COTIZACION REAL(12,4), MONTO_VALORIZADO REAL, ID_ENTIDAD INTEGER,  ID_CUENTA_BANCARIA TEXT, "
      + "FECHA_EMISION TEXT, FECHA_VENCIMIENTO TEXT, ID_MONEDA INTEGER, "
      + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA) "
      + "FOREIGN KEY (ID_CUENTA_BANCARIA) REFERENCES CBV_CUENTAS (ID_CUENTA), "
      + "FOREIGN KEY (ID_RECIBO_CAB) REFERENCES CBV_RECIBOS_CAB (ID_RECIBO_CAB))";

    return this.bd.getDataBase().executeSql(CREATE_RECIBOS_CAB_TABLE, []).then(recibos_cab => {
      console.log('se creo tabla recibos-cab', recibos_cab);
      return this.bd.getDataBase().executeSql(CREATE_RECIBOS_DET_TABLE, []).then(recibos_det => {
        console.log('se creo tabla recibos-det', recibos_det);
        return this.bd.getDataBase().executeSql(CREATE_RECIBOS_VALORES_TABLE, []).then(recibos_val => {
          console.log('se creo tabla recibos-det', recibos_val);
          return true;
        }).catch(err => {
          console.error('no se creo la tabla recibos-valores', err);
          return false;
        })
      }).catch(error => {
        console.error('no se creo la tabla recibos-valores', error);
        return false;
      })

    }).catch(error => {
      console.error('no se creo la tabla recibos-cab', error);
      return false;
    });
  }
  insertarReciboTabla(recibo: any) {
    let cabecera: any = recibo.cabecera;
    let detalle: Array<any> = recibo.detalles;
    let valores: Array<any> = recibo.valores;

    let paramsCabecera = [cabecera.ID_COBRADOR, cabecera.FECHA, 0, cabecera.HABILITACION_CAJA,
    cabecera.ESTADO, cabecera.ID_MONEDA, cabecera.COTIZACION, cabecera.TOTAL_RECIBO, cabecera.INTERES,
    cabecera.ANTICIPO, cabecera.LATITUD, cabecera.LONGITUD, cabecera.ID_CLIENTE, cabecera.NRO_RECIBO,
    cabecera.IMEI, cabecera.VERSION, cabecera.USUARIO, cabecera.ID_CONCEPTO, cabecera.MARCA];


    let sqlCabecera = "INSERT INTO " + this.TABLE_RECIBOS_CAB + " (ID_COBRADOR, FECHA, "
      + "REQUIERE_SINCRONIZACION, HABILITACION_CAJA, ESTADO, ID_MONEDA, COTIZACION, TOTAL_RECIBO, "
      + "INTERES, ANTICIPO, LATITUD, LONGITUD, ID_CLIENTE, NRO_RECIBO, IMEI, VERSION, USUARIO, ID_CONCEPTO, "
      + "MARCA) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    let sqlDetalle = "INSERT INTO " + this.TABLE_RECIBOS_DETALLE + " (ID_RECIBO_CAB, ID_FACTURA, "
      + "REQUIERE_SINCRONIZACION, MONTO, NRO_CUOTA, COTIZACION, MONTO_VALORIZADO, NO_FACTURA, ID_MONEDA) "
      + "VALUES(?,?,?,?,?,?,?,?,?)";

    let sqlValores = "INSERT INTO " + this.TABLE_RECIBOS_VALORES + "(ID_RECIBO_CAB, ID_TIPO_VALOR, "
      + "REQUIERE_SINCRONIZACION, NUMERO_VALOR, MONTO,COTIZACION, MONTO_VALORIZADO, ID_MONEDA, ID_ENTIDAD, "
      + "ID_CUENTA_BANCARIA, FECHA_EMISION, FECHA_VENCIMIENTO) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)"

    this.bd.getDataBase().executeSql(sqlCabecera, paramsCabecera).then(data => {
      let id_insert_value = data.insertId;
      console.log('se inserto registro en recibos-cab', data, paramsCabecera)
      for (let det of detalle) {
        let paramsDet = [id_insert_value, det.ID_FACTURA, 0, det.MONTO, det.NRO_CUOTA, det.COTIZACION,
          det.MONTO_VALORIZADO, det.NO_FACTURA, det.ID_MONEDA];
        this.bd.getDataBase().executeSql(sqlDetalle, paramsDet).then(data => {
          console.log('se inserto registro en recibos-detalle', data, paramsDet)
        }).catch(error => {
          console.error('no se puede insertar recibos-detalle', error)
        })
      }

      for (let val of valores) {
        let paramsVal = [id_insert_value, val.ID_TIPO_VALOR, 0, val.NUMERO_VALOR, val.MONTO, val.COTIZACION,
          val.MONTO_VALORIZADO, val.ID_MONEDA, val.ID_ENTIDAD, val.ID_CUENTA_BANCARIA, val.FECHA_EMISION,
          val.FECHA_VENCIMIENTO]
        this.bd.getDataBase().executeSql(sqlValores, paramsVal).then(data => {
          console.log('se inserto registro en recibos-valores', data, paramsVal)
        }).catch(error => {
          console.log('no se puede insertar en recibos-valores', error)
        })
      }

    }).catch(error => {
      console.error('no se puede insertar recibos-cab', error)
    })

  }
  async existeRecibo(recibo: any) {
    let sql = "SELECT rc.NRO_RECIBO FROM " + this.TABLE_RECIBOS_CAB
      + " rc WHERE rc.NRO_RECIBO=? AND MARCA=?";
    let params = [recibo.cabecera.NRO_RECIBO, recibo.cabecera.MARCA]
    return this.bd.getDataBase().executeSql(sql, params).then(data => {
      if (data.rows.length > 0) {
        return true;
      } else {
        return false;
      }
    })

  }
  existeSyncRecibosHabilitacion(habilitacion: number): Promise<any> {
    let query = "SELECT * FROM " + this.TABLE_RECIBOS_CAB + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
      "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
    return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(async data => {
      if (data.rows.length > 0) {
        await this.writeLog.escribirLog(new Date()+ " Numero de Recibos que faltan ser sincronizados: "+data.rows.length +" uno de ellos "+JSON.stringify(data.rows.item(0)));
        return true;
      } else {
        return false;
      }
    }).catch(err=>{
      return -1;
    })
  }

  getRecibosASincronizar(habilitacion: number):Promise<any> {
    let query = "SELECT * FROM " + this.TABLE_RECIBOS_CAB + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
      "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
    return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(async data => {
      console.log("Cantidad de recibos a sincronizar ", data.rows.length);
      let resp = [];
      let facturas = {};
      let valores = {};
      //let archivos = {};
      let reciboResp = {};
      let recibo;

      for (let i = 0; i < data.rows.length; i++) {

        recibo = data.rows.item(i);
        reciboResp = recibo;

        console.log("RECIBO ", recibo);

        facturas = await this.getDetalles(recibo.ID_RECIBO_CAB);
        valores = await this.getValores(recibo.ID_RECIBO_CAB);
        //archivos = await this.getArchivos(recibo.ULTIMO_TIME_IMPRESION_ORIGINAL, recibo.ULTIMO_TIME_IMPRESION_DUPLICADO, recibo.NRO_RECIBO);

        reciboResp["facturas"] = facturas;
        reciboResp["valores"] = valores;
        //reciboResp["files"] = archivos;
        reciboResp["totalFacturas"] = await this.getTotalFacturas(recibo.ID_RECIBO_CAB);
        reciboResp["totalValores"] = await this.getTotalValores(recibo.ID_RECIBO_CAB);

        if (reciboResp["totalFacturas"] == -1 || reciboResp["totalValores"] == -1 ||
          reciboResp["facturas"].length == 0 || reciboResp["valores"].length == 0) {
          return Promise.reject("Error al traer datos de base de datos para sincronizar recibos");
        }
        resp.push(reciboResp);
      }

      console.log("RECIBOS A ENVIAR A API para sincronizar", resp);

      return resp;
    }).catch(error => {
      console.error("Error trayendo recibos a sincronizar", error);
      return error;
    })
  }

  cantidadRecibosSync(habilitacion: number) {
    let query = "SELECT COUNT(ID_RECIBO_CAB) AS CANTIDAD FROM " + this.TABLE_RECIBOS_CAB + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION
      + "=? AND " + this.KEY_HABILITACION_CAJA + "= ? ";
    console.log("Trayendo cantidad de recibos", this.bd);
    return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(data => {
      console.log("CANT RECIBOS de habilitacion " + habilitacion, data.rows.item(0).CANTIDAD);
      return data.rows.item(0).CANTIDAD;
    }).catch(error => {
      console.error("Error trayendo cantidad de recibos", error);
      return -1;
    });
  }

  getDetalles(id_recibo_cab) {
    let query = "SELECT * FROM " + this.TABLE_RECIBOS_DETALLE + " WHERE ID_RECIBO_CAB=?";
    return this.bd.getDataBase().executeSql(query, [id_recibo_cab]).then(data => {
      let detalles = [];
      for (let i = 0; i < data.rows.length; i++) {
        detalles.push(data.rows.item(i));
      }
      return detalles;
    }).catch(error => {
      console.error("Error trayendo detalles de recibo", error);
      return [];
    });
  }

  getValores(id_recibo_cab) {
    let query = "SELECT * FROM " + this.TABLE_RECIBOS_VALORES + " WHERE ID_RECIBO_CAB=?";
    return this.bd.getDataBase().executeSql(query, [id_recibo_cab]).then(data => {
      let valores = [];
      for (let i = 0; i < data.rows.length; i++) {
        valores.push(data.rows.item(i));
      }
      return valores;
    }).catch(error => {
      console.error("Error trayendo valores de recibo", error);
      return [];
    });
  }

  getArchivos(original, duplicado, nro_recibo) {
    let ubicacion = this.fileService.externalApplicationStorageDirectory + "/recibos";

    //Si es al contado
    if (nro_recibo == 0 || original == null) {
      console.log("NO SE BUSCA NINGUN ARCHIVO, ES DE FACTURA AL CONTADO O NO TIENE ARCHIVOS LOCALES");
      return [
        { "ULTIMO_TIME_IMPRESION_ORIGINAL": "" },
        { "ULTIMO_TIME_IMPRESION": "" },
      ]
    } else {
      original = original + ".txt";
      //Si son a credito
      console.log("ARCHIVOS A BUSCAR", original, " y ", duplicado);
      return this.fileService.readAsText(ubicacion, original)
        .then((original_content) => {
          console.log("ORIGINAL TXT: ", original_content);
          if (duplicado != null) {
            duplicado = duplicado + ".txt";
            return this.fileService.readAsText(ubicacion, duplicado)
              .then((duplicado_content) => {
                console.log("DUPLICADO TXT: ", duplicado_content);
                return [
                  { "ULTIMO_TIME_IMPRESION_ORIGINAL": original_content },
                  { "ULTIMO_TIME_IMPRESION": duplicado_content },
                ]
              })
          } else {
            return [
              { "ULTIMO_TIME_IMPRESION_ORIGINAL": original_content },
              { "ULTIMO_TIME_IMPRESION": "" },
            ]
          }

        }).catch(error => {
          console.log("ERROR TRAYENDO ARCHIVO", error);
          return [];
        })
    }

  }
  recibosGenerados(id_factura: number): Promise<number> {
    let estado = "PENDIENTE";
    let query = "SELECT SUM (rd.MONTO) AS MONTO FROM CBV_RECIBOS_DET rd "
      + "INNER JOIN CBV_RECIBOS_CAB rc ON rc.ID_RECIBO_CAB = rd.ID_RECIBO_CAB "
      + "WHERE rd.ID_FACTURA=? AND rc.ESTADO=?";
    return this.bd.getDataBase().executeSql(query, [id_factura, estado]).then(data => {
      if (data.rows.length > 0) {
        let totalPagado = <number>(data.rows.item(0).MONTO);
        console.log('total pagado', totalPagado)
        return totalPagado;
      } else {
        return 0.0;
      }
    }).catch(error => {
      console.error("Error al traer recibos generados", error);
      return null;
    })
  }

  setRecibo(request): Promise<any> {
    //-------------------- COMIENZO DE INSERCION -----------------------------------
    if (!request.valid) {
      return Promise.reject({
        "error": request.message,
        "error_expanded": request
      });
    }
    //Nuevo recibo
    let data = {

      'id_cobrador': request.cobrador,
      'fecha': request.fecha,
      'estado': "PENDIENTE",
      'id_moneda': request.id_moneda,
      'cotizacion': request.cotizacion,
      'total_recibo': request.total_recibo,
      'interes': request.interes,
      'latitud': request.latitud,
      'longitud': request.longitud,
      'nro_recibo': request.nro_recibo,
      'habilitacion_caja': request.habilitacion,
      'id_cliente': request.id_cliente,
      'version': request.version,
      'marca': request.marca,
      'id_concepto': request.id_concepto,
      'imei': request.imei,
      'usuario': request.usuario,
      'requiere_sincronizacion': 1,
      'anticipo': request.anticipo
    };

    let self = this;

    return this.bd.insert(this.TABLE_RECIBOS_CAB, data).then(async resp => {
      console.log("Cabecera de recibo creado exitosamente con id ", resp.insertId, resp);
      let detallesInserts = await self.insertarDetalles(resp.insertId, request.facturas, request.moneda, request.cotizacion, self);
      let valoresInserts = await self.insertarValores(resp.insertId, request.valores, request.moneda, request.cotizacion, self);
      console.log("DETALLES A INSERTAR: ", detallesInserts);
      console.log("VALORES A INSERTAR: ", valoresInserts);

      //Transaccion para añadir detalles y valores a de recibo
      if (detallesInserts.length == 0 || valoresInserts.length == 0) {
        self.delete(resp.insertId);
        return Promise.reject({
          "error": "Error creando recibo, los detalles a insertar o valores estan vacios",
          "error_expanded": ""
        })
      }

      return this.bd.getDataBase().transaction((tx) => {
        //Insercion de detalles de recibo
        for (let detalle of detallesInserts) {
          tx.executeSql(detalle.query, detalle.params, self.thenTransaction, self.catchTransaction);
        }

        //Insercion de valores de recibo
        for (let valor of valoresInserts) {
          tx.executeSql(valor.query, valor.params, self.thenTransaction, self.catchTransaction);
        }

      }).then(() => {
        console.log("Exito en transaccion");
        return Promise.resolve({
          "exito": "Recibo creado con exito",
          "ID_RECIBO": resp.insertId,
          "RECIBO_ORIGINAL": "",
          "RECIBO_DUPLICADO": ""
        })
      }).catch(error => {
        console.error("Error en transaccion al insertar detalles y valores", error);
        self.delete(resp.insertId);
        return Promise.reject({
          "error": "Error creando recibo",
          "error_expanded": error
        })
      });

    }).catch(error => {
      console.error("Error en insercion de recibo con datos", data);
      return Promise.reject({
        "error": "Error creando recibo",
        "error_expanded": error
      })
    });

  }

  async insertarDetalles(id_recibo, facturas, moneda, cotizacion, self) {
    let detallesToInsert = [];
    let detalle;
    for (let factura of facturas) {
      detalle = await self.insertarDetalle(id_recibo, factura, moneda, cotizacion, self);
      if (detalle != null) {
        detallesToInsert = detallesToInsert.concat(detalle);
      } else {
        return [];
      }
    }
    return detallesToInsert;
  }

  async insertarDetalle(id_recibo, factura, moneda, cotizacion, self) {
    let detalles = [];
    let id_factura = factura['id_factura'];
    let a_cobrar = factura['monto'];
    let monedaFactura = factura['id_moneda'];
    let saldo = factura['saldo'];
    let montoVal = a_cobrar;
    let cuota = factura['cuota'];

    let montoPagado = 0;
    let montoPagadoVal = 0;


    //Obtener montoVal
    if (monedaFactura != moneda.ID_MONEDA) {
      montoVal = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, a_cobrar, moneda.CANT_DECIMALES);
    }

    let detalle = {};
    let datos = {};
    let toInsert = { query: '', params: [] };
    //Insertar detalle segun condicion
    if (a_cobrar <= saldo) {
      let facturaBD = await self.findFactura(id_factura);

      //Se inserta solo un detalle
      detalle = {
        'id_recibo_cab': id_recibo,
        'id_factura': facturaBD.ID_FACTURA,
        'monto': a_cobrar,
        'nro_cuota': cuota,
        'monto_valorizado': montoVal,
        'cotizacion': cotizacion,
        'id_moneda': monedaFactura,
        'no_factura': facturaBD.NO_FACTURA,
        'requiere_sincronizacion': 1,
      }
      toInsert = self.bd.generateQueryToInsert(self.TABLE_RECIBOS_DETALLE, detalle);
      console.log("TO INSERT DETALLE ", toInsert);
      detalles.push(toInsert);
      //transaction.executeSql(toInsert.query, toInsert.params);

    } else {

      //Se insertan mas de un detalle de recibo debido ya que el monto pagado es mayor que el saldo de la factura
      montoPagado = a_cobrar;
      montoPagadoVal = a_cobrar;

      let facturas = await self.getFacturasFrom(id_factura, { cuota: 'ASC' });

      if (facturas == null) {
        return null;
      }

      for (let f of facturas) {

        datos = {
          'id_recibo_cab': id_recibo,
          'id_factura': f.ID_FACTURA,
          'nro_cuota': f.CUOTA,
          'id_moneda': f.ID_MONEDA,
          'no_factura': f.NO_FACTURA,
          'requiere_sincronizacion': 1,
          'cotizacion': cotizacion
        };

        if (montoPagado > f.SALDO) {
          //Si el monto pagado aun es mayor al saldo
          montoPagado = montoPagado - f.SALDO;
          console.log("1- f.ID_MONEDA != moneda.ID_MONEDA ", f.ID_MONEDA != moneda.ID_MONEDA, f.ID_MONEDA, moneda.ID_MONEDA);
          if (f.ID_MONEDA != moneda.ID_MONEDA) {
            montoPagadoVal = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, f.SALDO, moneda.CANT_DECIMALES);
          }

          datos['monto'] = f.SALDO;
          datos['monto_valorizado'] = montoPagadoVal;

          montoPagadoVal = montoPagado;

        } else {
          //Si el montopagado es igual o menor al saldo
          console.log("2- f.ID_MONEDA != moneda.ID_MONEDA ", f.ID_MONEDA != moneda.ID_MONEDA, f.ID_MONEDA, moneda.ID_MONEDA);
          if (f.ID_MONEDA != moneda.ID_MONEDA) {
            montoPagadoVal = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, montoPagado, moneda.CANT_DECIMALES);
          }

          datos['monto'] = montoPagado;
          datos['monto_valorizado'] = montoPagadoVal;

          montoPagado = 0;

        }

        //Se inserta el detalle
        toInsert = self.bd.generateQueryToInsert(self.TABLE_RECIBOS_DETALLE, datos);
        console.log("TO INSERT DETALLE ", toInsert);
        detalles.push(toInsert);

        //transaction.executeSql(toInsert.query, toInsert.params);

        //Su el montoPagado ya llego a cero, se sale del bucle
        if (montoPagado == 0) {
          break;
        }
      }
    }
    return detalles;
  }

  async insertarValores(id_recibo, valores, moneda, cotizacion, self) {
    let valoresToInsert = [];
    for (let valor of valores) {
      valoresToInsert.push(await self.insertarValor(id_recibo, valor, moneda, cotizacion, self));
    }
    return valoresToInsert;
  }

  async insertarValor(id_recibo, valor, moneda, cotizacion, self) {
    console.log("VALOR PA GUARDAR", valor);
    let numero = valor.hasOwnProperty('numero_valor') ? valor['numero_valor'] : null;

    let entidad = valor.hasOwnProperty('id_entidad') ? valor['id_entidad'] : null;

    let cuenta_bancaria = valor.hasOwnProperty('id_cuenta_bancaria') ? valor['id_cuenta_bancaria'] : null;

    let monto = valor['monto'];

    let id_moneda = valor['id_moneda'];

    let id_tipo_valor = valor['id_tipo_valor']

    let fecha_emision, fecha_vencimiento, montoValorizado;

    if (valor['fecha_emision'] != "" && valor['fecha_emision'] != null) {
      fecha_emision = moment(valor['fecha_emision'], "YYYY-MM-DD").format("DD-MM-YYYY");
      //30 dias mas a partir de la fecha de emision, por eso se le suma 29 y no 30
      fecha_vencimiento = moment(fecha_emision, "DD-MM-YYYY").add(29, 'days').format("DD-MM-YYYY");
    } else {
      fecha_emision = null;
      fecha_vencimiento = null;
    }


    if (monto != null && id_tipo_valor != null && id_moneda != null) {
      if (id_moneda == moneda.ID_MONEDA) {
        montoValorizado = monto;
      } else {
        montoValorizado = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, monto, moneda.CANT_DECIMALES);
      }
    }

    console.log("EMISION " + fecha_emision, "VENCIMIENTO " + fecha_vencimiento);

    let datos = {
      "numero_valor": numero,
      "monto": monto,
      "id_tipo_valor": id_tipo_valor,
      "id_recibo_cab": id_recibo,
      "id_moneda": id_moneda,
      "id_entidad": entidad,
      "fecha_emision": fecha_emision,
      "fecha_vencimiento": fecha_vencimiento,
      "id_cuenta_bancaria": cuenta_bancaria,
      "monto_valorizado": montoValorizado,
      "cotizacion": cotizacion
    };

    //Insertar valor (elementos para hacerlo)
    return self.bd.generateQueryToInsert(self.TABLE_RECIBOS_VALORES, datos);

  }

  existeReciboConMarca(id_cobrador, marca) {
    let query = "SELECT * FROM " + this.TABLE_RECIBOS_CAB + " WHERE ID_COBRADOR=? AND MARCA=? LIMIT 1";
    return this.bd.getDataBase().executeSql(query, [id_cobrador, marca]).then(data => {
      console.log('Cantidad de recibos con la misma marca del cobrador: ', data.rows.length);
      if (data.rows.length > 0) {
        return true;
      }
      return false;
    }).catch(error => {
      console.error('No se pudo traer la cantidad de recibos con la misma marca', error);
      return false;
    });
  }

  getUltimoReciboCobrador(id_cobrador) {
    let query = "SELECT MAX(NRO_RECIBO) AS ULTIMO_RECIBO FROM " + this.TABLE_RECIBOS_CAB + " WHERE ID_COBRADOR=?";
    let params = [id_cobrador];
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log('Ultimo recibo de la base de datos local', data.rows.item(0).ULTIMO_RECIBO);
      return data.rows.item(0).ULTIMO_RECIBO;
    }).catch(error => {
      console.log('No se pudo obtener el ultimo recibo de la base de datos local', error);
      return -1;
    })
  }

  findFactura(id) {
    return this.bd.find("CBV_FACTURAS", id, "ID_FACTURA").then(data => {
      console.log("Factura traida :", data);
      return data
    }).catch(error => {
      console.log("Error trayendo factura", error);
      return null;
    });
  }

  getFacturasFrom(id_factura, orderBy = null) {
    let query = "SELECT * FROM CBV_FACTURAS WHERE ID_FACTURA=?";
    if (orderBy != null) {
      query = query + " ORDER BY ";
      Object.keys(orderBy).forEach(function (key) {
        query = query + key + " " + orderBy[key];
      });
    }
    return this.bd.getDataBase().executeSql(query, [id_factura]).then(data => {
      console.log("TRAER FACTURAS CON ID_FACTURA = " + id_factura + ", cantidad: " + data.rows.length);
      let facturas: Array<any> = [];
      for (let index = 0; index < data.rows.length; index++) {
        //console.log('FACTURA DE BD: ', data.rows.item(index));
        facturas.push(data.rows.item(index));
      }
      return facturas;
    }).catch(error => {
      console.error("Error trayendo FACTURAS " + id_factura, error);
      return null;
    })
  }

  thenTransaction(tx, result) {
    console.log("RESULTADO DE QUERY EN TRANSACCION: ", result);
  }
  catchTransaction(tx, err) {
    console.error("ERROR EN TRANSACCION EN QUERY: ", err);
    return true;//THIS IS IMPORTANT FOR TRANSACTION TO ROLLBACK ON QUERY ERROR
  }

  obtenerReciboCap(id_recibo: number) {
    let query_recibos = "SELECT ID_CONCEPTO, ID_COBRADOR, FECHA, ID_MONEDA, COTIZACION, TOTAL_RECIBO, "
      + "INTERES, ANTICIPO, NRO_RECIBO, HABILITACION_CAJA, ID_CLIENTE "
      + "FROM CBV_RECIBOS_CAB WHERE ID_RECIBO_CAB=?";
    let params_recibos = [id_recibo];
    return this.bd.getDataBase().executeSql(query_recibos, params_recibos).then(data => {
      console.log('recibo en get obtenerReciboCap', data);
      if (data.rows.length > 0) {
        return data.rows.item(0);
      } else {
        return null;
      }
    }).catch(error => {
      console.log('No se pudo obtener el ultimo recibo de la base de datos local', error);
      return -1;
    })
  }
  getValoresRecibo(id_recibo: number, id_moneda_recibo: number, cotizacion): Promise<Array<any>> {
    let valores: Array<any> = [];
    let params_valores = [id_recibo]

    let query_valores = "SELECT CASE WHEN V.ID_VALOR=12 THEN 1 ELSE 2 END PRIORIDAD, "
      + "RV.MONTO_VALORIZADO, RV.MONTO, V.DESCRIPCION, RV.NUMERO_VALOR, RV.ID_RECIBO_CAB, V.ID_VALOR, "
      + "V.IND_BANCO, V.IND_CUENTA, V.IND_FECHA, V.IND_NUMERO, RV.ID_MONEDA, RV.ID_ENTIDAD, "
      + "RV.ID_CUENTA_BANCARIA "
      + "FROM CBV_VALORES V JOIN CBV_RECIBOS_VALORES RV ON RV.ID_TIPO_VALOR=V.ID_VALOR "
      + "WHERE RV.ID_RECIBO_CAB=? ORDER BY 1, 2 DESC";
    return this.bd.getDataBase().executeSql(query_valores, params_valores).then(data => {
      //console.log('valores recibos', data.rows.length, data);
      if (data.rows.length > 0) {
        for (let index = 0; index < data.rows.length; index++) {
          valores.push(data.rows.item(index));
        }
        console.log('valores recibos', valores)
        return valores;
      } else {
        return null;
      }
    }).catch(error => {
      console.error('No se pudo obtener los valores de la base de datos local', error);
      return null;
    })
  }
  getDetallesRecibo(id_recibo): Promise<Array<any>> {
    let detalles: Array<any> = []
    let query_detalles = "SELECT ID_FACTURA, MONTO, ID_MONEDA, NRO_CUOTA FROM CBV_RECIBOS_DET "
      + "WHERE ID_RECIBO_CAB=?";
    let params_detalle = [id_recibo];
    return this.bd.getDataBase().executeSql(query_detalles, params_detalle).then(data => {
      console.log('detalles recibos', data.rows.length, data);
      if (data.rows.length > 0) {
        for (let index = 0; index < data.rows.length; index++) {
          detalles.push(data.rows.item(index));
        }
        console.log('detalles recibo', detalles)
        return detalles;
      } else {
        return null;
      }
    }).catch(error => {
      console.error('No se pudo obtener los detalles de la base de datos local', error);
      return null;
    })

  }

  delete(id) {
    return this.bd.delete(this.TABLE_RECIBOS_CAB, id, "ID_RECIBO_CAB");
  }

  obtenerRecibosFactura(id_factura) {
    let query = "SELECT rc.NRO_RECIBO,rc.FECHA as FEC_RECIBO,rc.ID_MONEDA,rc.TOTAL_RECIBO as TOT_RECIBO, rc.ESTADO, rc.ID_RECIBO_CAB, rc.REQUIERE_SINCRONIZACION"
      + " FROM " + this.TABLE_RECIBOS_CAB + " rc JOIN " + this.TABLE_RECIBOS_DETALLE + " rd on rd.ID_RECIBO_CAB=rc.ID_RECIBO_CAB"
      + " WHERE rd.ID_FACTURA=? GROUP BY rc.NRO_RECIBO,rc.FECHA,rc.ID_MONEDA,rc.TOTAL_RECIBO, rc.ESTADO, rc.ID_RECIBO_CAB, rc.REQUIERE_SINCRONIZACION";
    let params = [id_factura];

    return this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log('Recibos de factura ' + id_factura, data.rows.length);
      let recibos = [];
      for (let index = 0; index < data.rows.length; index++) {
        recibos.push(data.rows.item(index));
      }
      console.log('retorno recibo de facturas', recibos);
      return recibos;
    }).catch(error => {
      console.log('No se pudo traer los recibos de la factura', error);
      return [];
    })
  }

  anular(id_recibo_cab) {
    let query = "UPDATE CBV_RECIBOS_CAB SET ESTADO=?, REQUIERE_SINCRONIZACION=?, FECHA_ANULACION=?" +
      " WHERE ID_RECIBO_CAB=?";
    let params = ['ANULADO', 1, moment().format('DD-MM-YYYY'), id_recibo_cab];
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      return true;
    }).catch(error => {
      console.log("Error eliminando recibo", error);
      return false;
    })
  }

  getTotalValores(id_recibo_cab) {
    let query = "SELECT SUM(MONTO_VALORIZADO) AS TOTAL_VALORES FROM " + this.TABLE_RECIBOS_VALORES + " WHERE ID_RECIBO_CAB=?";
    let params = [id_recibo_cab];
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      return data.rows.item(0).TOTAL_VALORES;
    }).catch(error => {
      console.log("Error trayendo total de valores", error);
      return -1;
    })
  }

  getTotalFacturas(id_recibo_cab) {
    let query = "SELECT SUM(MONTO_VALORIZADO) AS TOTAL_FACTURAS FROM " + this.TABLE_RECIBOS_DETALLE + " WHERE ID_RECIBO_CAB=?";
    let params = [id_recibo_cab];
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      return data.rows.item(0).TOTAL_FACTURAS;
    }).catch(error => {
      console.log("Error trayendo total de facturas", error);
      return -1;
    })
  }

  guardarReciboDuplicado(id_recibo, nombre_archivo) {
    let strSQL = "UPDATE " + this.TABLE_RECIBOS_CAB + " SET ULTIMO_TIME_IMPRESION_DUPLICADO=?, "
      + "REQUIERE_SINCRONIZACION=? WHERE ID_RECIBO_CAB=? ";
    let params = [nombre_archivo, 1, id_recibo];
    this.bd.getDataBase().executeSql(strSQL, params).then(data => {
      console.log("se actualizo el archivo duplicado en la bd", data)
    }).catch(error => {
      console.log('no se pudo actualizar el archivo duplicado en la bd', error)
    });

  }
  guardarReciboOriginal(id_recibo, nombre_archivo) {
    let strSQL = "UPDATE " + this.TABLE_RECIBOS_CAB + " SET ULTIMO_TIME_IMPRESION_ORIGINAL=?, "
      + "REQUIERE_SINCRONIZACION=? WHERE ID_RECIBO_CAB=?";
    let params = [nombre_archivo, 1, id_recibo];
    this.bd.getDataBase().executeSql(strSQL, params).then(data => {
      console.log("se actualizo el archivo original en la bd", data)
    }).catch(error => {
      console.log('no se pudo actualizar el archivo original en la bd', error)
    });

  }

  actualizarSincronizacionDeRecibos(ids) {
    let query = "UPDATE " + this.TABLE_RECIBOS_CAB + " SET " + this.KEY_REQUIERE_SINCRONIZACION + "=? WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=?";
    let n = ids.length;
    let params = [0, 1];
    if (n > 0) {
      query = query + " AND(";
      for (let i = 0; i < n; i++) {
        params.push(ids[i]);
        query = query + "ID_RECIBO_CAB=?";
        if (i < n - 1) {
          query = query + " OR ";
        } else {
          query = query + ")";
        }
      }
    }

    console.log("QUERY UPDATE RECIBOS ", query);

    this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log("Columna de recibos que requieren sincronizacion cambiados a 0", data)
    }).catch(error => {
      console.log('No se pudo actualizar la columna de requiere sincronizacion a 0 de los recibos', error)
    });
  }

  deleteTableRecibos() {

    return this.bd.getDataBase().executeSql("DROP TABLE IF EXISTS " + this.TABLE_RECIBOS_CAB, []).then(data => {
      console.log('se elimino la tabla recibos-cabecera', data);
      return this.bd.getDataBase().executeSql("DROP TABLE IF EXISTS " + this.TABLE_RECIBOS_DETALLE, []).then(data => {
        console.log('se elimino la tabla recibos-detalle', data);
        return this.bd.getDataBase().executeSql("DROP TABLE IF EXISTS " + this.TABLE_RECIBOS_VALORES, []).then(data => {
          console.log('se elimino la tabla recibos-valores', data);
          return true;
        }).catch(error => {
          console.log('no se elimino la tabla recibos-valores', error);
          return false;
        })
      }).catch(error => {
        console.log('no se elimino la tabla recibos-detalle', error);
        return false;
      })
    }).catch(error => {
      console.log('no se elimino la tabla recibos-cabecera', error);
      return false;
    })

  }

  borrarReciboEnCascada(id_recibo_cab) {
    if (id_recibo_cab == 0) {
      return {
        "exito": "No existe recibo con id 0"
      }
    }
    console.log("ELIMINAR RECIBO CON ID " + id_recibo_cab);
    let self = this;
    return this.bd.getDataBase().transaction((tx) => {

      let params = [id_recibo_cab];
      //Eliminación de cabecera
      let query = "DELETE FROM " + this.TABLE_RECIBOS_CAB + " WHERE " + this.KEY_ID_RECIBO_CAB + "=?";
      tx.executeSql(query, params, self.thenTransaction, self.catchTransaction);

      //Eliminación de detalles
      query = "DELETE FROM " + this.TABLE_RECIBOS_DETALLE + " WHERE " + this.KEY_ID_RECIBO_CAB + "=?";
      tx.executeSql(query, params, self.thenTransaction, self.catchTransaction);

      //Eliminación de valores
      query = "DELETE FROM " + this.TABLE_RECIBOS_VALORES + " WHERE " + this.KEY_ID_RECIBO_CAB + "=?";
      tx.executeSql(query, params, self.thenTransaction, self.catchTransaction);

    }).then(() => {
      console.log("Exito en transaccion de recibo");
      return {
        "exito": "Recibo eliminado exitosamente",
        "ID_RECIBO": id_recibo_cab
      }
    }).catch(error => {
      console.error("Error en transaccion al eliminar detalles y valores", error);
      throw ({
        "error": "Error eliminado recibo",
        "error_expanded": error
      })
    });
  }
}
