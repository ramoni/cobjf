import { TestBed } from '@angular/core/testing';

import { RecibosBDService } from './recibos-bd.service';

describe('RecibosBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecibosBDService = TestBed.get(RecibosBDService);
    expect(service).toBeTruthy();
  });
});
