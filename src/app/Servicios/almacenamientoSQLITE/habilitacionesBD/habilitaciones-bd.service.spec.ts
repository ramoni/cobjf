import { TestBed } from '@angular/core/testing';

import { HabilitacionesBDService } from './habilitaciones-bd.service';

describe('HabilitacionesBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HabilitacionesBDService = TestBed.get(HabilitacionesBDService);
    expect(service).toBeTruthy();
  });
});
