import { Injectable } from '@angular/core';
import moment from 'moment';
import { BDManagementService } from '../bdManagement/bdmanagement.service';

@Injectable({
  providedIn: 'root'
})
export class HabilitacionesBDService {
  private TABLE_HABILITACIONES = "CBV_HABILITACIONES";
  private KEY_ID = "ID";
  private KEY_HABILITACION_CAJA = "HABILITACION_CAJA";
  private KEY_USUARIO = "USUARIO";
  private KEY_FECHA_APERTURA = "FECHA_APERTURA";
  private KEY_FECHA_CIERRE = "FECHA_CIERRE";
  private KEY_CERRADO = "CERRADO";
  private KEY_VALIDO = "VALIDO";
  
  constructor(private db:BDManagementService) { }

  createTableHabilitaciones(): Promise<boolean> {
    let CREATE_HABILITACIONES_TABLE= "CREATE TABLE IF NOT EXISTS "+this.TABLE_HABILITACIONES+
    " (ID INTEGER PRIMARY KEY AUTOINCREMENT, HABILITACION_CAJA INTEGER, USUARIO TEXT, "+
    "FECHA_APERTURA TEXT, FECHA_CIERRE TEXT, CERRADO INTEGER, VALIDO INTEGER)";

    return this.db.getDataBase().executeSql(CREATE_HABILITACIONES_TABLE, []).then(habilitacion => {
      console.log('se creo la table habilitacion', habilitacion);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla habilitacion', error);
      return false;
    });
  }

  cerrarHabilitacionLocal(habilitacion:number) {

    console.log("Cierre de habilitacion"+ habilitacion);

    let today = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    let cerrado = 1;
    let strSQL = "UPDATE "+ this.TABLE_HABILITACIONES +" SET CERRADO ="+cerrado+","+ this.KEY_FECHA_CIERRE+"="+"'"+today+"'"+","+this.KEY_VALIDO+"=0"+" WHERE HABILITACION_CAJA= "+ habilitacion;
    
    return this.db.getDataBase().executeSql(strSQL, []).then(resp => {
      console.log("Habilitacion "+habilitacion+" cerrada");
      return true;
    }).catch(error => {
      console.error("Habilitacion "+habilitacion+" no pude ser cerrada", error);
      return false;
    });

  }
  deleteTableHabilitacion() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_HABILITACIONES;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla habilitacion', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla habilitacion', error);
      return false;
    })
  }
  
}
