import { TestBed } from '@angular/core/testing';

import { CuentasBDService } from './cuentas-bd.service';

describe('CuentasBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CuentasBDService = TestBed.get(CuentasBDService);
    expect(service).toBeTruthy();
  });
});
