import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Cuenta } from 'src/app/Clases/cuenta';

@Injectable({
  providedIn: 'root'
})
export class CuentasBDService {
  private TABLE_CUENTAS = "CBV_CUENTAS";
  private KEY_ID = "ID";
  private KEY_ID_CUENTA = "ID_CUENTA";
  private KEY_DESCRIPCION = "DESCRIPCION";
  private KEY_ID_ENTIDAD = "ID_ENTIDAD";
  private KEY_ID_MONEDA = "ID_MONEDA";
  constructor(private db: BDManagementService) { }
  createTableCuentas(): Promise<boolean> {
    let CREATE_CUENTAS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_CUENTAS +
      " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_CUENTA TEXT, DESCRIPCION TEXT, " +
      "ID_ENTIDAD TEXT, ID_MONEDA TEXT)";

    return this.db.getDataBase().executeSql(CREATE_CUENTAS_TABLE, []).then(cuenta => {
      console.log('se creo la table cuenta', cuenta);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla cuenta', error);
      return false;
    });
  }

  addCuenta(cuenta: Cuenta) {
    let addLine = "INSERT INTO " + this.TABLE_CUENTAS + " (ID_CUENTA, DESCRIPCION,ID_ENTIDAD,ID_MONEDA)"+
    "VALUES (?,?,?,?)";
    return this.db.getDataBase().executeSql(addLine,
      [cuenta.id, cuenta.descripcion, cuenta.id_entidad, cuenta.id_moneda])
      .then(exito => {
        console.log('se agrego la Cuenta', exito)
        return true;
      }).catch(error => {
        console.log('no se puede agregar la Cuenta', error)
        return false;
      })
  }
  getCuentas(): Promise<Array<Cuenta>>{
    let query = "SELECT * FROM " + this.TABLE_CUENTAS;
    let cuentas: Array<Cuenta> = [];
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log("DATOS DE CUENTAS TRAIDA ", data.rows.length, data.rows.item(0), data)
      if (data.rows.length > 0) {
        for (let index=0; index < data.rows.length; index++) {
          cuentas.push(new Cuenta(data.rows.item(index)));
        }
      }
      return cuentas;
    }).catch(err => {
      console.log('no pudo obtenerse las cuentas localmente',err);
      return err;
    });
  }
  getCuenta(id_cuenta):Promise<Cuenta> {
    let query = "SELECT * FROM CBV_CUENTAS WHERE ID_CUENTA=?";
    return this.db.getDataBase().executeSql(query,[id_cuenta]).then(data=>{
      if(data.rows.length>0){
        return new Cuenta(data.rows.item(0));
      }else{
        return null;
      }
    }).catch(error=>{
      console.log("error al traer la cuenta por el id",error)
      return null;
    })
  }
  deleteAllCuentas() {
    let query = "DELETE FROM " + this.TABLE_CUENTAS;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla cuentas', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla cuentas',error);
      return false;
    })
  }
  deleteTableCuenta() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_CUENTAS;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla cuentas', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla cuentas', error);
      return false;
    })
  }
}
