import { TestBed } from '@angular/core/testing';

import { TablesManageService } from './tables-manage.service';

describe('TablesManageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TablesManageService = TestBed.get(TablesManageService);
    expect(service).toBeTruthy();
  });
});
