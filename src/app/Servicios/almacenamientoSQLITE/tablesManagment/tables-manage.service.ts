import { Injectable } from '@angular/core';
import { ClientesBDService } from '../clientesBD/clientes-bd.service';
import { EntregasDocBDService } from '../entregasDocBD/entregas-doc-bd.service';
import { UsuariosBDService } from '../usuariosBD/usuarios-bd.service';
import { FacturasBDService } from '../facturasBD/facturas-bd.service';
import { NotasBDService } from '../notasBD/notas-bd.service';
import { RecibosBDService } from '../recibosBD/recibos-bd.service';
import { ConceptosBDService } from '../conceptosBD/conceptos-bd.service';
import { EntidadesBDService } from '../entidadesBD/entidades-bd.service';
import { MonedasBDService } from '../monedasBD/monedas-bd.service';
import { ValoresBDService } from '../valoresBD/valores-bd.service';
import { CuentasBDService } from 'src/app/Servicios/almacenamientoSQLITE/cuentasBD/cuentas-bd.service';
import { HabilitacionesBDService } from 'src/app/Servicios/almacenamientoSQLITE/habilitacionesBD/habilitaciones-bd.service';

@Injectable({
  providedIn: 'root'
})
export class TablesManageService {

  constructor(
    private clientesBD: ClientesBDService,
    private entregasBD: EntregasDocBDService,
    private usuariosBD: UsuariosBDService,
    private facturasBD: FacturasBDService,
    private notasBD: NotasBDService,
    private recibosBD: RecibosBDService, 
    private conceptoBD:ConceptosBDService,
    private monedaBD: MonedasBDService,
    private entidadBD:EntidadesBDService,
    private valoresBD:ValoresBDService,
    private cuentasBD:CuentasBDService,
    private habilitacionesBD:HabilitacionesBDService,

    
    ) { }


  async createTables(): Promise<boolean> {
    console.log("Empezando creacion de tablas");
    let clientes: boolean = false;
    let notas: boolean = false;
    let facturas: boolean = false;
    let usuario: boolean = false;
    let recibos: boolean = false;
    let entregas: boolean = false;
    let conceptos:boolean=false;
    let entidades:boolean=false;
    let cuentas:boolean=false;
    let moneda:boolean=false;
    let valores:boolean=false;
    let habilitaciones:boolean=false;

    clientes= await this.clientesBD.createTableClientes();
    facturas = await this.facturasBD.createTableFacturas(); 
    usuario= await this.usuariosBD.createTableUsuarios();
    notas= await this.notasBD.createTableNotas();
    recibos=await this.recibosBD.createTableRecibos();
    entregas=await this.entregasBD.createTableEntregas();
    conceptos=await this.conceptoBD.createTableConcepto();
    entidades= await this.entidadBD.createTableEntidad();
    cuentas=await this.cuentasBD.createTableCuentas();
    moneda=await this.monedaBD.createTableMonedas();
    valores=await this.valoresBD.createTableValores();
    habilitaciones=await this.habilitacionesBD.createTableHabilitaciones();

    

    if (clientes && facturas && notas && cuentas && moneda && habilitaciones &&
      entregas && recibos && usuario && conceptos && entidades && valores) {
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }

  async deleteAllTables(){
    let clientes: boolean = false;
    let notas: boolean = false;
    let facturas: boolean = false;
    let usuario: boolean = false;
    let recibos: boolean = false;
    let entregas: boolean = false;
    let conceptos:boolean=false;
    let entidades:boolean=false;
    let cuentas:boolean=false;
    let moneda:boolean=false;
    let valores:boolean=false;
    let habilitaciones:boolean=false;

    clientes= await this.clientesBD.deleteTableCliente()
    facturas = await this.facturasBD.deleteTableFactura(); 
    usuario= await this.usuariosBD.deleteTableUsuario();
    notas= await this.notasBD.deleteTableNotas();
    entregas=await this.entregasBD.deleteTableEntregasDoc();
    recibos=await this.recibosBD.deleteTableRecibos();
    
    conceptos=await this.conceptoBD.deleteTableConcepto();
    entidades= await this.entidadBD.deleteTableEntidad();
    cuentas=await this.cuentasBD.deleteTableCuenta();
    moneda=await this.monedaBD.deleteTableMoneda();
    valores=await this.valoresBD.deleteTableCValores();
    habilitaciones=await this.habilitacionesBD.deleteTableHabilitacion();

    

    if (clientes && facturas && notas && cuentas && moneda && habilitaciones &&
      entregas && recibos && usuario && conceptos && entidades && valores) {
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }


}
