import { TestBed } from '@angular/core/testing';

import { MonedasBDService } from './monedas-bd.service';

describe('MonedasBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonedasBDService = TestBed.get(MonedasBDService);
    expect(service).toBeTruthy();
  });
});
