import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Moneda } from 'src/app/Clases/moneda';

@Injectable({
  providedIn: 'root'
})
export class MonedasBDService {
  private TABLE_MONEDAS = "CBV_MONEDAS";
  private KEY_ID_MONEDA = "ID_MONEDA";
  private KEY_DESCRIPCION_MONEDA = "DESCRIPCION_MONEDA";
  private KEY_ACTIVO = "ACTIVO";
  private KEY_CANT_DECIMALES = "CANT_DECIMALES";
  private KEY_SIMBOLO = "SIMBOLO";
  private KEY_MONEDA_PLURAL = "MONEDA_PLURAL";
  private KEY_MONEDA_SINGULAR = "MONEDA_SINGULAR";


  constructor(private db: BDManagementService) { }

  createTableMonedas(): Promise<boolean> {
    let CREATE_MONEDAS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_MONEDAS +
      " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_MONEDA INT, DESCRIPCION_MONEDA TEXT, ACTIVO INTEGER, " +
      "CANT_DECIMALES INT, SIMBOLO TEXT, MONEDA_PLURAL TEXT, MONEDA_SINGULAR TEXT )";

    return this.db.getDataBase().executeSql(CREATE_MONEDAS_TABLE, []).then(Moneda => {
      console.log('se creo la table Moneda', Moneda);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla Moneda', error);
      return false;
    });
  }

  addMoneda(moneda: Moneda): Promise<boolean> {
    let addLine = "INSERT INTO " + this.TABLE_MONEDAS + " (ID_MONEDA, DESCRIPCION_MONEDA, ACTIVO, " +
      "CANT_DECIMALES, SIMBOLO, MONEDA_PLURAL, MONEDA_SINGULAR) " +
      "VALUES (?,?,?,?,?,?,?)";
    let params = [moneda.id, moneda.descripcion, moneda.activo, moneda.cant_decimales,
    moneda.simbolo, moneda.plural, moneda.singular]
    return this.db.getDataBase().executeSql(addLine, params).then(exito => {
      console.log('se agrego la Moneda', exito)
      return true;
    }).catch(error => {
      console.log('no se puede agregar la Moneda', error)
      return false;
    })
  }
  getMonedas(): Promise<Array<Moneda>> {

    let query = "SELECT * FROM " + this.TABLE_MONEDAS;
    let monedas: Array<Moneda> = [];
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log("DATOS DE MONEDA TRAIDA ", data.rows.length, data.rows.item(0), data)
      if (data.rows.length > 0) {
        for (let index = 0; index < data.rows.length; index++) {
          monedas.push(new Moneda(data.rows.item(index)));
        }
      }
      return monedas;
    }).catch(err => {
      console.log('no pudo obtenerse las monedas localmente', err);
      return err;
    });
  }
  deleteAllMonedas() {
    let query = "DELETE FROM " + this.TABLE_MONEDAS;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla monedas', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla monedas', error);
      return false;
    })
  }
  
  find(id){
    return this.db.find(this.TABLE_MONEDAS, id, "ID_MONEDA").then(data => {
      return data
    }).catch(error => {
      console.error("ERROR TRAYENDO MONEDA ", error);
      return {} 
    });
  }

  generateMontoValorizado(moneda, cotizacion, monto, cant_decimales) {
    let montoVal = 0;
    if (moneda == 1) {
      montoVal = parseFloat((monto * cotizacion).toFixed(cant_decimales));
    } else {
      montoVal = parseFloat((monto / cotizacion).toFixed(cant_decimales));
    }
    return montoVal;
  }

  getMoneda(id_moneda):Promise<Moneda> {
    let query = "SELECT * FROM CBV_MONEDAS WHERE ID_MONEDA=?";
    return this.db.getDataBase().executeSql(query,[id_moneda]).then(data=>{
      if(data.rows.length>0){
        return new Moneda(data.rows.item(0));
      }else{
        return null;
      }
    }).catch(error=>{
      console.log("error al traer la moneda por el id ",error)
      return null;
    })
  }
  deleteTableMoneda() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_MONEDAS;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla moneda', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla moneda', error);
      return false;
    })
  }
}
