import { TestBed } from '@angular/core/testing';

import { EntidadesBDService } from './entidades-bd.service';

describe('EntidadesBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EntidadesBDService = TestBed.get(EntidadesBDService);
    expect(service).toBeTruthy();
  });
});
