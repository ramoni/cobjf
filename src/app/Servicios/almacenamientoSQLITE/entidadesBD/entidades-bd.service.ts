import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Entidad } from 'src/app/Clases/entidad';

@Injectable({
  providedIn: 'root'
})
export class EntidadesBDService {
  private TABLE_ENTIDADES = "CBV_ENTIDADES";
  private KEY_ID_ENTIDAD = "ID_ENTIDAD";
  private KEY_DESCRIPCION = "DESCRIPCION";

  constructor(private db: BDManagementService) { }

  createTableEntidad(): Promise<boolean> {
    let CREATE_ENTIDADES_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_ENTIDADES +
      " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_ENTIDAD INT, DESCRIPCION TEXT)";

    return this.db.getDataBase().executeSql(CREATE_ENTIDADES_TABLE, []).then(entidad => {
      console.log('se creo la tabla entidad', entidad);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla entidad', error);
      return false;
    });
  }

  addEntidad(entidad: Entidad) {
    let addLine = "INSERT INTO " + this.TABLE_ENTIDADES + " (ID_ENTIDAD, DESCRIPCION) VALUES (?,?)";
    return this.db.getDataBase().executeSql(addLine, [entidad.id, entidad.descripcion]).then(exito => {
      console.log('se agrego la entidad', exito)
      return true;
    }).catch(error => {
      console.log('no se puede agregar la entidad', error)
      return false;
    })
  }
  getEntidades(): Promise<Array<Entidad>> {
    let query = "SELECT * FROM CBV_ENTIDADES ORDER BY DESCRIPCION ASC";
    let entidades: Array<Entidad> = [];
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log("DATOS DE ENTIDADES TRAIDA ", data.rows.length, data.rows.item(0), data)
      if (data.rows.length > 0) {
        for (let index = 0; index < data.rows.length; index++) {
          entidades.push(new Entidad(data.rows.item(index)));
        }
      }
      return entidades;
    }).catch(err => {
      console.log('no pudo obtenerse las entidades localmente', err);
      return err;
    });
  }
  getEntidad(id_entidad): Promise<Entidad> {
    let query = "SELECT * FROM CBV_ENTIDADES WHERE ID_ENTIDAD=?";
    return this.db.getDataBase().executeSql(query, [id_entidad]).then(data => {
      if (data.rows.length > 0) {
        return new Entidad(data.rows.item(0));
      } else {
        return null;
      }
    }).catch(error => {
      console.log("error al traer la entidad por el id", error)
      return null;
    })
  }
  deleteAllEntidades() {
    let query = "DELETE FROM " + this.TABLE_ENTIDADES;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla entidades', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla entidades', error);
      return false;
    })
  }
  deleteTableEntidad() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_ENTIDADES;
    return this.db.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla entidad', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla entidad', error);
      return false;
    })
  }
}
