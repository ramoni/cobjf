import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Notas } from 'src/app/Clases/notas';
import { WriteFileService } from '../../writeFileService/logWrite.service';

@Injectable({
  providedIn: 'root'
})
export class NotasBDService {
  private TABLE_COB_VISITAS = "CBV_COB_VISITAS";
  private KEY_ID = "ID";
  private KEY_ID_CLIENTE = "ID_CLIENTE";
  private KEY_NOTA = "NOTA";
  private KEY_LATITUD = "LATITUD";
  private KEY_LONGITUD = "LONGITUD";
  private KEY_TIPO = "TIPO";
  private KEY_HABILITACION_CAJA = "HABILITACION_CAJA";
  private KEY_IMEI = "IMEI";
  private KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION";
  private KEY_ACTIVO = "ACTIVO";
  private KEY_FEC_VISITA = "FEC_VISITA";
  private KEY_ID_TABLE_API = "ID_API";
  constructor(private bd: BDManagementService, private writeLog:WriteFileService) { }

  createTableNotas(): Promise<boolean> {

    let CREATE_NOTAS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_COB_VISITAS + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, "
      + "ID_CLIENTE TEXT, NOTA TEXT, LATITUD TEXT, LONGITUD TEXT, TIPO TEXT, HABILITACION_CAJA INTEGER, "
      + "IMEI TEXT, REQUIERE_SINCRONIZACION INTEGER DEFAULT 0, ACTIVO INTEGER, FEC_VISITA TEXT, ID_API INTEGER  DEFAULT 0)";

    return this.bd.getDataBase().executeSql(CREATE_NOTAS_TABLE, []).then(notas => {
      console.log('se creo tabla notas', notas);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla notas', error);
      return false;
    });

  }

  setNotaConMensaje(nota){
    nota.requiere_sincronizacion = 1;
    return this.bd.insert(this.TABLE_COB_VISITAS, nota).then(resp => {
      return {
        "exito": "Exito al insertar nueva nota"
      }
    }).catch(error => {
      console.log("Error al guardar nota ", error);
      return {
        "error": "Error al insertar nota"
      }
    })
  }

  actualizarSincronizacionDeNotas(ids){
    let query = "UPDATE "+this.TABLE_COB_VISITAS+" SET "+this.KEY_REQUIERE_SINCRONIZACION+"=? WHERE "+this.KEY_REQUIERE_SINCRONIZACION+"=?";
    let n = ids.length;
    let params = [0, 1];
    if(n>0){
      query = query + " AND(";
      for(let i=0; i<n; i++){
        params.push(ids[i]);
        query = query+"ID=?";
        if(i<n-1){
          query = query + " OR ";
        }else{
          query = query + ")";
        }
      }
    }

    console.log("QUERY UPDATE NOTAS ", query);

    this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log("Columna de notas que requieren sincronizacion cambiados a 0", data)
    }).catch(error => {
      console.log('No se pudo actualizar la columna de requiere sincronizacion a 0 de las notas', error)
    });
  }

  updateNotaConMensaje(nota, conditions){
    nota.requiere_sincronizacion = 1;
    return this.bd.update(this.TABLE_COB_VISITAS, nota, conditions).then(resp => {
      return {
        "exito": "Exito al actualizar nota"
      }
    }).catch(error => {
      console.log("Error al actualizar nota ", error);
      return {
        "error": "Error al actualizar nota"
      }
    })
  }

  addNota(nota) {
    let values = {}
    values[this.KEY_ID_CLIENTE] = nota.ID_CLIENTE;
    values[this.KEY_NOTA] = nota.NOTAS;
    values[this.KEY_LATITUD] = nota.LATITUD;
    values[this.KEY_LONGITUD] = nota.LONGITUD;
    values[this.KEY_TIPO] = nota.TIPO;
    values[this.KEY_HABILITACION_CAJA] = nota.HABILITACION_CAJA;
    values[this.KEY_IMEI] = nota.IMEI;
    values[this.KEY_ACTIVO] = 1;
    values[this.KEY_ID_TABLE_API] = nota.ID;
    values[this.KEY_REQUIERE_SINCRONIZACION] = 0;
    return this.bd.insert(this.TABLE_COB_VISITAS, values);
  }
  syncNotasHabilitacion(habilitacion: number): Promise<any> {
    let query = "SELECT * FROM " + this.TABLE_COB_VISITAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
      "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
    return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(async data => {
      if (data.rows.length > 0) {
        await this.writeLog.escribirLog(new Date()+ " Numero de Notas que faltan ser sincronizadas: "+data.rows.length +" una de ellas "+JSON.stringify(data.rows.item(0)));
        return true;
      } else {
        return false;
      }
    }).catch(err=>{
      return -1;
    })
  }


  cantidadNotasSyncByType(tipo: string) {
    let query = "SELECT COUNT(*) as cantidad FROM " + this.TABLE_COB_VISITAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=1 AND TIPO=" + "'" + tipo + "'";
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log("CANT NOTAS " + tipo + " ", data.rows.item(0).cantidad);
      return data.rows.item(0).cantidad;
    }).catch(error => {
      console.log("Error trayendo cantidad de notas tipo: " + tipo, error);
      return -1;
    });
  }

  async cantidadNotasSync(habilitacion: number) {
    let cant_facturas = await this.cantidadNotasSyncByType("F");
    let cant_entregas = await this.cantidadNotasSyncByType("E");

    if (cant_facturas == -1) {
      cant_facturas = 0;
    }

    if (cant_entregas == -1) {
      cant_entregas = 0;
    }

    return {
      "NOTAS_VISITAS": parseInt(cant_facturas),
      "NOTAS_ENTREGAS": parseInt(cant_entregas)
    }
  }

  existeNotaLocal(id_cliente, habilitacion_caja, tipo) {
    let query = "SELECT * FROM " + this.TABLE_COB_VISITAS + " WHERE " + this.KEY_ID_CLIENTE + "=? AND "
      + this.KEY_HABILITACION_CAJA + "=? AND " + this.KEY_TIPO + "=? AND " + this.KEY_REQUIERE_SINCRONIZACION + "=?";
    console.log(query);
    return this.bd.getDataBase().executeSql(query, [id_cliente, habilitacion_caja, tipo, 0]).then(data => {
      //console.log("TAMAÑO NOTAS ",data.rows.length, data)
      if (data.rows.length > 0) {
        return true;
      }
      return false;
    }).catch(error => {
      console.log("Error comprobando existencia de notas en local ", error);
      return error;
    });
  }

  /*listAll(){
    let query = "SELECT * FROM "+this.TABLE_COB_VISITAS;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log(data.rows.length, data.rows.item(0), data)
    }).catch(error => {
      console.log(error);
    });
  }*/

  getNotasASincronizar(habilitacion) {
    let query = "SELECT * FROM " + this.TABLE_COB_VISITAS+ " WHERE "+ this.KEY_REQUIERE_SINCRONIZACION + "=? AND HABILITACION_CAJA=?";
    let params = [1,habilitacion];
    
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      let notas = [];
      for(let i=0;i<data.rows.length;i++){
        notas.push(data.rows.item(i));
      }
      console.log("NOTAS TRAIDAS DE LA BD PARA SINCRONIZAR: ", notas);
      return notas;
    }).catch(error => {
      console.log("ERROR OBTENIENDO NOTAS DE LA BD PARA SINCRONIZAR ", error)
      return error;
    });
  }


  getNotaCobrador(id_cliente, habilitacion, tipo: string):Promise<Notas>{
    let params = [id_cliente, habilitacion, tipo];
    let query = "SELECT * FROM " + this.TABLE_COB_VISITAS 
    + " WHERE ID_CLIENTE=? AND HABILITACION_CAJA =? AND TIPO=?";
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log('se obtuvo las notas del cobrador ', data.rows.item(0));
      if(data.rows.length>0){
        let nota = data.rows.item(0);
        nota.NOTAS = nota.NOTA;
        return new Notas(nota)
      }else{
        return new Notas();
      }
    }).catch(error=>{
      console.log('no pudo obtenerse las notas del cobrador', error)
      return error;
    })
  }
  deleteAllNotas() {
    let query = "DELETE FROM " + this.TABLE_COB_VISITAS;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla notas', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla notas', error);
      return false;
    })
  }
  deleteTableNotas() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_COB_VISITAS;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla notas', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla notas', error);
      return false;
    })
  }
}
