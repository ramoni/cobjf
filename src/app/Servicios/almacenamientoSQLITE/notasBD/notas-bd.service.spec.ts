import { TestBed } from '@angular/core/testing';

import { NotasBDService } from './notas-bd.service';

describe('NotasBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotasBDService = TestBed.get(NotasBDService);
    expect(service).toBeTruthy();
  });
});
