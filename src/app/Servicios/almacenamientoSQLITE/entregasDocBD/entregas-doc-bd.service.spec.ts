import { TestBed } from '@angular/core/testing';

import { EntregasDocBDService } from './entregas-doc-bd.service';

describe('EntregasDocBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EntregasDocBDService = TestBed.get(EntregasDocBDService);
    expect(service).toBeTruthy();
  });
});
