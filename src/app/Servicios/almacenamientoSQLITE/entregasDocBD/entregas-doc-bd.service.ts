import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import * as moment from 'moment';
import { Documentos } from 'src/app/Clases/documentos';
import { DocumentData } from 'src/app/Clases/document-data';
import { WriteFileService } from '../../writeFileService/logWrite.service';


@Injectable({
  providedIn: 'root'
})
export class EntregasDocBDService {
  private TABLE_ENTREGAS = "CBV_ENTREGAS";
  private KEY_ID = "ID";
  private KEY_ID_ENTREGA = "ID_ENTREGA";
  private KEY_ID_CLIENTE = "ID_CLIENTE";
  private KEY_ID_ENTREGA_DET = "ID_ENTREGA_DET";
  private KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION";
  private KEY_ID_DOCUMENTO = "ID_DOCUMENTO";
  private KEY_NO_DOCUMENTO = "NO_DOCUMENTO";
  private KEY_FECHA_DOCUMENTO = "FECHA_DOCUMENTO";
  private KEY_FECHA_ENTREGA = "FECHA_ENTREGA";
  private KEY_ID_MONEDA = "ID_MONEDA";
  private KEY_SIMBOLO = "SIMBOLO";
  private KEY_IMPORTE = "IMPORTE";
  private KEY_TIPO = "TIPO";
  private KEY_GUARDADO = "GUARDADO";
  private KEY_LATITUD = "LATITUD";
  private KEY_LONGITUD = "LONGITUD";
  private KEY_IMEI = "IMEI";
  private KEY_ID_COBRADOR = "ID_COBRADOR";
  private KEY_HABILITACION_CAJA = "HABILITACION_CAJA";

  constructor(private bd: BDManagementService,
    private writeLog:WriteFileService) { }

  createTableEntregas() {
    let CREATE_ENTREGAS_TABLE = "CREATE TABLE IF NOT EXISTS CBV_ENTREGAS (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
      "ID_ENTREGA INTEGER, ID_ENTREGA_DET INTEGER, REQUIERE_SINCRONIZACION INTEGER, " +
      "HABILITACION_CAJA INTEGER, ID_DOCUMENTO INTEGER, NO_DOCUMENTO TEXT, FECHA_DOCUMENTO TEXT, " +
      "ID_MONEDA INTEGER, SIMBOLO TEXT, IMPORTE REAL, GUARDADO INTEGER, TIPO TEXT, " +
      "ID_CLIENTE INTEGER, LATITUD TEXT, LONGITUD TEXT, IMEI TEXT, ID_COBRADOR INTEGER, " +
      "FECHA_ENTREGA TEXT, FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA))";

    return this.bd.getDataBase().executeSql(CREATE_ENTREGAS_TABLE, []).then(entregas => {
      console.log('se creo la tabla entregas-documentos', entregas);
      return Promise.resolve(entregas)
    }).catch(error => {
      console.error('no se creo la tabla entregas', error);
      return Promise.reject(error);
    });
  }
  insertarEntregaDocTabla(entrega: any) {
    let sql = "INSERT INTO " + this.TABLE_ENTREGAS + " (ID_ENTREGA, ID_CLIENTE, ID_ENTREGA_DET, "
      + "REQUIERE_SINCRONIZACION, ID_DOCUMENTO, NO_DOCUMENTO,FECHA_DOCUMENTO,ID_MONEDA, "
      + "SIMBOLO, IMPORTE, GUARDADO, TIPO, LATITUD, LONGITUD, IMEI, ID_COBRADOR, FECHA_ENTREGA) "
      + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    let params = [entrega.ID_ENTREGA, entrega.ID_CLIENTE, entrega.ID_ENTREGA_DET, 0, entrega.ID_DOCUMENTO,
    entrega.NO_DOCUMENTO, entrega.FECHA_DOCUMENTO, entrega.ID_MONEDA, entrega.SIMBOLO, entrega.IMPORTE,
    entrega.GUARDADO, entrega.TIPO, entrega.LATITUD, entrega.LONGITUD, entrega.IMEI, entrega.ID_COBRADOR,
    moment(new Date()).format('DD-MM-YYYY')];
    return this.bd.getDataBase().executeSql(sql, params).then(data => {
      console.log('se inserto una nueva entrega', data);
    }).catch(error => {
      console.log('no pudo insertarse la entregaDoc', error)
    })
  }
  updateEntregasDoc(entregaD: any) {
    let sql = "UPDATE " + this.TABLE_ENTREGAS + " SET IMPORTE =?, ID_ENTREGA =?, ID_ENTREGA_DET =?, "
      + "LATITUD=?, LONGITUD=?, IMEI=?, ID_CLIENTE=?, NO_DOCUMENTO=?, FECHA_DOCUMENTO=?, "
      + "FECHA_ENTREGA=? WHERE ID_DOCUMENTO=?";
    let params = [entregaD.IMPORTE, entregaD.ID_ENTREGA, entregaD.ID_ENTREGA_DET, entregaD.LATITUD,
    entregaD.LONGITUD, entregaD.IMEI, entregaD.ID_CLIENTE, entregaD.NO_DOCUMENTO, entregaD.FECHA_DOCUMENTO,
    moment(new Date()).format('DD-MM-YYYY'), entregaD.ID_DOCUMENTO]
    this.bd.getDataBase().executeSql(sql, params).then(data => {
      console.log('se inserto un registro de entrega doc', data)
    }).catch(error => {
      console.error('Error actualizar la entrega doc', error)
    })
  }
  syncEntregasHabilitacion(habilitacion: number): Promise<any> {
    let query = "SELECT * FROM " + this.TABLE_ENTREGAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
      "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
    return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(async data => {
      if (data.rows.length > 0) {
        await this.writeLog.escribirLog(new Date()+ " Numero de Entregas que faltan ser sincronizadas: "+data.rows.length +" una de ellas "+JSON.stringify(data.rows.item(0)));
        return true;
      } else {
        return false;
      }
    }).catch(err => {
      return -1;
    })
  }
  deleteAllEntregasDoc() {
    let query = "DELETE FROM " + this.TABLE_ENTREGAS;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los dfatos de la tabla entregas', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla entregas', error);
      return false;
    })
  }

  cantidadEntregaDocSync(habilitacion: number) {
    let query = "SELECT COUNT (*) as cantidad FROM " + this.TABLE_ENTREGAS + " WHERE HABILITACION_CAJA=? AND REQUIERE_SINCRONIZACION=1 AND GUARDADO=1";
    return this.bd.getDataBase().executeSql(query, [habilitacion]).then(data => {
      console.log("CANT ENTREGAS ", data.rows.item(0).cantidad);
      return data.rows.item(0).cantidad;
    }).catch(error => {
      console.log("Error trayendo cantidad de entregas");
      return -1;
    });
  }

  async getEntregasLocal(id_cliente: number, offset, limit): Promise<Array<Documentos>> {

    offset = offset > 0 ? offset - 1 : 0;

    let entregas: Array<Documentos> = []
    let today = moment(new Date()).format("DD-MM-YYYY");

    let query = "SELECT ID_ENTREGA, ID_ENTREGA_DET, REQUIERE_SINCRONIZACION, HABILITACION_CAJA, "
      + "ID_DOCUMENTO, NO_DOCUMENTO, FECHA_DOCUMENTO, ID_MONEDA, SIMBOLO, ROUND(IMPORTE,2) AS IMPORTE, "
      + "GUARDADO, TIPO, ID_CLIENTE, LATITUD, LONGITUD, IMEI, ID_COBRADOR "
      + "FROM CBV_ENTREGAS WHERE ID_CLIENTE=? AND FECHA_ENTREGA=? LIMIT " + limit + " OFFSET " + offset;
    let params = [id_cliente, today]
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log('entregas traidas de la bd', data.rows.length, data)
      for (let i = 0; i < data.rows.length; i++) {
        entregas.push(new Documentos(data.rows.item(i)));
      }
      return entregas;
    }).catch(error => {
      console.log("Error trayendo entregas de documentos", error);
      return null;
    });
  }

  async existeEntregaDoc(id_doc: any) {
    let doc: boolean = false;
    let query = "SELECT * FROM " + this.TABLE_ENTREGAS + " WHERE ID_DOCUMENTO=? AND REQUIERE_SINCRONIZACION=?";
    await this.bd.getDataBase().executeSql(query, [id_doc, 0]).then(data => {
      console.log('entrega si no hubo error', data)
      if (data.rows.length > 0) {
        doc = true;
      } else {
        doc = false;
      }

    }).catch(error => {
      console.log("Error al obtener una entrega", error);
      doc = null;

    });
    console.log('retorno de existeEntregaDoc', doc)
    return doc;
  }

  async existeEntregaDocSync(id_doc: any) {
    let doc: boolean = false;
    let query = "SELECT * FROM " + this.TABLE_ENTREGAS + " WHERE ID_DOCUMENTO=? AND REQUIERE_SINCRONIZACION=?";
    await this.bd.getDataBase().executeSql(query, [id_doc, 1]).then(data => {
      console.log('entrega si no hubo error', data)
      if (data.rows.length > 0) {
        doc = true;
      } else {
        doc = false;
      }

    }).catch(error => {
      console.log("Error al obtener una entrega", error);
      doc = null;

    });
    console.log('retorno de existeEntregaDocSync', doc)
    return doc;
  }

  getEntregasASincronizar(habilitacion) {
    let query = "SELECT ID,ID_ENTREGA, ID_ENTREGA_DET, REQUIERE_SINCRONIZACION, HABILITACION_CAJA,ID_DOCUMENTO, NO_DOCUMENTO, FECHA_DOCUMENTO, ID_MONEDA, SIMBOLO, round(IMPORTE,2) AS IMPORTE, "
      + "GUARDADO, TIPO, ID_CLIENTE, LATITUD, LONGITUD, IMEI, ID_COBRADOR FROM " + this.TABLE_ENTREGAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=? AND HABILITACION_CAJA=?";
    return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(data => {
      console.log("Exito trayendo entregas para sincronizar, cantidad: ", data.rows.length);
      let entregas = [];
      for (let i = 0; i < data.rows.length; i++) {
        entregas.push(data.rows.item(i));
      }
      console.log("ENTREGAS TRAIDAS DE LA BD PARA SINCRONIZAR: ", entregas);
      return entregas;
    }).catch(error => {
      console.error("Error trayendo entregas para sincronizar", error);
      return error;
    });
  }
  entregarDocumentos(documentos: DocumentData): Promise<any> {
    let strSQL = "UPDATE " + this.TABLE_ENTREGAS + " SET IMPORTE=?, GUARDADO =?, ID_ENTREGA =?, "
      + "ID_ENTREGA_DET=?, HABILITACION_CAJA=?, ID_CLIENTE=?, NO_DOCUMENTO=?, FECHA_DOCUMENTO=?, "
      + "REQUIERE_SINCRONIZACION=?, LATITUD=?, LONGITUD=?, IMEI=? WHERE ID_DOCUMENTO=?";
    for (let documento of documentos.entregas) {
      let params = [documento.importe, documento.guardado, documento.id_entrega, documento.id_entrega_det,
      documento.habilitacion_caja, documento.id_cliente, documento.no_documento,
      documento.fecha_documento, 1, documento.latitud, documento.longitud, documento.imei, documento.id_documento];
      this.bd.getDataBase().executeSql(strSQL, params).then(data => {
        console.log("Exito en entregar o actualizar un documento ", data);

      }).catch(error => {
        console.error("Error al insertar una entrega", error);

        return Promise.reject({ error: "No se pudo realizar la entrega de documentos" })
      });
    }
    return Promise.resolve({ exito: "Entregas realizadas con exito" });
  }
  deleteTableEntregasDoc() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_ENTREGAS;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla entregas', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla entregas', error);
      return false;
    })
  }

  actualizarSincronizacionDeEntregas(ids) {
    let query = "UPDATE " + this.TABLE_ENTREGAS + " SET " + this.KEY_REQUIERE_SINCRONIZACION + "=? WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=?";
    let n = ids.length;
    let params = [0, 1];

    if (n > 0) {
      query = query + " AND(";
      for (let i = 0; i < n; i++) {
        params.push(ids[i]);
        query = query + "ID=?";
        if (i < n - 1) {
          query = query + " OR ";
        } else {
          query = query + ")";
        }
      }
    }

    console.log("QUERY UPDATE ENTREGAS ", query);

    this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log("Columna de entregas que requieren sincronizacion cambiados a 0", data)
    }).catch(error => {
      console.log('No se pudo actualizar la columna de requiere sincronizacion a 0 de las entregas', error)
    });
  }
}
