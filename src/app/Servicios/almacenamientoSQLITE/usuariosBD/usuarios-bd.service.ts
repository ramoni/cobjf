import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { DatosUsuario } from 'src/app/Clases/datos-usuario';


@Injectable({
  providedIn: 'root'
})
export class UsuariosBDService {
  private TABLE_USER = "CBV_USER";
  private KEY_USER_ID = "USER_ID";
  private KEY_USUARIO = "USUARIO";
  private KEY_PASSWORD = "PASSWORD";
  private KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION";
  private KEY_HABILITACION = "HABILITACION_CAJA";
  private KEY_FACTURAS = "FACTURAS";
  private KEY_DOCUMENTOS = "DOCUMENTOS";
  private KEY_NOMBRE = "NOMBRE";
  private KEY_CAJA = "CAJA";
  private KEY_TOKEN_EXPIRED = "TOKEN_EXPIRED";
  private KEY_TOKEN = "TOKEN";
  private KEY_STATUS = "IS_STATUS"; //por defecto es 1 significa que la sesion de usuario es valida
  private KEY_ULTIMO_RECIBO = "ULTIMO_RECIBO";
  private KEY_TALONARIO = "TALONARIO"; //por defecto es 1 significa que la sesion de usuario es valida
  private KEY_ID_COBRADOR = "ID_COBRADOR";

  constructor(private bd: BDManagementService) { }

  createTableUsuarios(): Promise<boolean> {

    let CREATE_USUARIOS_TABLE = "CREATE TABLE IF NOT EXISTS CBV_USER (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
      "USUARIO TEXT,  PASSWORD TEXT, REQUIERE_SINCRONIZACION INTEGER, HABILITACION_CAJA INTEGER, " +
      "FACTURAS INTEGER, DOCUMENTOS INTEGER, NOMBRE TEXT, CAJA TEXT, TOKEN_EXPIRED TEXT, " +
      "TOKEN TEXT, IS_STATUS INTEGER, ULTIMO_RECIBO INTEGER, TALONARIO INTEGER, ID_COBRADOR INTEGER, " +
      "DATOS_BAJADA INTEGER, DATOS_SUBIDA INTEGER, RECIBOS_SYNC INTEGER DEFAULT 0, " +
      "ENTREGAS_SYNC INTEGER DEFAULT 0, NOTAS_VISITAS_SYNC INTEGER DEFAULT 0, " +
      "NOTAS_ENTREGAS_SYNC INTEGER DEFAULT 0)";

    return this.bd.getDataBase().executeSql(CREATE_USUARIOS_TABLE, []).then(users => {
      console.log('creo la tabla usuarios', users);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla usuarios', error);
      return false;
    });
  }

  existeUsuario(usuario: String, pass: String): Promise<any> {
    let sql = "SELECT * FROM CBV_USER WHERE USUARIO=? AND PASSWORD=? AND IS_STATUS=?";
    return this.bd.getDataBase().executeSql(sql, [usuario, pass, 1]).then(data => {
      console.log('se ejecuto si existe usuario', data)
      return data;
    })
  }
  existeHabilitacionUser(hab: number, usuario: String, pass: String): Promise<any> {
    let sql = "SELECT HABILITACION_CAJA FROM " + this.TABLE_USER + " WHERE HABILITACION_CAJA=? ";
    return this.bd.getDataBase().executeSql(sql, [hab]).then(data => {
      console.log('se ejecuto si existe usuario', data)
      return data;
    })
  }
  addUser(usuario, password, userInterface: DatosUsuario, token, expiredToken): Promise<any> {
    return this.existeUsuario(usuario, password).then(data => {
      if (data.rows.length > 0) {
        console.log('existe el usuario, se debe actualizar el usuario')
        let sql = "UPDATE " + this.TABLE_USER + " SET " + this.KEY_TOKEN + "=?, " + this.KEY_TOKEN_EXPIRED + "=?, " +
          this.KEY_CAJA + "=?, " + this.KEY_HABILITACION + "=?, " + this.KEY_ULTIMO_RECIBO + "=?, " +
          this.KEY_TALONARIO + "=?, " + this.KEY_ID_COBRADOR + "=?, RECIBOS_SYNC=?, ENTREGAS_SYNC=?, " +
          "NOTAS_VISITAS_SYNC=?, NOTAS_ENTREGAS_SYNC=? WHERE USUARIO=? AND PASSWORD=?";

        return this.bd.getDataBase().executeSql(sql, [token, expiredToken, userInterface.CAJA, userInterface.HABILITACION,
          userInterface.USUARIO.ULTIMO_RECIBO, userInterface.USUARIO.TALONARIO, userInterface.USUARIO.ID_COBRADOR,
          userInterface.DATOS_SINCRONIZADOS.RECIBOS, userInterface.DATOS_SINCRONIZADOS.ENTREGAS,
          userInterface.DATOS_SINCRONIZADOS.NOTAS_VISITAS, userInterface.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS,
          usuario, password]).then(exito => {
            console.log('exito al actualizar usuario', exito)
            return true;
          })
      } else {
        console.log('se debe insertar el usuario, no existe el usuario')
        let sql = "INSERT INTO " + this.TABLE_USER + " (USUARIO, PASSWORD, FACTURAS, DOCUMENTOS, " +
          "NOMBRE, HABILITACION_CAJA, CAJA, REQUIERE_SINCRONIZACION, TOKEN, TOKEN_EXPIRED, IS_STATUS, " +
          "TALONARIO, ID_COBRADOR, ULTIMO_RECIBO, RECIBOS_SYNC, ENTREGAS_SYNC, NOTAS_VISITAS_SYNC, " +
          "NOTAS_ENTREGAS_SYNC) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        let insert = [usuario, password, userInterface.FACTURAS, userInterface.DOCUMENTOS,
          userInterface.USUARIO.NOMBRE, userInterface.HABILITACION, userInterface.CAJA, 1, token,
          expiredToken, 1, userInterface.USUARIO.TALONARIO, userInterface.USUARIO.ID_COBRADOR,
          userInterface.USUARIO.ULTIMO_RECIBO, userInterface.DATOS_SINCRONIZADOS.RECIBOS,
          userInterface.DATOS_SINCRONIZADOS.ENTREGAS, userInterface.DATOS_SINCRONIZADOS.NOTAS_VISITAS,
          userInterface.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS];

        return this.bd.getDataBase().executeSql(sql, insert).then(exito => {
          console.log('exito al insertar usuario', exito)
          return true;
        })
      }
    }).catch(error=>{
      console.log('error al ver si existe usuario')
      return error;
    })
  }

  deleteAllUsuarios() {
    let query = "DELETE FROM " + this.TABLE_USER;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla usuarios', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla usuarios', error);
      return false;
    })
  }

  getUserData() {
    let query = "SELECT HABILITACION_CAJA, FACTURAS,DOCUMENTOS, CAJA, TOKEN, TOKEN_EXPIRED, "
      + "ID_COBRADOR,NOMBRE, TALONARIO, RECIBOS_SYNC, ENTREGAS_SYNC, NOTAS_VISITAS_SYNC, "
      + "NOTAS_ENTREGAS_SYNC FROM " + this.TABLE_USER + " LIMIT 1";
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('usuario obtenido', data.rows.item(0));
      let object = data.rows.item(0);
      let jsonObject = {};
      jsonObject["HABILITACION"] = object.HABILITACION_CAJA;
      jsonObject["FACTURAS"] = object.FACTURAS;
      jsonObject["DOCUMENTOS"] = object.DOCUMENTOS;
      jsonObject["CAJA"] = object.CAJA;

      jsonObject["TOKEN"] = object.TOKEN;
      jsonObject["EXPIRED"] = object.TOKEN_EXPIRED;

      let jsonUser = {};
      jsonUser["NOMBRE"] = object.NOMBRE;
      jsonUser["TALONARIO"] = object.TALONARIO;
      jsonUser["ID_COBRADOR"] = object.ID_COBRADOR;
      jsonUser["RECIBOS"] = object.RECIBOS_SYNC;
      jsonUser["ENTREGAS"] = object.ENTREGAS_SYNC;
      jsonUser["NOTAS_VISITAS"] = object.NOTAS_VISITAS_SYNC;
      jsonUser["NOTAS_ENTREGAS"] = object.NOTAS_ENTREGAS_SYNC;

      jsonObject["USUARIO"] = jsonUser;
      console.log('json user', jsonObject);
      return jsonObject;

    }).catch(error => {
      console.log('no se pudo traer usuario', error);
      return null;
    })

  }
  updateDatosUsuario(user: DatosUsuario) {
    let query = "UPDATE " + this.TABLE_USER + " SET DOCUMENTOS=?, FACTURAS=?, HABILITACION_CAJA=?, " +
      "ULTIMO_RECIBO=?, TALONARIO=?, CAJA=?, RECIBOS_SYNC=?, ENTREGAS_SYNC=?, NOTAS_VISITAS_SYNC=?, " +
      "NOTAS_ENTREGAS_SYNC=? WHERE ID_COBRADOR=?";

    let vector = [user.DOCUMENTOS, user.FACTURAS, user.HABILITACION, user.USUARIO.ULTIMO_RECIBO,
    user.USUARIO.TALONARIO, user.CAJA, user.DATOS_SINCRONIZADOS.RECIBOS, user.DATOS_SINCRONIZADOS.ENTREGAS,
    user.DATOS_SINCRONIZADOS.NOTAS_VISITAS, user.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS, user.USUARIO.ID_COBRADOR]

    return this.bd.getDataBase().executeSql(query, vector).then(data => {
      console.log('se actualizo correctamente la tabla usuarios', data);
      return true;
    }).catch(error => {
      console.log('no se pudo actualizar la tabla usuarios', error);
      return false;
    })
  }

  getUltimoRecibo(){
    let query = "SELECT MAX(ULTIMO_RECIBO) AS ULTIMO_RECIBO FROM "+this.TABLE_USER;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('Ultimo recibo del usuario', data.rows.item(0).ULTIMO_RECIBO);
      return data.rows.item(0).ULTIMO_RECIBO;
    }).catch(error => {
      console.log('No se pudo obtener el nro del ultimo recibo del usuario', error);
      return -1;
    })
  }
  getTalonario(id_cobrador){
    let talonario={
      TALONARIO: 0,
      NOMBRE:""
    }
    let query_usuario = "SELECT TALONARIO, NOMBRE FROM CBV_USER WHERE ID_COBRADOR=?"
    let params_usuario=[id_cobrador];
    return this.bd.getDataBase().executeSql(query_usuario, params_usuario).then(data => {
      
      if(data.rows.length>0){
        talonario.TALONARIO=data.rows.item(0).TALONARIO;
        talonario.NOMBRE=data.rows.item(0).NOMBRE;
        console.log('talonario y nombre ', talonario);
        return talonario
      }else{
        return null;
      }
      
    }).catch(error => {
      console.log('No se pudo obtener el talonario del usuario', error);
      return -1;
    })
  }
  deleteTableUsuario() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_USER;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla user', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla user', error);
      return false;
    })
  }
}
