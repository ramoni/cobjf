import { TestBed } from '@angular/core/testing';

import { UsuariosBDService } from './usuarios-bd.service';

describe('UsuariosBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsuariosBDService = TestBed.get(UsuariosBDService);
    expect(service).toBeTruthy();
  });
});
