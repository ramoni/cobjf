import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Factura } from 'src/app/Clases/factura';
import { Cliente } from 'src/app/Clases/cliente';
import { RecibosBDService } from '../recibosBD/recibos-bd.service';
import { ValorInteresService } from '../../interesValueService/valor-interes.service';

@Injectable({
  providedIn: 'root'
})
export class FacturasBDService {

  private TABLE_FACTURAS = "CBV_FACTURAS";
  private KEY_ID_FACTURA = "ID_FACTURA";
  private KEY_ID_CLIENTE = "ID_CLIENTE";
  private KEY_NO_FACTURA = "NO_FACTURA";
  private KEY_SALDO_FAC = "SALDO_FAC";
  private KEY_FECHA = "FECHA";
  private KEY_SALDO = "SALDO";
  private KEY_TOTAL = "TOTAL";
  private KEY_CUOTA = "CUOTA";
  private KEY_VENCIMIENTO = "VENCIMIENTO";
  private KEY_TIPO = "TIPO";
  private KEY_ID_MONEDA = "ID_MONEDA";
  private KEY_INTERES = "INTERES";
  private KEY_TOTAL_PAGADO_SERVER = "TOTAL_PAGADO_SERVER";

  constructor(
    public bd: BDManagementService,
    public recibosBD: RecibosBDService,
    public interesService:ValorInteresService
  ) {

  }
  createTableFacturas(): Promise<boolean> {

    let CREATE_FACTURAS_TABLE = "CREATE TABLE IF NOT EXISTS CBV_FACTURAS (ID INTEGER PRIMARY KEY AUTOINCREMENT, "
      + "ID_FACTURA INTEGER, NO_FACTURA TEXT, FECHA TEXT, CUOTA INTEGER, TOTAL REAL, SALDO REAL, "
      + "SALDO_FAC REAL, ID_CLIENTE INTEGER, VENCIMIENTO TEXT, TIPO TEXT, ID_MONEDA INTEGER, "
      + "INTERES REAL,TOTAL_PAGADO_SERVER REAL, "
      + "FOREIGN KEY (ID_CLIENTE) REFERENCES CBV_CLIENTES (ID_CLIENTE),"
      + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA))";

    return this.bd.getDataBase().executeSql(CREATE_FACTURAS_TABLE, []).then(facturas => {
      console.log('se creo la tabla facturas', facturas);
      return true;
    }).catch(error => {
      console.error('no se creo la tabla facturas', error);
      return false;
    });
  }

  deleteAllFacturas() {
    let query = "DELETE FROM " + this.TABLE_FACTURAS;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se eliminaron correctamente los datos de la tabla facturas', data);
      return true;
    }).catch(error => {
      console.log('no se eliminaron los datos de la tabla facturas', error);
      return false;
    })
  }

  async addFacturas(factura: any) {

    let fac = await this.existeFactura(factura);
    let params = [];
    
    if (fac === true) {
      console.log('fac a agregar', factura)
      params = [factura.TOTAL, factura.CUOTA, factura.SALDO, factura.TIPO, factura.ID_MONEDA,
      factura.SALDO_FAC, factura.VENCIMIENTO, factura.ID_CLIENTE, factura.FECHA, factura.TIPO,
      factura.ID_FACTURA];

      let sqlUpdate = "UPDATE " + this.TABLE_FACTURAS + " SET TOTAL =?, CUOTA=?, SALDO=?, TIPO=?, ID_MONEDA=?, "
        + "SALDO_FAC=?, VENCIMIENTO=?, ID_CLIENTE=?, NO_FACTURA=?, FECHA=?, TIPO=?, WHERE ID_FACTURA=? ";

      this.bd.getDataBase().executeSql(sqlUpdate, params).then(data => {
        console.log('se actualizo la factura', data)
      }).catch(error => {
        console.error('error al ACTUALIZAR la factura', error)
      })
    } else {
      if (fac === false) {
        console.log('factura a agregar', factura)
        params = [factura.ID_FACTURA, factura.NO_FACTURA, factura.FECHA, factura.ID_CLIENTE, factura.SALDO,
        factura.SALDO_FAC, factura.CUOTA, factura.VENCIMIENTO, factura.TIPO, factura.ID_MONEDA,
        factura.TOTAL, factura.TOTAL_PAGADO];

        let sqlInsert = "INSERT INTO " + this.TABLE_FACTURAS + " (ID_FACTURA, NO_FACTURA, FECHA, ID_CLIENTE, "
          + "SALDO, SALDO_FAC, CUOTA, VENCIMIENTO, TIPO, ID_MONEDA, TOTAL, TOTAL_PAGADO_SERVER) "
          + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"

        this.bd.getDataBase().executeSql(sqlInsert, params).then(data => {
          console.log('se inserto la factura', data)
        }).catch(error => {
          console.error('error al insertar la factura', error)
        })
      }
    }
  }
  existeFactura(factura: Factura) {
    let sql = "SELECT * FROM " + this.TABLE_FACTURAS + " WHERE ID_FACTURA=? AND CUOTA=?";
    return this.bd.getDataBase().executeSql(sql, [factura.id, factura.saldo_cuota]).then(data => {
      if (data.rows.length > 0) {
        console.log('existe la factura', data)
        return true;
      } else {
        console.log('no existe la factura', data)
        return false;
      }

    }).catch(error => {
      console.log('no puede obtenerse la factura', error);
      return null;
    })

  }
  getFacturasLocal(cliente: Cliente, offset, limit, orden) {
    
    offset = offset > 0 ? offset - 1 : 0;

    let params = [cliente.id, limit, offset]
    let query;
    if (orden === "nroFactura") {
      query = "SELECT y.ID_FACTURA, y.NO_FACTURA, y.TIPO, M.DESCRIPCION_MONEDA, y.ID_MONEDA, " +
        "y.CUOTA_MAXIMA,y.CUOTA_MINIMA, y.MONTO_FACTURA, y.SALDO_CUOTA, y.TOTAL, y.FECHA,y.VENCIMIENTO  "
        + "FROM (SELECT F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA, MAX(F.CUOTA) AS CUOTA_MAXIMA, "
        + "MIN(F.CUOTA) AS CUOTA_MINIMA, sum(F.SALDO) AS MONTO_FACTURA, min(F.SALDO) AS SALDO_CUOTA, "
        + "min(F.TOTAL) AS TOTAL,min(F.FECHA) AS FECHA, min(F.VENCIMIENTO) AS VENCIMIENTO FROM "
        + this.TABLE_FACTURAS + " F " + "WHERE F.ID_CLIENTE=? "
        + "GROUP BY F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA) y "
        + "INNER JOIN CBV_MONEDAS M ON y.ID_MONEDA = M.ID_MONEDA "
        + "ORDER BY y.NO_FACTURA LIMIT ?" + " OFFSET ?";
    }
    if (orden === "vencimiento") {
      query = "SELECT y.ID_FACTURA, y.NO_FACTURA, y.TIPO,M.DESCRIPCION_MONEDA,y.ID_MONEDA, "
        + "y.CUOTA_MAXIMA,y.CUOTA_MINIMA, y.MONTO_FACTURA,y.SALDO_CUOTA, y.TOTAL, y.FECHA, y.VENCIMIENTO  "
        + "FROM (SELECT F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA, MAX(F.CUOTA) AS CUOTA_MAXIMA, "
        + "MIN(F.CUOTA) AS CUOTA_MINIMA,sum(F.SALDO) AS MONTO_FACTURA,min(F.SALDO) AS SALDO_CUOTA, "
        + "min(F.TOTAL) AS TOTAL,min(F.FECHA) AS FECHA, min(F.VENCIMIENTO) AS VENCIMIENTO FROM "
        + this.TABLE_FACTURAS + " F " + "WHERE F.ID_CLIENTE=? "
        + "GROUP BY F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA) y "
        + "INNER JOIN CBV_MONEDAS M ON y.ID_MONEDA = M.ID_MONEDA "
        + "ORDER BY DATE(y.VENCIMIENTO) ASC " + "LIMIT ?" + " OFFSET ?";

    }
    if (orden === "monto") {
      query = "SELECT y.ID_FACTURA, y.NO_FACTURA, y.TIPO, M.DESCRIPCION_MONEDA, y.ID_MONEDA, "
        + "y.CUOTA_MAXIMA, y.CUOTA_MINIMA, y.MONTO_FACTURA,y.SALDO_CUOTA, y.TOTAL, y.FECHA, y.VENCIMIENTO  "
        + "FROM (SELECT F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA, MAX(F.CUOTA) AS CUOTA_MAXIMA, "
        + "MIN(F.CUOTA) AS CUOTA_MINIMA, sum(F.SALDO) AS MONTO_FACTURA, min(F.SALDO) AS SALDO_CUOTA, "
        + "min(F.TOTAL) AS TOTAL,min(F.FECHA) AS FECHA, min(F.VENCIMIENTO) AS VENCIMIENTO FROM "
        + this.TABLE_FACTURAS + " F " + "WHERE F.ID_CLIENTE=? "
        + "GROUP BY F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA) y "
        + "INNER JOIN CBV_MONEDAS M ON y.ID_MONEDA = M.ID_MONEDA "
        + "ORDER BY y.TOTAL ASC " + "LIMIT ?" + " OFFSET ?";
    }
    return this.bd.getDataBase().executeSql(query, params).then(async data => {
      //console.log("FACTURAS LOCALES TRAIDAS: ", data.rows.length);
      let facturas: Array<Factura> = [];
      for (let index = 0; index < data.rows.length; index++) {
        console.log('            ', data.rows.item(index));
        let fac = new Factura(data.rows.item(index), cliente);
        let monto_recibo = await this.recibosBD.recibosGenerados(parseInt(fac.id));
        console.log('monto recibido ', monto_recibo)
        fac.saldo = <number>(data.rows.item(index).MONTO_FACTURA) - monto_recibo;
        fac.interes = this.interesService.calcularInteres(fac.fecha_vencimiento, fac.saldo);
        facturas.push(fac)
        console.log('fac a agregar a facturas', fac)
      }
      console.log('facturas select', facturas)
      return facturas;
    }).catch(error => {
      console.log('Error en la consulta de facturas local', error)
    })
  }
  
  

  isFCO(id_factura) {
    //tipoFAC = Factura::select('tipo').where('activo', true).where('id_factura', f['id_factura']).first().tipo;
    let query = "SELECT TIPO FROM " + this.TABLE_FACTURAS + " WHERE ID_FACTURA=? LIMIT 1";
    let params = [id_factura];
    return this.bd.getDataBase().executeSql(query, params).then(data => {
      console.log("TIPO DE FACTURA CON ID " + id_factura + " ES " + data.rows.item(0).TIPO);
      if (data.rows.item(0).TIPO == "FCO") {
        return true;
      }
      return false;
    }).catch(error => {
      console.error("Error trayendo tipo de factura " + id_factura, error);
      return false;
    })

  }

  find(id) {
    return this.bd.find(this.TABLE_FACTURAS, id).then(data => {
      return data
    }).catch(error => {
      return {}
    });
  }

  getFacturasFrom(id_factura, orderBy) {
    let query = "SELECT * FROM " + this.TABLE_FACTURAS + " WHERE ID_FACTURA=?";
    if (orderBy != null) {
      query = query + " ORDER BY ";
      Object.keys(orderBy).forEach(function (key) {
        query = query + key + orderBy[key];
      });
    }
    return this.bd.getDataBase().executeSql(query, [id_factura]).then(data => {
      console.log("TRAER FACTURAS CON ID_FACTURA = " + id_factura + ", cantidad: " + data.rows.length);
      let facturas: Array<Factura> = [];
      for (let index = 0; index < data.rows.length; index++) {
        console.log('FACTURA DE BD: ', data.rows.item(index));
        let fac = new Factura(data.rows.item(index), data.rows.item(index).ID_CLIENTE);
        facturas.push(fac)
      }
      return facturas;
    }).catch(error => {
      console.error("Error trayendo FACTURAS " + id_factura, error);
      return [];
    })
  }
  async todosCredito(detallesFac: Array<any>) {
    let checkCredito: boolean = true;
    console.log("Metodo para saber si todas las facturas son creditos ");
    for (let factura of detallesFac) {
      let tipo = await this.isFCO(factura.ID_FACTURA)
      if (tipo === true) {
        checkCredito = false;
        return checkCredito;
      } else {
        checkCredito = true;
      }
    }
    return checkCredito;
  }
  detallesFactura(id_factura) {
    let query_facturas = "SELECT MAX(CUOTA) as MAX_CUOTA, ID_MONEDA, NO_FACTURA "
      + "FROM CBV_FACTURAS WHERE ID_FACTURA=? GROUP BY ID_MONEDA, NO_FACTURA";
    return this.bd.getDataBase().executeSql(query_facturas, [id_factura]).then(data => {
      console.log("traer facturas = " + id_factura + ", cantidad: " + data.rows.length);

      if (data.rows.length > 0) {
        return data.rows.item(0)

      }
      return null;
    }).catch(error => {
      console.log('Error al obtener detall de la factura', error)
    })
  }
  deleteTableFactura() {
    let query = "DROP TABLE IF EXISTS " + this.TABLE_FACTURAS;
    return this.bd.getDataBase().executeSql(query, []).then(data => {
      console.log('se elimino la tabla factura', data);
      return true;
    }).catch(error => {
      console.log('no se elimino la tabla factura', error);
      return false;
    })
  }
}
