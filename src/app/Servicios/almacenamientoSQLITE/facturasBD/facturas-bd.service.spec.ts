import { TestBed } from '@angular/core/testing';
import { FacturasBDService } from './facturas-bd.service';

describe('FacturasBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturasBDService = TestBed.get(FacturasBDService);
    expect(service).toBeTruthy();
  });
});
