import { TestBed } from '@angular/core/testing';

import { ValorInteresService } from './valor-interes.service';

describe('ValorInteresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValorInteresService = TestBed.get(ValorInteresService);
    expect(service).toBeTruthy();
  });
});
