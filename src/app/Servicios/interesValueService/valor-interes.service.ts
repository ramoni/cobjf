import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import * as moment from 'moment'

@Injectable({
  providedIn: 'root'
})
export class ValorInteresService {

  public totalInteres: number;
  
  constructor(private http: HttpService,) {}

  getInteres(monto: number, fecha: string): number {
    this.totalInteres = 0;
    this.loadInteres(monto, fecha);

    return this.totalInteres;
  }
  loadInteres(monto: number, fecha: string) {

    return this.http.doGet('comisiones/' + monto + '/' + fecha+'?').then(res => {
      let data = JSON.parse(res.data);
      //console.log('data de comisiones para interes ',data)
      return Number(data[0].COMISION);
    })
    

  }
  calcularInteres(vencimientoFac, saldo): number {
    let interes: number = 0.0;
    
    let today = ((moment(new Date(),"DD-MM-YYYY")).toDate()).getTime();
    console.log('today 1', (moment(new Date(),"DD-MM-YYYY")).toDate())
    console.log('today 2', today)
    console.log('vencimiento factura 1', vencimientoFac,'         ', (moment(vencimientoFac, "DD-MM-YYYY")).toDate())
    let vencimiento = ((moment(vencimientoFac, "DD-MM-YYYY")).toDate()).getTime();
    console.log('vencimiento factura 2', vencimiento)
    if (today <= vencimiento) {
      return interes;
    } else {
      /*
      int dias = (int) ((now - vencimiento.getTime()) / 86400000);
				interes = round(dias * saldo * (0.05 / 100), 2);
      */
      let dias = <number>(Math.trunc(((today - vencimiento) / 86400000)));
      //interes = this.round(dias * saldo * (0.05 / 100), 2);

      let value: number = dias * saldo * (0.05 / 100)
      let factor = <number>Math.pow(10, 2);
      let val: number = Number((value * factor).toFixed(0));
      interes = <number>(val / factor);

      return interes;
    }
  }
}
