import { Injectable } from '@angular/core';
import { ValueType } from '../../Clases/value-type';
import { HttpService } from '../httpFunctions/http.service';
import { ValoresBDService } from '../almacenamientoSQLITE/valoresBD/valores-bd.service';

@Injectable({
    providedIn: 'root'
})
export class ValoresLogicaService {
    valores: Array<ValueType> = [];
    cotizacion: number;

    constructor(private http: HttpService,
        private valoresBD: ValoresBDService) {
    }

    getByIdOrName(idOrName: string): ValueType {
        this.getValoresBD()
        let toRet;
        if (isNaN(parseFloat(idOrName))) {
            // it's a description
            toRet = this.valores.find(mon => mon.descripcion === idOrName);
        } else {
            // it's a id
            toRet = this.valores.find(mon => mon.id === idOrName.toString());
        }
        console.log('getByIdOrName', toRet)
        return toRet;

    }

    getDefaultType() {

        /*let value: Array<ValueType> = JSON.parse(localStorage.getItem('valores'));
        return value[0];*/
        return this.valores[0]
    }

    getCotizacionOnline() {
        return this.http.doGet('cambio?', {}).then(res => {
            let data = JSON.parse(res.data);
            console.log("cotizacion ", data)
            this.cotizacion = data.FN_TIP_CAMBIO;
            localStorage.setItem('cotHoy', JSON.stringify(this.cotizacion));
            return data.FN_TIP_CAMBIO;
        })
    }
    getCotizacion() {
       
        if (localStorage.getItem('cotHoy')) {
            this.cotizacion = JSON.parse(localStorage.getItem('cotHoy'));
        }
        return Number(this.cotizacion);
    }
    getValoresOnline() {
        return this.http.doGet('valores?', { first_result: 0, last_result: 100 }).then(res => {
            let data = JSON.parse(res.data).lista;
            let toRet: Array<ValueType> = [];
            toRet = data.map(v => {
                return new ValueType(v);
            })
            this.valores = toRet;
            console.log('valores', this.valores)
            return toRet;
        })
    }
    getValoresBD() {
        return this.valoresBD.getValores().then(valores=>{
            this.valores=valores;
            return valores;
        });
        
    }

}


