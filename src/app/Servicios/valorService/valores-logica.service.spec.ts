import { TestBed } from '@angular/core/testing';

import { ValoresLogicaService } from './valores-logica.service';

describe('ValoresLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValoresLogicaService = TestBed.get(ValoresLogicaService);
    expect(service).toBeTruthy();
  });
});
