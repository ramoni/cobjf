import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file/ngx'

@Injectable({
  providedIn: 'root'
})
export class WriteFileService {

  constructor(private file: File) { }

  escribirLog(texto: string) {
    texto = "\n"+texto;
    //console.log("Texto para el log ", texto)
    this.file.checkFile(this.file.externalApplicationStorageDirectory, 'cobranzas_log.txt').then(existe => {
      ///console.log("existe el archivo", existe)
      this.file.writeFile(this.file.externalApplicationStorageDirectory, 'cobranzas_log.txt', texto, {append: true, replace: false}).then(escribe => {
        //console.log("escribe", escribe)
      }).catch(no_escribe => {
        //console.log("no escribe", no_escribe)
      })
    }).catch(no_existe => {
      //console.log("no existe el archivo", no_existe)
      this.file.writeFile(this.file.externalApplicationStorageDirectory, 'cobranzas_log.txt', texto).then(creo => {

      }).catch(no_creo => {
        console.log("no creo el archivo", no_creo)
      })

    });
  }

  crearArchivoImpresionRecibo(nombre_archivo, texto) {
    this.file.createDir(this.file.externalApplicationStorageDirectory, 'recibos', false).then(data => {
      console.log('se creo el directorio en ', this.file.externalApplicationStorageDirectory + "/recibos")
      this.escribirArchivoRecibo(nombre_archivo, texto)
    }).catch(error => {
      console.error('El directorio se habia creado y ya existe ', error)
      this.escribirArchivoRecibo(nombre_archivo, texto)
    })


  }
  escribirArchivoRecibo(nombre_archivo, texto) {
    console.log("NOMBRE DE ARCHIVO "+nombre_archivo);
    this.file.writeFile(this.file.externalApplicationStorageDirectory + "/recibos", nombre_archivo + '.txt', texto, { replace: true }).then(creo => {
      console.log('creo el archivo de recibo', creo)
    }).catch(no_creo => {
      console.log("no creo el archivo de recibo", no_creo)
    });
  }
}
