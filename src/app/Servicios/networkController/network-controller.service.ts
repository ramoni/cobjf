import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { WriteFileService } from '../writeFileService/logWrite.service';

export enum ConnectionStatus {
  Online,
  Offline
}

@Injectable({
  providedIn: 'root'
})
export class NetworkControllerService {

  private status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject(ConnectionStatus.Offline);

  constructor(
    private network: Network,
    private plt: Platform,
    private writeLog: WriteFileService
  ) {
    this.plt.ready().then(() => {

      this.initializeNetworkEvents();
      let status = this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
      this.status.next(status);
    });
  }

  public initializeNetworkEvents() {

    this.network.onDisconnect().subscribe(async () => {
      if (this.status.getValue() === ConnectionStatus.Online) {
        await this.writeLog.escribirLog(new Date() + ' NIVEL OFFLINE \n')
        console.log('NIVEL OFFLINE');
        this.updateNetworkStatus(ConnectionStatus.Offline);
      }
    });

    this.network.onConnect().subscribe(async () => {
      if (this.status.getValue() === ConnectionStatus.Offline) {
        await this.writeLog.escribirLog(new Date() + 'NIVEL ONLINE \n')
        console.log('NIVEL ONLINE');
        this.updateNetworkStatus(ConnectionStatus.Online);
      }
    });
  }

  private async updateNetworkStatus(status: ConnectionStatus) {
    this.status.next(status);

    let connection = status == ConnectionStatus.Offline ? 'Offline' : 'Online';
    await this.writeLog.escribirLog(new Date() + ' Trabajando con conexion ' + connection + ' \n')
    console.log('Trabajando con conexion ', connection)
  }

  public onNetworkChange(): Observable<ConnectionStatus> {
    return this.status.asObservable();
  }

  public getCurrentNetworkStatus(): ConnectionStatus {
    return this.status.getValue();
  }
}