import { TestBed } from '@angular/core/testing';

import { ClientesLogicaService } from './clientes-logica.service';

describe('LeadsLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientesLogicaService = TestBed.get(ClientesLogicaService);
    expect(service).toBeTruthy();
  });
});
