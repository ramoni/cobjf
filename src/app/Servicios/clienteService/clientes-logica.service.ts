import { Injectable } from '@angular/core';
import { Notas } from '../../Clases/notas';
import { HttpService } from '../httpFunctions/http.service';
import { Cliente } from '../../Clases/cliente';
import { Asignacion } from '../../Clases/asignacion';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { Observable } from 'rxjs';
import { HTTPResponse } from '@ionic-native/http/ngx';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientesLogicaService {
  public notas: Notas;
  constructor(
    private http: HttpService,
    private geoLogic: GeolocalizacionLogicaService,
  ) {
  }

  search(query: string, offset: number = 0): Promise<Array<Cliente>> {
    return this.http.doGet('clientes?', { filter: query, first_result: offset })
      .then(this._mapResult);
  }

  getVisitasOnline(offset: number, last_result: number, filter: string, tipo: string): Promise<Array<Cliente>> {

    let envio;
    if (filter === null) {
      //filter=' ';
      envio = { first_result: offset, last_result: last_result, cliente: filter, tipo: tipo }
    } else {
      envio = { first_result: offset, last_result: last_result, filter: filter, tipo: tipo }
    }

    return this.http.doGet('visitas?', envio).then(res => {
      let data = JSON.parse(res.data).lista;

      let toRet: Array<Cliente> = [];

      toRet = data.map(c => {
        return new Cliente(c);
      })

      console.log("actividades clientes", toRet)
      return toRet;
    });

  }

  addToClientes(cliente: Cliente, notas: string): Observable<HTTPResponse> {
    return this.geoLogic.getPos().pipe(mergeMap(data => {
      return this.http.doPost('visitas/nuevo?',
        new NewCobroRequest(cliente.id, notas, data.coords.latitude.toString(),
          data.coords.longitude.toString()), null);
    }));
  }

  _mapResult(res: any): Array<Cliente> {
    let data = res.data;
    let toRet: Array<Cliente> = [];
    for (let row of data.lista) {
      toRet.push(new Cliente(row));
    }
    return toRet;
  }


  guardarCliente(cliente: Cliente): Promise<any> {
    let toRet = new Asignacion({});
    if (cliente.facturas > 0) {
      toRet.visitas = true;
    } else {
      toRet.visitas = false;
    }
    toRet.entregas = false;
    toRet.id_cliente = parseInt(cliente.id);
    return this.http.doPost('visitas/nuevo?', toRet, null).then(
      res => {
        return JSON.parse(res.data);

      });

  }

  actualizarCliente(cliente: Cliente): Promise<any> {
    return this.http.doPut('clientes/' + cliente.id + '/actualizar?', { latitud: cliente.latitud, longitud: cliente.longitud }, null).then(
      res => {
        return JSON.parse(res.data);
      });

  }

}
