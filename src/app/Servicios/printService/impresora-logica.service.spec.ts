import { TestBed } from '@angular/core/testing';

import { ImpresoraLogicaService } from './impresora-logica.service';

describe('ImpresoraLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImpresoraLogicaService = TestBed.get(ImpresoraLogicaService);
    expect(service).toBeTruthy();
  });
});
