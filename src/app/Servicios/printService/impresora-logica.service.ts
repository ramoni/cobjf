import { Injectable } from '@angular/core';
import { Impresora } from '../../Clases/impresora';
import { StarPRNT } from '@ionic-native/star-prnt/ngx';
import { Observable, from } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ImpresoraLogicaService {

    constructor(public starPrint: StarPRNT) {
    }

    /**
     * getAllImpresoras
     */

    public getAllPrinters(): Promise<Array<Impresora>> {
        let impresoras: Array<Impresora> = [];
        return this.starPrint.portDiscovery('Bluetooth').then(printerList => {
            console.log("Lista de impresoras del plugin", printerList);
            impresoras = printerList.map(i => {
                return new Impresora(i.portName, i.macAddress, i.modelName);
            })
            console.log("impresoras parseadas ", impresoras)
            return (impresoras);

        }).catch(err => {
            console.log("error para obtener las impresoras", err)
            return Promise.reject('No hay impresoras disponibles, compruebe su conexion a Bluetooth');
        })

    }

    public printString(printer: Impresora, text: string): Observable<any> {
        return from(this.starPrint.printRawText(printer.portName, 'StarPRNTL', { text: text }).then(print => {
            console.log("print", print);
            return Promise.resolve(print);
        }).catch(err => {
            console.log("error print", err)
            return Promise.reject('Impresora no disponible');
        }))
    }
    public printerStatus(printer: Impresora): Promise<any> {
        return this.starPrint.checkStatus(printer.portName, "StarPRNTL").then(status => {
            console.log("status printer", status);
            return Promise.resolve(status);
        }).catch(error => {
            console.error('Impresora fuera de linea', error)
            return Promise.reject(error);
        })
    }


    /*public getAllPrinters(): Observable<Array<Impresora>> {

        if (!(<any>window).plugins.starPrinter) {
            console.log('No encuentra el plugin de la impresora')
            return throwError("No hay impresoras disponibles");
        }
        return Observable.create(observer => {
            (<any>window).plugins.starPrinter.portDiscovery('Bluetooth',
                function (error, printerList) {
                    console.log("Start printing", arguments);
                    if (error) {
                        observer.error(error);
                    } else {
                        observer.next(printerList);
                        observer.complete();
                    }
                }
            );
        });
    }

    public printString(printer: Impresora, text: string): Observable<any> {

        if (!(<any>window).plugins.starPrinter) {
            console.log('No encuentra el plugin de la impresora')
            return throwError("No hay impresoras disponibles");
        }
        return Observable.create(observer => {
            (<any>window).plugins.starPrinter.printReceipt(printer.name, text,
                function (error, result) {
                    console.log("Finishing printing", arguments);
                    if (error) {
                        observer.error("Por favor, verifique la conexion a su impresora y vuelva a intentarlo!");
                    } else {
                        observer.next(result);
                        observer.complete();
                    }
                }
            );
        });
    }
    public printerStatus() {
        if (!(<any>window).plugins.starPrinter) {
            console.log('No encuentra el plugin de la impresora')
            return throwError("No hay impresoras disponibles");
        }
        return Observable.create(observer => {
            (<any>window).plugins.starPrinter.checkStatus('Bluetooth', function (error, result) {
                if (error) {
                    observer.error("Por favor, verifique la conexion a su impresora y vuelva a intentarlo!");
                } else {
                    if (result.offline) {
                        observer.error('Impresora fuera de linea, verifique la conexion');
                    } else {
                        console.log(result.offline ? "printer is offline" : "printer is online");
                        observer.complete();
                        observer.next(result);
                    }
                }
            });
        });
    }*/
}

