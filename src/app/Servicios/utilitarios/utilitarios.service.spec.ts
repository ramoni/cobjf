import { TestBed } from '@angular/core/testing';

import { UtilitariosService } from './utilitarios.service';

describe('UtilitariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilitariosService = TestBed.get(UtilitariosService);
    expect(service).toBeTruthy();
  });
});
