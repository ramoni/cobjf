import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilitariosService {

  constructor() { }

  Unidades(num) {

    switch (num) {
      case 1: return 'UN';
      case 2: return 'DOS';
      case 3: return 'TRES';
      case 4: return 'CUATRO';
      case 5: return 'CINCO';
      case 6: return 'SEIS';
      case 7: return 'SIETE';
      case 8: return 'OCHO';
      case 9: return 'NUEVE';
    }

    return '';
  }//Unidades()

  Decenas(num) {

    let decena = Math.floor(num / 10);
    let unidad = num - (decena * 10);

    switch (decena) {
      case 1:
        switch (unidad) {
          case 0: return 'DIEZ';
          case 1: return 'ONCE';
          case 2: return 'DOCE';
          case 3: return 'TRECE';
          case 4: return 'CATORCE';
          case 5: return 'QUINCE';
          default: return 'DIECI' + this.Unidades(unidad);
        }
      case 2:
        switch (unidad) {
          case 0: return 'VEINTE';
          default: return 'VEINTI' + this.Unidades(unidad);
        }
      case 3: return this.DecenasY('TREINTA', unidad);
      case 4: return this.DecenasY('CUARENTA', unidad);
      case 5: return this.DecenasY('CINCUENTA', unidad);
      case 6: return this.DecenasY('SESENTA', unidad);
      case 7: return this.DecenasY('SETENTA', unidad);
      case 8: return this.DecenasY('OCHENTA', unidad);
      case 9: return this.DecenasY('NOVENTA', unidad);
      case 0: return this.Unidades(unidad);
    }
  }//Unidades()

  DecenasY(strSin, numUnidades) {
    if (numUnidades > 0)
      return strSin + ' Y ' + this.Unidades(numUnidades)

    return strSin;
  }//DecenasY()

  Centenas(num) {
    let centenas = Math.floor(num / 100);
    let decenas = num - (centenas * 100);

    switch (centenas) {
      case 1:
        if (decenas > 0)
          return 'CIENTO ' + this.Decenas(decenas);
        return 'CIEN';
      case 2: return 'DOSCIENTOS ' + this.Decenas(decenas);
      case 3: return 'TRESCIENTOS ' + this.Decenas(decenas);
      case 4: return 'CUATROCIENTOS ' + this.Decenas(decenas);
      case 5: return 'QUINIENTOS ' + this.Decenas(decenas);
      case 6: return 'SEISCIENTOS ' + this.Decenas(decenas);
      case 7: return 'SETECIENTOS ' + this.Decenas(decenas);
      case 8: return 'OCHOCIENTOS ' + this.Decenas(decenas);
      case 9: return 'NOVECIENTOS ' + this.Decenas(decenas);
    }

    return this.Decenas(decenas);
  }//Centenas()

  Seccion(num, divisor, strSingular, strPlural) {
    let cientos = Math.floor(num / divisor)
    let resto = num - (cientos * divisor)

    let letras = '';

    if (cientos > 0)
      if (cientos > 1)
        letras = this.Centenas(cientos) + ' ' + strPlural;
      else
        letras = strSingular;

    if (resto > 0)
      letras += '';

    return letras;
  }//Seccion()

  Miles(num) {
    let divisor = 1000;
    let cientos = Math.floor(num / divisor)
    let resto = num - (cientos * divisor)

    let strMiles = this.Seccion(num, divisor, 'UN MIL', 'MIL');
    let strCentenas = this.Centenas(resto);

    if (strMiles == '')
      return strCentenas;

    return strMiles + ' ' + strCentenas;
  }//Miles()

  Millones(num) {
    let divisor = 1000000;
    let cientos = Math.floor(num / divisor)
    let resto = num - (cientos * divisor)

    let strMillones = this.Seccion(num, divisor, 'UN MILLON ', 'MILLONES ');
    let strMiles = this.Miles(resto);

    if (strMillones == '')
      return strMiles;

    return strMillones + ' ' + strMiles;
  }//Millones()

  numeroALetras(num, currency) {
    console.log("NUM ORIGINAL", num);
    num = Number(num.replace(',', '.'))

    let data = {
      numero: num,
      enteros: Math.floor(num),
      centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
      letrasCentavos: '',
      letrasMonedaPlural: currency.plural,
      letrasMonedaSingular: currency.singular,
      letrasMonedaCentavoPlural: 'CENTAVOS',
      letrasMonedaCentavoSingular: 'CENTAVO'
    };

    console.log("NUM: ", num, "DATA ", data);

    if (data.centavos > 0) {
      let centavos = ''
      if (data.centavos == 1)
        centavos = this.Millones(data.centavos) + ' ' + data.letrasMonedaCentavoSingular;
      else
        centavos = this.Millones(data.centavos) + ' ' + data.letrasMonedaCentavoPlural;
      data.letrasCentavos = 'CON ' + centavos
    };
    let resp = "";
    if (data.enteros == 0) {
      resp = 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    } else if (data.enteros == 1) {
      resp = this.Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
    } else {
      resp = this.Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    }
    console.log("NUMERO A LETRAS: ", resp);
    return resp;
  }
  round(value, places) {
    /*
    if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
    */

    let factor = <number>Math.pow(10, places);
    value = Number((value * factor).toFixed(0));
    return <number>(value / factor);
  }
  limpiar_caracteres_especiales(palabra: string) {

    /*****  Caracteres validos
     * 
     *  0-9 a-z A-Z ; : ! ? # $ % & , . @ _ - = Space / * + ~ ^ [ { ( ] } ) | \
     * 
    *****/
    console.log("Palabra recibida", palabra);
    if (palabra === "") {
      console.log("Palabra vacia");
      return "";
    } else {
      palabra = palabra.replace(/ñ/g, "n");
      palabra = palabra.replace(/Ñ/g, "N");
      palabra = palabra.replace(/À/g, "A");
      palabra = palabra.replace(/Á/g, "A");
      palabra = palabra.replace(/Ã/g, "A");
      palabra = palabra.replace(/Â/g, "A");
      palabra = palabra.replace(/Ä/g, "A");
      palabra = palabra.replace(/Å/g, "A");
      palabra = palabra.replace(/È/g, "E");
      palabra = palabra.replace(/É/g, "E");
      palabra = palabra.replace(/Ê/g, "E");
      palabra = palabra.replace(/Ë/g, "E");
      palabra = palabra.replace(/Ì/g, "I");
      palabra = palabra.replace(/Í/g, "I");
      palabra = palabra.replace(/Î/g, "I");
      palabra = palabra.replace(/Ï/g, "I");
      palabra = palabra.replace(/Ò/g, "O");
      palabra = palabra.replace(/Ó/g, "O");
      palabra = palabra.replace(/Ô/g, "O");
      palabra = palabra.replace(/Õ/g, "O");
      palabra = palabra.replace(/Õ/g, "O");
      palabra = palabra.replace(/Ù/g, "U");
      palabra = palabra.replace(/Ú/g, "U");
      palabra = palabra.replace(/Û/g, "U");
      palabra = palabra.replace(/Ü/g, "U");
      palabra = palabra.replace(/Ý/g, "Y");
      palabra = palabra.replace(/à/g, "a");
      palabra = palabra.replace(/á/g, "a");
      palabra = palabra.replace(/â/g, "a");
      palabra = palabra.replace(/ã/g, "a");
      palabra = palabra.replace(/ä/g, "a");
      palabra = palabra.replace(/å/g, "a");
      palabra = palabra.replace(/è/g, "e");
      palabra = palabra.replace(/é/g, "e");
      palabra = palabra.replace(/ê/g, "e");
      palabra = palabra.replace(/ë/g, "e");
      palabra = palabra.replace(/ì/g, "i");
      palabra = palabra.replace(/í/g, "i");
      palabra = palabra.replace(/î/g, "i");
      palabra = palabra.replace(/ï/g, "i");
      palabra = palabra.replace(/ð/g, "o");
      palabra = palabra.replace(/ò/g, "o");
      palabra = palabra.replace(/ó/g, "o");
      palabra = palabra.replace(/ô/g, "o");
      palabra = palabra.replace(/õ/g, "o");
      palabra = palabra.replace(/ö/g, "o");
      palabra = palabra.replace(/ù/g, "u");
      palabra = palabra.replace(/ú/g, "u");
      palabra = palabra.replace(/û/g, "u");
      palabra = palabra.replace(/ü/g, "u");
      palabra = palabra.replace(/ý/g, "y");
      palabra = palabra.replace(/ÿ/g, "y");
      palabra = palabra.replace(/ª/g, "a");
      palabra = palabra.replace(/º/g, "o");
      palabra = palabra.replace(/¬/g, "-");
      palabra = palabra.replace(/`/g, "'");
      palabra = palabra.replace(/´/g, "'");

      console.log("Palabra reemplazada ", palabra);
      return palabra;
    }

  }

  formatearNumber(numero: number) {
    console.log('numero en formatearNumber', numero)
    let num: string = null;
    num = numero.toString();
    let separador = "."; // separador para los miles
    let sepDecimal = ','; // separador para los decimales
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
    }
    console.log('retorno formatearNumber', splitLeft + splitRight)
    return splitLeft + splitRight;
  }

}