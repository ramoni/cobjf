import { TestBed } from '@angular/core/testing';

import { ActividadesLocalesService } from './actividades-locales.service';

describe('ActividadesLocalesBDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActividadesLocalesService = TestBed.get(ActividadesLocalesService);
    expect(service).toBeTruthy();
  });
});
