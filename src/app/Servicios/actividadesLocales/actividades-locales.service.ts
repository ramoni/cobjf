import { Injectable } from '@angular/core';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';

@Injectable({
  providedIn: 'root'
})
export class ActividadesLocalesService {

  constructor(
    private userService: UsuariosBDService,
    private recibosService: RecibosBDService,
    private entregasService: EntregasDocBDService,
    private notasService: NotasBDService
  ) { }

  async mostrarActividadesLocales(habilitacion: number): Promise<any> {
    let datosActualizados = {};
    let datosOnline = await this.userService.getUserData();
    let recibosSync: number = await this.recibosService.cantidadRecibosSync(habilitacion);
    let entregasSync: number = await this.entregasService.cantidadEntregaDocSync(habilitacion);
    let notasLocalesSync = await this.notasService.cantidadNotasSync(habilitacion);

    let totalRecibos = 0;
    let totalEntregas = 0;
    let totalNotasVisitas = 0;
    let totalNotasEntregas = 0;


    let userData = datosOnline["USUARIO"];
    console.log("USERDATA ", userData);

    totalRecibos = parseInt(userData["RECIBOS"]) + recibosSync;
    totalEntregas = parseInt(userData["ENTREGAS"]) + entregasSync;
    totalNotasVisitas = notasLocalesSync["NOTAS_VISITAS"] + parseInt(userData["NOTAS_VISITAS"]);
    totalNotasEntregas = notasLocalesSync["NOTAS_ENTREGAS"] + parseInt(userData["NOTAS_ENTREGAS"]);

    datosActualizados["RECIBOS"] = totalRecibos;
    datosActualizados["ENTREGAS"] = totalEntregas;
    datosActualizados["NOTAS_VISITAS"] = totalNotasVisitas;
    datosActualizados["NOTAS_ENTREGAS"] = totalNotasEntregas;

    console.log("DATOS ACTUALIZADOS ", datosActualizados)
    return datosActualizados;
  }
}
