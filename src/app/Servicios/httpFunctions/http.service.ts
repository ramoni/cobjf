import { Injectable } from '@angular/core';
import { ConfigService } from '../configuracion/config.service';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private config: ConfigService, private http: HTTP) { }

  headers: any;
  doPost(method: string, body: any, queryParams: any = null): Promise<HTTPResponse> { //Observable<Response> {
    let url = this.config.getUrl('post') + '/' + method;
    for (var propertyName in queryParams) {
      url += propertyName + '=' + queryParams[propertyName] + '&';
    }
    url = url.substring(0, url.length - 1)
    console.log('POST - Query to', url);
    this.http.clearCookies();
    this.http.setDataSerializer('json');
    this.headers = {
      'Content-Type': this.config.MEDIA_TYPE,
      'Accept': 'application/json'
    }
    if (localStorage.getItem(this.config.AUTHORIZATION_STRING)) {
      this.headers = {
        'Content-Type': this.config.MEDIA_TYPE,
        'Accept': 'application/json',
        'Authorization': localStorage.getItem(this.config.AUTHORIZATION_STRING)
      }
    }
    return this.http.post(url, body, this.headers).then(
      response => {

        let line_body = Object.keys(body).length;
        //console.log("imprime line_body ", line_body);
        let cantidad_enviada = new Buffer(JSON.stringify(body)).length + line_body;
        console.log('Total cantidad enviada', cantidad_enviada);

        if (localStorage.getItem('subida')) {
          let cantidad_actual = Number(localStorage.getItem('subida'));
          cantidad_actual = cantidad_actual + cantidad_enviada;
          localStorage.setItem('subida', JSON.stringify(cantidad_actual));
        } else {
          localStorage.setItem('subida', JSON.stringify(cantidad_enviada));
        }

        let headers = response.headers;
        console.log('cabecera ', headers)
        let cantidad_bajada = Number(headers['content-length']);
        if (localStorage.getItem('bajada')) {
          let cantidad_actual = Number(localStorage.getItem('bajada'));
          cantidad_actual += cantidad_enviada;
          localStorage.setItem('bajada', JSON.stringify(cantidad_actual));
        } else {
          localStorage.setItem('bajada', JSON.stringify(cantidad_bajada));
        }
        return Promise.resolve(response)
      }).catch(error => {

        console.log("error en post ", error)
        return Promise.reject(error)
      });
 
  }

  doGet(method: string, queryParams: any = null, body = {}): Promise<any> {
    let url = this.config.getUrl('get') + '/' + method;
    if (queryParams) {
      for (var propertyName in queryParams) {
        url += propertyName + '=' + queryParams[propertyName] + '&';
      }
    }
    url = url.substring(0, url.length - 1)
    console.log('GET - Query to', url, queryParams)
    this.headers = {
      'Content-Type': this.config.MEDIA_TYPE,
      'Accept': 'application/json',
      'Authorization': localStorage.getItem(this.config.AUTHORIZATION_STRING)
    }
    return this.http.get(url, body, this.headers).then(
      response => {
        let headers = response.headers;

        let cantidad = Number(headers['content-length']);
        if (localStorage.getItem('bajada')) {

          let cantidad_actual = Number(localStorage.getItem('bajada'));
          cantidad_actual += cantidad;
          //console.log(cantidad_actual);
          localStorage.setItem('bajada', JSON.stringify(cantidad_actual));
        } else {
          localStorage.setItem('bajada', JSON.stringify(cantidad));
        }
        return Promise.resolve(response)

      }).catch(error => {
        console.log("error en el metodo get de " + method + error)
        return Promise.reject(error);
      });
    //.timeout(25000, new Error('El servidor no esta disponible, intente nuevamente !'))
  }

  doPut(method: string, body: any, queryParams: any = null): Promise<any> {

    let url = this.config.getUrl('put') + '/' + method;
    if (queryParams) {
      for (var propertyName in queryParams) {
        url += propertyName + '=' + queryParams[propertyName] + '&';
      }
    }
    url = url.substring(0, url.length - 1)
    console.debug('PUT - Query to', url)
    this.headers = {
      'Content-Type': this.config.MEDIA_TYPE,
      'Accept': 'application/json',
      'Authorization': localStorage.getItem(this.config.AUTHORIZATION_STRING)
    }
    return this.http.put(url, body, this.headers)
      .then(response => {
        let line_body = Object.keys(body).length;
        //console.log("imprime line_body ", line_body);
        let cantidad_enviada = new Buffer(JSON.stringify(body)).length + line_body;
        console.log('Total cantidad enviada', cantidad_enviada);

        if (localStorage.getItem('subida')) {
          let cantidad_actual = Number(localStorage.getItem('subida'));
          cantidad_actual = cantidad_actual + cantidad_enviada;
          localStorage.setItem('subida', JSON.stringify(cantidad_actual));
        } else {
          localStorage.setItem('subida', JSON.stringify(cantidad_enviada));
        }

        let headers = response.headers;

        let cantidad_bajada = Number(headers['content-length']);
        if (localStorage.getItem('bajada')) {
          let cantidad_actual = Number(localStorage.getItem('bajada'));
          cantidad_actual += cantidad_enviada;
          localStorage.setItem('bajada', JSON.stringify(cantidad_actual));
        } else {
          localStorage.setItem('bajada', JSON.stringify(cantidad_bajada));
        }

        return Promise.resolve(response);
      }).catch(error => {

        console.log(error)
        return Promise.reject(error);
      });
    //,timeout(30000));
  }
}
