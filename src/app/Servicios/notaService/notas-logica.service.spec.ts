import { TestBed } from '@angular/core/testing';

import { NotasLogicaService } from './notas-logica.service';

describe('NotasLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotasLogicaService = TestBed.get(NotasLogicaService);
    expect(service).toBeTruthy();
  });
});
