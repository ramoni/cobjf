import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { Observable } from 'rxjs';
import { Notas } from 'src/app/Clases/notas';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { map, flatMap } from 'rxjs/operators';
import { NewNotasResponse } from 'src/app/Clases/new-notas-response';
import { MensajeResponse } from 'src/app/Clases/mensaje-response';
import { NuevaNota } from 'src/app/Clases/nueva-nota';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { SincronizacionService } from '../sincronizacionService/sincronizacion.service';
import { ToastControllerService } from '../toastController/toast-controller.service';

@Injectable({
  providedIn: 'root'
})
export class NotasLogicaService {
  public notas: Notas;

  constructor(
    private geoLogic: GeolocalizacionLogicaService,
    private http: HttpService,
    private notasBDService: NotasBDService,
    private sincronizacion: SincronizacionService,
    private toast: ToastControllerService,
  ) { }

  addOrUpdateNotas(data: Notas): Observable<NewNotasResponse> {
    let toAdd = new NuevaNota(
      data.id,
      data.id_cliente,
      data.nota,
      data.latitud,
      data.longitud,
      data.tipo,
      data.habilitacion_caja,
      localStorage.getItem('imei')
    );
    if (!data.id) {
      return this.geoLogic.getPos().pipe(
        map(data => {
          console.log(data);

          if (data !== null) {
            console.log('Entrega o visita con ubicacion');

            toAdd.latitud = String(data.coords.latitude);
            toAdd.longitud = String(data.coords.longitude);

            console.log(toAdd);
            let documentData = new NewNotasResponse();
            return documentData;
          }
          else {
            console.log('Entrega o visita sin ubicacion');

            toAdd.latitud = "";
            toAdd.longitud = "";
          }
          console.log(toAdd);
          let documentData = new NewNotasResponse();
          console.log(documentData);
          return documentData;
        }), flatMap((req: NewNotasResponse) => {
          //Aca se cambia a offline
          console.log("DATOS PARA AGREGAR NOTA", toAdd);

          return this.notasBDService.setNotaConMensaje(toAdd).then(response => {
            console.log('response al guardar nota', response)
            req.message = new MensajeResponse(response);
            console.log(req);
            this.sincronizacion.sincronizarNotas(Number(JSON.parse(localStorage.getItem('habilitacion'))))
            this.toast.presentToastAlert('Sincronizando...')
            return req;
          });

        }
        )
      )
    } else {
      return this.geoLogic.getPos().pipe(
        map(data => {

          if (data !== null) {
            console.log('Entrega o visita con ubicacion');

            toAdd.latitud = String(data.coords.latitude);
            toAdd.longitud = String(data.coords.longitude);

            console.log(toAdd);
            let documentData = new NewNotasResponse();
            return documentData;
          }
          else {
            console.log('Entrega o visita sin ubicacion');

            toAdd.latitud = "";
            toAdd.longitud = "";
          }
          console.log(toAdd);
          let documentData = new NewNotasResponse();
          console.log(documentData);
          return documentData;
        }), flatMap((req: NewNotasResponse) => {
          console.log("ACTUALIZAR NOTA ", toAdd);
          return this.notasBDService.updateNotaConMensaje(toAdd, [['ID', '=', toAdd.id]]).then(response => {
            console.log('response al actualizar nota', response)
            req.message = new MensajeResponse(response);
            console.log(req);
            this.sincronizacion.sincronizarNotas(Number(JSON.parse(localStorage.getItem('habilitacion'))));
            this.toast.presentToastAlert('Sincronizando...')
            return req;
          });
          /*
          return from(this.http.doPut('notas/' + data.id + '/editar?', toAdd)
            .then(response => {
              console.log('response al editar nota', response)
              req.message = new MensajeResponse(JSON.parse(response.data));
              console.log(req);
              return req;

            })
          )*/
        }))
    }
  }

  /*getNotas(id: number): Notas {
    if (!this.notas)
      this.loadList(id);
    return this.notas;
  }*/


  /*loadList(id: number) {
  
    if (localStorage.getItem('notas')) {

      this.notas = JSON.parse(localStorage.getItem('notas'));
    }
    return this.http.doGet('clientes/' + id + '/notas?').then(res => {
      let data = JSON.parse(res.data);
      this.notas = data

      localStorage.setItem('notas', JSON.stringify(this.notas));
      return this.notas;
    });
  }*/
  /*loadNotaCobrador(idCliente: number, tipo: string): Promise<Notas> {
    return this.http.doGet('clientes/' + idCliente + '/notas_cobradores?', { tipo: tipo }).then(resp => {
      let data = JSON.parse(resp.data);
      console.log('notas cobrador ', data);
      let toRet;
      if (data.length > 0) {
        toRet = new Notas(data[0]);
        //console.log('mayor a cero')
      } else {
        ///console.log('vacio')
        toRet = new Notas("");
      }
      //console.log()
      return toRet;

    })
  }*/
  loadNotaCobrador(idCliente: number, habilitacion_caja: number, tipo: string): Promise<Notas> {
    return this.notasBDService.getNotaCobrador(idCliente, habilitacion_caja, tipo);

  }
  getAllNotasOnline(habilitacion_caja) {
    return this.http.doGet('offline/habilitaciones/' + habilitacion_caja + '/notas?', { first_result: 0, last_result: 1000 }, {}).then(async resp => {
      let data = JSON.parse(resp.data);
      console.log('NOTAS ONLINE COBRADOR', data);
      let notas = data.lista;
      return notas;
    })

  }

  async guardarNotas(notas: any) {
    let existeNota;
    for (let nota of notas) {
      console.log("Preguntar si existe esta nota para el cliente, la habilitacion y el tipo");
      existeNota = await this.notasBDService.existeNotaLocal(nota.ID_CLIENTE, nota.HABILITACION_CAJA, nota.TIPO);
      if (existeNota == false) {
        console.log("Es una nota nueva que debemos de insertar");
        this.notasBDService.addNota(nota);
      } else {
        console.log("Ya existe la nota");
      }
    }
  }
}
