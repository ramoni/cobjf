import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { Cliente } from '../../Clases/cliente';
import { Documentos } from '../../Clases/documentos';
import { NewDocumentResponse } from '../../Clases/new-document-response';
import { MensajeResponse } from '../../Clases/mensaje-response';
import { DocumentData } from '../../Clases/document-data';
import { DocumentWithCheck } from '../../Clases/document-with-check';
import { Observable } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { SincronizacionService } from '../sincronizacionService/sincronizacion.service';
import { ToastControllerService } from '../toastController/toast-controller.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentosLogicaService {

  constructor(private http: HttpService,
    private geoLogic: GeolocalizacionLogicaService,
    private documentosBD: EntregasDocBDService, 
    private sincronizacion: SincronizacionService, 
    private toast:ToastControllerService) { }


  getList(cliente: Cliente, offset: number, last_result: number): Promise<Array<DocumentWithCheck>> {
    return this.http.doGet('clientes/' + cliente.id + '/entregas?', { first_result: offset, last_result: last_result }).then(res => {
      let data = JSON.parse(res.data);
      let toRet: Array<DocumentWithCheck> = [];
      console.log("documentos ", data)

      for (let row of data.lista) {
        let toDocument = new Documentos(row);

        if (toDocument.guardado === 1) {
          toRet.push(new DocumentWithCheck(toDocument, true));
        } else {
          toRet.push(new DocumentWithCheck(toDocument, false));
        }
      }

      return toRet;
    });
  }
  obtenerDocumentosLocal(cliente: Cliente, offset: number, last_result: number): Promise<Array<DocumentWithCheck>> {
    return this.documentosBD.getEntregasLocal(Number(cliente.id), offset, last_result).then((data: Array<Documentos>) => {
      let toRet: Array<DocumentWithCheck> = [];

      toRet = data.map(f => {
        if (f.guardado === 1) {
          return new DocumentWithCheck(f, true);
        } else {
          return new DocumentWithCheck(f, false);
        }
      })
      console.log('toRet entregas local', toRet)
      return toRet;
    })
  }


  obtenerEntregasDocOnline(clientes: Array<any>) {
    return this.http.doPost('offline/entregas?', { clientes: clientes }, {first_result:0,last_result:10000}).then(res => {
      let data = JSON.parse(res.data);
      
      let toRet: Array<any> = [];
      toRet = data.lista.map(ed => {
        return ed;
      })
      console.log('ENTREGAS ONLINE: ',toRet)
      return toRet;
    })
  }


  addOrUpdate(entregas: Array<Documentos>): Observable<NewDocumentResponse> {
    let toAdd = new DocumentData(
      entregas.map(f => {
        return {
          id_entrega: f.id_entrega,
          id_cobrador: f.id_cobrador,
          id_cliente: f.id_cliente,
          id_entrega_det: parseInt(f.id_entrega_det),
          id_documento: parseInt(f.id_documento),
          no_documento: f.no_documento,
          id_moneda: f.id_moneda,
          importe: f.importe,
          guardado: f.guardado,
          fecha_documento: f.fecha_documento,
          latitud: f.latitud,
          longitud: f.longitud,
          imei: f.imei,
          habilitacion_caja:Number(JSON.parse(localStorage.getItem('habilitacion')))

        }
      }),
    );
    return this.geoLogic.getPos().pipe(
      map(data => {
        console.log(data);
        if (data !== null) {
          console.log('Entrega de la factura con ubicacion');
          for (let row of toAdd.entregas) {
            row.latitud = String(data.coords.latitude);
            row.longitud = String(data.coords.longitude);
          }
          //console.log(toAdd);
          let documentData = new NewDocumentResponse();
          return documentData;
        } else {
          console.log('Entrega del recibo sin ubicacion');
          for (let row of toAdd.entregas) {
            row.latitud = "";
            row.longitud = "";
          }
          //console.log(toAdd);
          let documentData = new NewDocumentResponse();
          //console.log(documentData);
          return documentData;
        }
      }), flatMap((req: NewDocumentResponse) => {
        console.log('enviado para entrega de documentos', toAdd)
        return this.documentosBD.entregarDocumentos(toAdd)
        //return this.http.doPost('entregas/nuevo?', toAdd)
          .then(response => {
            console.log('response entrega de documentos', response)
            req.message = new MensajeResponse(response);
            console.log('documentResponse ', req);
            this.toast.presentToastAlert('Sincronizando...')
            this.sincronizacion.sincronizarEntregas(Number(JSON.parse(localStorage.getItem('habilitacion'))));
            return req;
            
          }).catch(error => {
            return (error.error)
          }
          )
      })
    )

  }

}
