import { TestBed } from '@angular/core/testing';

import { DocumentosLogicaService } from './documentos-logica.service';

describe('DocumentosLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentosLogicaService = TestBed.get(DocumentosLogicaService);
    expect(service).toBeTruthy();
  });
});
