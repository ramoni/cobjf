import { TestBed } from '@angular/core/testing';

import { ReciboRequestService } from './recibo-request.service';

describe('ReciboRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReciboRequestService = TestBed.get(ReciboRequestService);
    expect(service).toBeTruthy();
  });
});
