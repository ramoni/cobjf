import { Injectable } from '@angular/core';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { ConfigService } from '../configuracion/config.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { FacturasLogicaService } from '../facturasService/facturas-logica.service';
import { MonedaLogicaService } from '../monedaService/moneda-logica.service';
import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class ReciboRequestService {
    //Por defecto
    public fecha: string
    public id_cliente: number
    public id_moneda: number
    public cotizacion: number
    public interes: number
    public total_recibo: number
    public anticipo: number
    public version: string
    public facturas: Array<{
        id_factura:number,monto:number
    }>
    public valores: Array<{
        id_tipo_valor:number, numero_valor: string,
        id_moneda: number, monto: number
        id_entidad: number, id_cuenta_bancaria:string,
        fecha_emision: string,valorizado: number
    }>
    public latitud: string
    public longitud: string
    public marca: string
    public id_concepto: number
    public imei: string
    //Agregados, truqueados
    public totalFacturas: Number = 0
    public totalValores: Number = 0
    public moneda: any
    public nro_recibo: any
    public valid: Boolean = true
    public message: string = ""
    public cobrador: any
    public usuario: any
    public habilitacion: any

    constructor( 
        private monedaDBservice: MonedasBDService, 
        private userDBservice: UsuariosBDService, 
        private reciboDBservice: RecibosBDService,
        private facturaService: FacturasLogicaService,
        private monedaService: MonedaLogicaService) {
    }

    async formatData(data){
        //Inicializar con datos por defecto que es de Recibos-DTO
        this.initValues(data);

        //Fechas y horas
        let fecha = this.fecha;

        //Cobrador
        let usuario = await this.userDBservice.getUserData();

        //Datos generales
        let marca = data.marca;
        let moneda = await this.monedaDBservice.find(data.id_moneda);
        moneda.ID_MONEDA = moneda.ID_MONEDA;
        let cotizacion = data.cotizacion != null ? data.cotizacion : null;
        let facturasPost = data.facturas ? data.facturas : [];
        let valoresPost = data.valores ? data.valores : [];

        //Se fixea el total de recibo
        this.total_recibo = parseFloat((this.total_recibo).toFixed(moneda.CANT_DECIMALES));

        console.log("MONEDA DE RECIBO A CREAR CON ID: "+data.id_moneda, moneda);

        await this.verificarReciboConMarca(usuario.ID_COBRADOR, marca);
        this.verificarFecha(fecha);
        this.verificarMoneda(moneda.ID_MONEDA);
        this.verificarCotizacion(cotizacion);
        this.verificarValores(moneda, cotizacion, valoresPost);
        this.verificarFacturas(facturasPost, moneda, cotizacion);

        //this.verificarHabilitacion(cobrador);
        this.verificarTodosLosValores();
        await this.verificarNroRecibo(usuario.USUARIO.ID_COBRADOR, facturasPost);

        //Carga de campos adicionales al request
        this.moneda = moneda;
        this.cobrador = usuario.USUARIO.ID_COBRADOR;
        this.usuario = usuario.USUARIO.NOMBRE;
        this.habilitacion = Number(JSON.parse(localStorage.getItem("habilitacion")));

        return this;
    }

    private async verificarReciboConMarca(id_cobrador, marca){
        if(this.valid) {
            //Traer recibo con esta marca
            let existe_recibo = await this.reciboDBservice.existeReciboConMarca(id_cobrador,marca);
            
            if(existe_recibo == true && marca != "") {
                //Verificar si existe recibo con esa marca
                this.valid = false;
                this.message = "Ya existe un recibo con la misma marca";
            }
        }
    }

    private verificarFecha(fecha){
        if(this.valid){
            if(fecha == null|| fecha==""){
                this.valid = false;
                this.message = "Fecha no valida";
            }
        }
    }

    private verificarMoneda(moneda){
        if(this.valid){
            if (moneda == null){
                //Verificar que tenga moneda
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar una moneda valida para el recibo" ;
            }
        }
    }

    private verificarCotizacion(cotizacion){
        if(this.valid) {
            if (cotizacion == null){
                //Verificar cotizacion
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar la cotizacion del dia";
            }
        }
    }

    private verificarValores(moneda, cotizacion, valoresPost){
        let totalValores = 0.0;
        if(this.valid) {
            let cantidadValores = valoresPost.length;
            let montoValor = 0.0;
            let montoValorizado = 0.00;

            //Si hay mas de un valor que agregar
            if (cantidadValores > 0) {

                for(let valor of valoresPost)
                {
                    if ( !( valor.hasOwnProperty('monto') && valor.hasOwnProperty('id_moneda') && valor.hasOwnProperty('id_tipo_valor') ) ) {
                        this.valid = false;
                        this.message = "Recibo no creado, verifique los valores  ingresados" ;
                        break;
                    } else {

                        montoValor = valor['monto'];
                        //Si el valor ingresado es de la misma moneda que el recibo entonces se toma el mismo valor
                        if (valor['id_moneda'] == moneda.ID_MONEDA) {
                            totalValores += montoValor;
                        } else {
                            //Sino se realiza la conversion de dolar a guaranies o viceversa
                            montoValorizado = this.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, montoValor, moneda.CANT_DECIMALES);
                            totalValores = parseFloat( (totalValores+montoValorizado).toFixed(moneda.CANT_DECIMALES) );
                        }
                    }
                }

            } else {
                //Si no hay ningun valor entonces no es valido
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar al menos un valor";
            }
        }

        this.totalValores = parseFloat(totalValores.toFixed(moneda.CANT_DECIMALES));
    }

    private verificarFacturas(facturasPost, moneda, cotizacion){
        //Verificar facturas
        let totalFacturas = 0.00;
        if(this.valid){
            let montofactura = 0.00;
            let monedaFactura;
            let cantidadFacturas = facturasPost.length;
            let montoValorizado = 0.00;

            //Si no hay ninguna factura entonces no es valida la insercion del recibo
            if (cantidadFacturas > 0) {
                //Obtener el total de facturas
                for(let facturas of facturasPost) {
                    montofactura = facturas['monto'];
                    monedaFactura = facturas['id_moneda'];

                    //Si el valor ingresado es de la misma moneda que el recibo entonces se toma el mismo valor
                    if (monedaFactura ==moneda.ID_MONEDA) {
                        totalFacturas += montofactura;
                    } else {
                        //Sino se realiza la conversion de dolar a guaranies o viceversa
                        montoValorizado = this.monedaDBservice.generateMontoValorizado(moneda.ID, cotizacion, montofactura, moneda.CANT_DECIMALES);
                        totalFacturas = parseFloat( (totalFacturas+montoValorizado).toFixed(moneda.CANT_DECIMALES) );
                    }
                }
            } else {
                //Si no hay ninguna factura entonces no es valido
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar al menos una factura";
            }
        }
        this.totalFacturas = parseFloat(totalFacturas.toFixed(moneda.CANT_DECIMALES));
    }

    private verificarTodosLosValores(){
        if(this.valid){
            console.log("TOTAL VALORES: ", this.totalValores, "TOTAL FACTURAS: ",this.totalFacturas);
            if ( this.totalFacturas > this.totalValores) {
                this.valid = false;
                this.message = "Recibo no creado, valores menor a total facturas";
            } else if (this.total_recibo != this.totalValores){
                this.valid = false;
                this.message = "Recibo no creado, el total calculado en la app no es correcto, debe ser "+this.totalValores+" y no "+this.total_recibo;
            }
        }
    }

    private async verificarNroRecibo(id_cobrador, facturas){
        this.nro_recibo = 0;
        let nro_recibo = 0;
        let recibo1 = 0;
        let recibo2 = 0;
        let isFCO = false;
        if(this.valid) {
            isFCO = await this.facturaService.verificarFacturaFCO(facturas);
            if(isFCO){
                nro_recibo = 0;
            } else {
                recibo1 = await this.userDBservice.getUltimoRecibo();
                recibo2 = await this.reciboDBservice.getUltimoReciboCobrador(id_cobrador);
                
                if(recibo1 == -1 || recibo2 == -1) {
                    this.valid = false;
                    this.message = "Recibo no creado, no posee numero de recibo";
                } else {
                    if(recibo1 > recibo2){
                        nro_recibo = recibo1 + 1;
                    } else {
                        nro_recibo = recibo2 + 1;
                    }
                    
                }
                this.nro_recibo = nro_recibo;
            }
        }
    }

    private initValues(data){
        console.log("DATO A INGRESAR PARA RECIBO NUEVO", data);
        this.fecha = data.fecha;
        this.id_cliente = data.id_cliente;
        this.id_moneda = data.id_moneda;
        this.cotizacion = data.cotizacion;
        this.interes = data.interes;
        this.total_recibo = data.total_recibo;
        this.anticipo = data.anticipo;
        this.version = data.version;
        this.facturas = data.facturas;
        this.valores = data.valores;
        this.latitud = data.latitud;
        this.longitud = data.longitud;
        this.marca = data.marca;
        this.id_concepto = data.id_concepto;
        this.imei = data.imei;
    }
}
