import { Injectable } from '@angular/core';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { HttpService } from '../httpFunctions/http.service';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { WriteFileService } from '../writeFileService/logWrite.service';
import { BDManagementService } from '../almacenamientoSQLITE/bdManagement/bdmanagement.service';

@Injectable({
  providedIn: 'root'
})
export class SincronizacionService {

  constructor(
    private recibosDBservice: RecibosBDService,
    private http: HttpService,
    private notasDBservice: NotasBDService,
    private entregasDBservice: EntregasDocBDService,
    private writeLog: WriteFileService,
  ) {

  }
  async sincronizarTodoManual() {
    if (localStorage.getItem('habilitacion')) {
      await this.writeLog.escribirLog(new Date() + ' Peticion DE INICIO DE APP: Sincronizacion de datos \n ');
      this.sincronizarEntregas(Number(JSON.parse(localStorage.getItem('habilitacion'))));
      this.sincronizarNotas(Number(JSON.parse(localStorage.getItem('habilitacion'))));
      this.sincronizarRecibos(Number(JSON.parse(localStorage.getItem('habilitacion'))));
    }

  }

  sincronizarTodo() {
    this.sincronizarTodoManual();
    setInterval(async () => {
      if (localStorage.getItem('habilitacion')) {
        await this.writeLog.escribirLog(new Date() + ' Peticion: Sincronizacion de datos de cada 3 minutos \n ');
        this.sincronizarEntregas(Number(JSON.parse(localStorage.getItem('habilitacion'))));
        this.sincronizarNotas(Number(JSON.parse(localStorage.getItem('habilitacion'))));
        this.sincronizarRecibos(Number(JSON.parse(localStorage.getItem('habilitacion'))));
      }
    }, 180000);
  }

  sincronizarRecibos(habilitacion: number) {
    console.log("Inicio de sincronizacion de recibos");
    return this.recibosDBservice.getRecibosASincronizar(habilitacion).then(async recibos => {
      if (recibos.length > 0) {
        let recibosObject = {
          recibos: recibos,
          habilitacion: habilitacion
        }
        await this.writeLog.escribirLog(new Date() + " Info: Recibos a sincronizar: " + JSON.stringify(recibosObject));
        return this.http.doPost('offline/recibos/nuevo?', recibosObject).then(async data => {
          let respJson = JSON.parse(data.data);
          console.log("Respuesta de la sincronizacion de recibos ", respJson.exito);
          //Se genera un vector de con las ids de los datos a cambiar su columna requiere_sincronizacion
          let ids = [];
          for (let recibo of recibos) {
            ids.push(recibo.ID_RECIBO_CAB);
          }
          //Actualiza en la base de datos local la columna de requiere sincronizacion
          this.recibosDBservice.actualizarSincronizacionDeRecibos(ids);
          await this.writeLog.escribirLog(new Date() + ' Exito: Recibos sincronizados exitosamente \n ');
          return true;
        }).catch(async error => {
          let errorJson = JSON.parse(error.error);
          console.error("Error en sincronizar recibos");
          console.error(errorJson.error);
          await this.writeLog.escribirLog(new Date() + ' Error: Error al sincronizar recibos \n Error: ' + JSON.stringify(errorJson));
          return false;
        });
      } else {
        await this.writeLog.escribirLog(new Date() + " Alerta: No hay recibos para sincronizar");
        console.log("No hay recibos para sincronizar");
        return true;
      }
    }).catch(async error => {
      await this.writeLog.escribirLog(new Date() + " Error: Traer recibos de la base de datos con error " + JSON.stringify(error));
      console.error(error);
      return false;
    });
  }

  sincronizarNotas(habilitacion: number) {
    console.log("Inicio de sincronizacion de notas");
    return this.notasDBservice.getNotasASincronizar(habilitacion).then(async notas => {
      if (notas.length > 0) {
        let notasObject = {
          notas: notas,
          habilitacion: habilitacion
        }
        await this.writeLog.escribirLog(new Date() + " Info: Notas a sincronizar: " + JSON.stringify(notasObject));
        console.log("NOTAS PARA SINCRONIZAR: ", notas);
        return this.http.doPost('offline/notas/nuevo?', notasObject).then(async data => {
          let respJson = JSON.parse(data.data);
          console.log("Respuesta de la sincronizacion de notas ", respJson.exito);
          //Se genera un vector de con las ids de los datos a cambiar su columna requiere_sincronizacion
          let ids = [];
          for (let nota of notas) {
            ids.push(nota.ID);
          }
          this.notasDBservice.actualizarSincronizacionDeNotas(ids);
          await this.writeLog.escribirLog(new Date() + ' Exito: Notas sincronizadas exitosamente \n ');
          return true;
        }).catch(async error => {
          let errorJson = JSON.parse(error.error);
          console.error("Error en sincronizar notas");
          console.error(errorJson.error);
          await this.writeLog.escribirLog(new Date() + ' Error: Error al sincronizar notas \n Error: ' + JSON.stringify(errorJson));
          return false;
        });
      } else {
        console.log("No hay notas para sincronizar");
        await this.writeLog.escribirLog(new Date() + " Alerta: No hay notas para sincronizar");
        return true;
      }
    }).catch(async error => {
      console.error(error);
      await this.writeLog.escribirLog(new Date() + " Error: Traer notas de la base de datos con error " + JSON.stringify(error));
      return false;
    });
  }

  sincronizarEntregas(habilitacion: number) {
    console.log("Inicio de sincronizacion de entregas");
    return this.entregasDBservice.getEntregasASincronizar(habilitacion).then(async entregas => {
      if (entregas.length > 0) {
        let entregasObject = {
          entregas: entregas,
          habilitacion: habilitacion
        }
        await this.writeLog.escribirLog(new Date() + " Info: Entregas a sincronizar " + JSON.stringify(entregasObject));
        return this.http.doPost('offline/entregas/nuevo?', entregasObject).then(async data => {
          let respJson = JSON.parse(data.data);
          console.log("Respuesta de la sincronizacion de entregas ", respJson.exito);

          //Se genera un vector de con las ids de los datos a cambiar su columna requiere_sincronizacion
          let ids = [];
          for (let entrega of entregas) {
            ids.push(entrega.ID);
          }

          this.entregasDBservice.actualizarSincronizacionDeEntregas(ids);
          await this.writeLog.escribirLog(new Date() + ' Exito: Entregas sincronizadas exitosamente \n ');
          return true;
        }).catch(async error => {
          let errorJson = JSON.parse(error.error);
          console.error("Error en sincronizar entregas");
          console.error(errorJson.error);
          await this.writeLog.escribirLog(new Date() + ' Error: Error al sincronizar entregas \n Error: ' + JSON.stringify(errorJson));
          return false;
        });
      } else {
        console.log("No hay entregas para sincronizar");
        await this.writeLog.escribirLog(new Date() + " Alerta: No hay entregas para sincronizar");
        return true;
      }
    }).catch(async error => {
      console.error(error);
      await this.writeLog.escribirLog(new Date() + " Error: Traer entregas de la base de datos con error " + JSON.stringify(error));
      return false;
    });
  }

}
