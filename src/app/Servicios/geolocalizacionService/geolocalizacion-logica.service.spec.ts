import { TestBed } from '@angular/core/testing';

import { GeolocalizacionLogicaService } from './geolocalizacion-logica.service';

describe('GeolocalizacionLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeolocalizacionLogicaService = TestBed.get(GeolocalizacionLogicaService);
    expect(service).toBeTruthy();
  });
});
