import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { ConfigService } from '../configuracion/config.service';
import { Http } from '@angular/http';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
@Injectable({
  providedIn: 'root'
})
export class GeolocalizacionLogicaService {

  error: String;

  constructor(private http: Http, 
    private config: ConfigService, 
    private geolocation: Geolocation) {
  }

  getCiudad(latitud: number, longitud: number): Observable<string> {

    let url = this.config.GEO_REVERSE_URL.replace("LATITUD", latitud.toString()).replace("LONGITUD", longitud.toString());
    console.log("para ciudad ", url)
    return this.http.get(url).pipe(map(data => data.json().results[0].formatted_address));
  }

  getData(latitud: number, longitud: number): Observable<any> {

    let url = this.config.GEO_REVERSE_URL.replace("LATITUD", latitud.toString()).replace("LONGITUD", longitud.toString());
    return this.http.get(url).pipe(map(data => data.json().results[0]));
  }
  getPos(): Observable<Geoposition> {
    let options = {
      enableHighAccuracy: false,
      timeout: 5000,
      maximumAge: 0,
    }
    return from(
      this.geolocation.getCurrentPosition(options).then((resp) => {
        console.log("posicion tomada correctamente")
        return resp;
      }).catch((error) => {
        console.error("error en obtener la posicion actual ",error)
        return null;
      }));
  }

  findCurrentCiudad(): Observable<string> {
    return this.getPos().pipe(mergeMap(pos => {
      console.log("latitud y longitud ", pos.coords.latitude,pos.coords.longitude)
      return this.getCiudad(pos.coords.latitude, pos.coords.longitude);
    }));
  }
  onError(error) {
    alert('code: ' + error.code + '\n' +
      'message: ' + error.message + '\n');

  }

}
