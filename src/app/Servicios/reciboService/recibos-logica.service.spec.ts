import { TestBed } from '@angular/core/testing';

import { RecibosLogicaService } from './recibos-logica.service';

describe('RecibosLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecibosLogicaService = TestBed.get(RecibosLogicaService);
    expect(service).toBeTruthy();
  });
});
