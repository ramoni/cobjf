import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { CreateReceiptData } from 'src/app/Clases/create-receipt-data';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { Moneda } from 'src/app/Clases/moneda';
import { ReciboDTO } from 'src/app/Clases/recibo-DTO';
import { Recibo } from 'src/app/Clases/recibo';
import { Factura } from 'src/app/Clases/factura';
import { InvoiceWithACobrar } from 'src/app/Clases/invoice-with-acobrar';
import { NewReceiptResponse } from 'src/app/Clases/new-receipt-response';
import { Cliente } from 'src/app/Clases/cliente';
import { ImpresoraLogicaService } from 'src/app/Servicios/printService/impresora-logica.service';
import { Observable, throwError, from } from 'rxjs';
import { map, flatMap, catchError } from 'rxjs/operators';
import { Impresora } from 'src/app/Clases/impresora';
import * as moment from 'moment';
import { ValoresLogicaService } from '../valorService/valores-logica.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { ValorRecibo } from 'src/app/Clases/valor-recibo';
import { ReciboRequestService } from '../reciboRequestService/recibo-request.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { ClientesBDService } from '../almacenamientoSQLITE/clientesBD/clientes-bd.service';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';
import { FacturasBDService } from '../almacenamientoSQLITE/facturasBD/facturas-bd.service';
import { EntidadesBDService } from '../almacenamientoSQLITE/entidadesBD/entidades-bd.service';
import { CuentasBDService } from '../almacenamientoSQLITE/cuentasBD/cuentas-bd.service';
import { ConceptosBDService } from '../almacenamientoSQLITE/conceptosBD/conceptos-bd.service';
import { Entidad } from 'src/app/Clases/entidad';
import { Cuenta } from 'src/app/Clases/cuenta';
import { Concepto } from 'src/app/Clases/concepto';
import { UtilitariosService } from '../utilitarios/utilitarios.service';
import { SincronizacionService } from '../sincronizacionService/sincronizacion.service';
import { ToastControllerService } from '../toastController/toast-controller.service';

@Injectable({
  providedIn: 'root'
})
export class RecibosLogicaService {

  public reciboCreado: CreateReceiptData;

  constructor(
    private http: HttpService,
    private geoLogic: GeolocalizacionLogicaService,
    private printLogic: ImpresoraLogicaService,
    private valorLogic: ValoresLogicaService,
    private recibosDBservice: RecibosBDService,
    private reciboRequestService: ReciboRequestService,
    private userBDService: UsuariosBDService,
    private clienteBD: ClientesBDService,
    private monedaBD: MonedasBDService,
    private facturasBD: FacturasBDService,
    private entidadBD: EntidadesBDService,
    private cuentaBD: CuentasBDService,
    private conceptoBD: ConceptosBDService,
    private utils: UtilitariosService,
    private sincronizacion: SincronizacionService,
    private toast: ToastControllerService,
  ) {
  }
  /**
   * Retorna un valor por defecto,
   * a ser usado como primer valor en un
   * nuevo recibo.
   */
  getDefaultValor(moneda: Moneda): ValorRecibo {
    console.log('Valor por defecto ', this.valorLogic.getDefaultType());
    let valueDefault = new ValorRecibo(this.valorLogic.getDefaultType(), moneda, "0", 0, 0, null, null, null, 0);
    console.log('Valor por defecto en el recibo ', valueDefault);
    return valueDefault;
    //return new Value(this.valorLogic.getDefaultType(), moneda, "0", 0, 0, null, null, null);
  }

  getDefaultReceipt(cliente: Cliente, facturas: Array<Factura>): Recibo {

    let toRet = new Recibo(cliente, {});
    toRet.nro_recibo = 'A definir';
    toRet.facturas = facturas.map(f => {
      return new InvoiceWithACobrar(f, f.saldo_cuota);
    });
    toRet.valores = [];
    toRet.id_moneda = '1';
    toRet.interes = 0;
    toRet.fecha = new Date();
    toRet.total = 0;
    toRet.estado = 'CREADA';
    if (localStorage.getItem('imei')) {
      toRet.version = localStorage.getItem('version');
    } else {
      toRet.version = 'undefined';
    }
    toRet.imei = localStorage.getItem('imei');
    return toRet;
  }

  getRecibosPorFactura(factura: Factura): Promise<Array<Recibo>> {

    return this.recibosDBservice.obtenerRecibosFactura(factura.id).then(recibos => {
      console.log('recibos de la factura ', recibos)
      let toRet: Array<Recibo> = [];
      for (let row of recibos) {
        let toAdd = new Recibo(factura.cliente, row)
        toAdd.facturas.push(new InvoiceWithACobrar(factura, 0));
        toRet.push(toAdd);
      }
      return toRet;
    });
  }

  anular(recibo: Recibo): Promise<any> {
    recibo.imei = localStorage.getItem('imei');
    return this.recibosDBservice.anular(recibo.id).then(resp => {
      if (resp) {
        this.sincronizacion.sincronizarRecibos(Number(JSON.parse(localStorage.getItem('habilitacion'))))
        return {
          "exito": "Recibo anulado con exito"
        }
      }
      return {
        "error": "No pudo ser anulado el recibo"
      }
    }).catch(error => {
      console.log('No pudo anularse el recibo', error)
      return {
        "error": "No pudo ser anulado el recibo"
      }
    });
  }

  _doPrint(value: CreateReceiptData): Observable<CreateReceiptData> {
    console.log('Sending to printer', value);
    return this.printLogic.printString(value.printer, value.print).pipe(map(response => {
      console.log('the printer return:', response);
      return value;
    }), catchError(error => {
      console.log("error al imprimir el recibo en impresora", error)
      return throwError(error)
    }))
  }

  print(recibo: Recibo, tipo: string): Observable<CreateReceiptData> {
    let data = new CreateReceiptData();
    data.id = parseInt(recibo.id);
    data.data = new NewReceiptResponse(
      {
        ID_RECIBO: parseInt(recibo.id),
        exito: "Recibo impreso con exito"
      }
    )
    return this._handlePrinters().pipe(map((impresora: Impresora) => {
      console.log('handlerprints', impresora);
      data.printer = impresora;
      return data;
    })/*, catchError(err => {
      console.log('error al encontrar impresoras disponibles', err)
      return throwError(err);
    })*/, flatMap((res: CreateReceiptData) => {
      console.log("data recibido siendo res", res)
      return from(this.crearImpresionRecibo(res.id, tipo).then(imprimir => {
        if (tipo === '1') {
          res.print = imprimir.ORIGINAL;
          return res;
        } else {
          res.print = imprimir.DUPLICADO;
          return res;
        }
      }).catch(error => {
        console.error('no pudo obtenerse la impresion del recibo', error)
        return Promise.reject('No se pudo obtener el recibo para la impresion');
      }))

    }), flatMap((res: CreateReceiptData) => {
      return this._doPrint(res).pipe(catchError(error => {
        console.log("error al imprimir el recibo en reimpresiones", error)
        return throwError(error)
      }));
    }), catchError(error => {
      console.error('Ocurrio un error inesperado al imprimir un recibo en reimpresion', error)
      return throwError(error);
    }))
  }

  addOrUpdate(recibo: Recibo): Observable<CreateReceiptData> {
    console.log('recibo recibido en logica de recibos ', recibo);
    let toAdd = new ReciboDTO(
      moment(recibo.fecha).format('DD-MM-YYYY'),
      parseInt(recibo.cliente.id),
      parseInt(recibo.id_moneda),
      recibo.cotizacion,
      recibo.interes,
      recibo.total,
      recibo.anticipo,
      recibo.version,
      recibo.facturas.map(f => {
        return {
          id_factura: parseInt(f.data.id),
          monto: f.a_cobrar,
          cuota: f.data.cuota_minima,
          id_moneda: f.data.id_moneda,
          saldo: f.data.saldo_cuota,
          saldo_total: f.data.saldo,
          tipo: f.data.tipo
        }
      }),
      recibo.valores.map(v => {
        return {
          id_tipo_valor: parseInt(v.tipo.id),
          numero_valor: v.numero,
          id_moneda: parseInt(v.moneda.id),
          monto: v.total,
          id_entidad: v.banco,
          id_cuenta_bancaria: v.cuenta,
          fecha_emision: v.fecha,
          valorizado: v.valorizado
        }
      }),

      recibo.latitud,
      recibo.longitud,
      recibo.marca,
      parseInt(recibo.concepto),
      recibo.imei,
    );

    return this.geoLogic.getPos()
      .pipe(map(data => {
        if (data !== null) {
          toAdd.latitud = "" + data.coords.latitude;
          toAdd.longitud = "" + data.coords.longitude;
          let receiptData = new CreateReceiptData();
          receiptData.dto = toAdd;
          console.log('recibo DTO ', toAdd);
          return receiptData;
        } else {
          toAdd.latitud = "";
          toAdd.longitud = "";
          let receiptData = new CreateReceiptData();
          receiptData.dto = toAdd;
          console.log('recibo DTO ', toAdd);
          return receiptData;
        }
      }),
        // obtener impresoras
        flatMap((req: CreateReceiptData) => {
          return this._handlePrinters().pipe(map(printer => {
            console.log('printer para la impresion', printer);
            req.printer = printer;
            return req;
          }), catchError(err => {
            console.error('No se encontro impresoras disponibles ', err)
            return throwError(err);
          }))
        }),
        // imprimir texto vacio
        flatMap((req: CreateReceiptData) => {
          return this.printLogic.printString(req.printer, "").pipe(map(res => {
            console.log('imprimiendo texto vacio para prueba')
            return req;
          }), catchError(err => {
            console.log('error al imprimir texto vacio', err)
            return throwError(err);
          }))
        }),
        // crear recibo
        flatMap((req: CreateReceiptData) => {
          console.log('lo que se envia para que se cree el recibo en recibos/nuevo', toAdd)
          return from(this.addRecibo(toAdd).then(res => {
            console.log('respuesta del recibos/nuevo ', res)
            req.data = new NewReceiptResponse(res);
            req.id = Number(req.data.id);
            localStorage.setItem('recibo', JSON.stringify(req));
            localStorage.setItem('reciboGenerado', JSON.stringify(req.id));
            return req;
          }).catch(error => {
            console.error('error al crear el recibo/nuevo', error);
            return Promise.reject(error.error)
          })
          )
        }), 
        //crear impresion del recibo
        flatMap((req: CreateReceiptData) => {
          return from(this.crearImpresionRecibo(req.id, '1').then(res => {
            req.data.reciboOriginal = res.ORIGINAL;
            return req;
          }).catch(async error => {
            //Eliminar recibo
            await this.recibosDBservice.borrarReciboEnCascada(req.id);
            localStorage.removeItem('recibo');
            localStorage.removeItem('reciboGenerado');
            console.error('error en creacion de la impresion del recibo', error);
            return Promise.reject(error);
          })
          )
        }),
        //imprimir el recibo
        flatMap((req: CreateReceiptData) => {
          return this._doPrintOriginal(req).pipe(
            catchError(error => {
              //Eliminar recibo
              this.recibosDBservice.borrarReciboEnCascada(req.id);
              localStorage.removeItem('recibo');
              localStorage.removeItem('reciboGenerado');
              console.error('catchError doPrintOriginal', error);
              return throwError(error);
            })
          )
        }), 
        catchError(error => {
          console.error('ocurrio un error inesperado en crear el recibo', error)
          return throwError(error);
        }))
  }
  printDuplicado(): Observable<CreateReceiptData> {
    let req = new CreateReceiptData();
    req = JSON.parse(localStorage.getItem('recibo'));
    console.log('printTime recibo', req);
    req.id = req.data.id
    return from(this.crearImpresionRecibo(req.id, '2').then(res => {
      req.data.reciboDuplicado = res.DUPLICADO;
      this.sincronizacion.sincronizarRecibos(Number(JSON.parse(localStorage.getItem('habilitacion'))))
      this.toast.presentToastAlert('Sincronizando...')
      return req;
    }).catch(error => {
      console.error('error en el metodo de creacion de la impresion del recibo duplicado', error);
      return throwError(error);
    })).pipe(map(data => {
      console.log('Se creo la impresion del recibo ')
      return data;
    }), flatMap((req: CreateReceiptData) => {
      return this._doPrintDuplicado(req).pipe(
        catchError(error => {
          console.error('catchError doPrintDuplicado', error);
          return throwError(error);
        })
      )
    }),
      catchError(error => {
        console.error('ocurrio un error inesperado en la creacion de recibo duplicado', error)
        return throwError(error)
      }))
  }

  _doPrintOriginal(data: CreateReceiptData): Observable<CreateReceiptData> {
    console.log('doPrintOriginal')
    data.print = data.data.reciboOriginal;
    return this.printLogic.printString(data.printer, data.print).pipe(map(response => {
      console.log('the printer return:', response);
      return data;
    }), catchError(error => {
      console.log("error al imprimir el recibo en impresora", error)
      return throwError(error)
    }))

  }
  _doPrintDuplicado(data: CreateReceiptData): Observable<CreateReceiptData> {
    console.log('doPrintDuplicado')
    data.print = data.data.reciboDuplicado;
    return this.printLogic.printString(data.printer, data.print).pipe(map(response => {
      console.log('the printer return:', response);
      return data;
    }), catchError(error => {
      console.log('error al imprimir el duplicado del recibo', error)
      return throwError(error);
    }))
  }
  _handlePrinters(): Observable<any> {
    return from(this.printLogic.getAllPrinters().then((printers: any) => {
      console.log("impresoras", printers);
      if (printers.length <= 0) {
        console.error('No hay impresoras emparejadas con tu dispositivo o no tiene conexion a Bluetooth');
        return Promise.reject('No hay impresoras emparejadas con tu dispositivo o no tiene conexion a Bluetooth')
      } else {
        if (printers.length > 1) {
          this.toast.presentToastAlert('Muchas impresoras emparejadas encontradas, utilizaremos la impresora ' + printers[0].portName + ' asegurese de tener conexion actual con la misma');
        }
        return Promise.resolve(printers[0]);
      }
    }).catch(err => {
      return Promise.reject(err);
    }))
    /*let impres: Impresora = null;
    return this.printLogic.printerStatus(impresora).then(x => {
      console.log('respuesta correcta printer status ', x)
      impres = impresora;
      return impres;
    }).catch(err => {
      console.error('error printer status', err)
      return null;
    });*/
    /*data = data.filter(d => d !== null);
    if (data.length === 0) {
      throw new Error('No hay impresoras disponibles, verifique la conexion a Bluetooh');
    }
    if (data.length > 1) {
      alert('Muchas impresoras encontradas, usando la primera');
    }
    return data[0];*/

    /*for (let impresora of printers) {
                req.printer = await this._handlePrinters(impresora);
                if(req.printer!=null){
                  console.log('printer que sera utilizada', req.printer)
                  break;
                }
                if ((printers.lastIndexOf(impresora) + 1) === printers.length) {
                  console.log('No hay impresoras en linea');
                  return throwError('No hay impresoras en linea');
                }
              }*/

  }
  addOrUpdateFCO(recibo: Recibo): Observable<CreateReceiptData> {

    let toAdd = new ReciboDTO(
      moment(recibo.fecha).format('DD-MM-YYYY'),
      parseInt(recibo.cliente.id),
      parseInt(recibo.id_moneda),
      recibo.cotizacion,
      recibo.interes,
      recibo.total,
      recibo.anticipo,
      recibo.version,
      recibo.facturas.map(f => {
        return {
          id_factura: parseInt(f.data.id),
          monto: f.a_cobrar,
          cuota: f.data.cuota_minima,
          id_moneda: f.data.id_moneda,
          saldo: f.data.saldo,
        }
      }),
      recibo.valores.map(v => {
        return {
          id_tipo_valor: parseInt(v.tipo.id),
          numero_valor: v.numero,
          id_moneda: parseInt(v.moneda.id),
          monto: v.total,
          id_entidad: v.banco,
          id_cuenta_bancaria: v.cuenta,
          fecha_emision: v.fecha,
          valorizado: v.valorizado

        }
      }),
      recibo.latitud,
      recibo.longitud,
      recibo.marca,
      parseInt(recibo.concepto),
      recibo.imei);


    return this.geoLogic.getPos()
      .pipe(map(data => {
        if (data !== null) {
          toAdd.latitud = "" + data.coords.latitude;
          toAdd.longitud = "" + data.coords.longitude;
          let receiptData = new CreateReceiptData();
          receiptData.dto = toAdd;
          console.log(toAdd);
          return receiptData;
        } else {
          toAdd.latitud = "";
          toAdd.longitud = "";
          let receiptData = new CreateReceiptData();
          receiptData.dto = toAdd;
          console.log(toAdd);
          return receiptData;
        }
      }),
        //hacer el post nomas ya
        flatMap((req: CreateReceiptData) => {
          return from(this.addRecibo(toAdd).then(res => {
            console.log('respuesta del recibos/nuevo FCO', res)
            req.data = new NewReceiptResponse(res);
            req.id = Number(req.data.id);
            this.sincronizacion.sincronizarRecibos(Number(JSON.parse(localStorage.getItem('habilitacion'))))
            this.toast.presentToastAlert('Sincronizando...')
            return req;
          }).catch(error => {
            console.error('error al crear el recibo FCO', error);
            throw throwError(error);
          }))
        })//flatmap
        , catchError(error => {
          console.log('Error en recibo contado', error)
          return throwError(error)
        })
      )
  }


  getRecibosOnline(facturas: Array<any>): Promise<any> {
    return this.http.doPost('offline/recibos?', { facturas: facturas }, {}).then(response => {
      let data = JSON.parse(response.data);
      console.log("RECIBOS DE API:", data.lista);
      return data.lista;
    })
  }

  async addRecibo(data) {
    let request = await this.reciboRequestService.formatData(data);
    console.log("DATO PARA AGREGAR RECIBO ", request);

    return this.recibosDBservice.setRecibo(request).then(resp => {
      console.log("ADDRECIBO EXITOSO");
      return resp;
    }).catch(error => {
      console.error("ERROR ADDRECIBO", error);
      return Promise.reject(error);
    });
  }

  async crearImpresionRecibo(id_recibo: number, type: string): Promise<any> {
    console.log('crearImpresionRecibo, id_recibo', id_recibo)
    let linea: string = ""
    let cotizacion = 0.0;
    let totalRecibo = 0.0;
    let interes = 0.0;
    let anticipo = 0.0;
    let id_cobrador = 0;
    let id_concepto = 0;
    let id_moneda_recibo = 0;
    let nroRecibo = 0;
    let habilitacion = 0;
    let id_cliente = 0;
    let talonario = 0;
    let nombre_cobrador: string = "";
    let fecha_recibo: string = "";
    let numReciboFinal: string = "";
    let razonSocial: string = "";
    let rucCliente: string = null;
    let simboloCabecera: string = "";
    let cantidad_decimales = 0;
    let valoresPagos: Array<any> = [];
    let detallesPagos: Array<any> = [];

    let tipoCotizacion = 0.0;
    let montoCambio = 0.0;
    let total = 0.0;
    let totalReciboFCR = 0.0;
    let totalCabecera = 0.0;

    let totalFacturaFormateado: string = "";
    let totalReciboFormateado: string = "";
    let anticipoFormateado: string = "";
    let interesFormateado: string = "";

    let recibo: any = await this.recibosDBservice.obtenerReciboCap(id_recibo);
    console.log('recibo-cabecera en impresion', recibo)
    if (recibo !== null) {
      id_concepto = recibo.ID_CONCEPTO;
      id_cobrador = recibo.ID_COBRADOR;
      fecha_recibo = recibo.FECHA;
      id_moneda_recibo = recibo.ID_MONEDA;
      cotizacion = recibo.COTIZACION;
      totalRecibo = recibo.TOTAL_RECIBO;
      interes = recibo.INTERES;
      anticipo = recibo.ANTICIPO;
      nroRecibo = recibo.NRO_RECIBO;
      habilitacion = recibo.HABILITACION_CAJA;
      id_cliente = recibo.ID_CLIENTE;
      console.log('total_recibo', totalRecibo)
      totalReciboFormateado = this.utils.formatearNumber(totalRecibo);
      anticipoFormateado = this.utils.formatearNumber(anticipo);
      interesFormateado = this.utils.formatearNumber(interes);
      let user: any = await this.userBDService.getTalonario(id_cobrador);
      if (user !== null) {
        console.log('encontro el usuario', user)
        talonario = user.TALONARIO;
        nombre_cobrador = user.NOMBRE;
        //numero de recibo final
        if (talonario == 0) {
          numReciboFinal = "001-" + " " + "-000" + nroRecibo;
        } else {
          numReciboFinal = "001-" + talonario + "-000" + nroRecibo;
        }

        valoresPagos = await this.recibosDBservice.getValoresRecibo(id_recibo, id_moneda_recibo, cotizacion);
        if (valoresPagos !== null && valoresPagos.length !== 0) {
          detallesPagos = await this.recibosDBservice.getDetallesRecibo(id_recibo)
          if (detallesPagos !== null && detallesPagos.length !== 0) {
            let cli = await this.clienteBD.getCliente(id_cliente);
            if (cli !== null) {
              console.log('encontro el cliente', cli)
              rucCliente = cli.ruc;
              razonSocial = cli.razonSocial;
              let moneda: Moneda = await this.monedaBD.getMoneda(id_moneda_recibo);
              if (moneda !== null) {
                console.log('encontro la moneda valoresPagos', moneda)
                cantidad_decimales = moneda.cant_decimales;
                simboloCabecera = moneda.simbolo;
                /////// formateo de decimales falta
                linea = linea + "J. Fleischman y Cia S.R.L."
                  + "\r\nRuc: 80001490-1\r\nDireccion:Tte.Nicolas Cazenave\r\n"
                  + "y Tte.Victor Valdez\r\nTel.:(021) 238 2200\r\n"
                  + "******************************\r\n"
                  + "Recibo:" + numReciboFinal + "\r\n"
                  + "Fecha:" + fecha_recibo + "\r\n"
                  + "Recibimos de: \r\n"
                  + this.utils.limpiar_caracteres_especiales(razonSocial)
                  + "\r\nR.U.C: " + rucCliente + "\r\n"
                  + "Cotizacion: " + cotizacion + " Gs/USD \r\n"
                  + "______________________________\r\n"

                let credito = await this.facturasBD.todosCredito(detallesPagos)
                if (credito === false) {
                  // todos son contado
                  totalCabecera = totalRecibo;
                  linea = linea + "\r\nPor Facturas: \r\n"
                    + "NRO    IMPORTE   \r\n";
                } else {
                  for (let det of detallesPagos) {
                    let tipo = await this.facturasBD.isFCO(det.ID_FACTURA);
                    if (tipo === false) {//no es contado, es a credito
                      if (det.ID_MONEDA !== id_moneda_recibo) {
                        tipoCotizacion = cotizacion;
                        if (det.ID_MONEDA === 1) {
                          montoCambio = det.MONTO / tipoCotizacion;
                          montoCambio = Number(montoCambio.toFixed(0));
                        } else {
                          montoCambio = det.MONTO * tipoCotizacion;
                          montoCambio = Number(montoCambio.toFixed(2));
                        }

                        total = total + montoCambio;
                      } else {//son de la misma moneda
                        total = total + det.MONTO;
                        total = Number(total.toFixed(cantidad_decimales))
                      }
                    }
                  }
                  totalFacturaFormateado = this.utils.formatearNumber(Number(total.toFixed(cantidad_decimales)));
                  console.log('total_factura_formateado', totalFacturaFormateado)
                  totalReciboFCR = total;
                  if (interes > 0.0) {
                    totalReciboFCR += interes;
                  }
                  if (anticipo > 0.0) {
                    totalReciboFCR += anticipo;
                  }
                  totalCabecera = totalReciboFCR;
                  totalReciboFormateado = this.utils.formatearNumber(Number(totalReciboFCR.toFixed(cantidad_decimales)));
                  linea = linea + "\r\nPor Facturas: \r\n"
                    + "NRO        IMPORTE        CUOTA \r\n";
                }
                total = 0.0;
                for (let detalle of detallesPagos) {
                  let factura: any;
                  let num_factura: string = null;
                  let monto_formateado: string = null;
                  let tipo = await this.facturasBD.isFCO(detalle.ID_FACTURA);

                  if (tipo == false) {
                    factura = await this.facturasBD.detallesFactura(detalle.ID_FACTURA);
                    if (factura != null) {
                      console.log('encontro la factura', factura)
                      num_factura = factura.NO_FACTURA;
                      num_factura = num_factura.substring(4);
                      let mon: Moneda = await this.monedaBD.getMoneda(factura.ID_MONEDA);
                      if (mon !== null) {
                        cantidad_decimales = mon.cant_decimales;

                        monto_formateado = this.utils.formatearNumber(Number(detalle.MONTO.toFixed(cantidad_decimales)));
                        //formar el string
                        console.log('monto_formateado del MONTO DEL DETALLE', monto_formateado)
                        linea = linea + num_factura + "   " + monto_formateado + " "
                          + mon.simbolo + " " + detalle.NRO_CUOTA + "/" + factura.MAX_CUOTA + "\r\n";

                        if (factura.ID_MONEDA != id_moneda_recibo) {
                          montoCambio = cotizacion;
                          if (factura.ID_MONEDA == 1) {
                            montoCambio = detalle.MONTO / montoCambio;
                            montoCambio = Number(montoCambio.toFixed(0))
                          } else {
                            montoCambio = detalle.MONTO * montoCambio;
                            montoCambio = Number(montoCambio.toFixed(2));
                          }
                          total += montoCambio;
                        } else {
                          total += detalle.MONTO;
                        }
                      }
                    }
                  }
                }
                linea = linea + "\r\n";

                console.log("Total de facturas en imprimirRecibo " + total)

                if (anticipo > 0.0) {
                  linea = linea + "Anticipo: " + anticipoFormateado + "  " + simboloCabecera + "\r\n";
                }
                if (interes > 0.0) {
                  linea = linea + "Interes: " + interesFormateado + "  " + simboloCabecera + "\r\n";

                }
                linea = linea + "Total Importe: " + totalReciboFormateado + "  " + simboloCabecera + "\r\n"
                  + "______________________________\r\n"
                  + "Medios de Pago \r\n";

                let aux = 0.0;
                let montoValorizado = 0.0;
                let simbolo: string = null;
                aux = totalCabecera;
                console.log("Total Cabecera " + aux);
                let nombreEntidad: string;
                let nombreCuenta;
                let montoValorizadoFormateado;

                // empezar a iterar los valores

                for (let valor of valoresPagos) {
                  console.log('valor de valoresPagos', valor)
                  if (aux != 0.0) {
                    console.log('aux distinto a cero', aux);
                    montoValorizado = valor.MONTO_VALORIZADO;
                    let montoFormateado = null;

                    if (montoValorizado <= aux) {
                      console.log('monto valorizado menor a aux', montoValorizado)
                      linea = linea + this.utils.limpiar_caracteres_especiales(valor.DESCRIPCION) + "\r\n";

                      console.log("La moneda del valor es " + valor.ID_MONEDA);
                      let mon: Moneda = await this.monedaBD.getMoneda(valor.ID_MONEDA);
                      if (mon != null) {
                        cantidad_decimales = mon.cant_decimales;
                        simbolo = mon.simbolo;
                        //console.log('parseInt entidad',parseInt(valor.ID_ENTIDAD))
                        if (valor.NUMERO_VALOR === "0" && (parseInt(valor.ID_ENTIDAD) == 0 || valor.ID_ENTIDAD === null)) {
                          console.log('case1')
                          montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                          linea = linea + "IMPORTE:     " + montoFormateado + "  " + simbolo + "\r\n";

                        } else {
                          if (!(valor.NUMERO_VALOR === "0") && (parseInt(valor.ID_ENTIDAD) == 0 || valor.ID_ENTIDAD === null)) {
                            montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                            linea = linea + "IMPORTE:     " + montoFormateado + "  " + simbolo + "\r\n";
                            console.log('case2')
                          } else {
                            console.log("Cuenta bancaria 1" + valor.ID_CUENTA_BANCARIA);
                            if (valor.ID_CUENTA_BANCARIA === "" ||
                              valor.ID_CUENTA_BANCARIA === null) {
                              console.log("Entra a escribir en caso de que solo tenga entidad");
                              linea = linea + "Ch.NRO:    BANCO    IMPORTE\r\n";
                              montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                              console.log("Nombre de la entidad " + valor.ID_ENTIDAD);
                              let enti = await this.entidadBD.getEntidad(valor.ID_ENTIDAD);
                              nombreEntidad = enti.descripcion;
                              linea = linea + valor.NUMERO_VALOR + "     "
                                + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "    "
                                + montoFormateado + " " + simbolo + "\r\n";
                              //console.log(linea);

                            } else {
                              console.log("Entra a escribir en caso de que tenga cuenta bancaria");
                              linea = linea + "Ch.NRO:  BANCO   CUENTA   IMPORTE\r\n";
                              let enti: Entidad = await this.entidadBD.getEntidad(valor.ID_ENTIDAD);
                              nombreEntidad = enti.descripcion;
                              let cuen: Cuenta = await this.cuentaBD.getCuenta(valor.ID_CUENTA_BANCARIA)
                              nombreCuenta = cuen.descripcion;
                              montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));

                              linea = linea + valor.NUMERO_VALOR + "   "
                                + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "   "
                                + this.utils.limpiar_caracteres_especiales(nombreCuenta) + "   "
                                + montoFormateado + " " + simbolo + "\r\n";

                            }
                          }
                        }
                        aux = aux + montoValorizado;
                      }
                    } else {
                      console.log("Monto Valorizado del valor es mayor a total cabecera", aux, montoValorizado);
                      if (id_moneda_recibo != valor.ID_MONEDA) {
                        if (id_moneda_recibo == 1) {
                          montoValorizado = this.utils.round(aux / cotizacion, 2);
                        } else {
                          montoValorizado = this.utils.round(aux * cotizacion, 0);
                        }
                      } else {
                        montoValorizado = aux;
                      }
                      console.log("MontoValorizado " + montoValorizado);
                      linea = linea + this.utils.limpiar_caracteres_especiales(valor.DESCRIPCION) + "\r\n";

                      // OBTENER LOS DETALLES DE LA MONEDA DEL VALOR
                      let mon: Moneda = await this.monedaBD.getMoneda(valor.ID_MONEDA);

                      if (mon !== null) {
                        simbolo = mon.simbolo;
                        cantidad_decimales = mon.cant_decimales;

                        if (valor.NUMERO_VALOR === "0" &&
                          (parseInt(valor.ID_ENTIDAD) == 0 || valor.ID_ENTIDAD === null)) {

                          montoValorizadoFormateado = this.utils.formatearNumber(
                            Number(montoValorizado.toFixed(cantidad_decimales)));

                          console.log("MONTO FORMATEADO " + montoValorizadoFormateado);
                          linea = linea + "IMPORTE:     " + montoValorizadoFormateado + "  "
                            + simbolo + "\r\n";

                        } else {
                          if (!(valor.NUMERO_VALOR === "0")
                            && (parseInt(valor.ID_ENTIDAD) === 0 || valor.ID_ENTIDAD === null)) {
                            //montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                            montoValorizadoFormateado = this.utils.formatearNumber(
                              Number(montoValorizado.toFixed(cantidad_decimales)));
                            linea = linea + "IMPORTE:     " + montoValorizadoFormateado + "  " + simbolo + "\r\n";

                          } else {
                            console.log("Cuenta bancaria 2" + valor.ID_CUENTA_BANCARIA);
                            if (valor.ID_CUENTA_BANCARIA === null || valor.ID_CUENTA_BANCARIA === "") {
                              linea = linea + "Ch.NRO:    BANCO    IMPORTE\r\n";

                              let enti: Entidad = await this.entidadBD.getEntidad(valor.ID_ENTIDAD);
                              nombreEntidad = enti.descripcion;
                              montoValorizadoFormateado = this.utils.formatearNumber(
                                Number(montoValorizado.toFixed(cantidad_decimales)));

                              linea = linea + valor.NUMERO_VALOR + "     "
                                + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "    "
                                + montoValorizadoFormateado + " " + simbolo + "\r\n";

                            } else {
                              linea = linea + "Ch.NRO: BANCO  CUENTA  IMPORTE\r\n";
                              let enti: Entidad = await this.entidadBD.getEntidad(valor.ID_ENTIDAD);
                              nombreEntidad = enti.descripcion;
                              let cuen: Cuenta = await this.cuentaBD.getCuenta(valor.ID_CUENTA_BANCARIA)
                              nombreCuenta = cuen.descripcion;
                              montoValorizadoFormateado = this.utils.formatearNumber(
                                Number(montoValorizado.toFixed(cantidad_decimales)));

                              linea = linea + valor.NUMERO_VALOR + "   "
                                + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "   "
                                + this.utils.limpiar_caracteres_especiales(nombreCuenta) + "   "
                                + montoValorizadoFormateado + " " + simbolo + "\r\n";

                            }
                          }
                          aux = 0.0;
                        }
                      }

                    }
                  } else {
                    break;
                  }

                }
                let monedaCabecera: Moneda = moneda;
                cantidad_decimales = monedaCabecera.cant_decimales

                let xcifra = this.utils.formatearNumber(parseFloat(totalCabecera.toFixed(cantidad_decimales)));
                xcifra = xcifra.replace(/\./g, '');
                simbolo = monedaCabecera.simbolo;

                linea = linea + "TOTAL PAGO: " + this.utils.formatearNumber(Number(totalCabecera.toFixed(cantidad_decimales))) + " "
                  + simbolo + "\t\r\n"
                  + "______________________________\r\n"
                  + "Total Recibo: " + totalReciboFormateado + "  " + simbolo + "\r\n"
                  + "SON: " + this.utils.numeroALetras(xcifra, {
                    singular: monedaCabecera.singular,
                    plural: monedaCabecera.plural
                  }) + " \t\r\n";

                if (id_concepto !== null && id_concepto !== 0) {
                  let concep: Concepto = await this.conceptoBD.getConcepto(id_concepto)
                  linea = linea + "CONCEPTO: "
                    + this.utils.limpiar_caracteres_especiales(concep.descripcion)
                    + "\t\n";

                }
                linea = linea + "******************************\t\n"
                  + "\r\n" + "\r\n" + "\r\n";

                let fechaHoraActual = moment(new Date()).format("DD-MM-YYYY HH:mm:ss")
                linea = linea + "Cobrador: " + this.utils.limpiar_caracteres_especiales(nombre_cobrador) + "\t\r\n"
                  + "Fecha y Hora:" + fechaHoraActual + "\t\r\n";

                if (type == '1') {
                  // imprimir que es un recibo original para el cliente
                  linea = linea + "\t\t\t\t" + "ORIGINAL:CLIENTE" + "\r\n";
                  linea = linea + "******************************\t\n";
                  console.log('original ', linea)

                } else {
                  // imprimir que es un recibo duplicado para el cliente
                  linea = linea + "\t\t" + "DUPLICADO:ARCH.TRIBUTARIO" + "\r\n";
                  linea = linea + "******************************\t\n";
                  console.log('duplicado ', linea)

                }

                if (type == '1') {
                  console.log("Recibo original cliente");
                  let archivo_original = "1-" + (new Date()).getTime() + "-" + id_cliente;
                  this.recibosDBservice.guardarReciboOriginal(id_recibo, archivo_original)
                  //this.writeFile.crearArchivoImpresionRecibo(archivo_original, linea);
                  return Promise.resolve({
                    ORIGINAL: linea,
                  })

                } else {
                  // recibo duplicado
                  console.log("Recibo duplicado cliente");
                  let archivo_duplicado = "2-" + (new Date()).getTime() + "-" + id_cliente;
                  this.recibosDBservice.guardarReciboDuplicado(id_recibo, archivo_duplicado)
                  //this.writeFile.crearArchivoImpresionRecibo(archivo_duplicado, linea);
                  return Promise.resolve({
                    DUPLICADO: linea
                  })
                }
              } else {
                console.log("No se pudo obtener datos de la moneda utilizada")
                return Promise.reject("No se pudo obtener datos de la moneda utilizada")
              }

            } else {
              console.log("No se pudo obtener datos del cliente")
              return Promise.reject("No se pudo obtener datos del cliente")
            }
          } else {
            console.log("No se pudo obtener los detalles del recibo")
            return Promise.reject("No se pudo obtener los detalles del recibo")
          }
        } else {
          console.log("No se pudo obtener los valores del recibo")
          return Promise.reject("No se pudo obtener los valores del recibo")
        }

      } else {
        console.log("No se pudo obtener el usuario")
        return Promise.reject("No se pudo obtener el usuario")
      }

    } else {
      console.log("No se pudo obtener el recibo cabecera")
      return Promise.reject("No se pudo obtener la cabecera del recibo")
    }
  }

}
