import { TestBed } from '@angular/core/testing';

import { FacturasLogicaService } from './facturas-logica.service';

describe('FacturasLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturasLogicaService = TestBed.get(FacturasLogicaService);
    expect(service).toBeTruthy();
  });
});
