import { Injectable } from '@angular/core';
import { Factura } from 'src/app/Clases/factura';
import { HttpService } from '../httpFunctions/http.service';
import { FacturaWithCheck } from 'src/app/Clases/factura-with-check';
import { Cliente } from 'src/app/Clases/cliente';
import { FacturasBDService } from '../almacenamientoSQLITE/facturasBD/facturas-bd.service';



@Injectable({
  providedIn: 'root'
})
export class FacturasLogicaService {

  constructor(private http: HttpService,
    private facturasBD: FacturasBDService) { }


  getList(cliente: Cliente, offset: number, last_result: number, orden: string): Promise<Array<FacturaWithCheck>> {

    return this.http.doGet('clientes/' + cliente.id + '/facturas?',
      { first_result: offset, last_result: last_result, orderby: orden }).then(res => {
        let data = JSON.parse(res.data);
        console.log('facturas', data)
        let toRet: Array<FacturaWithCheck> = [];

        for (let row of data.lista) {
          let toFactura = new Factura(row, cliente);

          toRet.push(new FacturaWithCheck(toFactura, false));
        }

        return toRet;
      });

  }

  obtenerFacturasOnline(clientes: Array<Number>) {
    return this.http.doPost('offline/facturas?',{ clientes: clientes }, {first_result:0, last_result:10000}).then(res => {
      let data = JSON.parse(res.data);
      let toRet: Array<any> = []=data.lista;
      console.log("FACTURAS DE API:", toRet);
      return toRet;
    })
  }
  obtenerFacturasLocal(cliente: Cliente, offset: number, last_result: number, orden: string): Promise<Array<FacturaWithCheck>> {
    return this.facturasBD.getFacturasLocal(cliente, offset, last_result, orden).then((data: Array<Factura>) => {
      let toRet: Array<FacturaWithCheck> = [];

      toRet = data.map(f => {
        return new FacturaWithCheck(f, false);
      });

      return toRet;
    })
  }

  async verificarFacturaFCO(facturas){
    let facturaCheck=false;
    for(let f of facturas){
      if(await this.facturasBD.isFCO(f['id_factura'])){
          facturaCheck=true;
      } else {
          facturaCheck=false;
          break;
      }
    }
    return facturaCheck;
  }

}
