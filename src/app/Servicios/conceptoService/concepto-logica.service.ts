import { Injectable } from '@angular/core';
import { Concepto } from '../../Clases/concepto';
import { HttpService } from '../httpFunctions/http.service';
import { ConceptosBDService } from '../almacenamientoSQLITE/conceptosBD/conceptos-bd.service';

@Injectable({
  providedIn: 'root'
})
export class ConceptoLogicaService {

  public conceptos: Array<Concepto> = [];

  constructor(private http: HttpService, private conceptosBD:ConceptosBDService) { }

  getConceptosOnline() {

    return this.http.doGet('conceptos?', { first_result: 0, last_result: 500 })
      .then(res => {
        let data = JSON.parse(res.data).lista;

        let toRet: Array<Concepto> = [];
        toRet = data.map(c => {
          return new Concepto(c);
        });
        console.log('conceptos', toRet)
        this.conceptos = toRet;
        return toRet;
      });
  }
  getConceptosBD(){
    return this.conceptosBD.getConceptos().then(conceptos=>{
      this.conceptos=conceptos;
      return conceptos;
    });
    
}

}
