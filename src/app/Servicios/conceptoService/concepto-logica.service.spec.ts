import { TestBed } from '@angular/core/testing';

import { ConceptoLogicaService } from './concepto-logica.service';

describe('ConceptoLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConceptoLogicaService = TestBed.get(ConceptoLogicaService);
    expect(service).toBeTruthy();
  });
});
