import { Injectable } from '@angular/core';
import { ToastControllerService } from '../toastController/toast-controller.service';

@Injectable({
  providedIn: 'root'
})
export class SesionService {

  constructor(private toast:ToastControllerService) { }

  validarSesion(token: string, token_expired: string) {
    console.log("token: ", token, "token_expired", token_expired)
    let now: Date = new Date();
    let expired = new Date(token_expired);
    console.log("now: ", now, "expired", expired)
    if (token !== null && now <= expired) {

      console.log('Sesion valida');
      let resto = expired.getTime()-now.getTime();
      console.log("resto fecha: ", resto +"    "+resto/60000)
      if((resto/60000)>=1 && (resto/60000)<=5){
        this.toast.presentToastAlert('Su token expirara en '+ (resto/60000).toFixed(0) +' minutos')
      }
      return true;
    }
    console.log('Sesion no valida');
    return false;
  }
}
