import { Injectable } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { UsuarioService } from '../usuarioService/usuario.service';
import { LoadingControllerService } from '../loaddingController/loading-controller.service';
import { ToastControllerService } from '../toastController/toast-controller.service';
import { ConfigService } from '../configuracion/config.service';

@Injectable({
  providedIn: 'root'
})
export class AlertControllerService {

  constructor(private alertCtrl: AlertController,
    private userService: UsuarioService,
    private loadingService: LoadingControllerService,
    private toastService: ToastControllerService,
    private navControl: NavController,
    private config:ConfigService
  ) { }

  async simpleAlert(title, texto, buttons) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: texto,
      buttons: buttons,
    });

    await alert.present();
  }

  alertMessageWithHandler(titulo: string, mensaje: string, handler: (data: any) => any = null, conCancelar: boolean = false, habilitacion_caja:string = null) {
    console.log(habilitacion_caja);
    let buttons =
      [{
        text: 'Aceptar',
        handler: data => { if (handler) handler(data); }
      }]
    if (conCancelar) {
      buttons.unshift({
        text: 'Cancelar',
        handler: null
      });
    }
    this.simpleAlert(titulo, mensaje, buttons);
  }
  async confirmExitAPP() {
    
    const confirm = await this.alertCtrl.create({
      header: 'Cobranzas',
      message: 'Esta seguro de que desea salir de la Aplicación?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });
    await confirm.present();
  }

  async mostrarConfirmacionLogout(){
    const confirm = await this.alertCtrl.create({
      header: 'Cobranzas',
      message: 'Esta seguro de que desea salir de la Aplicación?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.loadingService.present("Verificando datos ...");
            this.userService.existenDatosHabilitacion(Number(JSON.parse(localStorage.getItem('habilitacion')))).then((resp)=>{
              if (resp) {
                this.loadingService.dismiss();
                this.toastService.presentToastDanger("Existen datos que faltan ser subidos, aguarde tres minutos, verifique su conexion a internet e intente nuevamente!");
              } else {
                this.loadingService.dismiss();
                localStorage.removeItem("expiredToken");
                localStorage.removeItem(this.config.AUTHORIZATION_STRING);
                this.navControl.navigateRoot("/login");
              }
            })
          }
        }
      ]
    });
    await confirm.present();
  }
}
