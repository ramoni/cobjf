import { TestBed } from '@angular/core/testing';

import { EntidadLogicaService } from './entidad-logica.service';

describe('EntidadLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EntidadLogicaService = TestBed.get(EntidadLogicaService);
    expect(service).toBeTruthy();
  });
});
