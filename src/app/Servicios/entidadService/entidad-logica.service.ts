import { Injectable } from '@angular/core';
import { Entidad } from '../../Clases/entidad';
import { HttpService } from '../httpFunctions/http.service';
import { EntidadesBDService } from '../almacenamientoSQLITE/entidadesBD/entidades-bd.service';

@Injectable({
    providedIn: 'root'
})
export class EntidadLogicaService {

    public entidades: Array<Entidad> = [];

    constructor(private http: HttpService,
        private entidadBD: EntidadesBDService) {
        
    }

    getByIdOrName(idOrName: string): Entidad {
        this.getEntidadesBD();
        let toRet;
        if (isNaN(parseFloat(idOrName))) {
            // it's a description
            toRet = this.entidades.find(mon => mon.descripcion === idOrName);
        } else {
            // it's a id
            toRet = this.entidades.find(mon => mon.id === idOrName.toString());
        }
        return toRet;

    }

    getEntidadesOnline() {
        return this.http.doGet('entidades?', { first_result: 0, last_result: 500 }).then(res => {
            let data = JSON.parse(res.data).lista;
            let toRet: Array<Entidad> = [];
            toRet = data.map(e => {
                return new Entidad(e);
            })
            this.entidades = toRet;

            console.log('entidades ', this.entidades)
            return toRet;
        });
    }
    getEntidadesBD() {
        return this.entidadBD.getEntidades().then(entidades => {
            this.entidades=entidades;
            return entidades;
        });
    }

}
