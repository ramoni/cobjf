import { Injectable } from '@angular/core';
import { DatosUsuario } from '../../Clases/datos-usuario';
import { HttpService } from '../httpFunctions/http.service';
import { NewReceiptResponse } from '../../Clases/new-receipt-response';
import { ConfigService } from '../configuracion/config.service';
import { NumHabilitacion } from '../../Clases/num-habilitacion';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { File } from '@ionic-native/file/ngx';
import { WriteFileService } from '../writeFileService/logWrite.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  ud: DatosUsuario;
  token: string;
  tokenExpired: string;
  req: NewReceiptResponse;
  credentials: { user: string, pass: string, version: string, imei: string };

  constructor(

    private http: HttpService,
    private config: ConfigService,
    private usersBD: UsuariosBDService,
    private notasBD: NotasBDService,
    private recibosBD: RecibosBDService,
    private entregasDocBD: EntregasDocBDService,
    private fileService: File,
    private writeLog: WriteFileService
  ) {
  }

  _doLoginRequest(user: string, pass: string, version: string, imei: string): Promise<DatosUsuario> {

    return this.http.doPost('login?', {},
      {
        user: user,
        password: pass,
        version: version,
        imei: imei
      }
    ).then(response => {

      this.token = response.headers[this.config.AUTHORIZATION_STRING];
      this.tokenExpired = JSON.parse(response.data).EXPIRED;
      console.log('token expired ', this.tokenExpired)
      if (this.token === null) {
        this.token = '';
      }
      localStorage.setItem(this.config.AUTHORIZATION_STRING, this.token);
      localStorage.setItem('expiredToken', this.tokenExpired);
      let user = <DatosUsuario>JSON.parse(response.data);
      console.log("USER : ",user);

      return user;
    });
  }

  iniciarSesion(usuario: string, password: string, version: string, imei: string): Promise<DatosUsuario> {

    this.credentials = {
      user: usuario,
      pass: password,
      version: version,
      imei: imei,
    };

    return this._doLoginRequest(usuario, password, version, imei).then(datos => {
        this.ud = datos;
        if(localStorage.getItem('user')){
          localStorage.removeItem('user')
        }
        localStorage.setItem('user', JSON.stringify(datos));
        localStorage.setItem('habilitacion', JSON.stringify(this.ud.HABILITACION));
        return datos;
      })

  }

  actualizarDatosUsuario(){
    return this.http.doGet('inicio?').then(response => {
      let data = JSON.parse(response.data);
      console.log('data en actualizar usuario',data )
      let userData = <DatosUsuario>(data);
      console.log('userData en actualizar usuario',data )
      this.setDatosUsuario(userData);
      localStorage.setItem('user', JSON.stringify(userData));
      localStorage.setItem('habilitacion', JSON.stringify(userData.HABILITACION));
      localStorage.setItem('expiredToken',userData.EXPIRED);
      return data;
    });
  }

  async cambiarHabilitacion(habilitacion: number): Promise<NewReceiptResponse> {
    await this.writeLog.escribirLog(new Date()+" Petición: Sincronización de logs para cerrar la caja \n")
    let ubicacion = this.fileService.externalApplicationStorageDirectory;
    return this.fileService.readAsText(ubicacion, "cobranzas_log.txt").then((content)=>{
      let data = new NumHabilitacion(habilitacion, Number(localStorage.getItem('datos')), localStorage.getItem('imei'));
      data.logs = content;
      console.log("SE ENVIA A CERRAR CAJA ",data);
      return this.http.doPost('habilitacion/cerrar?', data).then(res => {
        this.fileService.removeFile(ubicacion,"cobranzas_log.txt");
        let data = JSON.parse(res.data);
        this.req = new NewReceiptResponse(data);
        console.log('respuesta post habilitacion/cerrar ', this.req);
        localStorage.setItem('habilitacion', JSON.stringify(this.req.totalHabilitacion));
        let nuevoDato = 0;
        localStorage.setItem('bajada', JSON.stringify(nuevoDato));
        localStorage.setItem('subida', JSON.stringify(nuevoDato));
        localStorage.setItem('datos', JSON.stringify(nuevoDato));
  
        return this.req;
      })
      
    })

    
  }


  getDatosUsuario(): DatosUsuario {
    /*if (localStorage.getItem('user')) {
      return JSON.parse(localStorage.getItem('user'))
    }
    return null;*/
    return JSON.parse(localStorage.getItem('user'))
  }
  setDatosUsuario(u: DatosUsuario) {
    console.log(u);
    this.ud = u;
  }
  existeUsuario(user: String, pass: String):Promise<any> {
    return this.usersBD.existeUsuario(user, pass).then(data => {
      let usuario: Array<any> = [];
      if (data.rows.length > 0) {
        console.log('exite el usuario', data.rows)

        usuario = data.rows.item(0);
        console.log('usuario', usuario)
        return usuario;
      }else{
        return null;
      }
      
    }).catch(error => {
      console.error('no se pudo obtener los datos de la tabla', error);
      return error;
    });
  }
  existeHabilitacionUsuario(totalHabilitacion, usuario, pass) {
    return this.usersBD.existeHabilitacionUser(totalHabilitacion, usuario, pass).then(response => {
      if (response.rows.length > 0) {
        console.log('exite la habilitacion del usuario');
        return true;
      }
      return false;
    }).catch(error => {
      console.error('no se pudo obtener los datos de la tabla para habilitacion', error);
      return error;
    })
  }
  addUsuario(usuario, password, user, token, expiredToken) {
    return this.usersBD.addUser(usuario, password, user, token, expiredToken).then(() => {
      return true;
    }).catch(error => {
      console.log('no pudo agregarse el usuario', error)
      return error;
    });
  }
 
  async existenDatosHabilitacion(habilitacion: number): Promise<any> {

    return this.notasBD.syncNotasHabilitacion(habilitacion).then(notasSync => {
      return this.entregasDocBD.syncEntregasHabilitacion(habilitacion).then(entregasDocSync=> {
        return this.recibosBD.existeSyncRecibosHabilitacion(habilitacion).then(async recibosSync => {
          if(notasSync===-1 || entregasDocSync ===-1 || recibosSync===-1){
            await this.writeLog.escribirLog(new Date() +" Error de lectura de base de datos local para consulta de datos a sincronizar de la habilitacion "+habilitacion);
            return true;
          }
          if(notasSync || entregasDocSync || recibosSync){
            return true;               
          }else{
            return false;
          }
        })
      })
    })
  }
}


