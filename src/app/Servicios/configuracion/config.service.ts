import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  
  //api local Luis 
  //BASE_URL_REAL : string = "http://192.168.100.225:8080/api";

  //api remota
  //BASE_URL_REAL: string = "http://cobranzas-api.bypersoft.com/api";

  //api DESARROLLO
  /// BASE_URL_REAL: string = "https://gao.bypersoft.com/api";
 
  //api DESARROLLO
  BASE_URL_REAL: string = "https://cobranzas.bypar.com.py/api";


  MEDIA_TYPE: string = 'application/json; charset=utf-8';
  GEO_REVERSE_URL: string = "https://maps.googleapis.com/maps/api/geocode/json?latlng=LATITUD,LONGITUD&key=AIzaSyCLDuEZBotJyL6dN6Yd2bQuVxfMOEAXwI0";
  AUTHORIZATION_STRING = "authorization";
  DEFAULT_DATE_FORMAT = "DD-MM-YYYY";

  IS_OFFLINE: boolean = false;

  constructor(private plat: Platform) { }

  getUrl(tip = 'get') {
    // if (this.plat.is('mobileweb')) {
    if (this.plat.is('cordova')) {
      return this.BASE_URL_REAL;
    } else {
      return this.BASE_URL_REAL;
    }
  }
}