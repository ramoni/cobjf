import { TestBed } from '@angular/core/testing';

import { DatosOnlineService } from './datos-online.service';

describe('DatosOnlineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatosOnlineService = TestBed.get(DatosOnlineService);
    expect(service).toBeTruthy();
  });
});
