import { Injectable } from '@angular/core';
import { NetworkControllerService, ConnectionStatus } from '../networkController/network-controller.service';
import { ConceptoLogicaService } from '../conceptoService/concepto-logica.service';
import { Concepto } from 'src/app/Clases/concepto';
import { EntidadLogicaService } from '../entidadService/entidad-logica.service';
import { Entidad } from 'src/app/Clases/entidad';
import { Moneda } from 'src/app/Clases/moneda';
import { MonedaLogicaService } from '../monedaService/moneda-logica.service';
import { ValoresLogicaService } from '../valorService/valores-logica.service';
import { CuentaLogicaService } from '../cuentaService/cuenta-logica.service';
import { Cuenta } from 'src/app/Clases/cuenta';
import { ValueType } from 'src/app/Clases/value-type';
import { UsuarioService } from '../usuarioService/usuario.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { DatosUsuario } from 'src/app/Clases/datos-usuario';
import { ClientesLogicaService } from '../clienteService/clientes-logica.service';
import { Cliente } from 'src/app/Clases/cliente';
import { FacturasBDService } from '../almacenamientoSQLITE/facturasBD/facturas-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { FacturasLogicaService } from '../facturasService/facturas-logica.service';
import { NotasLogicaService } from '../notaService/notas-logica.service';
import { DocumentosLogicaService } from '../documentosService/documentos-logica.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { RecibosLogicaService } from '../reciboService/recibos-logica.service';
import { ClientesBDService } from '../almacenamientoSQLITE/clientesBD/clientes-bd.service';
import { EntidadesBDService } from '../almacenamientoSQLITE/entidadesBD/entidades-bd.service';
import { ConceptosBDService } from '../almacenamientoSQLITE/conceptosBD/conceptos-bd.service';
import { ValoresBDService } from '../almacenamientoSQLITE/valoresBD/valores-bd.service';
import { CuentasBDService } from '../almacenamientoSQLITE/cuentasBD/cuentas-bd.service';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';
import { WriteFileService } from '../writeFileService/logWrite.service';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DatosOnlineService {

  clientesFacturas: Array<Number> = [];
  clientesEntregasDoc: Array<Number> = [];

  constructor(
    private networkService: NetworkControllerService,
    private conceptoLogic: ConceptoLogicaService,
    private entidadesLogic: EntidadLogicaService,
    private monedaLogic: MonedaLogicaService,
    private valoresLogic: ValoresLogicaService,
    private cuentasLogic: CuentaLogicaService,
    private userService: UsuarioService,
    private usuarioBD: UsuariosBDService,
    private clienteService: ClientesLogicaService,
    private facturasBD: FacturasBDService,
    private entregasDocBD: EntregasDocBDService,
    private facturasService: FacturasLogicaService,
    private entregasService: DocumentosLogicaService,
    private notasService: NotasLogicaService,
    private recibosBD: RecibosBDService,
    private recibosService: RecibosLogicaService,
    private clientesBD: ClientesBDService,
    private conceptosBD: ConceptosBDService,
    private entidadesBD: EntidadesBDService,
    private valorBD: ValoresBDService,
    private cuentasBD: CuentasBDService,
    private monedaBD: MonedasBDService,
    private writeLog: WriteFileService,
    private notasBD: NotasBDService,
    private navCtrl:NavController,

  ) { }

  async bajarDatosOnline(): Promise<boolean> {
    let userData: boolean = false;
    let moneda: boolean = false;
    let entidad: boolean = false;
    let cuenta: boolean = false;
    let valor: boolean = false;
    let cotizacion: boolean = false;
    let concepto: boolean = false;
    let notas: boolean = false;
    let clientes: boolean = false;
    let facturas: boolean = false;
    let documentos: boolean = false;

    this.clientesEntregasDoc = [];
    this.clientesFacturas = [];


    userData = await this.actualizarDatosUsuario()

    moneda = await this.bajarMonedas();
    entidad = await this.bajarEntidades();
    cuenta = await this.bajarCuentas();
    valor = await this.bajarValores();
    cotizacion = await this.bajarCotizacion();
    concepto = await this.bajarConceptos();
    notas = await this.bajarNotas();
    clientes = await this.bajarClientes();
    facturas = await this.bajarFacturas();
    documentos = await this.bajarDocumentosEntregas()

    console.log('userdata', userData);
    console.log('entidad', entidad)
    console.log('cuenta', cuenta)
    console.log('valor', valor)
    console.log('cotizacion', cotizacion)
    console.log('concepto', concepto)
    console.log('notas ', notas)
    console.log('clientes', clientes)
    console.log('facturas', facturas)
    console.log('documentos', documentos)

    if (userData && moneda && entidad && cuenta && valor && cotizacion && concepto && notas && clientes
      && facturas && documentos) {
      console.log('return true')
      return true;
    } else {
      console.log('return false')
      return false;
    }

  }
  async actualizarDatosUsuario(): Promise<boolean> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.userService.actualizarDatosUsuario().then((data: DatosUsuario) => {
        return this.usuarioBD.updateDatosUsuario(data).then(resp => {
          //Se insertaron los datos refrescados del usuario
          return resp;
        }).catch(async error => {
          console.error('no se pudo actualizar los datos de usuario en la tabla', error);
          await this.writeLog.escribirLog(new Date() + ' Respuesta con error BD USUARIOS: ' + JSON.stringify(error) + ' \n');
          return false;

        });
      }).catch(async error => {
        console.error('no se pudo traer los datos del usuario', error)
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA USUARIOS: ' + JSON.stringify(error) + ' \n');
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      })
    } else {
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE USUARIOS \n');
      return Promise.reject(false);
    }
  }
  async bajarConceptos(): Promise<boolean> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.conceptoLogic.getConceptosOnline().then((conceptos: Array<Concepto>) => {
        return this.conceptosBD.deleteAllConceptos().then(async () => {
          for (let concepto of conceptos) {
            await this.conceptosBD.addConcepto(concepto);
          }
          return true;
        }).catch(() => {
          return false;
        })
      }).catch(async error => {
        console.log('Error al obtener conceptos online', error);
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA CONCEPTOS: ' + JSON.stringify(error) + ' \n');
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      });
    } else {
      console.log("No se pudieron obtener los datos, verifique su conexion")
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE CONCEPTOS \n');
      return Promise.reject(false);
    }
  }
  async bajarCotizacion(): Promise<boolean> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.valoresLogic.getCotizacionOnline().then(() => {
        console.log('se obtuvo la cotizacion')
        return true;
      }).catch(async error => {
        console.log('error al obtener la cotizacion', error);
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA COTIZACION: ' + JSON.stringify(error) + ' \n');
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      })
    } else {
      console.log("No se pudieron obtener los datos, verifique su conexion")
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE COTIZACION \n');
      return Promise.reject(false);
    }
  }
  async bajarEntidades(): Promise<boolean> {
    //obtenemos entidades online
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.entidadesLogic.getEntidadesOnline().then((entidades: Array<Entidad>) => {
        return this.entidadesBD.deleteAllEntidades().then(async () => {
          for (let entidad of entidades) {
            await this.entidadesBD.addEntidad(entidad)
          }
          return true;
        }).catch(() => {
          return false;
        })
      }).catch(async error => {
        console.log('Error al obtener entidades online', error);
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA ENTIDADES: ' + JSON.stringify(error) + ' \n');
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      });
    } else {
      console.log("No se pudieron obtener los datos, verifique su conexion")
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE ENTIDADES \n');
      return Promise.reject(false);
    }
  }
  async bajarMonedas(): Promise<boolean> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.monedaLogic.getMonedasOnline().then((monedas: Array<Moneda>) => {
        return this.monedaBD.deleteAllMonedas().then(async () => {
          for (let moneda of monedas) {
            await this.monedaBD.addMoneda(moneda);
          }
          return true;
        }).catch(() => {
          return false;
        })
      }).catch(async error => {
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA MONEDAS: ' + JSON.stringify(error) + ' \n');
        console.log('Error al obtener monedas online ', error);
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      });
    } else {
      console.log("No se pudieron obtener los datos, verifique su conexion")
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE MONEDAS \n');
      return Promise.reject(false);
    }
  }
  async bajarCuentas(): Promise<boolean> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.cuentasLogic.getCuentasOnline().then((cuentas: Array<Cuenta>) => {
        return this.cuentasBD.deleteAllCuentas().then(async () => {
          for (let cuenta of cuentas) {
            await this.cuentasBD.addCuenta(cuenta);
          }
          return true;
        }).catch(() => {
          return false;
        })
      }).catch(async error => {
        console.log('Error al obtener cuentas online ', error);
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA CUENTAS: ' + JSON.stringify(error) + ' \n');
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      });
    } else {
      console.log("No se pudieron obtener los datos, verifique su conexion")
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE CUENTAS \n');
      return Promise.reject(false);
    }
  }
  async bajarValores(): Promise<boolean> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.valoresLogic.getValoresOnline().then((valores: Array<ValueType>) => {
        return this.valorBD.deleteAllValores().then(async () => {
          for (let valor of valores) {
            await this.valorBD.addValor(valor);
          }
          return true;
        }).catch(() => {
          return false;
        })
      }).catch(async error => {
        console.log('Error al obtener cuentas online ', error);
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA VALORES: ' + JSON.stringify(error) + ' \n');
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      });
    } else {
      console.log("No se pudieron obtener los datos, verifique su conexion")
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE VALORES \n');
      return Promise.reject(false);
    }
  }
  async bajarNotas() {
    return this.notasBD.syncNotasHabilitacion(Number(JSON.parse(localStorage.getItem('habilitacion'))))
      .then(async booleano => {
        if (booleano === false) {
          return this.notasBD.deleteAllNotas().then(async () => {
            if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
              return this.notasService.getAllNotasOnline(Number(JSON.parse(localStorage.getItem('habilitacion'))))
                .then(async notas => {
                  await this.notasService.guardarNotas(notas);
                  return true;
                }).catch(async error => {
                  console.error('no se pudo obtener las notas', error)
                  await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA NOTAS: ' + JSON.stringify(error) + ' \n');
                  if(error.status===401){
                    this.navCtrl.navigateRoot('login');
                  }
                  return false;
                });
            } else {
              console.error("No se pudieron obtener los datos, verifique su conexion")
              await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE NOTAS \n');
              return Promise.reject(false);
            }
          }).catch(error => {
            console.error('no se pudo eliminar los datos de la tabla notas', error)
            return false;
          })
        } else {
          if (booleano === true) {//faltan sincronizar notas
            if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
              return this.notasService.getAllNotasOnline(Number(JSON.parse(localStorage.getItem('habilitacion'))))
                .then(async notas => {
                  await this.notasService.guardarNotas(notas);
                  return true;
                }).catch(async error => {
                  console.error('no se pudo obtener las notas', error)
                  await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA NOTAS: ' + JSON.stringify(error) + ' \n');
                  if(error.status===401){
                    this.navCtrl.navigateRoot('login');
                  }
                  return false;
                });
            } else {
              console.error("No se pudieron obtener los datos, verifique su conexion")
              await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE NOTAS \n');
              return Promise.reject(false);
            }
          }
        }
      }).catch(error => {
        console.log('No se pudo acceder a la base de datos local para notas', error)
        return false;
      })
  }

  async bajarClientes() {
    //llamada a visitas
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      return this.clienteService.getVisitasOnline(0, 5000, null, 'T').then((clientes: Array<Cliente>) => {
        return this.clientesBD.deleteAllClientes().then(async () => {
          if (clientes.length > 0) { //la lista no es vacia

            for (let cliente of clientes) {
              if (cliente.facturas > 0)
                this.clientesFacturas.push(Number(cliente.id));
              if (cliente.entregas > 0)
                this.clientesEntregasDoc.push(Number(cliente.id))
              //insertamos el cliente
              await this.clientesBD.addCliente(cliente)
            }
            return true
          } else {
            return true;
          }
        }).catch(error => {
          console.log('No se puede eliminar los datos de la tabla clientes', error)
          return false;
        })

      }).catch(async error => {
        console.log('no puede obtenerse las visitas', error)
        await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA CLIENTES: ' + JSON.stringify(error) + ' \n');
        if(error.status===401){
          this.navCtrl.navigateRoot('login');
        }
        return false;
      })
    } else {
      console.error("No se pudieron obtener los datos de las visitas, verifique su conexion")
      await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE CLIENTES \n');
      return Promise.reject(false);
    }
  }
  async bajarFacturas(): Promise<boolean> {
    let facturasID: Array<Number> = [];
    if (this.clientesFacturas.length > 0) {
      if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
        return this.facturasBD.deleteAllFacturas().then(() => {
          return this.facturasService.obtenerFacturasOnline(this.clientesFacturas)
            .then(async (facturas: Array<any>) => {
              for (let facturaOn of facturas) {
                facturasID.push(facturaOn.ID_FACTURA);
                await this.facturasBD.addFacturas(facturaOn);
                // facturasID.push(facturaOn.ID_FACTURA);
              }
              //obtenemos los recibos
              /*for (let fac of facturas) {
                facturasID.push(fac.ID_FACTURA);
              }*/
              console.log('recibos a solicitar de facturas', facturasID)
              return this.recibosService.getRecibosOnline(facturasID).then(async recibos => {

                if (recibos.length > 0) {
                  let existe;
                  for (let recibo of recibos) {
                    existe = await this.recibosBD.existeRecibo(recibo)
                    if (existe === false)
                      await this.recibosBD.insertarReciboTabla(recibo);
                  }
                }
                return true;
              }).catch(async error => {
                console.error('No se pudo obtener los recibos de las facturas', error)
                await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA RECIBOS: ' + JSON.stringify(error) + ' \n');
                if(error.status===401){
                  this.navCtrl.navigateRoot('login');
                }
                return false;
              })

            }).catch(async error => {
              console.error('Error en obtener facturas', error)
              await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA FACTURAS: ' + JSON.stringify(error) + ' \n');
              if(error.status===401){
                this.navCtrl.navigateRoot('login');
              }
              return false;
            })

        }).catch(error => {
          console.log('no se pudo eliminar la tabla facturas', error)
          return false;
        })
      } else {
        console.error("No se pudieron obtener los datos las facturas, verifique su conexion")
        await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE FACTUTAS \n');
        return Promise.reject(false)
      }

    } else {
      return Promise.resolve(true)
    }
  }
  bajarDocumentosEntregas(): Promise<boolean> {
    if (this.clientesEntregasDoc.length > 0) {
      return this.entregasDocBD.syncEntregasHabilitacion(Number(JSON.parse(localStorage.getItem('habilitacion')))).then(async booleano => {
        if (booleano === false) {
          return this.entregasDocBD.deleteAllEntregasDoc().then(async () => {//se elimino todo, insertas directo en la tabla
            if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
              return this.entregasService.obtenerEntregasDocOnline(this.clientesEntregasDoc)
                .then(async (entregasDoc: Array<any>) => {
                  for (let entregaD of entregasDoc) {
                    await this.entregasDocBD.insertarEntregaDocTabla(entregaD);
                  }
                  return true;
                }).catch(async error => {
                  console.error('Error en obtener documentos', error)
                  await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA ENTREGAS: ' + JSON.stringify(error) + ' \n');
                  
                  if(error.status===401){
                    this.navCtrl.navigateRoot('login');
                  }return false;

                })

            } else {
              console.error("No se pudieron obtener los datos las entregas, verifique su conexion")
              await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE ENTREGAS-DOCUMENTOS \n');
              return Promise.reject(false)
            }
          }).catch(error => {
            console.log('no se pudo eliminar la tabla entregas Doc', error)
            return false;
          })
        } else {//hay datos para sincronizar
          if (booleano === true) {
            if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
              return this.entregasService.obtenerEntregasDocOnline(this.clientesEntregasDoc)
                .then(async (entregasDoc: Array<any>) => {
                  for (let entregaD of entregasDoc) {
                    if (!this.entregasDocBD.existeEntregaDoc(entregaD.ID_DOCUMENTO)
                      && !this.entregasDocBD.existeEntregaDocSync(entregaD.ID_DOCUMENTO)) {
                      await this.entregasDocBD.insertarEntregaDocTabla(entregaD)
                    } else {//actualizar la tabla
                      await this.entregasDocBD.updateEntregasDoc(entregaD);
                    }
                  }
                  return true;
                }).catch(async error => {
                  console.error('Error en obtener documentos', error)
                  await this.writeLog.escribirLog(new Date() + ' Respuesta con error CONSULTA ENTREGAS-DOCUMENTOS: ' + JSON.stringify(error) + ' \n');
                  if(error.status===401){
                    this.navCtrl.navigateRoot('login');
                  }
                  return false;

                })

            } else {
              console.error("No se pudieron obtener los datos las entregas, verifique su conexion")
              await this.writeLog.escribirLog(new Date() + ' Respuesta OFFLINE DOCUMENTOS \n');
              return Promise.reject(false)

            }
          }
        }
      }).catch(error => {
        console.error('No se pudo acceder a la base de datos local', error)
        return false;
      })

    } else {
      return Promise.resolve(true)
    }
  }
  obtenerDatosInicio() {
    return this.usuarioBD.getUserData().then(data => {
      let user = data;
      return user;
    });
  }
}
