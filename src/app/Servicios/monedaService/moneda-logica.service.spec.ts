import { TestBed } from '@angular/core/testing';

import { MonedaLogicaService } from './moneda-logica.service';

describe('MonedaLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonedaLogicaService = TestBed.get(MonedaLogicaService);
    expect(service).toBeTruthy();
  });
});
