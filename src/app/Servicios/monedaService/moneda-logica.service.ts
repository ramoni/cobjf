import { Injectable } from '@angular/core';
import { Moneda } from '../../Clases/moneda';
import { HttpService } from '../httpFunctions/http.service';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';

@Injectable({
    providedIn: 'root'
})
export class MonedaLogicaService {
    monedas: Array<Moneda> = [];
    constructor(
        private http: HttpService,
        private monedaBD: MonedasBDService) {
    }

    getByIdOrName(idOrName: string): Moneda {
        this.getMonedasBD();
        let toRet;
        console.log('monedas getByIdOrName',this.monedas)
        if (isNaN(parseInt(idOrName))) {
            console.log('isNaN ',idOrName);
            toRet = this.monedas.find(mon => mon.descripcion === idOrName);
          
        } else {
        
            toRet = this.monedas.find(mon => mon.id === idOrName.toString());
        
        }
        console.log('getByIdIrName envia', toRet);
        return toRet;

    }

    /**
     * 1 == Gs
     * 2 == DS
     */
    convert(monto: number, monedaOrigen: Moneda, monedaDestino: Moneda, cot: number): number {

        if (monedaOrigen.id === "1" && monedaDestino.id === "2") {
            return Number((monto / cot).toFixed(2));
        }
        if (monedaOrigen.id === "2" && monedaDestino.id === "1") {
            return Number((monto * cot).toFixed(2));
        }
        return monto;
    }

    convertByDesc(monto: number, monedaOrigen: string, monedaDestino: string, cot: number): number {
        return this.convert(monto, this.getByIdOrName(monedaOrigen), this.getByIdOrName(monedaDestino), cot);
    }

    getMonedasOnline() {

        return this.http.doGet('monedas?', { first_result: 0, last_result: 100 }).then(res => {
            let data = JSON.parse(res.data).lista;

            let toRet: Array<Moneda> = [];

            toRet = data.map(m => {
                return new Moneda(m);
            });

            console.log("monedas ", toRet)
            this.monedas=toRet;
            return toRet;
        })
    }
    
    getMonedasBD(){
        return this.monedaBD.getMonedas().then(monedas=>{
            this.monedas=monedas
            return monedas;
        })
    }

    generateMontoValorizado(moneda, cotizacion, monto, cant_decimales){
        console.log("CONVERTIR A MONEDA "+moneda+" CON COTIZACION "+cotizacion+" EL MONTO "+monto+" CON CANT_DECIMALES "+cant_decimales)
        let montoVal = 0;
        if(moneda == 1){
          montoVal = parseFloat((monto*cotizacion).toFixed(cant_decimales));
        } else {
          montoVal = parseFloat((monto/cotizacion).toFixed(cant_decimales));
        }
        return montoVal;
    }
}
