import { Injectable } from '@angular/core';
import { ConfigService } from '../configuracion/config.service';
import { SesionService } from '../sesionValidation/sesion.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private config:ConfigService,
    private sessionValidation:SesionService
    ) { }

  isAuthenticated(){
    if (this.sessionValidation.validarSesion(localStorage.getItem(this.config.AUTHORIZATION_STRING), localStorage.getItem('expiredToken'))) {
      console.log("Sesion valida");
      return true;
    }
    console.log('Sesion expirada');
    return false;

  }  
}
