import { TestBed } from '@angular/core/testing';

import { CuentaLogicaService } from './cuenta-logica.service';

describe('CuentaLogicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CuentaLogicaService = TestBed.get(CuentaLogicaService);
    expect(service).toBeTruthy();
  });
});
