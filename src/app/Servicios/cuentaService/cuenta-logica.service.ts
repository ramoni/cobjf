import { Injectable } from '@angular/core';
import { Cuenta } from '../../Clases/Cuenta';
import { Entidad } from '../../Clases/entidad';

import { HttpService } from '../httpFunctions/http.service';
import { CuentasBDService } from '../almacenamientoSQLITE/cuentasBD/cuentas-bd.service';

@Injectable({
  providedIn: 'root'
})
export class CuentaLogicaService {

  public cuentas: Array<Cuenta> = [];
  public entidadCuentas: Array<Cuenta> = [];
  cuentaCache: any;


  constructor(private http: HttpService,
    private cuentasBD: CuentasBDService) {
    this.cuentaCache = {};
  }

  getByIdOrName(idOrName: string): Cuenta {
    if (this.cuentaCache[idOrName]) {
      return this.cuentaCache[idOrName]
    }

    let toRet;
    if (isNaN(parseFloat(idOrName))) {
      // it's a description
      toRet = this.cuentas.find(mon => mon.descripcion === idOrName);
    } else {
      // it's a id
      toRet = this.cuentas.find(mon => mon.id === idOrName.toString());
    }
    this.cuentaCache[idOrName] = toRet;
    return toRet;

  }

  getEntidadCuenta(banco: Entidad): Array<Cuenta> {


    console.log('Al retornar ya');
    console.log(this.entidadCuentas);
    return this.entidadCuentas;
  }
  loadEntidadCuentas(entidad: number): Promise<Array<Cuenta>> {
    //tenia timeout 4000
    return this.http.doGet('entidades/' + entidad + '/cuentas?', { first_result: 0, last_result: 100 })
      .then(res => {
        let data = JSON.parse(res.data);
        return data.lista;
      });
  }

  getCuentasOnline() {

    return this.http.doGet('cuentas?', { first_result: 0, last_result: 1000 })
      .then(res => {
        let data = JSON.parse(res.data).lista;
        let toRet: Array<Cuenta> = [];
        toRet = data.map(c => {
          return new Cuenta(c);
        })
        this.cuentas = toRet;
        console.log("cuentas ", toRet)

        return toRet;

      })
  }
  getCuentasBD() {
    return this.cuentasBD.getCuentas().then(cuentas=>{
      this.cuentas=cuentas;
      return cuentas;
    });
   
  }

}
