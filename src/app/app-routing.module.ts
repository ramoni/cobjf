import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './Guards/auth-guard.service';
import { NotAuthGuardService } from './Guards/notAuthGuard/not-auth-guard.service';


const routes: Routes = [
  //Por defecto enviar al login
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  //Rutas agrupadas con un guard que controla que no esten logueados
  {
    path:'',
    children: [
      //Ruta para loguearse
      { path: 'login', loadChildren: './Paginas/login/login.module#LoginPageModule', canActivate:[NotAuthGuardService] },
    ]
  },
  //Rutas agrupadas y controladas por un guard que controla que esten logueados
  {
    path: '',
    children: [
      { path: 'home', loadChildren: './Paginas/home/home.module#HomePageModule' ,canActivate: [AuthGuard]},
      { path: 'documentos', loadChildren: './Paginas/documentos/documentos.module#DocumentosPageModule' ,canActivate: [AuthGuard]},
      { path: 'actividades', loadChildren: './Paginas/actividades-con-clientes/actividades-con-clientes.module#ActividadesConClientesPageModule' ,canActivate: [AuthGuard]},
      { path: 'notas', loadChildren: './Paginas/notas/notas.module#NotasPageModule' ,canActivate: [AuthGuard]},
      { path: 'facturas', loadChildren: './Paginas/facturas/facturas.module#FacturasPageModule' ,canActivate: [AuthGuard]},
      { path: 'seleccionar-punto-ubicacion', loadChildren: './Paginas/seleccionar-punto-ubicacion/seleccionar-punto-ubicacion.module#SeleccionarPuntoUbicacionPageModule' ,canActivate: [AuthGuard]},
      { path: 'cambiar-pass', loadChildren: './Paginas/cambiar-pass/cambiar-pass.module#CambiarPassPageModule' ,canActivate: [AuthGuard]},
      { path: 'asignacion', loadChildren: './Paginas/asignacion/asignacion.module#AsignacionPageModule' ,canActivate: [AuthGuard]},
      { path: 'resumen-cobro', loadChildren: './Paginas/resumen-cobro/resumen-cobro.module#ResumenCobroPageModule' ,canActivate: [AuthGuard]},
      { path: 'crear-recibo', loadChildren: './Paginas/crear-recibo/crear-recibo.module#CrearReciboPageModule' ,canActivate: [AuthGuard]},
      { path: 'recibos-creados', loadChildren: './Paginas/recibos-creados/recibos-creados.module#RecibosCreadosPageModule' ,canActivate: [AuthGuard]},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    FormsModule,
    ReactiveFormsModule,
  
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
