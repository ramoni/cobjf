import { TestBed } from '@angular/core/testing';

import { NotAuthGuardService } from './not-auth-guard.service';

describe('NotAuthGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotAuthGuardService = TestBed.get(NotAuthGuardService);
    expect(service).toBeTruthy();
  });
});
