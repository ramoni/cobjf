import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Factura } from 'src/app/Clases/factura';
import { HttpService } from '../httpFunctions/http.service';
import { FacturaWithCheck } from 'src/app/Clases/factura-with-check';
import { FacturasBDService } from '../almacenamientoSQLITE/facturasBD/facturas-bd.service';
var FacturasLogicaService = /** @class */ (function () {
    function FacturasLogicaService(http, facturasBD) {
        this.http = http;
        this.facturasBD = facturasBD;
    }
    FacturasLogicaService.prototype.getList = function (cliente, offset, last_result, orden) {
        return this.http.doGet('clientes/' + cliente.id + '/facturas?', { first_result: offset, last_result: last_result, orderby: orden }).then(function (res) {
            var data = JSON.parse(res.data);
            console.log('facturas', data);
            var toRet = [];
            for (var _i = 0, _a = data.lista; _i < _a.length; _i++) {
                var row = _a[_i];
                var toFactura = new Factura(row, cliente);
                toRet.push(new FacturaWithCheck(toFactura, false));
            }
            return toRet;
        });
    };
    FacturasLogicaService.prototype.obtenerFacturasOnline = function (clientes) {
        return this.http.doGet('offline/facturas?first_result=1&last_result=100000?', {}, { clientes: clientes }).then(function (res) {
            var data = JSON.parse(res.data);
            var toRet = [];
            toRet = data.lista.map(function (c) {
                return c;
            });
            console.log("FACTURAS DE API:", toRet);
            return toRet;
        });
    };
    FacturasLogicaService.prototype.obtenerFacturasLocal = function (cliente, offset, last_result, orden) {
        return this.facturasBD.getFacturasLocal(cliente, offset, last_result, orden).then(function (data) {
            var toRet = [];
            toRet = data.map(function (f) {
                return new FacturaWithCheck(f, false);
            });
            return toRet;
        });
    };
    FacturasLogicaService.prototype.verificarFacturaFCO = function (facturas) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var facturaCheck, _i, facturas_1, f;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        facturaCheck = false;
                        _i = 0, facturas_1 = facturas;
                        _a.label = 1;
                    case 1:
                        if (!(_i < facturas_1.length)) return [3 /*break*/, 4];
                        f = facturas_1[_i];
                        return [4 /*yield*/, this.facturasBD.isFCO(f['id_factura'])];
                    case 2:
                        if (_a.sent()) {
                            facturaCheck = true;
                        }
                        else {
                            facturaCheck = false;
                            return [3 /*break*/, 4];
                        }
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, facturaCheck];
                }
            });
        });
    };
    FacturasLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            FacturasBDService])
    ], FacturasLogicaService);
    return FacturasLogicaService;
}());
export { FacturasLogicaService };
//# sourceMappingURL=facturas-logica.service.js.map