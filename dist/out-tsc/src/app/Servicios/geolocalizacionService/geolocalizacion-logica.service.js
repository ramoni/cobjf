import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { ConfigService } from '../configuracion/config.service';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
var GeolocalizacionLogicaService = /** @class */ (function () {
    function GeolocalizacionLogicaService(http, config, geolocation) {
        this.http = http;
        this.config = config;
        this.geolocation = geolocation;
    }
    GeolocalizacionLogicaService.prototype.getCiudad = function (latitud, longitud) {
        var url = this.config.GEO_REVERSE_URL.replace("LATITUD", latitud.toString()).replace("LONGITUD", longitud.toString());
        console.log("para ciudad ", url);
        return this.http.get(url).pipe(map(function (data) { return data.json().results[0].formatted_address; }));
    };
    GeolocalizacionLogicaService.prototype.getData = function (latitud, longitud) {
        var url = this.config.GEO_REVERSE_URL.replace("LATITUD", latitud.toString()).replace("LONGITUD", longitud.toString());
        return this.http.get(url).pipe(map(function (data) { return data.json().results[0]; }));
    };
    GeolocalizacionLogicaService.prototype.getPos = function () {
        var options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0,
        };
        return from(this.geolocation.getCurrentPosition(options).then(function (resp) {
            console.log("posicion tomada correctamente");
            return resp;
        }).catch(function (error) {
            console.error("error en obtener la posicion actual ", error);
            return null;
        }));
    };
    GeolocalizacionLogicaService.prototype.findCurrentCiudad = function () {
        var _this = this;
        return this.getPos().pipe(mergeMap(function (pos) {
            console.log("latitud y longitud ", pos.coords.latitude, pos.coords.longitude);
            return _this.getCiudad(pos.coords.latitude, pos.coords.longitude);
        }));
    };
    GeolocalizacionLogicaService.prototype.onError = function (error) {
        alert('code: ' + error.code + '\n' +
            'message: ' + error.message + '\n');
    };
    GeolocalizacionLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Http,
            ConfigService,
            Geolocation])
    ], GeolocalizacionLogicaService);
    return GeolocalizacionLogicaService;
}());
export { GeolocalizacionLogicaService };
//# sourceMappingURL=geolocalizacion-logica.service.js.map