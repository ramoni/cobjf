import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ConfigService } from '../configuracion/config.service';
import { SesionService } from '../sesionValidation/sesion.service';
var AuthService = /** @class */ (function () {
    function AuthService(config, sessionValidation) {
        this.config = config;
        this.sessionValidation = sessionValidation;
    }
    AuthService.prototype.isAuthenticated = function () {
        if (this.sessionValidation.validarSesion(localStorage.getItem(this.config.AUTHORIZATION_STRING), localStorage.getItem('expiredToken'))) {
            console.log("Sesion valida");
            return true;
        }
        console.log('Sesion expirada');
        return false;
    };
    AuthService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [ConfigService,
            SesionService])
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map