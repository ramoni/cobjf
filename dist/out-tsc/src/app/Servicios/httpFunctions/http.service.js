import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ConfigService } from '../configuracion/config.service';
import { HTTP } from '@ionic-native/http/ngx';
var HttpService = /** @class */ (function () {
    function HttpService(config, http) {
        this.config = config;
        this.http = http;
    }
    HttpService.prototype.doPost = function (method, body, queryParams) {
        if (queryParams === void 0) { queryParams = null; }
        var url = this.config.getUrl('post') + '/' + method;
        for (var propertyName in queryParams) {
            url += propertyName + '=' + queryParams[propertyName] + '&';
        }
        url = url.substring(0, url.length - 1);
        console.log('POST - Query to', url);
        this.http.clearCookies();
        this.http.setDataSerializer('json');
        this.headers = {
            'Content-Type': this.config.MEDIA_TYPE,
            'Accept': 'application/json'
        };
        if (localStorage.getItem(this.config.AUTHORIZATION_STRING)) {
            this.headers = {
                'Content-Type': this.config.MEDIA_TYPE,
                'Accept': 'application/json',
                'Authorization': localStorage.getItem(this.config.AUTHORIZATION_STRING)
            };
        }
        return this.http.post(url, body, this.headers).then(function (response) {
            var line_body = Object.keys(body).length;
            //console.log("imprime line_body ", line_body);
            var cantidad_enviada = new Buffer(JSON.stringify(body)).length + line_body;
            console.log('Total cantidad enviada', cantidad_enviada);
            if (localStorage.getItem('subida')) {
                var cantidad_actual = Number(localStorage.getItem('subida'));
                cantidad_actual = cantidad_actual + cantidad_enviada;
                localStorage.setItem('subida', JSON.stringify(cantidad_actual));
            }
            else {
                localStorage.setItem('subida', JSON.stringify(cantidad_enviada));
            }
            var headers = response.headers;
            console.log('cabecera ', headers);
            var cantidad_bajada = Number(headers['content-length']);
            if (localStorage.getItem('bajada')) {
                var cantidad_actual = Number(localStorage.getItem('bajada'));
                cantidad_actual += cantidad_enviada;
                localStorage.setItem('bajada', JSON.stringify(cantidad_actual));
            }
            else {
                localStorage.setItem('bajada', JSON.stringify(cantidad_bajada));
            }
            return Promise.resolve(response);
        }).catch(function (error) {
            console.log("error en post ", error);
            return Promise.reject(error);
        });
        //.timeout(30000, new Error('El servidor no esta disponible, intente nuevamente!'));
    };
    HttpService.prototype.doGet = function (method, queryParams, body) {
        if (queryParams === void 0) { queryParams = null; }
        if (body === void 0) { body = {}; }
        var url = this.config.getUrl('get') + '/' + method;
        if (queryParams) {
            for (var propertyName in queryParams) {
                url += propertyName + '=' + queryParams[propertyName] + '&';
            }
        }
        url = url.substring(0, url.length - 1);
        console.log('GET - Query to', url, queryParams);
        this.headers = {
            'Content-Type': this.config.MEDIA_TYPE,
            'Accept': 'application/json',
            'Authorization': localStorage.getItem(this.config.AUTHORIZATION_STRING)
        };
        return this.http.get(url, body, this.headers).then(function (response) {
            var headers = response.headers;
            //console.log("header lengt get ", headers['content-length']);
            var cantidad = Number(headers['content-length']);
            if (localStorage.getItem('bajada')) {
                var cantidad_actual = Number(localStorage.getItem('bajada'));
                cantidad_actual += cantidad;
                //console.log(cantidad_actual);
                localStorage.setItem('bajada', JSON.stringify(cantidad_actual));
            }
            else {
                localStorage.setItem('bajada', JSON.stringify(cantidad));
            }
            return Promise.resolve(response);
        }).catch(function (error) {
            console.log("error en el metodo get de " + method + error);
            return Promise.reject(error);
        });
        //.timeout(25000, new Error('El servidor no esta disponible, intente nuevamente !'))
    };
    HttpService.prototype.doPut = function (method, body, queryParams) {
        if (queryParams === void 0) { queryParams = null; }
        //let cantidad = Number(this._buildOptions(queryParams).headers.get('content-length'));
        //console.log(cantidad)
        var url = this.config.getUrl('put') + '/' + method;
        if (queryParams) {
            for (var propertyName in queryParams) {
                url += propertyName + '=' + queryParams[propertyName] + '&';
            }
        }
        url = url.substring(0, url.length - 1);
        console.debug('PUT - Query to', url);
        this.headers = {
            'Content-Type': this.config.MEDIA_TYPE,
            'Accept': 'application/json',
            'Authorization': localStorage.getItem(this.config.AUTHORIZATION_STRING)
        };
        return this.http.put(url, body, this.headers)
            .then(function (response) {
            var line_body = Object.keys(body).length;
            //console.log("imprime line_body ", line_body);
            var cantidad_enviada = new Buffer(JSON.stringify(body)).length + line_body;
            console.log('Total cantidad enviada', cantidad_enviada);
            if (localStorage.getItem('subida')) {
                var cantidad_actual = Number(localStorage.getItem('subida'));
                cantidad_actual = cantidad_actual + cantidad_enviada;
                localStorage.setItem('subida', JSON.stringify(cantidad_actual));
            }
            else {
                localStorage.setItem('subida', JSON.stringify(cantidad_enviada));
            }
            var headers = response.headers;
            var cantidad_bajada = Number(headers['content-length']);
            if (localStorage.getItem('bajada')) {
                var cantidad_actual = Number(localStorage.getItem('bajada'));
                cantidad_actual += cantidad_enviada;
                localStorage.setItem('bajada', JSON.stringify(cantidad_actual));
            }
            else {
                localStorage.setItem('bajada', JSON.stringify(cantidad_bajada));
            }
            return Promise.resolve(response);
        }).catch(function (error) {
            console.log(error);
            return Promise.reject(error);
        });
        //,timeout(30000));
    };
    HttpService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [ConfigService, HTTP])
    ], HttpService);
    return HttpService;
}());
export { HttpService };
//# sourceMappingURL=http.service.js.map