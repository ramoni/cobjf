import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
var ConfigService = /** @class */ (function () {
    function ConfigService(plat) {
        this.plat = plat;
        //api local Luis 
        //BASE_URL_REAL : string = "http://192.168.100.225:8000/api";
        //api remota Luis
        this.BASE_URL_REAL = "http://cobranzas-api.bypersoft.com/api";
        this.BASE_URL_POST = "http://localhost:8100/api";
        this.BASE_URL = "http://localhost:10000/cobranzas/service/v1";
        this.MEDIA_TYPE = 'application/json; charset=utf-8';
        this.GEO_REVERSE_URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=LATITUD,LONGITUD&key=AIzaSyCLDuEZBotJyL6dN6Yd2bQuVxfMOEAXwI0";
        this.AUTHORIZATION_STRING = "authorization";
        this.DEFAULT_DATE_FORMAT = "DD-MM-YYYY";
        this.IS_OFFLINE = false;
    }
    ConfigService.prototype.getUrl = function (tip) {
        if (tip === void 0) { tip = 'get'; }
        // if (this.plat.is('mobileweb')) {
        if (this.plat.is('cordova')) {
            return this.BASE_URL_REAL;
        }
        else {
            return this.BASE_URL;
        }
    };
    ConfigService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform])
    ], ConfigService);
    return ConfigService;
}());
export { ConfigService };
//# sourceMappingURL=config.service.js.map