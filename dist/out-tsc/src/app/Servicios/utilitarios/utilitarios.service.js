import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var UtilitariosService = /** @class */ (function () {
    function UtilitariosService() {
    }
    UtilitariosService.prototype.Unidades = function (num) {
        switch (num) {
            case 1: return 'UN';
            case 2: return 'DOS';
            case 3: return 'TRES';
            case 4: return 'CUATRO';
            case 5: return 'CINCO';
            case 6: return 'SEIS';
            case 7: return 'SIETE';
            case 8: return 'OCHO';
            case 9: return 'NUEVE';
        }
        return '';
    }; //Unidades()
    UtilitariosService.prototype.Decenas = function (num) {
        var decena = Math.floor(num / 10);
        var unidad = num - (decena * 10);
        switch (decena) {
            case 1:
                switch (unidad) {
                    case 0: return 'DIEZ';
                    case 1: return 'ONCE';
                    case 2: return 'DOCE';
                    case 3: return 'TRECE';
                    case 4: return 'CATORCE';
                    case 5: return 'QUINCE';
                    default: return 'DIECI' + this.Unidades(unidad);
                }
            case 2:
                switch (unidad) {
                    case 0: return 'VEINTE';
                    default: return 'VEINTI' + this.Unidades(unidad);
                }
            case 3: return this.DecenasY('TREINTA', unidad);
            case 4: return this.DecenasY('CUARENTA', unidad);
            case 5: return this.DecenasY('CINCUENTA', unidad);
            case 6: return this.DecenasY('SESENTA', unidad);
            case 7: return this.DecenasY('SETENTA', unidad);
            case 8: return this.DecenasY('OCHENTA', unidad);
            case 9: return this.DecenasY('NOVENTA', unidad);
            case 0: return this.Unidades(unidad);
        }
    }; //Unidades()
    UtilitariosService.prototype.DecenasY = function (strSin, numUnidades) {
        if (numUnidades > 0)
            return strSin + ' Y ' + this.Unidades(numUnidades);
        return strSin;
    }; //DecenasY()
    UtilitariosService.prototype.Centenas = function (num) {
        var centenas = Math.floor(num / 100);
        var decenas = num - (centenas * 100);
        switch (centenas) {
            case 1:
                if (decenas > 0)
                    return 'CIENTO ' + this.Decenas(decenas);
                return 'CIEN';
            case 2: return 'DOSCIENTOS ' + this.Decenas(decenas);
            case 3: return 'TRESCIENTOS ' + this.Decenas(decenas);
            case 4: return 'CUATROCIENTOS ' + this.Decenas(decenas);
            case 5: return 'QUINIENTOS ' + this.Decenas(decenas);
            case 6: return 'SEISCIENTOS ' + this.Decenas(decenas);
            case 7: return 'SETECIENTOS ' + this.Decenas(decenas);
            case 8: return 'OCHOCIENTOS ' + this.Decenas(decenas);
            case 9: return 'NOVECIENTOS ' + this.Decenas(decenas);
        }
        return this.Decenas(decenas);
    }; //Centenas()
    UtilitariosService.prototype.Seccion = function (num, divisor, strSingular, strPlural) {
        var cientos = Math.floor(num / divisor);
        var resto = num - (cientos * divisor);
        var letras = '';
        if (cientos > 0)
            if (cientos > 1)
                letras = this.Centenas(cientos) + ' ' + strPlural;
            else
                letras = strSingular;
        if (resto > 0)
            letras += '';
        return letras;
    }; //Seccion()
    UtilitariosService.prototype.Miles = function (num) {
        var divisor = 1000;
        var cientos = Math.floor(num / divisor);
        var resto = num - (cientos * divisor);
        var strMiles = this.Seccion(num, divisor, 'UN MIL', 'MIL');
        var strCentenas = this.Centenas(resto);
        if (strMiles == '')
            return strCentenas;
        return strMiles + ' ' + strCentenas;
    }; //Miles()
    UtilitariosService.prototype.Millones = function (num) {
        var divisor = 1000000;
        var cientos = Math.floor(num / divisor);
        var resto = num - (cientos * divisor);
        var strMillones = this.Seccion(num, divisor, 'UN MILLON ', 'MILLONES ');
        var strMiles = this.Miles(resto);
        if (strMillones == '')
            return strMiles;
        return strMillones + ' ' + strMiles;
    }; //Millones()
    UtilitariosService.prototype.numeroALetras = function (num, currency) {
        console.log("NUM ORIGINAL", num);
        num = Number(num.replace(',', '.'));
        var data = {
            numero: num,
            enteros: Math.floor(num),
            centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
            letrasCentavos: '',
            letrasMonedaPlural: currency.plural,
            letrasMonedaSingular: currency.singular,
            letrasMonedaCentavoPlural: 'CENTAVOS',
            letrasMonedaCentavoSingular: 'CENTAVO'
        };
        console.log("NUM: ", num, "DATA ", data);
        if (data.centavos > 0) {
            var centavos = '';
            if (data.centavos == 1)
                centavos = this.Millones(data.centavos) + ' ' + data.letrasMonedaCentavoSingular;
            else
                centavos = this.Millones(data.centavos) + ' ' + data.letrasMonedaCentavoPlural;
            data.letrasCentavos = 'CON ' + centavos;
        }
        ;
        var resp = "";
        if (data.enteros == 0) {
            resp = 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
        }
        else if (data.enteros == 1) {
            resp = this.Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
        }
        else {
            resp = this.Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
        }
        console.log("NUMERO A LETRAS: ", resp);
        return resp;
    };
    UtilitariosService.prototype.round = function (value, places) {
        /*
        if (places < 0)
                throw new IllegalArgumentException();
    
            long factor = (long) Math.pow(10, places);
            value = value * factor;
            long tmp = Math.round(value);
            return (double) tmp / factor;
        */
        var factor = Math.pow(10, places);
        value = Number((value * factor).toFixed(0));
        return (value / factor);
    };
    UtilitariosService.prototype.limpiar_caracteres_especiales = function (palabra) {
        console.log("Palabra recibida", palabra);
        if (palabra === "") {
            console.log("Palabra vacia");
            return "";
        }
        else {
            palabra = palabra.replace("ñ", "n");
            palabra = palabra.replace("Ñ", "N");
            palabra = palabra.replace("À", "A");
            palabra = palabra.replace("Á", "A");
            palabra = palabra.replace("Ã", "A");
            palabra = palabra.replace("Â", "A");
            palabra = palabra.replace("Ä", "A");
            palabra = palabra.replace("Å", "A");
            palabra = palabra.replace("È", "E");
            palabra = palabra.replace("É", "E");
            palabra = palabra.replace("Ê", "E");
            palabra = palabra.replace("Ë", "E");
            palabra = palabra.replace("Ì", "I");
            palabra = palabra.replace("Í", "I");
            palabra = palabra.replace("Î", "I");
            palabra = palabra.replace("Ï", "I");
            palabra = palabra.replace("Ò", "O");
            palabra = palabra.replace("Ó", "O");
            palabra = palabra.replace("Ô", "O");
            palabra = palabra.replace("Õ", "O");
            palabra = palabra.replace("Õ", "O");
            palabra = palabra.replace("Ù", "U");
            palabra = palabra.replace("Ú", "U");
            palabra = palabra.replace("Û", "U");
            palabra = palabra.replace("Ü", "U");
            palabra = palabra.replace("Ý", "Y");
            palabra = palabra.replace("à", "a");
            palabra = palabra.replace("á", "a");
            palabra = palabra.replace("â", "a");
            palabra = palabra.replace("ã", "a");
            palabra = palabra.replace("ä", "a");
            palabra = palabra.replace("å", "a");
            palabra = palabra.replace("è", "e");
            palabra = palabra.replace("é", "e");
            palabra = palabra.replace("ê", "e");
            palabra = palabra.replace("ë", "e");
            palabra = palabra.replace("ì", "i");
            palabra = palabra.replace("í", "i");
            palabra = palabra.replace("î", "i");
            palabra = palabra.replace("ï", "i");
            palabra = palabra.replace("ð", "o");
            palabra = palabra.replace("ò", "o");
            palabra = palabra.replace("ó", "o");
            palabra = palabra.replace("ô", "o");
            palabra = palabra.replace("õ", "o");
            palabra = palabra.replace("ö", "o");
            palabra = palabra.replace("ù", "u");
            palabra = palabra.replace("ú", "u");
            palabra = palabra.replace("û", "u");
            palabra = palabra.replace("ü", "u");
            palabra = palabra.replace("ý", "y");
            palabra = palabra.replace("ÿ", "y");
            palabra = palabra.replace("&", "y");
            console.log("Palabra reemplazada ", palabra);
            return palabra;
        }
    };
    UtilitariosService.prototype.formatearNumber = function (numero) {
        console.log('numero en formatearNumber', numero);
        var num = null;
        num = numero.toString();
        var separador = "."; // separador para los miles
        var sepDecimal = ','; // separador para los decimales
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
        }
        console.log('retorno formatearNumber', splitLeft + splitRight);
        return splitLeft + splitRight;
    };
    UtilitariosService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UtilitariosService);
    return UtilitariosService;
}());
export { UtilitariosService };
//# sourceMappingURL=utilitarios.service.js.map