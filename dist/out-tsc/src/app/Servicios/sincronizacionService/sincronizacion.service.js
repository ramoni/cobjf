import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { HttpService } from '../httpFunctions/http.service';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
//import { ConnectionStatus, NetworkControllerService } from '../networkController/network-controller.service';
import { WriteFileService } from '../writeFileService/logWrite.service';
import { File } from '@ionic-native/file/ngx';
var SincronizacionService = /** @class */ (function () {
    function SincronizacionService(recibosDBservice, http, notasDBservice, entregasDBservice, 
    //private networkService:NetworkControllerService,
    writeLog, fileService) {
        this.recibosDBservice = recibosDBservice;
        this.http = http;
        this.notasDBservice = notasDBservice;
        this.entregasDBservice = entregasDBservice;
        this.writeLog = writeLog;
        this.fileService = fileService;
    }
    SincronizacionService.prototype.sincronizarTodoUnavez = function () {
        this.sincronizarEntregas(JSON.parse(localStorage.getItem('habilitacion')));
        this.sincronizarNotas(JSON.parse(localStorage.getItem('habilitacion')));
        this.sincronizarRecibos(JSON.parse(localStorage.getItem('habilitacion')));
    };
    SincronizacionService.prototype.sincronizarTodo = function () {
        var _this = this;
        console.log('Empezando Sincronizacion');
        setInterval(function () {
            _this.writeLog.escribirLog(new Date() + 'Peticion: Sincronizacion de datos \n ');
            console.log('De vuelta a la sincronizacion');
            if (localStorage.getItem('habilitacion')) {
                _this.sincronizarEntregas(JSON.parse(localStorage.getItem('habilitacion')));
                _this.sincronizarNotas(JSON.parse(localStorage.getItem('habilitacion')));
                _this.sincronizarRecibos(JSON.parse(localStorage.getItem('habilitacion')));
            }
        }, 180000);
    };
    SincronizacionService.prototype.sincronizarRecibos = function (habilitacion) {
        var _this = this;
        console.log("Inicio de sincronizacion de recibos");
        return this.recibosDBservice.getRecibosASincronizar(habilitacion).then(function (recibos) {
            if (recibos.length > 0) {
                var recibosObject = {
                    recibos: recibos,
                    habilitacion: habilitacion
                };
                _this.writeLog.escribirLog(new Date() + "Info: Recibos a sincronizar: " + JSON.stringify(recibosObject));
                //if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
                return _this.http.doPost('offline/recibos/nuevo?', recibosObject).then(function (data) {
                    var respJson = JSON.parse(data.data);
                    console.log("Respuesta de la sincronizacion de recibos ", respJson.exito);
                    //Actualiza en la base de datos local la columna de requiere sincronizacion
                    _this.recibosDBservice.actualizarSincronizacionDeRecibos();
                    _this.writeLog.escribirLog(new Date() + 'Exito: Recibos sincronizados exitosamente \n ');
                    return true;
                }).catch(function (error) {
                    var errorJson = JSON.parse(error.error);
                    console.error("Error en sincronizar recibos");
                    console.error(errorJson.error);
                    _this.writeLog.escribirLog(new Date() + 'Error: Error al sincronizar recibos \n Error: ' + JSON.stringify(errorJson));
                    return false;
                });
                /*} else {
                  console.error("No tiene conexion a internet");
                  this.writeLog.escribirLog(new Date()+ "Error: No tiene conexion a internet "+this.networkService.getCurrentNetworkStatus())
                }*/
            }
            else {
                _this.writeLog.escribirLog(new Date() + "Alerta: No hay recibos para sincronizar");
                console.log("No hay recibos para sincronizar");
                return true;
            }
        }).catch(function (error) {
            _this.writeLog.escribirLog(new Date() + "Error: Traer recibos de la base de datos con error " + JSON.stringify(error));
            console.error(error);
            return false;
        });
    };
    SincronizacionService.prototype.sincronizarNotas = function (habilitacion) {
        var _this = this;
        console.log("Inicio de sincronizacion de notas");
        return this.notasDBservice.getNotasASincronizar(habilitacion).then(function (notas) {
            if (notas.length > 0) {
                var notasObject = {
                    notas: notas,
                    habilitacion: habilitacion
                };
                _this.writeLog.escribirLog(new Date() + "Info: Notas a sincronizar: " + JSON.stringify(notasObject));
                //if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
                console.log("NOTAS PARA SINCRONIZAR: ", notas);
                return _this.http.doPost('offline/notas/nuevo?', notasObject).then(function (data) {
                    var respJson = JSON.parse(data.data);
                    console.log("Respuesta de la sincronizacion de notas ", respJson.exito);
                    _this.notasDBservice.actualizarSincronizacionDeNotas();
                    _this.writeLog.escribirLog(new Date() + 'Exito: Notas sincronizadas exitosamente \n ');
                    return true;
                }).catch(function (error) {
                    var errorJson = JSON.parse(error.error);
                    console.error("Error en sincronizar notas");
                    console.error(errorJson.error);
                    _this.writeLog.escribirLog(new Date() + 'Error: Error al sincronizar notas \n Error: ' + JSON.stringify(errorJson));
                    return false;
                });
                //}
            }
            else {
                console.log("No hay notas para sincronizar");
                _this.writeLog.escribirLog(new Date() + "Alerta: No hay notas para sincronizar");
                return true;
            }
        }).catch(function (error) {
            console.error(error);
            _this.writeLog.escribirLog(new Date() + "Error: Traer notas de la base de datos con error " + JSON.stringify(error));
            return false;
        });
    };
    SincronizacionService.prototype.sincronizarEntregas = function (habilitacion) {
        var _this = this;
        console.log("Inicio de sincronizacion de entregas");
        return this.entregasDBservice.getEntregasASincronizar(habilitacion).then(function (entregas) {
            if (entregas.length > 0) {
                var entregasObject = {
                    entregas: entregas,
                    habilitacion: habilitacion
                };
                _this.writeLog.escribirLog(new Date() + "Info: Entregas a sincronizar " + JSON.stringify(entregasObject));
                //if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
                return _this.http.doPost('offline/entregas/nuevo?', entregasObject).then(function (data) {
                    var respJson = JSON.parse(data.data);
                    console.log("Respuesta de la sincronizacion de entregas ", respJson.exito);
                    _this.entregasDBservice.actualizarSincronizacionDeEntregas();
                    _this.writeLog.escribirLog(new Date() + 'Exito: Entregas sincronizadas exitosamente \n ');
                    return true;
                }).catch(function (error) {
                    var errorJson = JSON.parse(error.error);
                    console.error("Error en sincronizar entregas");
                    console.error(errorJson.error);
                    _this.writeLog.escribirLog(new Date() + 'Error: Error al sincronizar entregas \n Error: ' + JSON.stringify(errorJson));
                    return false;
                });
                //}
            }
            else {
                console.log("No hay entregas para sincronizar");
                _this.writeLog.escribirLog(new Date() + "Alerta: No hay entregas para sincronizar");
                return true;
            }
        }).catch(function (error) {
            console.error(error);
            _this.writeLog.escribirLog(new Date() + "Error: Traer entregas de la base de datos con error " + JSON.stringify(error));
            return false;
        });
    };
    SincronizacionService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [RecibosBDService,
            HttpService,
            NotasBDService,
            EntregasDocBDService,
            WriteFileService,
            File])
    ], SincronizacionService);
    return SincronizacionService;
}());
export { SincronizacionService };
//# sourceMappingURL=sincronizacion.service.js.map