import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { UsuarioService } from '../usuarioService/usuario.service';
import { LoadingControllerService } from '../loaddingController/loading-controller.service';
import { ToastControllerService } from '../toastController/toast-controller.service';
var AlertControllerService = /** @class */ (function () {
    function AlertControllerService(alertCtrl, userService, loadingService, toastService, navControl) {
        this.alertCtrl = alertCtrl;
        this.userService = userService;
        this.loadingService = loadingService;
        this.toastService = toastService;
        this.navControl = navControl;
    }
    AlertControllerService.prototype.simpleAlert = function (title, texto, buttons) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: title,
                            message: texto,
                            buttons: buttons,
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AlertControllerService.prototype.alertMessageWithHandler = function (titulo, mensaje, handler, conCancelar, habilitacion_caja) {
        if (handler === void 0) { handler = null; }
        if (conCancelar === void 0) { conCancelar = false; }
        if (habilitacion_caja === void 0) { habilitacion_caja = null; }
        console.log(habilitacion_caja);
        var buttons = [{
                text: 'Aceptar',
                handler: function (data) { if (handler)
                    handler(data); }
            }];
        if (conCancelar) {
            buttons.unshift({
                text: 'Cancelar',
                handler: null
            });
        }
        this.simpleAlert(titulo, mensaje, buttons);
    };
    AlertControllerService.prototype.confirmExitAPP = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var confirm;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Cobranzas',
                            message: 'Esta seguro de que desea salir de la Aplicación?',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Aceptar',
                                    handler: function () {
                                        navigator['app'].exitApp();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        confirm = _a.sent();
                        return [4 /*yield*/, confirm.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AlertControllerService.prototype.mostrarConfirmacionLogout = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var confirm;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Cobranzas',
                            message: 'Esta seguro de que desea salir de la Aplicación?',
                            buttons: [
                                {
                                    text: 'Cancelar'
                                },
                                {
                                    text: 'Aceptar',
                                    handler: function () {
                                        _this.loadingService.present("Verificando datos ...");
                                        _this.userService.existenDatosHabilitacion(Number(localStorage.getItem('habilitacion'))).then(function (resp) {
                                            if (resp) {
                                                _this.loadingService.dismiss();
                                                _this.toastService.presentToast("Existen datos que faltan ser subidos, aguarde unos minutos, verifique su conexion a internet e intente nuevamente!");
                                            }
                                            else {
                                                _this.loadingService.dismiss();
                                                localStorage.removeItem("expiredToken");
                                                localStorage.removeItem("authorization");
                                                _this.navControl.navigateRoot("/login");
                                            }
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        confirm = _a.sent();
                        return [4 /*yield*/, confirm.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AlertControllerService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [AlertController,
            UsuarioService,
            LoadingControllerService,
            ToastControllerService,
            NavController])
    ], AlertControllerService);
    return AlertControllerService;
}());
export { AlertControllerService };
//# sourceMappingURL=alert-controller.service.js.map