import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { NetworkControllerService, ConnectionStatus } from '../networkController/network-controller.service';
import { ConceptoLogicaService } from '../conceptoService/concepto-logica.service';
import { EntidadLogicaService } from '../entidadService/entidad-logica.service';
import { MonedaLogicaService } from '../monedaService/moneda-logica.service';
import { ValoresLogicaService } from '../valorService/valores-logica.service';
import { CuentaLogicaService } from '../cuentaService/cuenta-logica.service';
import { UsuarioService } from '../usuarioService/usuario.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { ClientesLogicaService } from '../clienteService/clientes-logica.service';
import { FacturasBDService } from '../almacenamientoSQLITE/facturasBD/facturas-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { FacturasLogicaService } from '../facturasService/facturas-logica.service';
import { NotasLogicaService } from '../notaService/notas-logica.service';
import { DocumentosLogicaService } from '../documentosService/documentos-logica.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { RecibosLogicaService } from '../reciboService/recibos-logica.service';
import { ClientesBDService } from '../almacenamientoSQLITE/clientesBD/clientes-bd.service';
import { EntidadesBDService } from '../almacenamientoSQLITE/entidadesBD/entidades-bd.service';
import { ConceptosBDService } from '../almacenamientoSQLITE/conceptosBD/conceptos-bd.service';
import { ValoresBDService } from '../almacenamientoSQLITE/valoresBD/valores-bd.service';
import { CuentasBDService } from '../almacenamientoSQLITE/cuentasBD/cuentas-bd.service';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';
var DatosOnlineService = /** @class */ (function () {
    function DatosOnlineService(networkService, conceptoLogic, entidadesLogic, monedaLogic, valoresLogic, cuentasLogic, userService, usuarioBD, clienteService, facturasBD, entregasDocBD, facturasService, entregasService, notasService, recibosBD, recibosService, clientesBD, conceptosBD, entidadesBD, valorBD, cuentasBD, monedaBD) {
        this.networkService = networkService;
        this.conceptoLogic = conceptoLogic;
        this.entidadesLogic = entidadesLogic;
        this.monedaLogic = monedaLogic;
        this.valoresLogic = valoresLogic;
        this.cuentasLogic = cuentasLogic;
        this.userService = userService;
        this.usuarioBD = usuarioBD;
        this.clienteService = clienteService;
        this.facturasBD = facturasBD;
        this.entregasDocBD = entregasDocBD;
        this.facturasService = facturasService;
        this.entregasService = entregasService;
        this.notasService = notasService;
        this.recibosBD = recibosBD;
        this.recibosService = recibosService;
        this.clientesBD = clientesBD;
        this.conceptosBD = conceptosBD;
        this.entidadesBD = entidadesBD;
        this.valorBD = valorBD;
        this.cuentasBD = cuentasBD;
        this.monedaBD = monedaBD;
        this.clientesFacturas = [];
        this.clientesEntregasDoc = [];
        this.facturasID = [];
    }
    DatosOnlineService.prototype.bajarDatosOnline = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var userData, moneda, entidad, cuenta, valor, cotizacion, concepto, notas, clientes, facturas, documentos;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        userData = false;
                        moneda = false;
                        entidad = false;
                        cuenta = false;
                        valor = false;
                        cotizacion = false;
                        concepto = false;
                        notas = false;
                        clientes = false;
                        facturas = false;
                        documentos = false;
                        return [4 /*yield*/, this.actualizarDatosUsuario()];
                    case 1:
                        userData = _a.sent();
                        return [4 /*yield*/, this.bajarMonedas()];
                    case 2:
                        moneda = _a.sent();
                        return [4 /*yield*/, this.bajarEntidades()];
                    case 3:
                        entidad = _a.sent();
                        return [4 /*yield*/, this.bajarCuentas()];
                    case 4:
                        cuenta = _a.sent();
                        return [4 /*yield*/, this.bajarValores()];
                    case 5:
                        valor = _a.sent();
                        return [4 /*yield*/, this.bajarCotizacion()];
                    case 6:
                        cotizacion = _a.sent();
                        return [4 /*yield*/, this.bajarConceptos()];
                    case 7:
                        concepto = _a.sent();
                        return [4 /*yield*/, this.bajarNotas()];
                    case 8:
                        notas = _a.sent();
                        return [4 /*yield*/, this.bajarClientes()];
                    case 9:
                        clientes = _a.sent();
                        return [4 /*yield*/, this.bajarFacturas()];
                    case 10:
                        facturas = _a.sent();
                        return [4 /*yield*/, this.bajarDocumentosEntregas()];
                    case 11:
                        documentos = _a.sent();
                        console.log('userdata', userData);
                        console.log('entidad', entidad);
                        console.log('cuenta', cuenta);
                        console.log('valor', valor);
                        console.log('cotizacion', cotizacion);
                        console.log('concepto', concepto);
                        console.log('notas ', notas);
                        console.log('clientes', clientes);
                        console.log('facturas', facturas);
                        console.log('documentos', documentos);
                        if (userData && moneda && entidad && cuenta && valor && cotizacion && concepto && notas && clientes
                            && facturas && documentos) {
                            console.log('return true');
                            return [2 /*return*/, true];
                        }
                        else {
                            console.log('return false');
                            return [2 /*return*/, false];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    DatosOnlineService.prototype.actualizarDatosUsuario = function () {
        var _this = this;
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.userService.actualizarDatosUsuario().then(function (data) {
                return _this.usuarioBD.updateDatosUsuario(data).then(function (resp) {
                    //Se insertaron los datos refrescados del usuario
                    return resp;
                }).catch(function (error) {
                    console.error('no se pudo actualizar los datos de usuario en la tabla', error);
                    return false;
                });
            }).catch(function (error) {
                console.error('no se pudo traer los datos del usuario', error);
                return false;
            });
        }
        else {
            return Promise.reject(false);
        }
    };
    DatosOnlineService.prototype.bajarConceptos = function () {
        var _this = this;
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.conceptoLogic.getConceptosOnline().then(function (conceptos) {
                return _this.conceptosBD.deleteAllConceptos().then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _i, conceptos_1, concepto;
                    return tslib_1.__generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _i = 0, conceptos_1 = conceptos;
                                _a.label = 1;
                            case 1:
                                if (!(_i < conceptos_1.length)) return [3 /*break*/, 4];
                                concepto = conceptos_1[_i];
                                return [4 /*yield*/, this.conceptosBD.addConcepto(concepto)];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/, true];
                        }
                    });
                }); }).catch(function () {
                    return false;
                });
            }).catch(function (error) {
                console.log('Error al obtener conceptos online', error);
                return false;
            });
        }
        else {
            console.log("No se pudieron obtener los datos, verifique su conexion");
            return Promise.reject(false);
        }
    };
    DatosOnlineService.prototype.bajarCotizacion = function () {
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.valoresLogic.getCotizacionOnline().then(function () {
                console.log('se obtuvo la cotizacion');
                return true;
            }).catch(function (error) {
                console.log('error al obtener la cotizacion', error);
                return false;
            });
        }
        else {
            console.log("No se pudieron obtener los datos, verifique su conexion");
            return Promise.reject(false);
        }
    };
    DatosOnlineService.prototype.bajarEntidades = function () {
        var _this = this;
        //obtenemos entidades online
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.entidadesLogic.getEntidadesOnline().then(function (entidades) {
                return _this.entidadesBD.deleteAllEntidades().then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _i, entidades_1, entidad;
                    return tslib_1.__generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _i = 0, entidades_1 = entidades;
                                _a.label = 1;
                            case 1:
                                if (!(_i < entidades_1.length)) return [3 /*break*/, 4];
                                entidad = entidades_1[_i];
                                return [4 /*yield*/, this.entidadesBD.addEntidad(entidad)];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/, true];
                        }
                    });
                }); }).catch(function () {
                    return false;
                });
            }).catch(function (error) {
                console.log('Error al obtener entidades online', error);
                return false;
            });
        }
        else {
            console.log("No se pudieron obtener los datos, verifique su conexion");
            return Promise.reject(false);
        }
    };
    DatosOnlineService.prototype.bajarMonedas = function () {
        var _this = this;
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.monedaLogic.getMonedasOnline().then(function (monedas) {
                return _this.monedaBD.deleteAllMonedas().then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _i, monedas_1, moneda;
                    return tslib_1.__generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _i = 0, monedas_1 = monedas;
                                _a.label = 1;
                            case 1:
                                if (!(_i < monedas_1.length)) return [3 /*break*/, 4];
                                moneda = monedas_1[_i];
                                return [4 /*yield*/, this.monedaBD.addMoneda(moneda)];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/, true];
                        }
                    });
                }); }).catch(function () {
                    return false;
                });
            }).catch(function (error) {
                console.log('Error al obtener monedas online ', error);
                return false;
            });
        }
        else {
            console.log("No se pudieron obtener los datos, verifique su conexion");
            return Promise.reject(false);
        }
    };
    DatosOnlineService.prototype.bajarCuentas = function () {
        var _this = this;
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.cuentasLogic.getCuentasOnline().then(function (cuentas) {
                return _this.cuentasBD.deleteAllCuentas().then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _i, cuentas_1, cuenta;
                    return tslib_1.__generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _i = 0, cuentas_1 = cuentas;
                                _a.label = 1;
                            case 1:
                                if (!(_i < cuentas_1.length)) return [3 /*break*/, 4];
                                cuenta = cuentas_1[_i];
                                return [4 /*yield*/, this.cuentasBD.addCuenta(cuenta)];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/, true];
                        }
                    });
                }); }).catch(function () {
                    return false;
                });
            }).catch(function (error) {
                console.log('Error al obtener cuentas online ', error);
                return false;
            });
        }
        else {
            console.log("No se pudieron obtener los datos, verifique su conexion");
            return Promise.reject(false);
        }
    };
    DatosOnlineService.prototype.bajarValores = function () {
        var _this = this;
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.valoresLogic.getValoresOnline().then(function (valores) {
                return _this.valorBD.deleteAllValores().then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _i, valores_1, valor;
                    return tslib_1.__generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _i = 0, valores_1 = valores;
                                _a.label = 1;
                            case 1:
                                if (!(_i < valores_1.length)) return [3 /*break*/, 4];
                                valor = valores_1[_i];
                                return [4 /*yield*/, this.valorBD.addValor(valor)];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/, true];
                        }
                    });
                }); }).catch(function () {
                    return false;
                });
            }).catch(function (error) {
                console.log('Error al obtener cuentas online ', error);
                return false;
            });
        }
        else {
            console.log("No se pudieron obtener los datos, verifique su conexion");
            return Promise.reject(false);
        }
    };
    DatosOnlineService.prototype.bajarNotas = function () {
        var _this = this;
        if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
            return this.notasService.getAllNotasOnline(JSON.parse(localStorage.getItem('habilitacion')))
                .then(function (notas) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                return tslib_1.__generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.notasService.guardarNotas(notas)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, true];
                    }
                });
            }); }).catch(function (error) {
                console.log('no se pudo obtener las notas', error);
                return false;
            });
        }
        else {
            console.log("No se pudieron obtener los datos, verifique su conexion");
            return false;
        }
    };
    DatosOnlineService.prototype.bajarClientes = function () {
        var _this = this;
        //llamada a visitas
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
            return this.clienteService.getVisitasOnline(0, 5000, null, 'T').then(function (clientes) {
                return _this.clientesBD.deleteAllClientes().then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _i, clientes_1, cliente;
                    return tslib_1.__generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!(clientes.length > 0)) return [3 /*break*/, 5];
                                _i = 0, clientes_1 = clientes;
                                _a.label = 1;
                            case 1:
                                if (!(_i < clientes_1.length)) return [3 /*break*/, 4];
                                cliente = clientes_1[_i];
                                if (cliente.facturas > 0)
                                    this.clientesFacturas.push(Number(cliente.id));
                                if (cliente.entregas > 0)
                                    this.clientesEntregasDoc.push(Number(cliente.id));
                                //insertamos el cliente
                                return [4 /*yield*/, this.clientesBD.addCliente(cliente)];
                            case 2:
                                //insertamos el cliente
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4: return [2 /*return*/, true];
                            case 5: return [2 /*return*/, true];
                        }
                    });
                }); }).catch(function (error) {
                    console.log('No se puede eliminar los datos de la tabla clientes', error);
                    return false;
                });
            }).catch(function (error) {
                console.log('no puede obtenerse las visitas', error);
                return false;
            });
        }
        else {
            console.error("No se pudieron obtener los datos de las visitas, verifique su conexion");
        }
    };
    DatosOnlineService.prototype.bajarFacturas = function () {
        var _this = this;
        if (this.clientesFacturas.length > 0) {
            if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
                return this.facturasBD.deleteAllFacturas().then(function () {
                    return _this.facturasService.obtenerFacturasOnline(_this.clientesFacturas)
                        .then(function (facturas) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                        var _i, facturas_1, facturaOn, _a, facturas_2, fac;
                        var _this = this;
                        return tslib_1.__generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _i = 0, facturas_1 = facturas;
                                    _b.label = 1;
                                case 1:
                                    if (!(_i < facturas_1.length)) return [3 /*break*/, 4];
                                    facturaOn = facturas_1[_i];
                                    return [4 /*yield*/, this.facturasBD.addFacturas(facturaOn)];
                                case 2:
                                    _b.sent();
                                    _b.label = 3;
                                case 3:
                                    _i++;
                                    return [3 /*break*/, 1];
                                case 4:
                                    //obtenemos los recibos
                                    for (_a = 0, facturas_2 = facturas; _a < facturas_2.length; _a++) {
                                        fac = facturas_2[_a];
                                        this.facturasID.push(fac.ID_FACTURA);
                                    }
                                    return [2 /*return*/, this.recibosService.getRecibosOnline(this.facturasID).then(function (recibos) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                            var existe, _i, recibos_1, recibo;
                                            return tslib_1.__generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        if (!(recibos.length > 0)) return [3 /*break*/, 5];
                                                        existe = void 0;
                                                        _i = 0, recibos_1 = recibos;
                                                        _a.label = 1;
                                                    case 1:
                                                        if (!(_i < recibos_1.length)) return [3 /*break*/, 5];
                                                        recibo = recibos_1[_i];
                                                        return [4 /*yield*/, this.recibosBD.existeRecibo(recibo)];
                                                    case 2:
                                                        existe = _a.sent();
                                                        if (!(existe === false)) return [3 /*break*/, 4];
                                                        return [4 /*yield*/, this.recibosBD.insertarReciboTabla(recibo)];
                                                    case 3:
                                                        _a.sent();
                                                        _a.label = 4;
                                                    case 4:
                                                        _i++;
                                                        return [3 /*break*/, 1];
                                                    case 5: return [2 /*return*/, true];
                                                }
                                            });
                                        }); }).catch(function (error) {
                                            console.error('No se pudo obtener los recibos de las facturas', error);
                                            return false;
                                        })];
                            }
                        });
                    }); }).catch(function (error) {
                        console.error('Error en obtener facturas', error);
                        return false;
                    });
                }).catch(function (error) {
                    console.log('no se pudo eliminar la tabla facturas', error);
                    return false;
                });
            }
            else {
                console.error("No se pudieron obtener los datos las facturas, verifique su conexion");
                return Promise.reject(false);
            }
        }
        else {
            return Promise.resolve(true);
        }
    };
    DatosOnlineService.prototype.bajarDocumentosEntregas = function () {
        var _this = this;
        if (this.clientesEntregasDoc.length > 0) {
            return this.entregasDocBD.syncEntregasHabilitacion(Number(JSON.parse(localStorage.getItem('habilitacion')))).then(function (booleano) {
                if (booleano === false) {
                    return _this.entregasDocBD.deleteAllEntregasDoc().then(function () {
                        if (_this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
                            return _this.entregasService.obtenerEntregasDocOnline(_this.clientesEntregasDoc).then(function (entregasDoc) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                var _i, entregasDoc_1, entregaD;
                                return tslib_1.__generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            _i = 0, entregasDoc_1 = entregasDoc;
                                            _a.label = 1;
                                        case 1:
                                            if (!(_i < entregasDoc_1.length)) return [3 /*break*/, 4];
                                            entregaD = entregasDoc_1[_i];
                                            return [4 /*yield*/, this.entregasDocBD.insertarEntregaDocTabla(entregaD)];
                                        case 2:
                                            _a.sent();
                                            _a.label = 3;
                                        case 3:
                                            _i++;
                                            return [3 /*break*/, 1];
                                        case 4: return [2 /*return*/, true];
                                    }
                                });
                            }); }).catch(function (error) {
                                console.error('Error en obtener documentos', error);
                                return false;
                            });
                        }
                        else {
                            return Promise.reject(false);
                        }
                    }).catch(function (error) {
                        console.error("No se pudieron obtener los datos las entregas, verifique su conexion");
                        console.log('no se pudo eliminar la tabla entregas Doc', error);
                        return false;
                    });
                }
                else { //hay datos para sincronizar
                    if (booleano === true) {
                        if (_this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
                            return _this.entregasService.obtenerEntregasDocOnline(_this.clientesEntregasDoc).then(function (entregasDoc) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                var _i, entregasDoc_2, entregaD;
                                return tslib_1.__generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            _i = 0, entregasDoc_2 = entregasDoc;
                                            _a.label = 1;
                                        case 1:
                                            if (!(_i < entregasDoc_2.length)) return [3 /*break*/, 6];
                                            entregaD = entregasDoc_2[_i];
                                            if (!(!this.entregasDocBD.existeEntregaDoc(entregaD.ID_DOCUMENTO)
                                                && !this.entregasDocBD.existeEntregaDocSync(entregaD.ID_DOCUMENTO))) return [3 /*break*/, 3];
                                            return [4 /*yield*/, this.entregasDocBD.insertarEntregaDocTabla(entregaD)];
                                        case 2:
                                            _a.sent();
                                            return [3 /*break*/, 5];
                                        case 3: //actualizar la tabla
                                        return [4 /*yield*/, this.entregasDocBD.updateEntregasDoc(entregaD)];
                                        case 4:
                                            _a.sent();
                                            _a.label = 5;
                                        case 5:
                                            _i++;
                                            return [3 /*break*/, 1];
                                        case 6: return [2 /*return*/, true];
                                    }
                                });
                            }); }).catch(function (error) {
                                console.error('Error en obtener documentos', error);
                                return false;
                            });
                        }
                        else {
                            console.error("No se pudieron obtener los datos las entregas, verifique su conexion");
                            return Promise.reject(false);
                        }
                    }
                }
            });
        }
        else {
            return Promise.resolve(true);
        }
    };
    DatosOnlineService.prototype.obtenerDatosInicio = function () {
        return this.usuarioBD.getUserData().then(function (data) {
            var user = data;
            return user;
        });
    };
    DatosOnlineService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [NetworkControllerService,
            ConceptoLogicaService,
            EntidadLogicaService,
            MonedaLogicaService,
            ValoresLogicaService,
            CuentaLogicaService,
            UsuarioService,
            UsuariosBDService,
            ClientesLogicaService,
            FacturasBDService,
            EntregasDocBDService,
            FacturasLogicaService,
            DocumentosLogicaService,
            NotasLogicaService,
            RecibosBDService,
            RecibosLogicaService,
            ClientesBDService,
            ConceptosBDService,
            EntidadesBDService,
            ValoresBDService,
            CuentasBDService,
            MonedasBDService])
    ], DatosOnlineService);
    return DatosOnlineService;
}());
export { DatosOnlineService };
//# sourceMappingURL=datos-online.service.js.map