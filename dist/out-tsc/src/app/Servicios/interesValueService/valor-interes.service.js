import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import * as moment from 'moment';
var ValorInteresService = /** @class */ (function () {
    function ValorInteresService(http) {
        this.http = http;
    }
    ValorInteresService.prototype.getInteres = function (monto, fecha) {
        this.totalInteres = 0;
        this.loadInteres(monto, fecha);
        return this.totalInteres;
    };
    ValorInteresService.prototype.loadInteres = function (monto, fecha) {
        return this.http.doGet('comisiones/' + monto + '/' + fecha + '?').then(function (res) {
            var data = JSON.parse(res.data);
            //console.log('data de comisiones para interes ',data)
            return Number(data[0].COMISION);
        });
    };
    ValorInteresService.prototype.calcularInteres = function (vencimientoFac, saldo) {
        var interes = 0.0;
        var today = ((moment(new Date(), "DD-MM-YYYY")).toDate()).getTime();
        console.log('today 1', (moment(new Date(), "DD-MM-YYYY")).toDate());
        console.log('today 2', today);
        console.log('vencimiento factura 1', vencimientoFac, '         ', (moment(vencimientoFac, "DD-MM-YYYY")).toDate());
        var vencimiento = ((moment(vencimientoFac, "DD-MM-YYYY")).toDate()).getTime();
        console.log('vencimiento factura 2', vencimiento);
        if (today <= vencimiento) {
            return interes;
        }
        else {
            /*
            int dias = (int) ((now - vencimiento.getTime()) / 86400000);
                      interes = round(dias * saldo * (0.05 / 100), 2);
            */
            var dias = (Math.trunc(((today - vencimiento) / 86400000)));
            //interes = this.round(dias * saldo * (0.05 / 100), 2);
            var value = dias * saldo * (0.05 / 100);
            var factor = Math.pow(10, 2);
            var val = Number((value * factor).toFixed(0));
            interes = (val / factor);
            return interes;
        }
    };
    ValorInteresService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService])
    ], ValorInteresService);
    return ValorInteresService;
}());
export { ValorInteresService };
//# sourceMappingURL=valor-interes.service.js.map