import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { Cliente } from '../../Clases/cliente';
import { Asignacion } from '../../Clases/asignacion';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { mergeMap } from 'rxjs/operators';
var ClientesLogicaService = /** @class */ (function () {
    function ClientesLogicaService(http, geoLogic) {
        this.http = http;
        this.geoLogic = geoLogic;
    }
    ClientesLogicaService.prototype.search = function (query, offset) {
        if (offset === void 0) { offset = 0; }
        return this.http.doGet('clientes?', { filter: query, first_result: offset })
            .then(this._mapResult);
    };
    ClientesLogicaService.prototype.getVisitasOnline = function (offset, last_result, filter, tipo) {
        var envio;
        if (filter === null) {
            //filter=' ';
            envio = { first_result: offset, last_result: last_result, cliente: filter, tipo: tipo };
        }
        else {
            envio = { first_result: offset, last_result: last_result, filter: filter, tipo: tipo };
        }
        return this.http.doGet('visitas?', envio).then(function (res) {
            var data = JSON.parse(res.data).lista;
            var toRet = [];
            toRet = data.map(function (c) {
                return new Cliente(c);
            });
            console.log("actividades clientes", toRet);
            return toRet;
        });
    };
    ClientesLogicaService.prototype.addToClientes = function (cliente, notas) {
        var _this = this;
        return this.geoLogic.getPos().pipe(mergeMap(function (data) {
            return _this.http.doPost('visitas/nuevo?', new NewCobroRequest(cliente.id, notas, data.coords.latitude.toString(), data.coords.longitude.toString()), null);
        }));
    };
    ClientesLogicaService.prototype._mapResult = function (res) {
        var data = res.data;
        var toRet = [];
        for (var _i = 0, _a = data.lista; _i < _a.length; _i++) {
            var row = _a[_i];
            toRet.push(new Cliente(row));
        }
        return toRet;
    };
    ClientesLogicaService.prototype.guardarCliente = function (cliente) {
        var toRet = new Asignacion({});
        if (cliente.facturas > 0) {
            toRet.visitas = true;
        }
        else {
            toRet.visitas = false;
        }
        toRet.entregas = false;
        toRet.id_cliente = parseInt(cliente.id);
        return this.http.doPost('visitas/nuevo?', toRet, null).then(function (res) {
            return JSON.parse(res.data);
        });
    };
    ClientesLogicaService.prototype.actualizarCliente = function (cliente) {
        return this.http.doPut('clientes/' + cliente.id + '/actualizar?', { latitud: cliente.latitud, longitud: cliente.longitud }, null).then(function (res) {
            return JSON.parse(res.data);
        });
    };
    ClientesLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            GeolocalizacionLogicaService])
    ], ClientesLogicaService);
    return ClientesLogicaService;
}());
export { ClientesLogicaService };
//# sourceMappingURL=clientes-logica.service.js.map