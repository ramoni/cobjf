import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Moneda } from '../../Clases/moneda';
import { HttpService } from '../httpFunctions/http.service';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';
var MonedaLogicaService = /** @class */ (function () {
    function MonedaLogicaService(http, monedaBD) {
        this.http = http;
        this.monedaBD = monedaBD;
        this.monedas = [];
        this.monedaCache = {};
    }
    MonedaLogicaService.prototype.getByIdOrName = function (idOrName) {
        //console.log('idOrName ',idOrName)
        if (this.monedaCache[idOrName]) {
            //console.log('encontrado y retornado')
            return this.monedaCache[idOrName];
        }
        var toRet;
        if (isNaN(parseFloat(idOrName))) {
            toRet = this.monedas.find(function (mon) { return mon.descripcion === idOrName; });
            //console.log(toRet);
        }
        else {
            toRet = this.monedas.find(function (mon) { return mon.id === idOrName; });
        }
        this.monedaCache[idOrName] = toRet;
        return toRet;
    };
    /**
     * 1 == Gs
     * 2 == DS
     */
    MonedaLogicaService.prototype.convert = function (monto, monedaOrigen, monedaDestino, cot) {
        if (monedaOrigen.id === "1" && monedaDestino.id === "2") {
            return monto / cot;
        }
        if (monedaOrigen.id === "2" && monedaDestino.id === "1") {
            return monto * cot;
        }
        return monto;
    };
    MonedaLogicaService.prototype.convertByDesc = function (monto, monedaOrigen, monedaDestino, cot) {
        return this.convert(monto, this.getByIdOrName(monedaOrigen), this.getByIdOrName(monedaDestino), cot);
    };
    MonedaLogicaService.prototype.getMonedasOnline = function () {
        var _this = this;
        return this.http.doGet('monedas?', { first_result: 0, last_result: 100 }).then(function (res) {
            var data = JSON.parse(res.data).lista;
            var toRet = [];
            toRet = data.map(function (m) {
                return new Moneda(m);
            });
            console.log("monedas ", toRet);
            _this.monedas = toRet;
            return toRet;
        });
    };
    MonedaLogicaService.prototype.getMonedasBD = function () {
        var _this = this;
        return this.monedaBD.getMonedas().then(function (monedas) {
            _this.monedas = monedas;
            return monedas;
        });
    };
    MonedaLogicaService.prototype.generateMontoValorizado = function (moneda, cotizacion, monto, cant_decimales) {
        console.log("CONVERTIR A MONEDA " + moneda + " CON COTIZACION " + cotizacion + " EL MONTO " + monto + " CON CANT_DECIMALES " + cant_decimales);
        var montoVal = 0;
        if (moneda == 1) {
            montoVal = parseFloat((monto * cotizacion).toFixed(cant_decimales));
        }
        else {
            montoVal = parseFloat((monto / cotizacion).toFixed(cant_decimales));
        }
        return montoVal;
    };
    MonedaLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            MonedasBDService])
    ], MonedaLogicaService);
    return MonedaLogicaService;
}());
export { MonedaLogicaService };
//# sourceMappingURL=moneda-logica.service.js.map