import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Entidad } from '../../Clases/entidad';
import { HttpService } from '../httpFunctions/http.service';
import { EntidadesBDService } from '../almacenamientoSQLITE/entidadesBD/entidades-bd.service';
var EntidadLogicaService = /** @class */ (function () {
    function EntidadLogicaService(http, entidadBD) {
        this.http = http;
        this.entidadBD = entidadBD;
        this.entidades = [];
        this.entidadesCache = {};
    }
    EntidadLogicaService.prototype.getByIdOrName = function (idOrName) {
        if (this.entidadesCache[idOrName]) {
            return this.entidadesCache[idOrName];
        }
        var toRet;
        if (isNaN(parseFloat(idOrName))) {
            // it's a description
            toRet = this.entidades.find(function (mon) { return mon.descripcion === idOrName; });
        }
        else {
            // it's a id
            toRet = this.entidades.find(function (mon) { return mon.id === idOrName; });
        }
        this.entidadesCache[idOrName] = toRet;
        return toRet;
    };
    EntidadLogicaService.prototype.getEntidadesOnline = function () {
        var _this = this;
        return this.http.doGet('entidades?', { first_result: 0, last_result: 500 }).then(function (res) {
            var data = JSON.parse(res.data).lista;
            var toRet = [];
            toRet = data.map(function (e) {
                return new Entidad(e);
            });
            _this.entidades = toRet;
            console.log('entidades ', _this.entidades);
            return toRet;
        });
    };
    EntidadLogicaService.prototype.getEntidadesBD = function () {
        var _this = this;
        return this.entidadBD.getEntidades().then(function (entidades) {
            _this.entidades = entidades;
            return entidades;
        });
    };
    EntidadLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            EntidadesBDService])
    ], EntidadLogicaService);
    return EntidadLogicaService;
}());
export { EntidadLogicaService };
//# sourceMappingURL=entidad-logica.service.js.map