import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { NewReceiptResponse } from '../../Clases/new-receipt-response';
import { ConfigService } from '../configuracion/config.service';
import { NumHabilitacion } from '../../Clases/num-habilitacion';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { File } from '@ionic-native/file/ngx';
import { WriteFileService } from '../writeFileService/logWrite.service';
var UsuarioService = /** @class */ (function () {
    function UsuarioService(http, config, usersBD, notasBD, recibosBD, entregasDocBD, fileService, writeLog) {
        this.http = http;
        this.config = config;
        this.usersBD = usersBD;
        this.notasBD = notasBD;
        this.recibosBD = recibosBD;
        this.entregasDocBD = entregasDocBD;
        this.fileService = fileService;
        this.writeLog = writeLog;
    }
    UsuarioService.prototype._doLoginRequest = function (user, pass, version, imei) {
        var _this = this;
        return this.http.doPost('login?', {}, {
            user: user,
            password: pass,
            version: version,
            imei: imei
        }).then(function (response) {
            _this.token = response.headers[_this.config.AUTHORIZATION_STRING];
            _this.tokenExpired = JSON.parse(response.data).EXPIRED;
            console.log('token expired ', _this.tokenExpired);
            if (_this.token === null) {
                _this.token = '';
            }
            localStorage.setItem('authorization', _this.token);
            localStorage.setItem('expiredToken', _this.tokenExpired);
            var user = JSON.parse(response.data);
            //let user=new DatosUsuario(JSON.parse(response.data))
            console.log("USER : ", user);
            return user;
        });
    };
    UsuarioService.prototype.iniciarSesion = function (usuario, password, version, imei) {
        var _this = this;
        this.credentials = {
            user: usuario,
            pass: password,
            version: version,
            imei: imei,
        };
        return this._doLoginRequest(usuario, password, version, imei).then(function (datos) {
            _this.ud = datos;
            localStorage.setItem('user', JSON.stringify(datos));
            localStorage.setItem('habilitacion', JSON.stringify(_this.ud.HABILITACION));
            return datos;
        });
    };
    UsuarioService.prototype.actualizarDatosUsuario = function () {
        var _this = this;
        return this.http.doGet('inicio?').then(function (response) {
            var data = JSON.parse(response.data);
            console.log('data en actualizar usuario', data);
            var userData = (data);
            console.log('userData en actualizar usuario', data);
            _this.setDatosUsuario(userData);
            localStorage.setItem('user', JSON.stringify(userData));
            localStorage.setItem('habilitacion', JSON.stringify(userData.HABILITACION));
            return data;
        });
    };
    UsuarioService.prototype.cambiarHabilitacion = function (habilitacion) {
        var _this = this;
        this.writeLog.escribirLog(new Date() + " Petición: Sincronización de logs luego de cerrar exitosamente la caja\n");
        var ubicacion = this.fileService.externalApplicationStorageDirectory;
        return this.fileService.readAsText(ubicacion, "cobranzas_log.txt").then(function (content) {
            var data = new NumHabilitacion(habilitacion, Number(localStorage.getItem('datos')), JSON.parse(localStorage.getItem('imei')));
            data.logs = content;
            console.log("SE ENVIA A CERRAR CAJA ", data);
            return _this.http.doPost('habilitacion/cerrar?', data).then(function (res) {
                _this.fileService.removeFile(ubicacion, "cobranzas_log.txt");
                var data = JSON.parse(res.data);
                _this.req = new NewReceiptResponse(data);
                console.log('respuesta post habilitacion/cerrar ', _this.req);
                localStorage.setItem('habilitacion', JSON.stringify(_this.req.totalHabilitacion));
                var nuevoDato = 0;
                localStorage.setItem('bajada', JSON.stringify(nuevoDato));
                localStorage.setItem('subida', JSON.stringify(nuevoDato));
                localStorage.setItem('datos', JSON.stringify(nuevoDato));
                return _this.req;
            });
        });
    };
    UsuarioService.prototype.getDatosUsuario = function () {
        if (localStorage.getItem('user')) {
            return JSON.parse(localStorage.getItem('user'));
        }
        return null;
    };
    UsuarioService.prototype.setDatosUsuario = function (u) {
        console.log(u);
        this.ud = u;
    };
    UsuarioService.prototype.existeUsuario = function (user, pass) {
        return this.usersBD.existeUsuario(user, pass).then(function (data) {
            var usuario = [];
            if (data.rows.length > 0) {
                console.log('exite el usuario', data.rows);
                usuario = data.rows.item(0);
                console.log('usuario', usuario);
                return usuario;
            }
            return null;
        }).catch(function (error) {
            console.error('no se pudo obtener los datos de la tabla', error);
            return error;
        });
    };
    UsuarioService.prototype.existeHabilitacionUsuario = function (totalHabilitacion, usuario, pass) {
        return this.usersBD.existeHabilitacionUser(totalHabilitacion, usuario, pass).then(function (response) {
            if (response.rows.length > 0) {
                console.log('exite la habilitacion del usuario');
                return true;
            }
            return false;
        }).catch(function (error) {
            console.error('no se pudo obtener los datos de la tabla para habilitacion', error);
        });
    };
    UsuarioService.prototype.addUsuario = function (usuario, password, user, token, expiredToken) {
        return this.usersBD.addUser(usuario, password, user, token, expiredToken).then(function () {
            return true;
        }).catch(function (error) {
            console.log('no pudo agregarse el usuario', error);
        });
    };
    UsuarioService.prototype.existenDatosHabilitacion = function (habilitacion) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var notasSync, entregasDocSync, recibosSync;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        notasSync = false;
                        entregasDocSync = false;
                        recibosSync = false;
                        return [4 /*yield*/, this.notasBD.syncNotasHabilitacion(habilitacion)];
                    case 1:
                        notasSync = _a.sent();
                        return [4 /*yield*/, this.entregasDocBD.syncEntregasHabilitacion(habilitacion)];
                    case 2:
                        entregasDocSync = _a.sent();
                        return [4 /*yield*/, this.recibosBD.existeSyncRecibosHabilitacion(habilitacion)];
                    case 3:
                        recibosSync = _a.sent();
                        if (recibosSync || entregasDocSync || notasSync) {
                            return [2 /*return*/, Promise.resolve(true)];
                        }
                        else {
                            return [2 /*return*/, Promise.resolve(false)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    UsuarioService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            ConfigService,
            UsuariosBDService,
            NotasBDService,
            RecibosBDService,
            EntregasDocBDService,
            File,
            WriteFileService])
    ], UsuarioService);
    return UsuarioService;
}());
export { UsuarioService };
//# sourceMappingURL=usuario.service.js.map