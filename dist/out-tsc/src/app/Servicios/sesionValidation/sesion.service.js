import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var SesionService = /** @class */ (function () {
    function SesionService() {
    }
    SesionService.prototype.validarSesion = function (token, token_expired) {
        console.log("token: ", token, "token_expired", token_expired);
        var now = new Date();
        var expired = new Date(token_expired);
        console.log("now: ", token, "expired", token_expired);
        if (token !== null && now <= expired) {
            console.log('Sesion valida');
            return true;
        }
        console.log('Sesion no valida');
        return false;
    };
    SesionService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SesionService);
    return SesionService;
}());
export { SesionService };
//# sourceMappingURL=sesion.service.js.map