import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ValueType } from '../../Clases/value-type';
import { HttpService } from '../httpFunctions/http.service';
import { ValoresBDService } from '../almacenamientoSQLITE/valoresBD/valores-bd.service';
var ValoresLogicaService = /** @class */ (function () {
    function ValoresLogicaService(http, valoresBD) {
        this.http = http;
        this.valoresBD = valoresBD;
        this.valores = [];
        this.valoresCache = {};
    }
    ValoresLogicaService.prototype.getByIdOrName = function (idOrName) {
        if (this.valoresCache[idOrName]) {
            return this.valoresCache[idOrName];
        }
        var toRet;
        if (isNaN(parseFloat(idOrName))) {
            // it's a description
            toRet = this.valores.find(function (mon) { return mon.descripcion === idOrName; });
        }
        else {
            // it's a id
            toRet = this.valores.find(function (mon) { return mon.id === idOrName; });
        }
        this.valoresCache[idOrName] = toRet;
        return toRet;
    };
    /*getTypes(): Array<ValueType> {
        if (this.valores.length === 0) {
            if (localStorage.getItem('cotHoy') && localStorage.getItem('valores')) {
                if (localStorage.getItem('cotHoy')) {
                    this.cotizacion = JSON.parse(localStorage.getItem('cotHoy'));
                }
                if (localStorage.getItem('valores')) {
                    this.valores = JSON.parse(localStorage.getItem('valores'));
                }
                return this.valores;

            }
            //this.loadList();
        }
        return this.valores;
    }*/
    ValoresLogicaService.prototype.getDefaultType = function () {
        /*let value: Array<ValueType> = JSON.parse(localStorage.getItem('valores'));
        return value[0];*/
        return this.valores[0];
    };
    ValoresLogicaService.prototype.getCotizacionOnline = function () {
        var _this = this;
        return this.http.doGet('cambio?', {}).then(function (res) {
            var data = JSON.parse(res.data);
            console.log("cotizacion ", data);
            _this.cotizacion = data.FN_TIP_CAMBIO;
            localStorage.setItem('cotHoy', JSON.stringify(_this.cotizacion));
            return data.FN_TIP_CAMBIO;
        });
    };
    ValoresLogicaService.prototype.getCotizacion = function () {
        if (localStorage.getItem('cotHoy')) {
            this.cotizacion = JSON.parse(localStorage.getItem('cotHoy'));
        }
        return Number(this.cotizacion);
    };
    ValoresLogicaService.prototype.getValoresOnline = function () {
        var _this = this;
        return this.http.doGet('valores?', { first_result: 0, last_result: 100 }).then(function (res) {
            var data = JSON.parse(res.data).lista;
            var toRet = [];
            toRet = data.map(function (v) {
                return new ValueType(v);
            });
            _this.valores = toRet;
            console.log('valores', _this.valores);
            return toRet;
        });
    };
    ValoresLogicaService.prototype.getValoresBD = function () {
        var _this = this;
        return this.valoresBD.getValores().then(function (valores) {
            _this.valores = valores;
            return valores;
        });
    };
    ValoresLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            ValoresBDService])
    ], ValoresLogicaService);
    return ValoresLogicaService;
}());
export { ValoresLogicaService };
//# sourceMappingURL=valores-logica.service.js.map