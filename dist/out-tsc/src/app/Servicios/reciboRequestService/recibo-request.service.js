import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { FacturasLogicaService } from '../facturasService/facturas-logica.service';
import { MonedaLogicaService } from '../monedaService/moneda-logica.service';
var ReciboRequestService = /** @class */ (function () {
    function ReciboRequestService(monedaDBservice, userDBservice, reciboDBservice, facturaService, monedaService) {
        this.monedaDBservice = monedaDBservice;
        this.userDBservice = userDBservice;
        this.reciboDBservice = reciboDBservice;
        this.facturaService = facturaService;
        this.monedaService = monedaService;
        //Agregados, truqueados
        this.totalFacturas = 0;
        this.totalValores = 0;
        this.valid = true;
        this.message = "";
    }
    ReciboRequestService.prototype.formatData = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fecha, usuario, marca, moneda, cotizacion, facturasPost, valoresPost;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //Inicializar con datos por defecto que es de Recibos-DTO
                        this.initValues(data);
                        fecha = this.fecha;
                        return [4 /*yield*/, this.userDBservice.getUserData()];
                    case 1:
                        usuario = _a.sent();
                        marca = data.marca;
                        return [4 /*yield*/, this.monedaDBservice.find(data.id_moneda)];
                    case 2:
                        moneda = _a.sent();
                        cotizacion = data.cotizacion != null ? data.cotizacion : null;
                        facturasPost = data.facturas ? data.facturas : [];
                        valoresPost = data.valores ? data.valores : [];
                        console.log("MONEDA DE RECIBO A CREAR CON ID: " + data.id_moneda, moneda);
                        return [4 /*yield*/, this.verificarReciboConMarca(usuario.ID_COBRADOR, marca)];
                    case 3:
                        _a.sent();
                        this.verificarFecha(fecha);
                        this.verificarMoneda(moneda.ID_MONEDA);
                        this.verificarCotizacion(cotizacion);
                        this.verificarValores(moneda, cotizacion, valoresPost);
                        this.verificarFacturas(facturasPost, moneda, cotizacion);
                        //this.verificarHabilitacion(cobrador);
                        this.verificarTodosLosValores();
                        return [4 /*yield*/, this.verificarNroRecibo(usuario.USUARIO.ID_COBRADOR, facturasPost)];
                    case 4:
                        _a.sent();
                        //Carga de campos adicionales al request
                        this.moneda = moneda;
                        this.cobrador = usuario.USUARIO.ID_COBRADOR;
                        this.usuario = usuario.USUARIO.NOMBRE;
                        this.habilitacion = JSON.parse(localStorage.getItem("habilitacion"));
                        return [2 /*return*/, this];
                }
            });
        });
    };
    ReciboRequestService.prototype.verificarReciboConMarca = function (id_cobrador, marca) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var existe_recibo;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.valid) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.reciboDBservice.existeReciboConMarca(id_cobrador, marca)];
                    case 1:
                        existe_recibo = _a.sent();
                        if (existe_recibo == true && marca != "") {
                            //Verificar si existe recibo con esa marca
                            this.valid = false;
                            this.message = "Ya existe un recibo con la misma marca";
                        }
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    ReciboRequestService.prototype.verificarFecha = function (fecha) {
        if (this.valid) {
            if (fecha == null || fecha == "") {
                this.valid = false;
                this.message = "Fecha no valida";
            }
        }
    };
    ReciboRequestService.prototype.verificarMoneda = function (moneda) {
        if (this.valid) {
            if (moneda == null) {
                //Verificar que tenga moneda
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar una moneda valida para el recibo";
            }
        }
    };
    ReciboRequestService.prototype.verificarCotizacion = function (cotizacion) {
        if (this.valid) {
            if (cotizacion == null) {
                //Verificar cotizacion
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar la cotizacion del dia";
            }
        }
    };
    ReciboRequestService.prototype.verificarValores = function (moneda, cotizacion, valoresPost) {
        var totalValores = 0.0;
        if (this.valid) {
            var cantidadValores = valoresPost.length;
            var montoValor = 0.0;
            var montoValorizado = 0.00;
            //Si hay mas de un valor que agregar
            if (cantidadValores > 0) {
                for (var _i = 0, valoresPost_1 = valoresPost; _i < valoresPost_1.length; _i++) {
                    var valor = valoresPost_1[_i];
                    if (!(valor.hasOwnProperty('monto') && valor.hasOwnProperty('id_moneda') && valor.hasOwnProperty('id_tipo_valor'))) {
                        this.valid = false;
                        this.message = "Recibo no creado, verifique los valores  ingresados";
                        break;
                    }
                    else {
                        montoValor = valor['monto'];
                        //Si el valor ingresado es de la misma moneda que el recibo entonces se toma el mismo valor
                        if (valor['id_moneda'] == moneda.ID_MONEDA) {
                            totalValores += montoValor;
                        }
                        else {
                            //Sino se realiza la conversion de dolar a guaranies o viceversa
                            montoValorizado = this.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, montoValor, moneda.CANT_DECIMALES);
                            totalValores = parseFloat((totalValores + montoValorizado).toFixed(moneda.CANT_DECIMALES));
                        }
                    }
                }
            }
            else {
                //Si no hay ningun valor entonces no es valido
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar al menos un valor";
            }
        }
        this.totalValores = totalValores;
    };
    ReciboRequestService.prototype.verificarFacturas = function (facturasPost, moneda, cotizacion) {
        //Verificar facturas
        var totalFacturas = 0.00;
        if (this.valid) {
            var montofactura = 0.00;
            var monedaFactura = void 0;
            var cantidadFacturas = facturasPost.length;
            var montoValorizado = 0.00;
            //Si no hay ninguna factura entonces no es valida la insercion del recibo
            if (cantidadFacturas > 0) {
                //Obtener el total de facturas
                for (var _i = 0, facturasPost_1 = facturasPost; _i < facturasPost_1.length; _i++) {
                    var facturas = facturasPost_1[_i];
                    montofactura = facturas['monto'];
                    monedaFactura = facturas['id_moneda'];
                    //Si el valor ingresado es de la misma moneda que el recibo entonces se toma el mismo valor
                    if (monedaFactura == moneda.ID_MONEDA) {
                        totalFacturas += montofactura;
                    }
                    else {
                        //Sino se realiza la conversion de dolar a guaranies o viceversa
                        montoValorizado = this.monedaDBservice.generateMontoValorizado(moneda.ID, cotizacion, montofactura, moneda.CANT_DECIMALES);
                        totalFacturas = parseFloat((totalFacturas + montoValorizado).toFixed(moneda.CANT_DECIMALES));
                    }
                }
            }
            else {
                //Si no hay ninguna factura entonces no es valido
                this.valid = false;
                this.message = "Recibo no creado, debe ingresar al menos una factura";
            }
        }
        this.totalFacturas = totalFacturas;
    };
    ReciboRequestService.prototype.verificarTodosLosValores = function () {
        if (this.valid) {
            console.log("TOTAL VALORES: ", this.totalValores, "TOTAL FACTURAS: ", this.totalFacturas);
            if (this.totalFacturas > this.totalValores) {
                this.valid = false;
                this.message = "Recibo no creado, valores menor a total facturas";
            }
            else if (this.total_recibo != this.totalValores) {
                this.valid = false;
                this.message = "Recibo no creado, el total calculado en la app no es correcto, debe ser " + this.totalValores + " y no " + this.total_recibo;
            }
        }
    };
    ReciboRequestService.prototype.verificarNroRecibo = function (id_cobrador, facturas) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var nro_recibo, recibo1, recibo2, isFCO;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.nro_recibo = 0;
                        nro_recibo = 0;
                        recibo1 = 0;
                        recibo2 = 0;
                        isFCO = false;
                        if (!this.valid) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.facturaService.verificarFacturaFCO(facturas)];
                    case 1:
                        isFCO = _a.sent();
                        if (!isFCO) return [3 /*break*/, 2];
                        nro_recibo = 0;
                        return [3 /*break*/, 5];
                    case 2: return [4 /*yield*/, this.userDBservice.getUltimoRecibo()];
                    case 3:
                        recibo1 = _a.sent();
                        return [4 /*yield*/, this.reciboDBservice.getUltimoReciboCobrador(id_cobrador)];
                    case 4:
                        recibo2 = _a.sent();
                        if (recibo1 == -1 || recibo2 == -1) {
                            this.valid = false;
                            this.message = "Recibo no creado, no posee numero de recibo";
                        }
                        else {
                            if (recibo1 > recibo2) {
                                nro_recibo = recibo1 + 1;
                            }
                            else {
                                nro_recibo = recibo2 + 1;
                            }
                        }
                        this.nro_recibo = nro_recibo;
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ReciboRequestService.prototype.initValues = function (data) {
        console.log("DATO A INGRESAR PARA RECIBO NUEVO", data);
        this.fecha = data.fecha;
        this.id_cliente = data.id_cliente;
        this.id_moneda = data.id_moneda;
        this.cotizacion = data.cotizacion;
        this.interes = data.interes;
        this.total_recibo = data.total_recibo;
        this.anticipo = data.anticipo;
        this.version = data.version;
        this.facturas = data.facturas;
        this.valores = data.valores;
        this.latitud = data.latitud;
        this.longitud = data.longitud;
        this.marca = data.marca;
        this.id_concepto = data.id_concepto;
        this.imei = data.imei;
    };
    ReciboRequestService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [MonedasBDService,
            UsuariosBDService,
            RecibosBDService,
            FacturasLogicaService,
            MonedaLogicaService])
    ], ReciboRequestService);
    return ReciboRequestService;
}());
export { ReciboRequestService };
//# sourceMappingURL=recibo-request.service.js.map