import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { WriteFileService } from '../writeFileService/logWrite.service';
export var ConnectionStatus;
(function (ConnectionStatus) {
    ConnectionStatus[ConnectionStatus["Online"] = 0] = "Online";
    ConnectionStatus[ConnectionStatus["Offline"] = 1] = "Offline";
})(ConnectionStatus || (ConnectionStatus = {}));
var NetworkControllerService = /** @class */ (function () {
    function NetworkControllerService(network, plt, writeLog) {
        var _this = this;
        this.network = network;
        this.plt = plt;
        this.writeLog = writeLog;
        this.status = new BehaviorSubject(ConnectionStatus.Offline);
        this.plt.ready().then(function () {
            _this.initializeNetworkEvents();
            var status = _this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
            _this.status.next(status);
        });
    }
    NetworkControllerService.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status.getValue() === ConnectionStatus.Online) {
                _this.writeLog.escribirLog(new Date() + 'NIVEL OFFLINE \n');
                console.log('NIVEL OFFLINE');
                _this.updateNetworkStatus(ConnectionStatus.Offline);
            }
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status.getValue() === ConnectionStatus.Offline) {
                _this.writeLog.escribirLog(new Date() + 'NIVEL ONLINE \n');
                console.log('NIVEL ONLINE');
                _this.updateNetworkStatus(ConnectionStatus.Online);
            }
        });
    };
    NetworkControllerService.prototype.updateNetworkStatus = function (status) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var connection;
            return tslib_1.__generator(this, function (_a) {
                this.status.next(status);
                connection = status == ConnectionStatus.Offline ? 'Offline' : 'Online';
                this.writeLog.escribirLog(new Date() + 'Trabajando con conexion ' + connection + ' \n');
                console.log('Trabajando con conexion ', connection);
                return [2 /*return*/];
            });
        });
    };
    NetworkControllerService.prototype.onNetworkChange = function () {
        return this.status.asObservable();
    };
    NetworkControllerService.prototype.getCurrentNetworkStatus = function () {
        return this.status.getValue();
    };
    NetworkControllerService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Network,
            Platform,
            WriteFileService])
    ], NetworkControllerService);
    return NetworkControllerService;
}());
export { NetworkControllerService };
//# sourceMappingURL=network-controller.service.js.map