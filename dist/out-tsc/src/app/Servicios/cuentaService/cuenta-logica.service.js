import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Cuenta } from '../../Clases/Cuenta';
import { HttpService } from '../httpFunctions/http.service';
import { CuentasBDService } from '../almacenamientoSQLITE/cuentasBD/cuentas-bd.service';
var CuentaLogicaService = /** @class */ (function () {
    function CuentaLogicaService(http, cuentasBD) {
        this.http = http;
        this.cuentasBD = cuentasBD;
        this.cuentas = [];
        this.entidadCuentas = [];
        this.cuentaCache = {};
    }
    CuentaLogicaService.prototype.getByIdOrName = function (idOrName) {
        if (this.cuentaCache[idOrName]) {
            return this.cuentaCache[idOrName];
        }
        var toRet;
        if (isNaN(parseFloat(idOrName))) {
            // it's a description
            toRet = this.cuentas.find(function (mon) { return mon.descripcion === idOrName; });
        }
        else {
            // it's a id
            toRet = this.cuentas.find(function (mon) { return mon.id === idOrName; });
        }
        this.cuentaCache[idOrName] = toRet;
        return toRet;
    };
    CuentaLogicaService.prototype.getEntidadCuenta = function (banco) {
        console.log('Al retornar ya');
        console.log(this.entidadCuentas);
        return this.entidadCuentas;
    };
    CuentaLogicaService.prototype.loadEntidadCuentas = function (entidad) {
        //tenia timeout 4000
        return this.http.doGet('entidades/' + entidad + '/cuentas?', { first_result: 0, last_result: 100 })
            .then(function (res) {
            var data = JSON.parse(res.data);
            return data.lista;
        });
    };
    CuentaLogicaService.prototype.getCuentasOnline = function () {
        var _this = this;
        return this.http.doGet('cuentas?', { first_result: 0, last_result: 1000 })
            .then(function (res) {
            var data = JSON.parse(res.data).lista;
            var toRet = [];
            toRet = data.map(function (c) {
                return new Cuenta(c);
            });
            _this.cuentas = toRet;
            console.log("cuentas ", toRet);
            return toRet;
        });
    };
    CuentaLogicaService.prototype.getCuentasBD = function () {
        var _this = this;
        return this.cuentasBD.getCuentas().then(function (cuentas) {
            _this.cuentas = cuentas;
            return cuentas;
        });
    };
    CuentaLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            CuentasBDService])
    ], CuentaLogicaService);
    return CuentaLogicaService;
}());
export { CuentaLogicaService };
//# sourceMappingURL=cuenta-logica.service.js.map