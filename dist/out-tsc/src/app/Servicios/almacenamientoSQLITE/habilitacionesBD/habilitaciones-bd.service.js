import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import moment from 'moment';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
var HabilitacionesBDService = /** @class */ (function () {
    function HabilitacionesBDService(db) {
        this.db = db;
        this.TABLE_HABILITACIONES = "CBV_HABILITACIONES";
        this.KEY_ID = "ID";
        this.KEY_HABILITACION_CAJA = "HABILITACION_CAJA";
        this.KEY_USUARIO = "USUARIO";
        this.KEY_FECHA_APERTURA = "FECHA_APERTURA";
        this.KEY_FECHA_CIERRE = "FECHA_CIERRE";
        this.KEY_CERRADO = "CERRADO";
        this.KEY_VALIDO = "VALIDO";
    }
    HabilitacionesBDService.prototype.createTableHabilitaciones = function () {
        var CREATE_HABILITACIONES_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_HABILITACIONES +
            " (ID INTEGER PRIMARY KEY AUTOINCREMENT, HABILITACION_CAJA INTEGER, USUARIO TEXT, " +
            "FECHA_APERTURA TEXT, FECHA_CIERRE TEXT, CERRADO INTEGER, VALIDO INTEGER)";
        return this.db.getDataBase().executeSql(CREATE_HABILITACIONES_TABLE, []).then(function (habilitacion) {
            console.log('se creo la table habilitacion', habilitacion);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla habilitacion', error);
            return false;
        });
    };
    HabilitacionesBDService.prototype.cerrarHabilitacionLocal = function (habilitacion) {
        console.log("Cierre de habilitacion" + habilitacion);
        var today = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        var cerrado = 1;
        var strSQL = "UPDATE " + this.TABLE_HABILITACIONES + " SET CERRADO =" + cerrado + "," + this.KEY_FECHA_CIERRE + "=" + "'" + today + "'" + "," + this.KEY_VALIDO + "=0" + " WHERE HABILITACION_CAJA= " + habilitacion;
        return this.db.getDataBase().executeSql(strSQL, []).then(function (resp) {
            console.log("Habilitacion " + habilitacion + " cerrada");
            return true;
        }).catch(function (error) {
            console.log("Habilitacion " + habilitacion + " no pude ser cerrada");
            return false;
        });
    };
    HabilitacionesBDService.prototype.deleteTableHabilitacion = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_HABILITACIONES;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla habilitacion', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla habilitacion', error);
            return false;
        });
    };
    HabilitacionesBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], HabilitacionesBDService);
    return HabilitacionesBDService;
}());
export { HabilitacionesBDService };
//# sourceMappingURL=habilitaciones-bd.service.js.map