import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Notas } from 'src/app/Clases/notas';
var NotasBDService = /** @class */ (function () {
    function NotasBDService(bd) {
        this.bd = bd;
        this.TABLE_COB_VISITAS = "CBV_COB_VISITAS";
        this.KEY_ID = "ID";
        this.KEY_ID_CLIENTE = "ID_CLIENTE";
        this.KEY_NOTA = "NOTA";
        this.KEY_LATITUD = "LATITUD";
        this.KEY_LONGITUD = "LONGITUD";
        this.KEY_TIPO = "TIPO";
        this.KEY_HABILITACION_CAJA = "HABILITACION_CAJA";
        this.KEY_IMEI = "IMEI";
        this.KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION";
        this.KEY_ACTIVO = "ACTIVO";
        this.KEY_FEC_VISITA = "FEC_VISITA";
        this.KEY_ID_TABLE_API = "ID_API";
    }
    NotasBDService.prototype.createTableNotas = function () {
        var CREATE_NOTAS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_COB_VISITAS + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "ID_CLIENTE TEXT, NOTA TEXT, LATITUD TEXT, LONGITUD TEXT, TIPO TEXT, HABILITACION_CAJA INTEGER, "
            + "IMEI TEXT, REQUIERE_SINCRONIZACION INTEGER DEFAULT 0, ACTIVO INTEGER, FEC_VISITA TEXT, ID_API INTEGER  DEFAULT 0)";
        return this.bd.getDataBase().executeSql(CREATE_NOTAS_TABLE, []).then(function (notas) {
            console.log('se creo tabla notas', notas);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla notas', error);
            return false;
        });
    };
    NotasBDService.prototype.setNotaConMensaje = function (nota) {
        nota.requiere_sincronizacion = 1;
        return this.bd.insert(this.TABLE_COB_VISITAS, nota).then(function (resp) {
            return {
                "exito": "Exito al insertar nueva nota"
            };
        }).catch(function (error) {
            console.log("Error al guardar nota ", error);
            return {
                "error": "Error al insertar nota"
            };
        });
    };
    NotasBDService.prototype.actualizarSincronizacionDeNotas = function () {
        var query = "UPDATE " + this.TABLE_COB_VISITAS + " SET " + this.KEY_REQUIERE_SINCRONIZACION + "=? WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=?";
        var params = [0, 1];
        this.bd.getDataBase().executeSql(query, params).then(function (data) {
            console.log("Columna de notas que requieren sincronizacion cambiados a 0", data);
        }).catch(function (error) {
            console.log('No se pudo actualizar la columna de requiere sincronizacion a 0 de las notas', error);
        });
    };
    NotasBDService.prototype.updateNotaConMensaje = function (nota, conditions) {
        nota.requiere_sincronizacion = 1;
        return this.bd.update(this.TABLE_COB_VISITAS, nota, conditions).then(function (resp) {
            return {
                "exito": "Exito al actualizar nota"
            };
        }).catch(function (error) {
            console.log("Error al actualizar nota ", error);
            return {
                "error": "Error al actualizar nota"
            };
        });
    };
    NotasBDService.prototype.addNota = function (nota) {
        var values = {};
        values[this.KEY_ID_CLIENTE] = nota.ID_CLIENTE;
        values[this.KEY_NOTA] = nota.NOTAS;
        values[this.KEY_LATITUD] = nota.LATITUD;
        values[this.KEY_LONGITUD] = nota.LONGITUD;
        values[this.KEY_TIPO] = nota.TIPO;
        values[this.KEY_HABILITACION_CAJA] = nota.HABILITACION_CAJA;
        values[this.KEY_IMEI] = nota.IMEI;
        values[this.KEY_ACTIVO] = 1;
        values[this.KEY_ID_TABLE_API] = nota.ID;
        values[this.KEY_REQUIERE_SINCRONIZACION] = 0;
        return this.bd.insert(this.TABLE_COB_VISITAS, values);
    };
    NotasBDService.prototype.syncNotasHabilitacion = function (habilitacion) {
        var query = "SELECT * FROM " + this.TABLE_COB_VISITAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
            "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
        return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(function (data) {
            if (data.rows.length > 0) {
                return true;
            }
            else {
                return false;
            }
        });
    };
    NotasBDService.prototype.cantidadNotasSyncByType = function (tipo) {
        var query = "SELECT COUNT(*) as cantidad FROM " + this.TABLE_COB_VISITAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=1 AND TIPO=" + "'" + tipo + "'";
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log("CANT NOTAS " + tipo + " ", data.rows.item(0).cantidad);
            return data.rows.item(0).cantidad;
        }).catch(function (error) {
            console.log("Error trayendo cantidad de notas tipo: " + tipo, error);
            return -1;
        });
    };
    NotasBDService.prototype.cantidadNotasSync = function (habilitacion) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var cant_facturas, cant_entregas;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.cantidadNotasSyncByType("F")];
                    case 1:
                        cant_facturas = _a.sent();
                        return [4 /*yield*/, this.cantidadNotasSyncByType("E")];
                    case 2:
                        cant_entregas = _a.sent();
                        if (cant_facturas == -1) {
                            cant_facturas = 0;
                        }
                        if (cant_entregas == -1) {
                            cant_entregas = 0;
                        }
                        return [2 /*return*/, {
                                "NOTAS_VISITAS": parseInt(cant_facturas),
                                "NOTAS_ENTREGAS": parseInt(cant_entregas)
                            }];
                }
            });
        });
    };
    NotasBDService.prototype.existeNotaLocal = function (id_cliente, habilitacion_caja, tipo) {
        var query = "SELECT * FROM " + this.TABLE_COB_VISITAS + " WHERE " + this.KEY_ID_CLIENTE + "=? AND "
            + this.KEY_HABILITACION_CAJA + "=? AND " + this.KEY_TIPO + "=? AND " + this.KEY_REQUIERE_SINCRONIZACION + "=?";
        console.log(query);
        return this.bd.getDataBase().executeSql(query, [id_cliente, habilitacion_caja, tipo, 0]).then(function (data) {
            //console.log("TAMAÑO NOTAS ",data.rows.length, data)
            if (data.rows.length > 0) {
                return true;
            }
            return false;
        }).catch(function (error) {
            console.log("Error comprobando existencia de notas en local ", error);
            return false;
        });
    };
    /*listAll(){
      let query = "SELECT * FROM "+this.TABLE_COB_VISITAS;
      return this.bd.getDataBase().executeSql(query, []).then(data => {
        console.log(data.rows.length, data.rows.item(0), data)
      }).catch(error => {
        console.log(error);
      });
    }*/
    NotasBDService.prototype.getNotasASincronizar = function (habilitacion) {
        var query = "SELECT * FROM " + this.TABLE_COB_VISITAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=? AND HABILITACION_CAJA=?";
        var params = [1, habilitacion];
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            var notas = [];
            for (var i = 0; i < data.rows.length; i++) {
                notas.push(data.rows.item(i));
            }
            console.log("NOTAS TRAIDAS DE LA BD PARA SINCRONIZAR: ", notas);
            return notas;
        }).catch(function (error) {
            console.log("ERROR OBTENIENDO NOTAS DE LA BD PARA SINCRONIZAR ", error);
            return [];
        });
    };
    NotasBDService.prototype.getNotaCobrador = function (id_cliente, habilitacion, tipo) {
        var params = [id_cliente, habilitacion, tipo];
        var query = "SELECT * FROM " + this.TABLE_COB_VISITAS
            + " WHERE ID_CLIENTE=? AND HABILITACION_CAJA =? AND TIPO=?";
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            console.log('se obtuvo las notas del cobrador ', data.rows.item(0));
            if (data.rows.length > 0) {
                var nota = data.rows.item(0);
                nota.NOTAS = nota.NOTA;
                return new Notas(nota);
            }
            else {
                return new Notas();
            }
        }).catch(function (error) {
            console.log('no pudo obtenerse las notas del cobrador', error);
            return error;
        });
    };
    NotasBDService.prototype.deleteAllNotas = function () {
        var query = "DELETE FROM " + this.TABLE_COB_VISITAS;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla notas', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla notas', error);
            return false;
        });
    };
    NotasBDService.prototype.deleteTableNotas = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_COB_VISITAS;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla notas', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla notas', error);
            return false;
        });
    };
    NotasBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], NotasBDService);
    return NotasBDService;
}());
export { NotasBDService };
//# sourceMappingURL=notas-bd.service.js.map