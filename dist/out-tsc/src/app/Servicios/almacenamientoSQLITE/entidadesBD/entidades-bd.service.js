import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Entidad } from 'src/app/Clases/entidad';
var EntidadesBDService = /** @class */ (function () {
    function EntidadesBDService(db) {
        this.db = db;
        this.TABLE_ENTIDADES = "CBV_ENTIDADES";
        this.KEY_ID_ENTIDAD = "ID_ENTIDAD";
        this.KEY_DESCRIPCION = "DESCRIPCION";
    }
    EntidadesBDService.prototype.createTableEntidad = function () {
        var CREATE_ENTIDADES_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_ENTIDADES +
            " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_ENTIDAD INT, DESCRIPCION TEXT)";
        return this.db.getDataBase().executeSql(CREATE_ENTIDADES_TABLE, []).then(function (entidad) {
            console.log('se creo la tabla entidad', entidad);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla entidad', error);
            return false;
        });
    };
    EntidadesBDService.prototype.addEntidad = function (entidad) {
        var addLine = "INSERT INTO " + this.TABLE_ENTIDADES + " (ID_ENTIDAD, DESCRIPCION) VALUES (?,?)";
        return this.db.getDataBase().executeSql(addLine, [entidad.id, entidad.descripcion]).then(function (exito) {
            console.log('se agrego la entidad', exito);
            return true;
        }).catch(function (error) {
            console.log('no se puede agregar la entidad', error);
            return false;
        });
    };
    EntidadesBDService.prototype.getEntidades = function () {
        var query = "SELECT * FROM CBV_ENTIDADES ";
        var entidades = [];
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            if (data.rows.length > 0) {
                for (var index = 0; index < data.rows.length; index++) {
                    entidades.push(new Entidad(data.rows.item(index)));
                }
            }
            return entidades;
        }).catch(function (err) {
            console.log('no pudo obtenerse las entidades localmente', err);
            return err;
        });
    };
    EntidadesBDService.prototype.getEntidad = function (id_entidad) {
        var query = "SELECT * FROM CBV_ENTIDADES WHERE ID_ENTIDAD=?";
        return this.db.getDataBase().executeSql(query, [id_entidad]).then(function (data) {
            if (data.rows.length > 0) {
                return new Entidad(data.rows.item(0));
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.log("error al traer la entidad por el id", error);
            return null;
        });
    };
    EntidadesBDService.prototype.deleteAllEntidades = function () {
        var query = "DELETE FROM " + this.TABLE_ENTIDADES;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla entidades', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla entidades', error);
            return false;
        });
    };
    EntidadesBDService.prototype.deleteTableEntidad = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_ENTIDADES;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla entidad', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla entidad', error);
            return false;
        });
    };
    EntidadesBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], EntidadesBDService);
    return EntidadesBDService;
}());
export { EntidadesBDService };
//# sourceMappingURL=entidades-bd.service.js.map