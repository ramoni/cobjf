import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ClientesBDService } from '../clientesBD/clientes-bd.service';
import { EntregasDocBDService } from '../entregasDocBD/entregas-doc-bd.service';
import { UsuariosBDService } from '../usuariosBD/usuarios-bd.service';
import { FacturasBDService } from '../facturasBD/facturas-bd.service';
import { NotasBDService } from '../notasBD/notas-bd.service';
import { RecibosBDService } from '../recibosBD/recibos-bd.service';
import { ConceptosBDService } from '../conceptosBD/conceptos-bd.service';
import { EntidadesBDService } from '../entidadesBD/entidades-bd.service';
import { MonedasBDService } from '../monedasBD/monedas-bd.service';
import { ValoresBDService } from '../valoresBD/valores-bd.service';
import { CuentasBDService } from 'src/app/Servicios/almacenamientoSQLITE/cuentasBD/cuentas-bd.service';
import { HabilitacionesBDService } from 'src/app/Servicios/almacenamientoSQLITE/habilitacionesBD/habilitaciones-bd.service';
var TablesManageService = /** @class */ (function () {
    function TablesManageService(clientesBD, entregasBD, usuariosBD, facturasBD, notasBD, recibosBD, conceptoBD, monedaBD, entidadBD, valoresBD, cuentasBD, habilitacionesBD) {
        this.clientesBD = clientesBD;
        this.entregasBD = entregasBD;
        this.usuariosBD = usuariosBD;
        this.facturasBD = facturasBD;
        this.notasBD = notasBD;
        this.recibosBD = recibosBD;
        this.conceptoBD = conceptoBD;
        this.monedaBD = monedaBD;
        this.entidadBD = entidadBD;
        this.valoresBD = valoresBD;
        this.cuentasBD = cuentasBD;
        this.habilitacionesBD = habilitacionesBD;
    }
    TablesManageService.prototype.createTables = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var clientes, notas, facturas, usuario, recibos, entregas, conceptos, entidades, cuentas, moneda, valores, habilitaciones;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("Empezando creacion de tablas");
                        clientes = false;
                        notas = false;
                        facturas = false;
                        usuario = false;
                        recibos = false;
                        entregas = false;
                        conceptos = false;
                        entidades = false;
                        cuentas = false;
                        moneda = false;
                        valores = false;
                        habilitaciones = false;
                        return [4 /*yield*/, this.clientesBD.createTableClientes()];
                    case 1:
                        clientes = _a.sent();
                        return [4 /*yield*/, this.facturasBD.createTableFacturas()];
                    case 2:
                        facturas = _a.sent();
                        return [4 /*yield*/, this.usuariosBD.createTableUsuarios()];
                    case 3:
                        usuario = _a.sent();
                        return [4 /*yield*/, this.notasBD.createTableNotas()];
                    case 4:
                        notas = _a.sent();
                        return [4 /*yield*/, this.recibosBD.createTableRecibos()];
                    case 5:
                        recibos = _a.sent();
                        return [4 /*yield*/, this.entregasBD.createTableEntregas()];
                    case 6:
                        entregas = _a.sent();
                        return [4 /*yield*/, this.conceptoBD.createTableConcepto()];
                    case 7:
                        conceptos = _a.sent();
                        return [4 /*yield*/, this.entidadBD.createTableEntidad()];
                    case 8:
                        entidades = _a.sent();
                        return [4 /*yield*/, this.cuentasBD.createTableCuentas()];
                    case 9:
                        cuentas = _a.sent();
                        return [4 /*yield*/, this.monedaBD.createTableMonedas()];
                    case 10:
                        moneda = _a.sent();
                        return [4 /*yield*/, this.valoresBD.createTableValores()];
                    case 11:
                        valores = _a.sent();
                        return [4 /*yield*/, this.habilitacionesBD.createTableHabilitaciones()];
                    case 12:
                        habilitaciones = _a.sent();
                        if (clientes && facturas && notas && cuentas && moneda && habilitaciones &&
                            entregas && recibos && usuario && conceptos && entidades && valores) {
                            return [2 /*return*/, Promise.resolve(true)];
                        }
                        else {
                            return [2 /*return*/, Promise.resolve(false)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    TablesManageService.prototype.deleteAllData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var entregas, notas, facturas, usuario, clientes;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        entregas = false;
                        notas = false;
                        facturas = false;
                        usuario = false;
                        clientes = false;
                        return [4 /*yield*/, this.clientesBD.deleteAllClientes()];
                    case 1:
                        clientes = _a.sent();
                        return [4 /*yield*/, this.entregasBD.deleteAllEntregasDoc()];
                    case 2:
                        entregas = _a.sent();
                        return [4 /*yield*/, this.facturasBD.deleteAllFacturas()];
                    case 3:
                        facturas = _a.sent();
                        return [4 /*yield*/, this.usuariosBD.deleteAllUsuarios()];
                    case 4:
                        usuario = _a.sent();
                        return [4 /*yield*/, this.notasBD.deleteAllNotas()];
                    case 5:
                        notas = _a.sent();
                        if (clientes && notas && usuario && facturas && entregas && clientes) {
                            return [2 /*return*/, Promise.resolve(true)];
                        }
                        else {
                            return [2 /*return*/, Promise.reject(false)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    TablesManageService.prototype.deleteAllTables = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var clientes, notas, facturas, usuario, recibos, entregas, conceptos, entidades, cuentas, moneda, valores, habilitaciones;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        clientes = false;
                        notas = false;
                        facturas = false;
                        usuario = false;
                        recibos = false;
                        entregas = false;
                        conceptos = false;
                        entidades = false;
                        cuentas = false;
                        moneda = false;
                        valores = false;
                        habilitaciones = false;
                        return [4 /*yield*/, this.clientesBD.deleteTableCliente()];
                    case 1:
                        clientes = _a.sent();
                        return [4 /*yield*/, this.facturasBD.deleteTableFactura()];
                    case 2:
                        facturas = _a.sent();
                        return [4 /*yield*/, this.usuariosBD.deleteTableUsuario()];
                    case 3:
                        usuario = _a.sent();
                        return [4 /*yield*/, this.notasBD.deleteTableNotas()];
                    case 4:
                        notas = _a.sent();
                        return [4 /*yield*/, this.entregasBD.deleteTableEntregasDoc()];
                    case 5:
                        entregas = _a.sent();
                        return [4 /*yield*/, this.recibosBD.deleteTableRecibos()];
                    case 6:
                        recibos = _a.sent();
                        return [4 /*yield*/, this.conceptoBD.deleteTableConcepto()];
                    case 7:
                        conceptos = _a.sent();
                        return [4 /*yield*/, this.entidadBD.deleteTableEntidad()];
                    case 8:
                        entidades = _a.sent();
                        return [4 /*yield*/, this.cuentasBD.deleteTableCuenta()];
                    case 9:
                        cuentas = _a.sent();
                        return [4 /*yield*/, this.monedaBD.deleteTableMoneda()];
                    case 10:
                        moneda = _a.sent();
                        return [4 /*yield*/, this.valoresBD.deleteTableCValores()];
                    case 11:
                        valores = _a.sent();
                        return [4 /*yield*/, this.habilitacionesBD.deleteTableHabilitacion()];
                    case 12:
                        habilitaciones = _a.sent();
                        if (clientes && facturas && notas && cuentas && moneda && habilitaciones &&
                            entregas && recibos && usuario && conceptos && entidades && valores) {
                            return [2 /*return*/, Promise.resolve(true)];
                        }
                        else {
                            return [2 /*return*/, Promise.resolve(false)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    TablesManageService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [ClientesBDService,
            EntregasDocBDService,
            UsuariosBDService,
            FacturasBDService,
            NotasBDService,
            RecibosBDService,
            ConceptosBDService,
            MonedasBDService,
            EntidadesBDService,
            ValoresBDService,
            CuentasBDService,
            HabilitacionesBDService])
    ], TablesManageService);
    return TablesManageService;
}());
export { TablesManageService };
//# sourceMappingURL=tables-manage.service.js.map