import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Factura } from 'src/app/Clases/factura';
import { RecibosBDService } from '../recibosBD/recibos-bd.service';
import { ValorInteresService } from '../../interesValueService/valor-interes.service';
var FacturasBDService = /** @class */ (function () {
    function FacturasBDService(bd, recibosBD, interesService) {
        this.bd = bd;
        this.recibosBD = recibosBD;
        this.interesService = interesService;
        this.TABLE_FACTURAS = "CBV_FACTURAS";
        this.KEY_ID_FACTURA = "ID_FACTURA";
        this.KEY_ID_CLIENTE = "ID_CLIENTE";
        this.KEY_NO_FACTURA = "NO_FACTURA";
        this.KEY_SALDO_FAC = "SALDO_FAC";
        this.KEY_FECHA = "FECHA";
        this.KEY_SALDO = "SALDO";
        this.KEY_TOTAL = "TOTAL";
        this.KEY_CUOTA = "CUOTA";
        this.KEY_VENCIMIENTO = "VENCIMIENTO";
        this.KEY_TIPO = "TIPO";
        this.KEY_ID_MONEDA = "ID_MONEDA";
        this.KEY_INTERES = "INTERES";
        this.KEY_TOTAL_PAGADO_SERVER = "TOTAL_PAGADO_SERVER";
    }
    FacturasBDService.prototype.createTableFacturas = function () {
        var CREATE_FACTURAS_TABLE = "CREATE TABLE IF NOT EXISTS CBV_FACTURAS (ID INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "ID_FACTURA INTEGER, NO_FACTURA TEXT, FECHA TEXT, CUOTA INTEGER, TOTAL REAL, SALDO REAL, "
            + "SALDO_FAC REAL, ID_CLIENTE INTEGER, VENCIMIENTO TEXT, TIPO TEXT, ID_MONEDA INTEGER, "
            + "INTERES REAL,TOTAL_PAGADO_SERVER REAL, "
            + "FOREIGN KEY (ID_CLIENTE) REFERENCES CBV_CLIENTES (ID_CLIENTE),"
            + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA))";
        return this.bd.getDataBase().executeSql(CREATE_FACTURAS_TABLE, []).then(function (facturas) {
            console.log('se creo la tabla facturas', facturas);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla facturas', error);
            return false;
        });
    };
    FacturasBDService.prototype.deleteAllFacturas = function () {
        var query = "DELETE FROM " + this.TABLE_FACTURAS;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla facturas', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla facturas', error);
            return false;
        });
    };
    FacturasBDService.prototype.addFacturas = function (factura) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fac, params, sqlUpdate, sqlInsert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.existeFactura(factura)];
                    case 1:
                        fac = _a.sent();
                        params = [];
                        //console.log('factura a agregar', factura)
                        if (fac === true) {
                            params = [factura.TOTAL, factura.CUOTA, factura.SALDO, factura.TIPO, factura.ID_MONEDA,
                                factura.SALDO_FAC, factura.VENCIMIENTO, factura.ID_CLIENTE, factura.FECHA, factura.TIPO,
                                factura.ID_FACTURA];
                            sqlUpdate = "UPDATE " + this.TABLE_FACTURAS + " SET TOTAL =?, CUOTA=?, SALDO=?, TIPO=?, ID_MONEDA=?, "
                                + "SALDO_FAC=?, VENCIMIENTO=?, ID_CLIENTE=?, NO_FACTURA=?, FECHA=?, TIPO=?, WHERE ID_FACTURA=? ";
                            this.bd.getDataBase().executeSql(sqlUpdate, params).then(function (data) {
                                console.log('se actualizo la factura', data);
                            }).catch(function (error) {
                                console.error('error al ACTUALIZAR la factura', error);
                            });
                        }
                        else {
                            if (fac === false) {
                                params = [factura.ID_FACTURA, factura.NO_FACTURA, factura.FECHA, factura.ID_CLIENTE, factura.SALDO,
                                    factura.SALDO_FAC, factura.CUOTA, factura.VENCIMIENTO, factura.TIPO, factura.ID_MONEDA,
                                    factura.TOTAL, factura.TOTAL_PAGADO];
                                sqlInsert = "INSERT INTO " + this.TABLE_FACTURAS + " (ID_FACTURA, NO_FACTURA, FECHA, ID_CLIENTE, "
                                    + "SALDO, SALDO_FAC, CUOTA, VENCIMIENTO, TIPO, ID_MONEDA, TOTAL, TOTAL_PAGADO_SERVER) "
                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                                this.bd.getDataBase().executeSql(sqlInsert, params).then(function (data) {
                                    console.log('se inserto la factura', data);
                                }).catch(function (error) {
                                    console.error('error al insertar la factura', error);
                                });
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    FacturasBDService.prototype.existeFactura = function (factura) {
        var sql = "SELECT * FROM " + this.TABLE_FACTURAS + " WHERE ID_FACTURA=? AND CUOTA=?";
        return this.bd.getDataBase().executeSql(sql, [factura.id, factura.saldo_cuota]).then(function (data) {
            if (data.rows.length > 0) {
                console.log('existe la factura', data);
                return true;
            }
            else {
                console.log('no existe la factura', data);
                return false;
            }
        }).catch(function (error) {
            console.log('no puede obtenerse la factura', error);
            return null;
        });
    };
    FacturasBDService.prototype.getFacturasLocal = function (cliente, offset, limit, orden) {
        var _this = this;
        var params = [cliente.id, limit, offset];
        var query;
        if (orden === "nroFactura") {
            query = "SELECT y.ID_FACTURA, y.NO_FACTURA, y.TIPO, M.DESCRIPCION_MONEDA, y.ID_MONEDA, " +
                "y.CUOTA_MAXIMA,y.CUOTA_MINIMA, y.MONTO_FACTURA, y.SALDO_CUOTA, y.TOTAL, y.FECHA,y.VENCIMIENTO  "
                + "FROM (SELECT F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA, MAX(F.CUOTA) AS CUOTA_MAXIMA, "
                + "MIN(F.CUOTA) AS CUOTA_MINIMA, sum(F.SALDO) AS MONTO_FACTURA, min(F.SALDO) AS SALDO_CUOTA, "
                + "min(F.TOTAL) AS TOTAL,min(F.FECHA) AS FECHA, min(F.VENCIMIENTO) AS VENCIMIENTO FROM "
                + this.TABLE_FACTURAS + " F " + "WHERE F.ID_CLIENTE=? "
                + "GROUP BY F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA) y "
                + "INNER JOIN CBV_MONEDAS M ON y.ID_MONEDA = M.ID_MONEDA "
                + "ORDER BY y.NO_FACTURA LIMIT ?" + " OFFSET ?";
        }
        if (orden === "vencimiento") {
            query = "SELECT y.ID_FACTURA, y.NO_FACTURA, y.TIPO,M.DESCRIPCION_MONEDA,y.ID_MONEDA, "
                + "y.CUOTA_MAXIMA,y.CUOTA_MINIMA, y.MONTO_FACTURA,y.SALDO_CUOTA, y.TOTAL, y.FECHA, y.VENCIMIENTO  "
                + "FROM (SELECT F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA, MAX(F.CUOTA) AS CUOTA_MAXIMA, "
                + "MIN(F.CUOTA) AS CUOTA_MINIMA,sum(F.SALDO) AS MONTO_FACTURA,min(F.SALDO) AS SALDO_CUOTA, "
                + "min(F.TOTAL) AS TOTAL,min(F.FECHA) AS FECHA, min(F.VENCIMIENTO) AS VENCIMIENTO FROM "
                + this.TABLE_FACTURAS + " F " + "WHERE F.ID_CLIENTE=? "
                + "GROUP BY F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA) y "
                + "INNER JOIN CBV_MONEDAS M ON y.ID_MONEDA = M.ID_MONEDA "
                + "ORDER BY y.VENCIMIENTO DESC " + "LIMIT ?" + " OFFSET ?";
        }
        if (orden === "monto") {
            query = "SELECT y.ID_FACTURA, y.NO_FACTURA, y.TIPO, M.DESCRIPCION_MONEDA, y.ID_MONEDA, "
                + "y.CUOTA_MAXIMA, y.CUOTA_MINIMA, y.MONTO_FACTURA,y.SALDO_CUOTA, y.TOTAL, y.FECHA, y.VENCIMIENTO  "
                + "FROM (SELECT F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA, MAX(F.CUOTA) AS CUOTA_MAXIMA, "
                + "MIN(F.CUOTA) AS CUOTA_MINIMA, sum(F.SALDO) AS MONTO_FACTURA, min(F.SALDO) AS SALDO_CUOTA, "
                + "min(F.TOTAL) AS TOTAL,min(F.FECHA) AS FECHA, min(F.VENCIMIENTO) AS VENCIMIENTO FROM "
                + this.TABLE_FACTURAS + " F " + "WHERE F.ID_CLIENTE=? "
                + "GROUP BY F.ID_FACTURA, F.NO_FACTURA, F.TIPO, F.ID_MONEDA) y "
                + "INNER JOIN CBV_MONEDAS M ON y.ID_MONEDA = M.ID_MONEDA "
                + "ORDER BY y.TOTAL ASC " + "LIMIT ?" + " OFFSET ?";
        }
        return this.bd.getDataBase().executeSql(query, params).then(function (data) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var facturas, index, fac, monto_recibo;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        facturas = [];
                        index = 0;
                        _a.label = 1;
                    case 1:
                        if (!(index < data.rows.length)) return [3 /*break*/, 4];
                        console.log('            ', data.rows.item(index));
                        fac = new Factura(data.rows.item(index), cliente);
                        return [4 /*yield*/, this.recibosBD.recibosGenerados(parseInt(fac.id))];
                    case 2:
                        monto_recibo = _a.sent();
                        console.log('monto recibido ', monto_recibo);
                        fac.saldo = (data.rows.item(index).MONTO_FACTURA) - monto_recibo;
                        fac.interes = this.interesService.calcularInteres(fac.fecha_vencimiento, fac.saldo);
                        facturas.push(fac);
                        _a.label = 3;
                    case 3:
                        index++;
                        return [3 /*break*/, 1];
                    case 4: 
                    //console.log('facturas select', facturas)
                    return [2 /*return*/, facturas];
                }
            });
        }); }).catch(function (error) {
            console.log('Error en la consulta de facturas local', error);
        });
    };
    FacturasBDService.prototype.isFCO = function (id_factura) {
        //tipoFAC = Factura::select('tipo').where('activo', true).where('id_factura', f['id_factura']).first().tipo;
        var query = "SELECT TIPO FROM " + this.TABLE_FACTURAS + " WHERE ID_FACTURA=? LIMIT 1";
        var params = [id_factura];
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            console.log("TIPO DE FACTURA CON ID " + id_factura + " ES " + data.rows.item(0).TIPO);
            if (data.rows.item(0).TIPO == "FCO") {
                return true;
            }
            return false;
        }).catch(function (error) {
            console.error("Error trayendo tipo de factura " + id_factura, error);
            return false;
        });
    };
    FacturasBDService.prototype.find = function (id) {
        return this.bd.find(this.TABLE_FACTURAS, id).then(function (data) {
            return data;
        }).catch(function (error) {
            return {};
        });
    };
    FacturasBDService.prototype.getFacturasFrom = function (id_factura, orderBy) {
        var query = "SELECT * FROM " + this.TABLE_FACTURAS + " WHERE ID_FACTURA=?";
        if (orderBy != null) {
            query = query + " ORDER BY ";
            Object.keys(orderBy).forEach(function (key) {
                query = query + key + orderBy[key];
            });
        }
        return this.bd.getDataBase().executeSql(query, [id_factura]).then(function (data) {
            console.log("TRAER FACTURAS CON ID_FACTURA = " + id_factura + ", cantidad: " + data.rows.length);
            var facturas = [];
            for (var index = 0; index < data.rows.length; index++) {
                console.log('FACTURA DE BD: ', data.rows.item(index));
                var fac = new Factura(data.rows.item(index), data.rows.item(index).ID_CLIENTE);
                facturas.push(fac);
            }
            return facturas;
        }).catch(function (error) {
            console.error("Error trayendo FACTURAS " + id_factura, error);
            return [];
        });
    };
    FacturasBDService.prototype.todosCredito = function (detallesFac) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var checkCredito, _i, detallesFac_1, factura, tipo;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        checkCredito = true;
                        console.log("Metodo para saber si todas las facturas son creditos ");
                        _i = 0, detallesFac_1 = detallesFac;
                        _a.label = 1;
                    case 1:
                        if (!(_i < detallesFac_1.length)) return [3 /*break*/, 4];
                        factura = detallesFac_1[_i];
                        return [4 /*yield*/, this.isFCO(factura.ID_FACTURA)];
                    case 2:
                        tipo = _a.sent();
                        if (tipo === true) {
                            checkCredito = false;
                            return [2 /*return*/, checkCredito];
                        }
                        else {
                            checkCredito = true;
                        }
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, checkCredito];
                }
            });
        });
    };
    FacturasBDService.prototype.detallesFactura = function (id_factura) {
        var query_facturas = "SELECT MAX(CUOTA) as MAX_CUOTA, ID_MONEDA, NO_FACTURA "
            + "FROM CBV_FACTURAS WHERE ID_FACTURA=? GROUP BY ID_MONEDA, NO_FACTURA";
        return this.bd.getDataBase().executeSql(query_facturas, [id_factura]).then(function (data) {
            console.log("traer facturas = " + id_factura + ", cantidad: " + data.rows.length);
            if (data.rows.length > 0) {
                return data.rows.item(0);
            }
            return null;
        }).catch(function (error) {
            console.log('Error al obtener detall de la factura', error);
        });
    };
    FacturasBDService.prototype.deleteTableFactura = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_FACTURAS;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla factura', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla factura', error);
            return false;
        });
    };
    FacturasBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService,
            RecibosBDService,
            ValorInteresService])
    ], FacturasBDService);
    return FacturasBDService;
}());
export { FacturasBDService };
//# sourceMappingURL=facturas-bd.service.js.map