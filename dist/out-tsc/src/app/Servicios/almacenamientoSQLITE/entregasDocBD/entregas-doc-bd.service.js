import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import * as moment from 'moment';
import { Documentos } from 'src/app/Clases/documentos';
var EntregasDocBDService = /** @class */ (function () {
    function EntregasDocBDService(bd) {
        this.bd = bd;
        this.TABLE_ENTREGAS = "CBV_ENTREGAS";
        this.KEY_ID = "ID";
        this.KEY_ID_ENTREGA = "ID_ENTREGA";
        this.KEY_ID_CLIENTE = "ID_CLIENTE";
        this.KEY_ID_ENTREGA_DET = "ID_ENTREGA_DET";
        this.KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION";
        this.KEY_ID_DOCUMENTO = "ID_DOCUMENTO";
        this.KEY_NO_DOCUMENTO = "NO_DOCUMENTO";
        this.KEY_FECHA_DOCUMENTO = "FECHA_DOCUMENTO";
        this.KEY_FECHA_ENTREGA = "FECHA_ENTREGA";
        this.KEY_ID_MONEDA = "ID_MONEDA";
        this.KEY_SIMBOLO = "SIMBOLO";
        this.KEY_IMPORTE = "IMPORTE";
        this.KEY_TIPO = "TIPO";
        this.KEY_GUARDADO = "GUARDADO";
        this.KEY_LATITUD = "LATITUD";
        this.KEY_LONGITUD = "LONGITUD";
        this.KEY_IMEI = "IMEI";
        this.KEY_ID_COBRADOR = "ID_COBRADOR";
        this.KEY_HABILITACION_CAJA = "HABILITACION_CAJA";
    }
    EntregasDocBDService.prototype.createTableEntregas = function () {
        var CREATE_ENTREGAS_TABLE = "CREATE TABLE IF NOT EXISTS CBV_ENTREGAS (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "ID_ENTREGA INTEGER, ID_ENTREGA_DET INTEGER, REQUIERE_SINCRONIZACION INTEGER, " +
            "HABILITACION_CAJA INTEGER, ID_DOCUMENTO INTEGER, NO_DOCUMENTO TEXT, FECHA_DOCUMENTO TEXT, " +
            "ID_MONEDA INTEGER, SIMBOLO TEXT, IMPORTE REAL, GUARDADO INTEGER, TIPO TEXT, " +
            "ID_CLIENTE INTEGER, LATITUD TEXT, LONGITUD TEXT, IMEI TEXT, ID_COBRADOR INTEGER, " +
            "FECHA_ENTREGA TEXT, FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA))";
        return this.bd.getDataBase().executeSql(CREATE_ENTREGAS_TABLE, []).then(function (entregas) {
            console.log('se creo la tabla entregas-documentos', entregas);
            return Promise.resolve(entregas);
        }).catch(function (error) {
            console.error('no se creo la tabla entregas', error);
            return Promise.reject(error);
        });
    };
    EntregasDocBDService.prototype.insertarEntregaDocTabla = function (entrega) {
        var sql = "INSERT INTO " + this.TABLE_ENTREGAS + " (ID_ENTREGA, ID_CLIENTE, ID_ENTREGA_DET, "
            + "REQUIERE_SINCRONIZACION, ID_DOCUMENTO, NO_DOCUMENTO,FECHA_DOCUMENTO,ID_MONEDA, "
            + "SIMBOLO, IMPORTE, GUARDADO, TIPO, LATITUD, LONGITUD, IMEI, ID_COBRADOR, FECHA_ENTREGA) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        var params = [entrega.ID_ENTREGA, entrega.ID_CLIENTE, entrega.ID_ENTREGA_DET, 0, entrega.ID_DOCUMENTO,
            entrega.NO_DOCUMENTO, entrega.FECHA_DOCUMENTO, entrega.ID_MONEDA, entrega.SIMBOLO, entrega.IMPORTE,
            entrega.GUARDADO, entrega.TIPO, entrega.LATITUD, entrega.LONGITUD, entrega.IMEI, entrega.ID_COBRADOR,
            moment(new Date()).format('DD-MM-YYYY')];
        return this.bd.getDataBase().executeSql(sql, params).then(function (data) {
            console.log('se inserto una nueva entrega', data);
        }).catch(function (error) {
            console.log('no pudo insertarse la entregaDoc', error);
        });
    };
    EntregasDocBDService.prototype.updateEntregasDoc = function (entregaD) {
        var sql = "UPDATE " + this.TABLE_ENTREGAS + " SET IMPORTE =?, ID_ENTREGA =?, ID_ENTREGA_DET =?, "
            + "LATITUD=?, LONGITUD=?, IMEI=?, ID_CLIENTE=?, NO_DOCUMENTO=?, FECHA_DOCUMENTO=?, "
            + "FECHA_ENTREGA=? WHERE ID_DOCUMENTO=?";
        var params = [entregaD.IMPORTE, entregaD.ID_ENTREGA, entregaD.ID_ENTREGA_DET, entregaD.LATITUD,
            entregaD.LONGITUD, entregaD.IMEI, entregaD.ID_CLIENTE, entregaD.NO_DOCUMENTO, entregaD.FECHA_DOCUMENTO,
            moment(new Date()).format('DD-MM-YYYY'), entregaD.ID_DOCUMENTO];
        this.bd.getDataBase().executeSql(sql, params).then(function (data) {
            console.log('se inserto un registro de entrega doc', data);
        }).catch(function (error) {
            console.error('Error actualizar la entrega doc', error);
        });
    };
    EntregasDocBDService.prototype.syncEntregasHabilitacion = function (habilitacion) {
        var query = "SELECT * FROM " + this.TABLE_ENTREGAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
            "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
        return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(function (data) {
            if (data.rows.length > 0) {
                return true;
            }
            else {
                return false;
            }
        });
    };
    EntregasDocBDService.prototype.deleteAllEntregasDoc = function () {
        var query = "DELETE FROM " + this.TABLE_ENTREGAS;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los dfatos de la tabla entregas', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla entregas', error);
            return false;
        });
    };
    EntregasDocBDService.prototype.cantidadEntregaDocSync = function (habilitacion) {
        var query = "SELECT COUNT (*) as cantidad FROM " + this.TABLE_ENTREGAS + " WHERE HABILITACION_CAJA=? AND REQUIERE_SINCRONIZACION=1 AND GUARDADO=1";
        return this.bd.getDataBase().executeSql(query, [habilitacion]).then(function (data) {
            console.log("CANT ENTREGAS ", data.rows.item(0).cantidad);
            return data.rows.item(0).cantidad;
        }).catch(function (error) {
            console.log("Error trayendo cantidad de entregas");
            return -1;
        });
    };
    EntregasDocBDService.prototype.getEntregasLocal = function (id_cliente, offset, limit) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var entregas, today, query, params;
            return tslib_1.__generator(this, function (_a) {
                entregas = [];
                today = moment(new Date()).format("DD-MM-YYYY");
                query = "SELECT ID_ENTREGA, ID_ENTREGA_DET, REQUIERE_SINCRONIZACION, HABILITACION_CAJA, "
                    + "ID_DOCUMENTO, NO_DOCUMENTO, FECHA_DOCUMENTO, ID_MONEDA, SIMBOLO, ROUND(IMPORTE,2) AS IMPORTE, "
                    + "GUARDADO, TIPO, ID_CLIENTE, LATITUD, LONGITUD, IMEI, ID_COBRADOR "
                    + "FROM CBV_ENTREGAS WHERE ID_CLIENTE=? AND FECHA_ENTREGA=? LIMIT " + limit + " OFFSET " + offset;
                params = [id_cliente, today];
                return [2 /*return*/, this.bd.getDataBase().executeSql(query, params).then(function (data) {
                        console.log('entregas traidas de la bd', data.rows.length, data);
                        for (var i = 0; i < data.rows.length; i++) {
                            entregas.push(new Documentos(data.rows.item(i)));
                        }
                        return entregas;
                    }).catch(function (error) {
                        console.log("Error trayendo entregas de documentos", error);
                        return null;
                    })];
            });
        });
    };
    EntregasDocBDService.prototype.existeEntregaDoc = function (id_doc) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var doc, query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        doc = false;
                        query = "SELECT * FROM " + this.TABLE_ENTREGAS + " WHERE ID_DOCUMENTO=? AND REQUIERE_SINCRONIZACION=?";
                        return [4 /*yield*/, this.bd.getDataBase().executeSql(query, [id_doc, 0]).then(function (data) {
                                console.log('entrega si no hubo error', data);
                                if (data.rows.length > 0) {
                                    doc = true;
                                }
                                else {
                                    doc = false;
                                }
                            }).catch(function (error) {
                                console.log("Error al obtener una entrega", error);
                                doc = null;
                            })];
                    case 1:
                        _a.sent();
                        console.log('retorno de existeEntregaDoc', doc);
                        return [2 /*return*/, doc];
                }
            });
        });
    };
    EntregasDocBDService.prototype.existeEntregaDocSync = function (id_doc) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var doc, query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        doc = false;
                        query = "SELECT * FROM " + this.TABLE_ENTREGAS + " WHERE ID_DOCUMENTO=? AND REQUIERE_SINCRONIZACION=?";
                        return [4 /*yield*/, this.bd.getDataBase().executeSql(query, [id_doc, 1]).then(function (data) {
                                console.log('entrega si no hubo error', data);
                                if (data.rows.length > 0) {
                                    doc = true;
                                }
                                else {
                                    doc = false;
                                }
                            }).catch(function (error) {
                                console.log("Error al obtener una entrega", error);
                                doc = null;
                            })];
                    case 1:
                        _a.sent();
                        console.log('retorno de existeEntregaDocSync', doc);
                        return [2 /*return*/, doc];
                }
            });
        });
    };
    EntregasDocBDService.prototype.getEntregasASincronizar = function (habilitacion) {
        var query = "SELECT ID,ID_ENTREGA, ID_ENTREGA_DET, REQUIERE_SINCRONIZACION, HABILITACION_CAJA,ID_DOCUMENTO, NO_DOCUMENTO, FECHA_DOCUMENTO, ID_MONEDA, SIMBOLO, round(IMPORTE,2) AS IMPORTE, "
            + "GUARDADO, TIPO, ID_CLIENTE, LATITUD, LONGITUD, IMEI, ID_COBRADOR FROM " + this.TABLE_ENTREGAS + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=? AND HABILITACION_CAJA=?";
        return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(function (data) {
            console.log("Exito trayendo entregas para sincronizar, cantidad: ", data.rows.length);
            var entregas = [];
            for (var i = 0; i < data.rows.length; i++) {
                entregas.push(data.rows.item(i));
            }
            console.log("ENTREGAS TRAIDAS DE LA BD PARA SINCRONIZAR: ", entregas);
            return entregas;
        }).catch(function (error) {
            console.error("Error trayendo entregas para sincronizar", error);
            return [];
        });
    };
    EntregasDocBDService.prototype.entregarDocumentos = function (documentos) {
        var strSQL = "UPDATE " + this.TABLE_ENTREGAS + " SET IMPORTE=?, GUARDADO =?, ID_ENTREGA =?, "
            + "ID_ENTREGA_DET=?, HABILITACION_CAJA=?, ID_CLIENTE=?, NO_DOCUMENTO=?, FECHA_DOCUMENTO=?, "
            + "REQUIERE_SINCRONIZACION=?, LATITUD=?, LONGITUD=?, IMEI=? WHERE ID_DOCUMENTO=?";
        for (var _i = 0, _a = documentos.entregas; _i < _a.length; _i++) {
            var documento = _a[_i];
            var params = [documento.importe, documento.guardado, documento.id_entrega, documento.id_entrega_det,
                documento.habilitacion_caja, documento.id_cliente, documento.no_documento,
                documento.fecha_documento, 1, documento.latitud, documento.longitud, documento.imei, documento.id_documento];
            this.bd.getDataBase().executeSql(strSQL, params).then(function (data) {
                console.log("Exito en entregar o actualizar un documento ", data);
            }).catch(function (error) {
                console.error("Error al insertar una entrega", error);
                return Promise.reject({ error: "No se pudo realizar la entrega de documentos" });
            });
        }
        return Promise.resolve({ exito: "Entregas realizadas con exito" });
    };
    EntregasDocBDService.prototype.deleteTableEntregasDoc = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_ENTREGAS;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla entregas', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla entregas', error);
            return false;
        });
    };
    EntregasDocBDService.prototype.actualizarSincronizacionDeEntregas = function () {
        var query = "UPDATE " + this.TABLE_ENTREGAS + " SET " + this.KEY_REQUIERE_SINCRONIZACION + "=? WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=?";
        var params = [0, 1];
        this.bd.getDataBase().executeSql(query, params).then(function (data) {
            console.log("Columna de entregas que requieren sincronizacion cambiados a 0", data);
        }).catch(function (error) {
            console.log('No se pudo actualizar la columna de requiere sincronizacion a 0 de las entregas', error);
        });
    };
    EntregasDocBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], EntregasDocBDService);
    return EntregasDocBDService;
}());
export { EntregasDocBDService };
//# sourceMappingURL=entregas-doc-bd.service.js.map