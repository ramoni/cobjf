import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Concepto } from 'src/app/Clases/concepto';
var ConceptosBDService = /** @class */ (function () {
    function ConceptosBDService(db) {
        this.db = db;
        this.TABLE_CONCEPTOS = "CBV_CONCEPTOS";
        this.KEY_ID_CONCEPTO = "ID_CONCEPTO";
        this.KEY_DESCRIPCION = "DESCRIPCION";
    }
    ConceptosBDService.prototype.createTableConcepto = function () {
        var CREATE_CONCEPTOS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_CONCEPTOS +
            " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_CONCEPTO INTEGER, DESCRIPCION TEXT)";
        return this.db.getDataBase().executeSql(CREATE_CONCEPTOS_TABLE, []).then(function (concepto) {
            console.log('se creo la tabla concepto', concepto);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla concepto', error);
            return false;
        });
    };
    ConceptosBDService.prototype.addConcepto = function (concepto) {
        var addLine = "INSERT INTO " + this.TABLE_CONCEPTOS + " (ID_CONCEPTO, DESCRIPCION) VALUES (?,?)";
        return this.db.getDataBase().executeSql(addLine, [concepto.id_concepto, concepto.descripcion]).then(function (exito) {
            console.log('se agrego el concepto', exito);
            return true;
        }).catch(function (error) {
            console.log('no se puede agregar el concepto', error);
            return false;
        });
    };
    ConceptosBDService.prototype.getConceptos = function () {
        var query = "SELECT * FROM " + this.TABLE_CONCEPTOS;
        var conceptos = [];
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            if (data.rows.length > 0) {
                for (var index = 0; index < data.rows.length; index++) {
                    conceptos.push(new Concepto(data.rows.item(index)));
                }
            }
            return conceptos;
        }).catch(function (err) {
            console.log('no pudo obtenerse las conceptos localmente', err);
            return err;
        });
    };
    ConceptosBDService.prototype.getConcepto = function (id_concepto) {
        var query = "SELECT * FROM " + this.TABLE_CONCEPTOS + " WHERE ID_CONCEPTO=?";
        return this.db.getDataBase().executeSql(query, [id_concepto]).then(function (data) {
            if (data.rows.length > 0) {
                return new Concepto(data.rows.item(0));
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.log("error al traer la concepto por el id", error);
            return null;
        });
    };
    ConceptosBDService.prototype.deleteAllConceptos = function () {
        var query = "DELETE FROM " + this.TABLE_CONCEPTOS;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla conceptos', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla conceptos', error);
            return false;
        });
    };
    ConceptosBDService.prototype.deleteTableConcepto = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_CONCEPTOS;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla concepto', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla concepto', error);
            return false;
        });
    };
    ConceptosBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], ConceptosBDService);
    return ConceptosBDService;
}());
export { ConceptosBDService };
//# sourceMappingURL=conceptos-bd.service.js.map