import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
var UsuariosBDService = /** @class */ (function () {
    function UsuariosBDService(bd) {
        this.bd = bd;
        this.TABLE_USER = "CBV_USER";
        this.KEY_USER_ID = "USER_ID";
        this.KEY_USUARIO = "USUARIO";
        this.KEY_PASSWORD = "PASSWORD";
        this.KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION";
        this.KEY_HABILITACION = "HABILITACION_CAJA";
        this.KEY_FACTURAS = "FACTURAS";
        this.KEY_DOCUMENTOS = "DOCUMENTOS";
        this.KEY_NOMBRE = "NOMBRE";
        this.KEY_CAJA = "CAJA";
        this.KEY_TOKEN_EXPIRED = "TOKEN_EXPIRED";
        this.KEY_TOKEN = "TOKEN";
        this.KEY_STATUS = "IS_STATUS"; //por defecto es 1 significa que la sesion de usuario es valida
        this.KEY_ULTIMO_RECIBO = "ULTIMO_RECIBO";
        this.KEY_TALONARIO = "TALONARIO"; //por defecto es 1 significa que la sesion de usuario es valida
        this.KEY_ID_COBRADOR = "ID_COBRADOR";
    }
    UsuariosBDService.prototype.createTableUsuarios = function () {
        var CREATE_USUARIOS_TABLE = "CREATE TABLE IF NOT EXISTS CBV_USER (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "USUARIO TEXT,  PASSWORD TEXT, REQUIERE_SINCRONIZACION INTEGER, HABILITACION_CAJA INTEGER, " +
            "FACTURAS INTEGER, DOCUMENTOS INTEGER, NOMBRE TEXT, CAJA TEXT, TOKEN_EXPIRED TEXT, " +
            "TOKEN TEXT, IS_STATUS INTEGER, ULTIMO_RECIBO INTEGER, TALONARIO INTEGER, ID_COBRADOR INTEGER, " +
            "DATOS_BAJADA INTEGER, DATOS_SUBIDA INTEGER, RECIBOS_SYNC INTEGER DEFAULT 0, " +
            "ENTREGAS_SYNC INTEGER DEFAULT 0, NOTAS_VISITAS_SYNC INTEGER DEFAULT 0, " +
            "NOTAS_ENTREGAS_SYNC INTEGER DEFAULT 0)";
        return this.bd.getDataBase().executeSql(CREATE_USUARIOS_TABLE, []).then(function (users) {
            console.log('creo la tabla usuarios', users);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla usuarios', error);
            return false;
        });
    };
    UsuariosBDService.prototype.existeUsuario = function (usuario, pass) {
        var sql = "SELECT * FROM CBV_USER WHERE USUARIO=? AND PASSWORD=? AND IS_STATUS=?";
        return this.bd.getDataBase().executeSql(sql, [usuario, pass, 1]).then(function (data) {
            console.log('se ejecuto si existe usuario', data);
            return data;
        });
    };
    UsuariosBDService.prototype.existeHabilitacionUser = function (hab, usuario, pass) {
        var sql = "SELECT HABILITACION_CAJA FROM " + this.TABLE_USER + " WHERE HABILITACION_CAJA=? " +
            "AND USUARIO=? AND PASSWORD=?";
        return this.bd.getDataBase().executeSql(sql, [hab, usuario, pass]).then(function (data) {
            console.log('se ejecuto si existe usuario', data);
            return data;
        });
    };
    UsuariosBDService.prototype.addUser = function (usuario, password, userInterface, token, expiredToken) {
        var _this = this;
        return this.existeUsuario(usuario, password).then(function (data) {
            if (data.rows.length > 0) {
                var sql = "UPDATE " + _this.TABLE_USER + " SET " + _this.KEY_TOKEN + "=?, " + _this.KEY_TOKEN_EXPIRED + "=?, " +
                    _this.KEY_CAJA + "=?, " + _this.KEY_HABILITACION + "=?, " + _this.KEY_ULTIMO_RECIBO + "=?, " +
                    _this.KEY_TALONARIO + "=?, " + _this.KEY_ID_COBRADOR + "=?, RECIBOS_SYNC=?, ENTREGAS_SYNC=?, " +
                    "NOTAS_VISITAS_SYNC=?, NOTAS_ENTREGAS_SYNC=? WHERE USUARIO=? AND PASSWORD=?";
                _this.bd.getDataBase().executeSql(sql, [token, expiredToken, userInterface.CAJA, userInterface.HABILITACION,
                    userInterface.USUARIO.ULTIMO_RECIBO, userInterface.USUARIO.TALONARIO, userInterface.USUARIO.ID_COBRADOR,
                    userInterface.DATOS_SINCRONIZADOS.RECIBOS, userInterface.DATOS_SINCRONIZADOS.ENTREGAS,
                    userInterface.DATOS_SINCRONIZADOS.NOTAS_VISITAS, userInterface.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS,
                    usuario, password]).then(function (exito) {
                    console.log('exito al actualizar usuario', exito);
                    return 1;
                });
            }
            else {
                var sql = "INSERT INTO " + _this.TABLE_USER + " (USUARIO, PASSWORD, FACTURAS, DOCUMENTOS, " +
                    "NOMBRE, HABILITACION_CAJA, CAJA, REQUIERE_SINCRONIZACION, TOKEN, TOKEN_EXPIRED, IS_STATUS, " +
                    "TALONARIO, ID_COBRADOR, ULTIMO_RECIBO, RECIBOS_SYNC, ENTREGAS_SYNC, NOTAS_VISITAS_SYNC, " +
                    "NOTAS_ENTREGAS_SYNC) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                var insert = [usuario, password, userInterface.FACTURAS, userInterface.DOCUMENTOS,
                    userInterface.USUARIO.NOMBRE, userInterface.HABILITACION, userInterface.CAJA, 1, token,
                    expiredToken, 1, userInterface.USUARIO.TALONARIO, userInterface.USUARIO.ID_COBRADOR,
                    userInterface.USUARIO.ULTIMO_RECIBO, userInterface.DATOS_SINCRONIZADOS.RECIBOS,
                    userInterface.DATOS_SINCRONIZADOS.ENTREGAS, userInterface.DATOS_SINCRONIZADOS.NOTAS_VISITAS,
                    userInterface.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS];
                _this.bd.getDataBase().executeSql(sql, insert).then(function (exito) {
                    console.log('exito al insertar usuario', exito);
                    return 1;
                });
            }
        });
    };
    UsuariosBDService.prototype.deleteAllUsuarios = function () {
        var query = "DELETE FROM " + this.TABLE_USER;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla usuarios', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla usuarios', error);
            return false;
        });
    };
    UsuariosBDService.prototype.getUserData = function () {
        var query = "SELECT HABILITACION_CAJA, FACTURAS,DOCUMENTOS, CAJA, TOKEN, TOKEN_EXPIRED, "
            + "ID_COBRADOR,NOMBRE, TALONARIO, RECIBOS_SYNC, ENTREGAS_SYNC, NOTAS_VISITAS_SYNC, "
            + "NOTAS_ENTREGAS_SYNC FROM " + this.TABLE_USER + " LIMIT 1";
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('usuario obtenido', data.rows.item(0));
            var object = data.rows.item(0);
            var jsonObject = {};
            jsonObject["HABILITACION"] = object.HABILITACION_CAJA;
            jsonObject["FACTURAS"] = object.FACTURAS;
            jsonObject["DOCUMENTOS"] = object.DOCUMENTOS;
            jsonObject["CAJA"] = object.CAJA;
            jsonObject["TOKEN"] = object.TOKEN;
            jsonObject["EXPIRED"] = object.TOKEN_EXPIRED;
            var jsonUser = {};
            jsonUser["NOMBRE"] = object.NOMBRE;
            jsonUser["TALONARIO"] = object.TALONARIO;
            jsonUser["ID_COBRADOR"] = object.ID_COBRADOR;
            jsonUser["RECIBOS"] = object.RECIBOS_SYNC;
            jsonUser["ENTREGAS"] = object.ENTREGAS_SYNC;
            jsonUser["NOTAS_VISITAS"] = object.NOTAS_VISITAS_SYNC;
            jsonUser["NOTAS_ENTREGAS"] = object.NOTAS_ENTREGAS_SYNC;
            jsonObject["USUARIO"] = jsonUser;
            console.log('json user', jsonObject);
            return jsonObject;
        }).catch(function (error) {
            console.log('no se pudo traer usuario', error);
            return null;
        });
    };
    UsuariosBDService.prototype.updateDatosUsuario = function (user) {
        var query = "UPDATE " + this.TABLE_USER + " SET DOCUMENTOS=?, FACTURAS=?, HABILITACION_CAJA=?, " +
            "ULTIMO_RECIBO=?, TALONARIO=?, CAJA=?, RECIBOS_SYNC=?, ENTREGAS_SYNC=?, NOTAS_VISITAS_SYNC=?, " +
            "NOTAS_ENTREGAS_SYNC=? WHERE ID_COBRADOR=?";
        var vector = [user.DOCUMENTOS, user.FACTURAS, user.HABILITACION, user.USUARIO.ULTIMO_RECIBO,
            user.USUARIO.TALONARIO, user.CAJA, user.DATOS_SINCRONIZADOS.RECIBOS, user.DATOS_SINCRONIZADOS.ENTREGAS,
            user.DATOS_SINCRONIZADOS.NOTAS_VISITAS, user.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS, user.USUARIO.ID_COBRADOR];
        return this.bd.getDataBase().executeSql(query, vector).then(function (data) {
            console.log('se actualizo correctamente la tabla usuarios', data);
            return true;
        }).catch(function (error) {
            console.log('no se pudo actualizar la tabla usuarios', error);
            return false;
        });
    };
    UsuariosBDService.prototype.getUltimoRecibo = function () {
        var query = "SELECT MAX(ULTIMO_RECIBO) AS ULTIMO_RECIBO FROM " + this.TABLE_USER;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('Ultimo recibo del usuario', data.rows.item(0).ULTIMO_RECIBO);
            return data.rows.item(0).ULTIMO_RECIBO;
        }).catch(function (error) {
            console.log('No se pudo obtener el nro del ultimo recibo del usuario', error);
            return -1;
        });
    };
    UsuariosBDService.prototype.getTalonario = function (id_cobrador) {
        var talonario = {
            TALONARIO: 0,
            NOMBRE: ""
        };
        var query_usuario = "SELECT TALONARIO, NOMBRE FROM CBV_USER WHERE ID_COBRADOR=?";
        var params_usuario = [id_cobrador];
        return this.bd.getDataBase().executeSql(query_usuario, params_usuario).then(function (data) {
            if (data.rows.length > 0) {
                talonario.TALONARIO = data.rows.item(0).TALONARIO;
                talonario.NOMBRE = data.rows.item(0).NOMBRE;
                console.log('talonario y nombre ', talonario);
                return talonario;
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.log('No se pudo obtener el talonario del usuario', error);
            return -1;
        });
    };
    UsuariosBDService.prototype.deleteTableUsuario = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_USER;
        return this.bd.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla user', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla user', error);
            return false;
        });
    };
    UsuariosBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], UsuariosBDService);
    return UsuariosBDService;
}());
export { UsuariosBDService };
//# sourceMappingURL=usuarios-bd.service.js.map