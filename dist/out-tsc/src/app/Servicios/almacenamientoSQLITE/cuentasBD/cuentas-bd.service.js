import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Cuenta } from 'src/app/Clases/cuenta';
var CuentasBDService = /** @class */ (function () {
    function CuentasBDService(db) {
        this.db = db;
        this.TABLE_CUENTAS = "CBV_CUENTAS";
        this.KEY_ID = "ID";
        this.KEY_ID_CUENTA = "ID_CUENTA";
        this.KEY_DESCRIPCION = "DESCRIPCION";
        this.KEY_ID_ENTIDAD = "ID_ENTIDAD";
        this.KEY_ID_MONEDA = "ID_MONEDA";
    }
    CuentasBDService.prototype.createTableCuentas = function () {
        var CREATE_CUENTAS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_CUENTAS +
            " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_CUENTA TEXT, DESCRIPCION TEXT, " +
            "ID_ENTIDAD TEXT, ID_MONEDA TEXT)";
        return this.db.getDataBase().executeSql(CREATE_CUENTAS_TABLE, []).then(function (cuenta) {
            console.log('se creo la table cuenta', cuenta);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla cuenta', error);
            return false;
        });
    };
    CuentasBDService.prototype.addCuenta = function (cuenta) {
        var addLine = "INSERT INTO " + this.TABLE_CUENTAS + " (ID_CUENTA, DESCRIPCION,ID_ENTIDAD,ID_MONEDA)" +
            "VALUES (?,?,?,?)";
        return this.db.getDataBase().executeSql(addLine, [cuenta.id, cuenta.descripcion, cuenta.id_entidad, cuenta.id_moneda])
            .then(function (exito) {
            console.log('se agrego la Cuenta', exito);
            return true;
        }).catch(function (error) {
            console.log('no se puede agregar la Cuenta', error);
            return false;
        });
    };
    CuentasBDService.prototype.getCuentas = function () {
        var query = "SELECT * FROM " + this.TABLE_CUENTAS;
        var cuentas = [];
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            //console.log("DATOS DE CUENTAS TRAIDA ", data.rows.length, data.rows.item(0), data)
            if (data.rows.length > 0) {
                for (var index = 0; index < data.rows.length; index++) {
                    cuentas.push(new Cuenta(data.rows.item(index)));
                }
            }
            return cuentas;
        }).catch(function (err) {
            console.log('no pudo obtenerse las cuentas localmente', err);
            return err;
        });
    };
    CuentasBDService.prototype.getCuenta = function (id_cuenta) {
        var query = "SELECT * FROM CBV_CUENTAS WHERE ID_CUENTA=?";
        return this.db.getDataBase().executeSql(query, [id_cuenta]).then(function (data) {
            if (data.rows.length > 0) {
                return new Cuenta(data.rows.item(0));
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.log("error al traer la cuenta por el id", error);
            return null;
        });
    };
    CuentasBDService.prototype.deleteAllCuentas = function () {
        var query = "DELETE FROM " + this.TABLE_CUENTAS;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla cuentas', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla cuentas', error);
            return false;
        });
    };
    CuentasBDService.prototype.deleteTableCuenta = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_CUENTAS;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla cuentas', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla cuentas', error);
            return false;
        });
    };
    CuentasBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], CuentasBDService);
    return CuentasBDService;
}());
export { CuentasBDService };
//# sourceMappingURL=cuentas-bd.service.js.map