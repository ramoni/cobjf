import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';
import { MonedaLogicaService } from '../../monedaService/moneda-logica.service';
import { Factura } from 'src/app/Clases/factura';
import * as moment from 'moment';
var RecibosBDService = /** @class */ (function () {
    function RecibosBDService(bd, fileService, platform, monedaService) {
        this.bd = bd;
        this.fileService = fileService;
        this.platform = platform;
        this.monedaService = monedaService;
        this.TABLE_RECIBOS_CAB = "CBV_RECIBOS_CAB";
        this.KEY_ID_COBRADOR = "ID_COBRADOR"; //cabecera
        this.KEY_FECHA = "FECHA"; //cabecera
        this.KEY_REQUIERE_SINCRONIZACION = "REQUIERE_SINCRONIZACION"; //cabecera, detalle, valores
        this.KEY_HABILITACION_CAJA = "HABILITACION_CAJA"; //cabecera
        this.KEY_ESTADO = "ESTADO"; //cabecera
        this.KEY_ID_MONEDA = "ID_MONEDA"; //cabecera, detalle
        this.KEY_COTIZACION = "COTIZACION"; //cabecera, detalle, valores
        this.KEY_TOTAL_RECIBO = "TOTAL_RECIBO"; //cabecera
        this.KEY_INTERES = "INTERES"; //cabecera
        this.KEY_ANTICIPO = "ANTICIPO"; //cabecera
        this.KEY_LATITUD = "LATITUD"; //cabecera
        this.KEY_LONGITUD = "LONGITUD"; //cabecera
        this.KEY_ID_CLIENTE = "ID_CLIENTE"; //cabecera
        this.KEY_NRO_RECIBO = "NRO_RECIBO"; //cabecera
        this.KEY_IMEI = "IMEI"; //cabecera
        this.KEY_VERSION_APP = "VERSION"; //cabecera
        this.KEY_USUARIO = "USUARIO"; //cabecera
        this.KEY_ID_CONCEPTO = "ID_CONCEPTO"; //cabecera
        this.KEY_MARCA = "MARCA"; //cabecera
        this.KEY_FECHA_ANULACION = "FECHA_ANULACION"; //cabecera
        this.TABLE_RECIBOS_DETALLE = "CBV_RECIBOS_DET";
        this.KEY_ID_RECIBO_CAB = "ID_RECIBO_CAB"; //detalle, valores
        this.KEY_ID_FACTURA = "ID_FACTURA";
        this.KEY_MONTO = "MONTO"; //detalle, valores
        this.KEY_NRO_CUOTA = "NRO_CUOTA"; //detalle
        this.KEY_MONTO_VALORIZADO = "MONTO_VALORIZADO"; //detalle
        this.KEY_NO_FACTURA = "NO_FACTURA"; //detalle
        this.TABLE_RECIBOS_VALORES = "CBV_RECIBOS_VALORES";
        this.KEY_ID_TIPO_VALOR = "ID_TIPO_VALOR"; //valores
        this.KEY_NUMERO_VALOR = "NUMERO_VALOR"; //valores
        this.KEY_ID_ENTIDAD = "ID_ENTIDAD"; //valores
        this.KEY_ID_CUENTA_BANCARIA = "ID_CUENTA_BANCARIA"; //valores
        this.KEY_FECHA_EMISION = "FECHA_EMISION"; //valores
        this.KEY_FECHA_VENCIMIENTO = "FECHA_VENCIMIENTO"; //valores
    }
    RecibosBDService.prototype.createTableRecibos = function () {
        var _this = this;
        var CREATE_RECIBOS_CAB_TABLE = "CREATE TABLE IF NOT EXISTS CBV_RECIBOS_CAB (ID_RECIBO_CAB INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "ID_COBRADOR INTEGER, FECHA TEXT, REQUIERE_SINCRONIZACION INTEGER, HABILITACION_CAJA INTEGER, "
            + "ESTADO TEXT, ID_MONEDA INTEGER, COTIZACION REAL(12,4), TOTAL_RECIBO REAL, INTERES REAL, "
            + "ANTICIPO REAL, LATITUD TEXT, LONGITUD TEXT, NRO_RECIBO INTEGER, ID_CLIENTE INTEGER, "
            + "FECHA_ANULACION TEXT, USUARIO TEXT, IMEI TEXT,VERSION TEXT,ULTIMO_TIME_IMPRESION_DUPLICADO TEXT, "
            + "ULTIMO_TIME_IMPRESION_ORIGINAL TEXT, ID_CONCEPTO INTEGER,MARCA TEXT, "
            + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA),"
            + "FOREIGN KEY (ID_CONCEPTO) REFERENCES CBV_CONCEPTOS (ID_CONCEPTO))";
        var CREATE_RECIBOS_DET_TABLE = "CREATE TABLE IF NOT EXISTS CBV_RECIBOS_DET (ID_RECIBO_DET INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "ID_RECIBO_CAB INTEGER, ID_FACTURA INTEGER, REQUIERE_SINCRONIZACION INTEGER , "
            + "MONTO REAL(20,4), NRO_CUOTA INTEGER, COTIZACION REAL(12,4), MONTO_VALORIZADO REAL, "
            + "NO_FACTURA TEXT, ID_MONEDA INTEGER, "
            + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA), "
            + "FOREIGN KEY (ID_RECIBO_CAB) REFERENCES CBV_RECIBOS_CAB (ID_RECIBO_CAB))";
        var CREATE_RECIBOS_VALORES_TABLE = "CREATE TABLE IF NOT EXISTS CBV_RECIBOS_VALORES (ID_RECIBO_VALOR INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "ID_RECIBO_CAB INTEGER, ID_TIPO_VALOR INTEGER, NUMERO_VALOR TEXT, "
            + "REQUIERE_SINCRONIZACION INTEGER, MONTO REAL(18,2), "
            + "COTIZACION REAL(12,4), MONTO_VALORIZADO REAL, ID_ENTIDAD INTEGER,  ID_CUENTA_BANCARIA TEXT, "
            + "FECHA_EMISION TEXT, FECHA_VENCIMIENTO TEXT, ID_MONEDA INTEGER, "
            + "FOREIGN KEY (ID_MONEDA) REFERENCES CBV_MONEDAS (ID_MONEDA) "
            + "FOREIGN KEY (ID_CUENTA_BANCARIA) REFERENCES CBV_CUENTAS (ID_CUENTA), "
            + "FOREIGN KEY (ID_RECIBO_CAB) REFERENCES CBV_RECIBOS_CAB (ID_RECIBO_CAB))";
        return this.bd.getDataBase().executeSql(CREATE_RECIBOS_CAB_TABLE, []).then(function (recibos_cab) {
            console.log('se creo tabla recibos-cab', recibos_cab);
            return _this.bd.getDataBase().executeSql(CREATE_RECIBOS_DET_TABLE, []).then(function (recibos_det) {
                console.log('se creo tabla recibos-det', recibos_det);
                return _this.bd.getDataBase().executeSql(CREATE_RECIBOS_VALORES_TABLE, []).then(function (recibos_val) {
                    console.log('se creo tabla recibos-det', recibos_val);
                    return true;
                }).catch(function (err) {
                    console.error('no se creo la tabla recibos-valores', err);
                    return false;
                });
            }).catch(function (error) {
                console.error('no se creo la tabla recibos-valores', error);
                return false;
            });
        }).catch(function (error) {
            console.error('no se creo la tabla recibos-cab', error);
            return false;
        });
    };
    RecibosBDService.prototype.insertarReciboTabla = function (recibo) {
        var _this = this;
        var cabecera = recibo.cabecera;
        var detalle = recibo.detalles;
        var valores = recibo.valores;
        var paramsCabecera = [cabecera.ID_COBRADOR, cabecera.FECHA, 0, cabecera.HABILITACION_CAJA,
            cabecera.ESTADO, cabecera.ID_MONEDA, cabecera.COTIZACION, cabecera.TOTAL_RECIBO, cabecera.INTERES,
            cabecera.ANTICIPO, cabecera.LATITUD, cabecera.LONGITUD, cabecera.ID_CLIENTE, cabecera.NRO_RECIBO,
            cabecera.IMEI, cabecera.VERSION, cabecera.USUARIO, cabecera.ID_CONCEPTO, cabecera.MARCA];
        var sqlCabecera = "INSERT INTO " + this.TABLE_RECIBOS_CAB + " (ID_COBRADOR, FECHA, "
            + "REQUIERE_SINCRONIZACION, HABILITACION_CAJA, ESTADO, ID_MONEDA, COTIZACION, TOTAL_RECIBO, "
            + "INTERES, ANTICIPO, LATITUD, LONGITUD, ID_CLIENTE, NRO_RECIBO, IMEI, VERSION, USUARIO, ID_CONCEPTO, "
            + "MARCA) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        var sqlDetalle = "INSERT INTO " + this.TABLE_RECIBOS_DETALLE + " (ID_RECIBO_CAB, ID_FACTURA, "
            + "REQUIERE_SINCRONIZACION, MONTO, NRO_CUOTA, COTIZACION, MONTO_VALORIZADO, NO_FACTURA, ID_MONEDA) "
            + "VALUES(?,?,?,?,?,?,?,?,?)";
        var sqlValores = "INSERT INTO " + this.TABLE_RECIBOS_VALORES + "(ID_RECIBO_CAB, ID_TIPO_VALOR, "
            + "REQUIERE_SINCRONIZACION, NUMERO_VALOR, MONTO,COTIZACION, MONTO_VALORIZADO, ID_MONEDA, ID_ENTIDAD, "
            + "ID_CUENTA_BANCARIA, FECHA_EMISION, FECHA_VENCIMIENTO) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        this.bd.getDataBase().executeSql(sqlCabecera, paramsCabecera).then(function (data) {
            var id_insert_value = data.insertId;
            console.log('se inserto registro en recibos-cab', data, paramsCabecera);
            var _loop_1 = function (det) {
                var paramsDet = [id_insert_value, det.ID_FACTURA, 0, det.MONTO, det.NRO_CUOTA, det.COTIZACION,
                    det.MONTO_VALORIZADO, det.NO_FACTURA, det.ID_MONEDA];
                _this.bd.getDataBase().executeSql(sqlDetalle, paramsDet).then(function (data) {
                    console.log('se inserto registro en recibos-detalle', data, paramsDet);
                }).catch(function (error) {
                    console.error('no se puede insertar recibos-detalle', error);
                });
            };
            for (var _i = 0, detalle_1 = detalle; _i < detalle_1.length; _i++) {
                var det = detalle_1[_i];
                _loop_1(det);
            }
            var _loop_2 = function (val) {
                var paramsVal = [id_insert_value, val.ID_TIPO_VALOR, 0, val.NUMERO_VALOR, val.MONTO, val.COTIZACION,
                    val.MONTO_VALORIZADO, val.ID_MONEDA, val.ID_ENTIDAD, val.ID_CUENTA_BANCARIA, val.FECHA_EMISION,
                    val.FECHA_VENCIMIENTO];
                _this.bd.getDataBase().executeSql(sqlValores, paramsVal).then(function (data) {
                    console.log('se inserto registro en recibos-valores', data, paramsVal);
                }).catch(function (error) {
                    console.log('no se puede insertar en recibos-valores', error);
                });
            };
            for (var _a = 0, valores_1 = valores; _a < valores_1.length; _a++) {
                var val = valores_1[_a];
                _loop_2(val);
            }
        }).catch(function (error) {
            console.error('no se puede insertar recibos-cab', error);
        });
    };
    RecibosBDService.prototype.existeRecibo = function (recibo) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var sql, params;
            return tslib_1.__generator(this, function (_a) {
                sql = "SELECT rc.NRO_RECIBO FROM " + this.TABLE_RECIBOS_CAB
                    + " rc WHERE rc.NRO_RECIBO=? AND MARCA=?";
                params = [recibo.cabecera.NRO_RECIBO, recibo.cabecera.MARCA];
                return [2 /*return*/, this.bd.getDataBase().executeSql(sql, params).then(function (data) {
                        if (data.rows.length > 0) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    })];
            });
        });
    };
    RecibosBDService.prototype.existeSyncRecibosHabilitacion = function (habilitacion) {
        var query = "SELECT * FROM " + this.TABLE_RECIBOS_CAB + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
            "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
        return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(function (data) {
            if (data.rows.length > 0) {
                return true;
            }
            else {
                return false;
            }
        });
    };
    RecibosBDService.prototype.getRecibosASincronizar = function (habilitacion) {
        var _this = this;
        var query = "SELECT * FROM " + this.TABLE_RECIBOS_CAB + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION +
            "=? AND " + this.KEY_HABILITACION_CAJA + "=?";
        return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(function (data) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var resp, facturas, valores, archivos, reciboResp, recibo, i, _a, _b, _c, _d;
            return tslib_1.__generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        console.log("Cantidad de recibos a sincronizar ", data.rows.length);
                        resp = [];
                        facturas = {};
                        valores = {};
                        archivos = {};
                        reciboResp = {};
                        i = 0;
                        _e.label = 1;
                    case 1:
                        if (!(i < data.rows.length)) return [3 /*break*/, 8];
                        recibo = data.rows.item(i);
                        reciboResp = recibo;
                        console.log("RECIBO ", recibo);
                        return [4 /*yield*/, this.getDetalles(recibo.ID_RECIBO_CAB)];
                    case 2:
                        facturas = _e.sent();
                        return [4 /*yield*/, this.getValores(recibo.ID_RECIBO_CAB)];
                    case 3:
                        valores = _e.sent();
                        return [4 /*yield*/, this.getArchivos(recibo.ULTIMO_TIME_IMPRESION_ORIGINAL, recibo.ULTIMO_TIME_IMPRESION_DUPLICADO, recibo.NRO_RECIBO)];
                    case 4:
                        archivos = _e.sent();
                        reciboResp["facturas"] = facturas;
                        reciboResp["valores"] = valores;
                        reciboResp["files"] = archivos;
                        _a = reciboResp;
                        _b = "totalFacturas";
                        return [4 /*yield*/, this.getTotalFacturas(recibo.ID_RECIBO_CAB)];
                    case 5:
                        _a[_b] = _e.sent();
                        _c = reciboResp;
                        _d = "totalValores";
                        return [4 /*yield*/, this.getTotalValores(recibo.ID_RECIBO_CAB)];
                    case 6:
                        _c[_d] = _e.sent();
                        if (reciboResp["totalFacturas"] == -1 || reciboResp["totalValores"] == -1 || reciboResp["facturas"].length == 0 || reciboResp["valores"].length == 0 || reciboResp["files"].length == 0) {
                            throw Error("Error al traer datos de base de datos para sincronizar recibos");
                        }
                        resp.push(reciboResp);
                        _e.label = 7;
                    case 7:
                        i++;
                        return [3 /*break*/, 1];
                    case 8:
                        console.log("RECIBOS A ENVIAR A API para sincronizar", resp);
                        return [2 /*return*/, resp];
                }
            });
        }); }).catch(function (error) {
            console.error("Error trayendo recibos a sincronizar", error);
            return [];
        });
    };
    RecibosBDService.prototype.cantidadRecibosSync = function (habilitacion) {
        var query = "SELECT COUNT(ID_RECIBO_CAB) AS CANTIDAD FROM " + this.TABLE_RECIBOS_CAB + " WHERE " + this.KEY_REQUIERE_SINCRONIZACION
            + "=? AND " + this.KEY_HABILITACION_CAJA + "= ? ";
        console.log("Trayendo cantidad de recibos", this.bd);
        return this.bd.getDataBase().executeSql(query, [1, habilitacion]).then(function (data) {
            console.log("CANT RECIBOS de habilitacion " + habilitacion, data.rows.item(0).CANTIDAD);
            return data.rows.item(0).CANTIDAD;
        }).catch(function (error) {
            console.error("Error trayendo cantidad de recibos", error);
            return -1;
        });
    };
    RecibosBDService.prototype.getDetalles = function (id_recibo_cab) {
        var query = "SELECT * FROM " + this.TABLE_RECIBOS_DETALLE + " WHERE ID_RECIBO_CAB=?";
        return this.bd.getDataBase().executeSql(query, [id_recibo_cab]).then(function (data) {
            var detalles = [];
            for (var i = 0; i < data.rows.length; i++) {
                detalles.push(data.rows.item(i));
            }
            return detalles;
        }).catch(function (error) {
            console.error("Error trayendo detalles de recibo", error);
            return [];
        });
    };
    RecibosBDService.prototype.getValores = function (id_recibo_cab) {
        var query = "SELECT * FROM " + this.TABLE_RECIBOS_VALORES + " WHERE ID_RECIBO_CAB=?";
        return this.bd.getDataBase().executeSql(query, [id_recibo_cab]).then(function (data) {
            var valores = [];
            for (var i = 0; i < data.rows.length; i++) {
                valores.push(data.rows.item(i));
            }
            return valores;
        }).catch(function (error) {
            console.error("Error trayendo valores de recibo", error);
            return [];
        });
    };
    RecibosBDService.prototype.getArchivos = function (original, duplicado, nro_recibo) {
        var _this = this;
        var ubicacion = this.fileService.externalApplicationStorageDirectory + "/recibos";
        //Si es al contado
        if (nro_recibo == 0 || original == null || duplicado == null) {
            console.log("NO SE BUSCA NINGUN ARCHIVO, ES DE FACTURA AL CONTADO O NO TIENE ARCHIVOS LOCALES");
            return [
                { "ULTIMO_TIME_IMPRESION_ORIGINAL": "" },
                { "ULTIMO_TIME_IMPRESION": "" },
            ];
        }
        else {
            original = original + ".txt";
            //Si son a credito
            console.log("ARCHIVOS A BUSCAR", original, " y ", duplicado);
            return this.fileService.readAsText(ubicacion, original)
                .then(function (original_content) {
                console.log("ORIGINAL TXT: ", original_content);
                if (duplicado != null) {
                    duplicado = duplicado + ".txt";
                    return _this.fileService.readAsText(ubicacion, duplicado)
                        .then(function (duplicado_content) {
                        console.log("DUPLICADO TXT: ", duplicado_content);
                        return [
                            { "ULTIMO_TIME_IMPRESION_ORIGINAL": original_content },
                            { "ULTIMO_TIME_IMPRESION": duplicado_content },
                        ];
                    });
                }
                else {
                    return [
                        { "ULTIMO_TIME_IMPRESION_ORIGINAL": original_content },
                        { "ULTIMO_TIME_IMPRESION": "" },
                    ];
                }
            }).catch(function (error) {
                console.log("ERROR TRAYENDO ARCHIVO", error);
                return [];
            });
        }
    };
    RecibosBDService.prototype.recibosGenerados = function (id_factura) {
        var estado = "PENDIENTE";
        var query = "SELECT SUM (rd.MONTO) AS MONTO FROM CBV_RECIBOS_DET rd "
            + "INNER JOIN CBV_RECIBOS_CAB rc ON rc.ID_RECIBO_CAB = rd.ID_RECIBO_CAB "
            + "WHERE rd.ID_FACTURA=? AND rc.ESTADO=?";
        return this.bd.getDataBase().executeSql(query, [id_factura, estado]).then(function (data) {
            if (data.rows.length > 0) {
                var totalPagado = (data.rows.item(0).MONTO);
                console.log('total pagado', totalPagado);
                return totalPagado;
            }
            else {
                return 0.0;
            }
        }).catch(function (error) {
            console.error("Error al traer recibos generados", error);
            return null;
        });
    };
    RecibosBDService.prototype.setRecibo = function (request) {
        var _this = this;
        //-------------------- COMIENZO DE INSERCION -----------------------------------
        if (!request.valid) {
            return Promise.reject({
                "error": request.message,
                "error_expanded": request
            });
        }
        //Nuevo recibo
        var data = {
            'id_cobrador': request.cobrador,
            'fecha': request.fecha,
            'estado': "PENDIENTE",
            'id_moneda': request.id_moneda,
            'cotizacion': request.cotizacion,
            'total_recibo': request.total_recibo,
            'interes': request.interes,
            'latitud': request.latitud,
            'longitud': request.longitud,
            'nro_recibo': request.nro_recibo,
            'habilitacion_caja': request.habilitacion,
            'id_cliente': request.id_cliente,
            'version': request.version,
            'marca': request.marca,
            'id_concepto': request.id_concepto,
            'imei': request.imei,
            'usuario': request.usuario,
            'requiere_sincronizacion': 1,
            'anticipo': request.anticipo
        };
        var self = this;
        return this.bd.insert(this.TABLE_RECIBOS_CAB, data).then(function (resp) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var detallesInserts, valoresInserts;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("Cabecera de recibo creado exitosamente con id ", resp.insertId, resp);
                        return [4 /*yield*/, self.insertarDetalles(resp.insertId, request.facturas, request.moneda, request.cotizacion, self)];
                    case 1:
                        detallesInserts = _a.sent();
                        return [4 /*yield*/, self.insertarValores(resp.insertId, request.valores, request.moneda, request.cotizacion, self)];
                    case 2:
                        valoresInserts = _a.sent();
                        console.log("DETALLES A INSERTAR: ", detallesInserts);
                        console.log("VALORES A INSERTAR: ", valoresInserts);
                        //Transaccion para añadir detalles y valores a de recibo
                        return [2 /*return*/, this.bd.getDataBase().transaction(function (tx) {
                                //Insercion de detalles de recibo
                                for (var _i = 0, detallesInserts_1 = detallesInserts; _i < detallesInserts_1.length; _i++) {
                                    var detalle = detallesInserts_1[_i];
                                    tx.executeSql(detalle.query, detalle.params, self.thenTransaction, self.catchTransaction);
                                }
                                //Insercion de valores de recibo
                                for (var _a = 0, valoresInserts_1 = valoresInserts; _a < valoresInserts_1.length; _a++) {
                                    var valor = valoresInserts_1[_a];
                                    tx.executeSql(valor.query, valor.params, self.thenTransaction, self.catchTransaction);
                                }
                            }).then(function () {
                                console.log("Exito en transaccion");
                                return Promise.resolve({
                                    "exito": "Recibo creado con exito",
                                    "ID_RECIBO": resp.insertId,
                                    "RECIBO_ORIGINAL": "",
                                    "RECIBO_DUPLICADO": ""
                                });
                            }).catch(function (error) {
                                console.error("Error en transaccion al insertar detalles y valores", error);
                                self.delete(resp.insertId);
                                return Promise.reject({
                                    "error": "Error creando recibo",
                                    "error_expanded": error
                                });
                            })];
                }
            });
        }); }).catch(function (error) {
            console.error("Error en insercion de recibo con datos", data);
            return Promise.reject({
                "error": "Error creando recibo",
                "error_expanded": error
            });
        });
    };
    RecibosBDService.prototype.insertarDetalles = function (id_recibo, facturas, moneda, cotizacion, self) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var detallesToInsert, _i, facturas_1, factura, _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        detallesToInsert = [];
                        _i = 0, facturas_1 = facturas;
                        _c.label = 1;
                    case 1:
                        if (!(_i < facturas_1.length)) return [3 /*break*/, 4];
                        factura = facturas_1[_i];
                        _b = (_a = detallesToInsert).concat;
                        return [4 /*yield*/, self.insertarDetalle(id_recibo, factura, moneda, cotizacion, self)];
                    case 2:
                        detallesToInsert = _b.apply(_a, [_c.sent()]);
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, detallesToInsert];
                }
            });
        });
    };
    RecibosBDService.prototype.insertarDetalle = function (id_recibo, factura, moneda, cotizacion, self) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var detalles, id_factura, montofactura, monedaFactura, saldo, montoVal, cuota, montoPagado, montoPagadoVal, detalle, datos, toInsert, facturaBD, facturas, _i, facturas_2, f;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        detalles = [];
                        id_factura = factura['id_factura'];
                        montofactura = factura['monto'];
                        monedaFactura = factura['id_moneda'];
                        saldo = factura['saldo'];
                        montoVal = montofactura;
                        cuota = factura['cuota'];
                        montoPagado = 0;
                        montoPagadoVal = 0;
                        //Obtener montoVal
                        if (monedaFactura != moneda.ID_MONEDA) {
                            montoVal = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, montofactura, moneda.CANT_DECIMALES);
                        }
                        detalle = {};
                        datos = {};
                        toInsert = { query: '', params: [] };
                        if (!(montofactura <= saldo)) return [3 /*break*/, 2];
                        return [4 /*yield*/, self.findFactura(id_factura)];
                    case 1:
                        facturaBD = _a.sent();
                        //Se inserta solo un detalle
                        detalle = {
                            'id_recibo_cab': id_recibo,
                            'id_factura': facturaBD.ID_FACTURA,
                            'monto': montofactura,
                            'nro_cuota': cuota,
                            'monto_valorizado': montoVal,
                            'cotizacion': cotizacion,
                            'id_moneda': monedaFactura,
                            'no_factura': facturaBD.NO_FACTURA,
                            'requiere_sincronizacion': 1,
                        };
                        toInsert = self.bd.generateQueryToInsert(self.TABLE_RECIBOS_DETALLE, detalle);
                        console.log("TO INSERT DETALLE ", toInsert);
                        detalles.push(toInsert);
                        return [3 /*break*/, 4];
                    case 2:
                        //Se insertan mas de un detalle de recibo debido ya que el monto pagado es mayor que el saldo de la factura
                        montoPagado = montofactura;
                        montoPagadoVal = montofactura;
                        return [4 /*yield*/, self.getFacturasFrom(id_factura, { cuota: 'ASC' })];
                    case 3:
                        facturas = _a.sent();
                        for (_i = 0, facturas_2 = facturas; _i < facturas_2.length; _i++) {
                            f = facturas_2[_i];
                            datos = {
                                'id_recibo_cab': id_recibo,
                                'id_factura': f.id_factura,
                                'nro_cuota': f.cuota,
                                'id_moneda': f.id_moneda,
                                'no_factura': f.no_factura,
                                'requiere_sincronizacion': 1
                            };
                            if (montoPagado > f.saldo) {
                                //Si el monto pagado aun es mayor al saldo
                                montoPagado = montoPagado - f.saldo;
                                if (f.id_moneda != moneda.getId()) {
                                    montoPagadoVal = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, f.saldo, moneda.CANT_DECIMALES);
                                }
                                datos['monto'] = f.saldo;
                                datos['monto_valorizado'] = montoPagadoVal;
                            }
                            else {
                                //Si el montopagado es igual o menor al saldo
                                if (f.id_moneda != moneda.getId()) {
                                    montoPagadoVal = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, montoPagado, moneda.CANT_DECIMALES);
                                }
                                datos['monto'] = montoPagado;
                                datos['monto_valorizado'] = montoPagadoVal;
                                montoPagado = 0;
                            }
                            //Se inserta el detalle
                            toInsert = self.bd.generateQueryToInsert(self.TABLE_RECIBOS_DETALLE, datos);
                            console.log("TO INSERT DETALLE ", toInsert);
                            detalles.push(toInsert);
                            //transaction.executeSql(toInsert.query, toInsert.params);
                            //Su el montoPagado ya llego a cero, se sale del bucle
                            if (montoPagado == 0) {
                                break;
                            }
                        }
                        _a.label = 4;
                    case 4: return [2 /*return*/, detalles];
                }
            });
        });
    };
    RecibosBDService.prototype.insertarValores = function (id_recibo, valores, moneda, cotizacion, self) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var valoresToInsert, _i, valores_2, valor, _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        valoresToInsert = [];
                        _i = 0, valores_2 = valores;
                        _c.label = 1;
                    case 1:
                        if (!(_i < valores_2.length)) return [3 /*break*/, 4];
                        valor = valores_2[_i];
                        _b = (_a = valoresToInsert).push;
                        return [4 /*yield*/, self.insertarValor(id_recibo, valor, moneda, cotizacion, self)];
                    case 2:
                        _b.apply(_a, [_c.sent()]);
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, valoresToInsert];
                }
            });
        });
    };
    RecibosBDService.prototype.insertarValor = function (id_recibo, valor, moneda, cotizacion, self) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var numero, entidad, cuenta_bancaria, monto, id_moneda, id_tipo_valor, fecha_emision, fecha_vencimiento, montoValorizado, datos;
            return tslib_1.__generator(this, function (_a) {
                console.log("VALOR PA GUARDAR", valor);
                numero = valor.hasOwnProperty('numero_valor') ? valor['numero_valor'] : null;
                entidad = valor.hasOwnProperty('id_entidad') ? valor['id_entidad'] : null;
                cuenta_bancaria = valor.hasOwnProperty('id_cuenta_bancaria') ? valor['id_cuenta_bancaria'] : null;
                monto = valor['monto'];
                id_moneda = valor['id_moneda'];
                id_tipo_valor = valor['id_tipo_valor'];
                if (valor['fecha_emision'] != "" && valor['fecha_emision'] != null) {
                    fecha_emision = moment(valor['fecha_emision'], "YYYY-MM-DD").format("DD-MM-YYYY");
                    //30 dias mas a partir de la fecha de emision, por eso se le suma 29 y no 30
                    fecha_vencimiento = moment(fecha_emision, "DD-MM-YYYY").add(29, 'days').format("DD-MM-YYYY");
                }
                else {
                    fecha_emision = null;
                    fecha_vencimiento = null;
                }
                if (monto != null && id_tipo_valor != null && id_moneda != null) {
                    if (id_moneda == moneda.ID_MONEDA) {
                        montoValorizado = monto;
                    }
                    else {
                        montoValorizado = self.monedaService.generateMontoValorizado(moneda.ID_MONEDA, cotizacion, monto, moneda.CANT_DECIMALES);
                    }
                }
                console.log("EMISION " + fecha_emision, "VENCIMIENTO " + fecha_vencimiento);
                datos = {
                    "numero_valor": numero,
                    "monto": monto,
                    "id_tipo_valor": id_tipo_valor,
                    "id_recibo_cab": id_recibo,
                    "id_moneda": id_moneda,
                    "id_entidad": entidad,
                    "fecha_emision": fecha_emision,
                    "fecha_vencimiento": fecha_vencimiento,
                    "id_cuenta_bancaria": cuenta_bancaria,
                    "monto_valorizado": montoValorizado,
                    "cotizacion": cotizacion
                };
                //Insertar valor (elementos para hacerlo)
                return [2 /*return*/, self.bd.generateQueryToInsert(self.TABLE_RECIBOS_VALORES, datos)];
            });
        });
    };
    RecibosBDService.prototype.existeReciboConMarca = function (id_cobrador, marca) {
        var query = "SELECT * FROM " + this.TABLE_RECIBOS_CAB + " WHERE ID_COBRADOR=? AND MARCA=? LIMIT 1";
        return this.bd.getDataBase().executeSql(query, [id_cobrador, marca]).then(function (data) {
            console.log('Cantidad de recibos con la misma marca del cobrador: ', data.rows.length);
            if (data.rows.length > 0) {
                return true;
            }
            return false;
        }).catch(function (error) {
            console.error('No se pudo traer la cantidad de recibos con la misma marca', error);
            return false;
        });
    };
    RecibosBDService.prototype.getUltimoReciboCobrador = function (id_cobrador) {
        var query = "SELECT MAX(NRO_RECIBO) AS ULTIMO_RECIBO FROM " + this.TABLE_RECIBOS_CAB + " WHERE ID_COBRADOR=?";
        var params = [id_cobrador];
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            console.log('Ultimo recibo de la base de datos local', data.rows.item(0).ULTIMO_RECIBO);
            return data.rows.item(0).ULTIMO_RECIBO;
        }).catch(function (error) {
            console.log('No se pudo obtener el ultimo recibo de la base de datos local', error);
            return -1;
        });
    };
    RecibosBDService.prototype.findFactura = function (id) {
        return this.bd.find("CBV_FACTURAS", id, "ID_FACTURA").then(function (data) {
            console.log("Factura traida :", data);
            return data;
        }).catch(function (error) {
            console.log("Error trayendo factura", error);
            return {};
        });
    };
    RecibosBDService.prototype.getFacturasFrom = function (id_factura, orderBy) {
        if (orderBy === void 0) { orderBy = null; }
        var query = "SELECT * FROM CBV_FACTURAS WHERE ID_FACTURA=?";
        if (orderBy != null) {
            query = query + " ORDER BY ";
            Object.keys(orderBy).forEach(function (key) {
                query = query + key + orderBy[key];
            });
        }
        return this.bd.getDataBase().executeSql(query, [id_factura]).then(function (data) {
            console.log("TRAER FACTURAS CON ID_FACTURA = " + id_factura + ", cantidad: " + data.rows.length);
            var facturas = [];
            for (var index = 0; index < data.rows.length; index++) {
                console.log('FACTURA DE BD: ', data.rows.item(index));
                var fac = new Factura(data.rows.item(index), data.rows.item(index).ID_CLIENTE);
                facturas.push(fac);
            }
            return facturas;
        }).catch(function (error) {
            console.error("Error trayendo FACTURAS " + id_factura, error);
            return [];
        });
    };
    RecibosBDService.prototype.thenTransaction = function (tx, result) {
        console.log("RESULTADO DE QUERY EN TRANSACCION: ", result);
    };
    RecibosBDService.prototype.catchTransaction = function (tx, err) {
        console.error("ERROR EN TRANSACCION EN QUERY: ", err);
        return true; //THIS IS IMPORTANT FOR TRANSACTION TO ROLLBACK ON QUERY ERROR
    };
    RecibosBDService.prototype.obtenerReciboCap = function (id_recibo) {
        var query_recibos = "SELECT ID_CONCEPTO, ID_COBRADOR, FECHA, ID_MONEDA, COTIZACION, TOTAL_RECIBO, "
            + "INTERES, ANTICIPO, NRO_RECIBO, HABILITACION_CAJA, ID_CLIENTE "
            + "FROM CBV_RECIBOS_CAB WHERE ID_RECIBO_CAB=?";
        var params_recibos = [id_recibo];
        return this.bd.getDataBase().executeSql(query_recibos, params_recibos).then(function (data) {
            console.log('recibo en get obtenerReciboCap', data);
            if (data.rows.length > 0) {
                return data.rows.item(0);
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.log('No se pudo obtener el ultimo recibo de la base de datos local', error);
            return -1;
        });
    };
    RecibosBDService.prototype.getValoresRecibo = function (id_recibo, id_moneda_recibo, cotizacion) {
        var valores = [];
        var params_valores = [cotizacion, id_moneda_recibo, cotizacion, id_recibo];
        var query_valores = "SELECT CASE WHEN V.ID_VALOR=12 THEN 1 ELSE 2 END PRIORIDAD, "
            + "RV.MONTO*(CASE WHEN RV.ID_MONEDA=1 THEN 1 ELSE ? END)/(CASE WHEN ?=1 THEN 1 ELSE ? END) "
            + "as MONTO_VALORIZADO, RV.MONTO, V.DESCRIPCION, RV.NUMERO_VALOR, RV.ID_RECIBO_CAB, V.ID_VALOR, "
            + "V.IND_BANCO, V.IND_CUENTA, V.IND_FECHA, V.IND_NUMERO, RV.ID_MONEDA, RV.ID_ENTIDAD, "
            + "RV.ID_CUENTA_BANCARIA "
            + "FROM CBV_VALORES V JOIN CBV_RECIBOS_VALORES RV ON RV.ID_TIPO_VALOR=V.ID_VALOR "
            + "WHERE RV.ID_RECIBO_CAB=? ORDER BY 1, 2 DESC";
        return this.bd.getDataBase().executeSql(query_valores, params_valores).then(function (data) {
            //console.log('valores recibos', data.rows.length, data);
            if (data.rows.length > 0) {
                for (var index = 0; index < data.rows.length; index++) {
                    valores.push(data.rows.item(index));
                }
                console.log('valores recibos', valores);
                return valores;
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.error('No se pudo obtener los valores de la base de datos local', error);
            return null;
        });
    };
    RecibosBDService.prototype.getDetallesRecibo = function (id_recibo) {
        var detalles = [];
        var query_detalles = "SELECT ID_FACTURA, MONTO, ID_MONEDA, NRO_CUOTA FROM CBV_RECIBOS_DET "
            + "WHERE ID_RECIBO_CAB=?";
        var params_detalle = [id_recibo];
        return this.bd.getDataBase().executeSql(query_detalles, params_detalle).then(function (data) {
            console.log('detalles recibos', data.rows.length, data);
            if (data.rows.length > 0) {
                for (var index = 0; index < data.rows.length; index++) {
                    detalles.push(data.rows.item(index));
                }
                console.log('detalles recibo', detalles);
                return detalles;
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.error('No se pudo obtener los detalles de la base de datos local', error);
            return null;
        });
    };
    RecibosBDService.prototype.delete = function (id) {
        return this.bd.delete(this.TABLE_RECIBOS_CAB, id, "ID_RECIBO_CAB");
    };
    RecibosBDService.prototype.obtenerRecibosFactura = function (id_factura) {
        var query = "SELECT rc.NRO_RECIBO,rc.FECHA as FEC_RECIBO,rc.ID_MONEDA,rc.TOTAL_RECIBO as TOT_RECIBO, rc.ESTADO, rc.ID_RECIBO_CAB, rc.REQUIERE_SINCRONIZACION"
            + " FROM " + this.TABLE_RECIBOS_CAB + " rc " + " JOIN " + this.TABLE_RECIBOS_DETALLE + " rd on rd.ID_RECIBO_CAB=rc.ID_RECIBO_CAB"
            + " WHERE rd.ID_FACTURA=?";
        var params = [id_factura];
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            console.log('Recibos de factura ' + id_factura, data.rows.length);
            var recibos = [];
            for (var index = 0; index < data.rows.length; index++) {
                recibos.push(data.rows.item(index));
            }
            return recibos;
        }).catch(function (error) {
            console.log('No se pudo traer los recibos de la factura', error);
            return [];
        });
    };
    RecibosBDService.prototype.anular = function (id_recibo_cab) {
        var query = "UPDATE CBV_RECIBOS_CAB SET ESTADO=?, REQUIERE_SINCRONIZACION=?, FECHA_ANULACION=?" +
            " WHERE ID_RECIBO_CAB=?";
        var params = ['ANULADO', 1, moment().format('DD-MM-YYYY'), id_recibo_cab];
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            return true;
        }).catch(function (error) {
            console.log("Error eliminando recibo", error);
            return false;
        });
    };
    RecibosBDService.prototype.getTotalValores = function (id_recibo_cab) {
        var query = "SELECT SUM(MONTO_VALORIZADO) AS TOTAL_VALORES FROM " + this.TABLE_RECIBOS_VALORES + " WHERE ID_RECIBO_CAB=?";
        var params = [id_recibo_cab];
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            return data.rows.item(0).TOTAL_VALORES;
        }).catch(function (error) {
            console.log("Error trayendo total de valores", error);
            return -1;
        });
    };
    RecibosBDService.prototype.getTotalFacturas = function (id_recibo_cab) {
        var query = "SELECT SUM(MONTO_VALORIZADO) AS TOTAL_FACTURAS FROM " + this.TABLE_RECIBOS_DETALLE + " WHERE ID_RECIBO_CAB=?";
        var params = [id_recibo_cab];
        return this.bd.getDataBase().executeSql(query, params).then(function (data) {
            return data.rows.item(0).TOTAL_FACTURAS;
        }).catch(function (error) {
            console.log("Error trayendo total de facturas", error);
            return -1;
        });
    };
    RecibosBDService.prototype.guardarReciboDuplicado = function (id_recibo, nombre_archivo) {
        var strSQL = "UPDATE " + this.TABLE_RECIBOS_CAB + " SET ULTIMO_TIME_IMPRESION_DUPLICADO=?, "
            + "REQUIERE_SINCRONIZACION=? WHERE ID_RECIBO_CAB=? ";
        var params = [nombre_archivo, 1, id_recibo];
        this.bd.getDataBase().executeSql(strSQL, params).then(function (data) {
            console.log("se actualizo el archivo duplicado en la bd", data);
        }).catch(function (error) {
            console.log('no se pudo actualizar el archivo duplicado en la bd', error);
        });
    };
    RecibosBDService.prototype.guardarReciboOriginal = function (id_recibo, nombre_archivo) {
        var strSQL = "UPDATE " + this.TABLE_RECIBOS_CAB + " SET ULTIMO_TIME_IMPRESION_ORIGINAL=?, "
            + "REQUIERE_SINCRONIZACION=? WHERE ID_RECIBO_CAB=?";
        var params = [nombre_archivo, 1, id_recibo];
        this.bd.getDataBase().executeSql(strSQL, params).then(function (data) {
            console.log("se actualizo el archivo original en la bd", data);
        }).catch(function (error) {
            console.log('no se pudo actualizar el archivo original en la bd', error);
        });
    };
    RecibosBDService.prototype.actualizarSincronizacionDeRecibos = function () {
        var query = "UPDATE " + this.TABLE_RECIBOS_CAB + " SET " + this.KEY_REQUIERE_SINCRONIZACION + "=? WHERE " + this.KEY_REQUIERE_SINCRONIZACION + "=?";
        var params = [0, 1];
        this.bd.getDataBase().executeSql(query, params).then(function (data) {
            console.log("Columna de recibos que requieren sincronizacion cambiados a 0", data);
        }).catch(function (error) {
            console.log('No se pudo actualizar la columna de requiere sincronizacion a 0 de los recibos', error);
        });
    };
    RecibosBDService.prototype.deleteTableRecibos = function () {
        var _this = this;
        return this.bd.getDataBase().executeSql("DROP TABLE IF EXISTS " + this.TABLE_RECIBOS_CAB, []).then(function (data) {
            console.log('se elimino la tabla recibos-cabecera', data);
            return _this.bd.getDataBase().executeSql("DROP TABLE IF EXISTS " + _this.TABLE_RECIBOS_DETALLE, []).then(function (data) {
                console.log('se elimino la tabla recibos-detalle', data);
                return _this.bd.getDataBase().executeSql("DROP TABLE IF EXISTS " + _this.TABLE_RECIBOS_VALORES, []).then(function (data) {
                    console.log('se elimino la tabla recibos-valores', data);
                    return true;
                }).catch(function (error) {
                    console.log('no se elimino la tabla recibos-valores', error);
                    return false;
                });
            }).catch(function (error) {
                console.log('no se elimino la tabla recibos-detalle', error);
                return false;
            });
        }).catch(function (error) {
            console.log('no se elimino la tabla recibos-cabecera', error);
            return false;
        });
    };
    RecibosBDService.prototype.borrarReciboEnCascada = function (id_recibo_cab) {
        var _this = this;
        if (id_recibo_cab == 0) {
            return {
                "exito": "No existe recibo con id 0"
            };
        }
        console.log("ELIMINAR RECIBO CON ID " + id_recibo_cab);
        var self = this;
        return this.bd.getDataBase().transaction(function (tx) {
            var params = [id_recibo_cab];
            //Eliminación de cabecera
            var query = "DELETE FROM " + _this.TABLE_RECIBOS_CAB + " WHERE " + _this.KEY_ID_RECIBO_CAB + "=?";
            tx.executeSql(query, params, self.thenTransaction, self.catchTransaction);
            //Eliminación de detalles
            query = "DELETE FROM " + _this.TABLE_RECIBOS_DETALLE + " WHERE " + _this.KEY_ID_RECIBO_CAB + "=?";
            tx.executeSql(query, params, self.thenTransaction, self.catchTransaction);
            //Eliminación de valores
            query = "DELETE FROM " + _this.TABLE_RECIBOS_VALORES + " WHERE " + _this.KEY_ID_RECIBO_CAB + "=?";
            tx.executeSql(query, params, self.thenTransaction, self.catchTransaction);
        }).then(function () {
            console.log("Exito en transaccion de recibo");
            return {
                "exito": "Recibo eliminado exitosamente",
                "ID_RECIBO": id_recibo_cab
            };
        }).catch(function (error) {
            console.error("Error en transaccion al eliminar detalles y valores", error);
            throw ({
                "error": "Error eliminado recibo",
                "error_expanded": error
            });
        });
    };
    RecibosBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService,
            File,
            Platform,
            MonedaLogicaService])
    ], RecibosBDService);
    return RecibosBDService;
}());
export { RecibosBDService };
//# sourceMappingURL=recibos-bd.service.js.map