import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { ValueType } from 'src/app/Clases/value-type';
var ValoresBDService = /** @class */ (function () {
    function ValoresBDService(db) {
        this.db = db;
        this.TABLE_VALORES = "CBV_VALORES";
        this.KEY_ID_VALOR = "ID_VALOR";
        this.KEY_DESCRIPCION = "DESCRIPCION";
        this.KEY_ABREVIACION = "ABREVIACION";
        this.KEY_IND_NUMERO = "IND_NUMERO";
        this.KEY_IND_CUENTA = "IND_CUENTA";
        this.KEY_IND_BANCO = "IND_BANCO";
        this.KEY_IND_FECHA = "IND_FECHA";
        this.KEY_ID_MONEDA = "ID_MONEDA";
    }
    ValoresBDService.prototype.createTableValores = function () {
        var CREATE_VALORES_TABLE = " CREATE TABLE IF NOT EXISTS " + this.TABLE_VALORES +
            " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_VALOR INTEGER, DESCRIPCION TEXT, ABREVIACION TEXT, "
            + "IND_NUMERO INTEGER, IND_CUENTA INTEGER, IND_BANCO INTEGER, IND_FECHA INTEGER, ID_MONEDA TEXT)";
        return this.db.getDataBase().executeSql(CREATE_VALORES_TABLE, []).then(function (valores) {
            console.log('se creo la table valores', valores);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla valores', error);
            return false;
        });
    };
    ValoresBDService.prototype.addValor = function (valor) {
        var addLine = "INSERT INTO " + this.TABLE_VALORES + " (ID_VALOR, DESCRIPCION, ABREVIACION, " +
            "IND_NUMERO, IND_CUENTA, IND_BANCO, IND_FECHA, ID_MONEDA) " +
            "VALUES (?,?,?,?,?,?,?,?)";
        return this.db.getDataBase().executeSql(addLine, [valor.id, valor.descripcion, valor.abreviacion,
            valor.has_number, valor.has_cuenta, valor.has_banco, valor.has_fecha, valor.id_moneda])
            .then(function (exito) {
            console.log('se agrego el valor', exito);
            return true;
        }).catch(function (error) {
            console.log('no se puede agregar el valor', error);
            return false;
        });
    };
    ValoresBDService.prototype.getValores = function () {
        var query = "SELECT * FROM " + this.TABLE_VALORES;
        var valores = [];
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            //console.log("DATOS DE VALORES TRAIDA ", data.rows.length, data.rows.item(0), data)
            if (data.rows.length > 0) {
                for (var index = 0; index < data.rows.length; index++) {
                    var val = new ValueType(data.rows.item(index));
                    valores.push(val);
                }
            }
            return valores;
        }).catch(function (err) {
            console.log('no pudo obtenerse las valores localmente', err);
            return err;
        });
    };
    ValoresBDService.prototype.deleteAllValores = function () {
        var query = "DELETE FROM " + this.TABLE_VALORES;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla valores', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla valores', error);
            return false;
        });
    };
    ValoresBDService.prototype.deleteTableCValores = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_VALORES;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla valores', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla valores', error);
            return false;
        });
    };
    ValoresBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], ValoresBDService);
    return ValoresBDService;
}());
export { ValoresBDService };
//# sourceMappingURL=valores-bd.service.js.map