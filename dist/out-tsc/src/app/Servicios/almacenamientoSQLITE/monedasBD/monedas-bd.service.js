import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BDManagementService } from '../bdManagement/bdmanagement.service';
import { Moneda } from 'src/app/Clases/moneda';
var MonedasBDService = /** @class */ (function () {
    function MonedasBDService(db) {
        this.db = db;
        this.TABLE_MONEDAS = "CBV_MONEDAS";
        this.KEY_ID_MONEDA = "ID_MONEDA";
        this.KEY_DESCRIPCION_MONEDA = "DESCRIPCION_MONEDA";
        this.KEY_ACTIVO = "ACTIVO";
        this.KEY_CANT_DECIMALES = "CANT_DECIMALES";
        this.KEY_SIMBOLO = "SIMBOLO";
        this.KEY_MONEDA_PLURAL = "MONEDA_PLURAL";
        this.KEY_MONEDA_SINGULAR = "MONEDA_SINGULAR";
    }
    MonedasBDService.prototype.createTableMonedas = function () {
        var CREATE_MONEDAS_TABLE = "CREATE TABLE IF NOT EXISTS " + this.TABLE_MONEDAS +
            " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_MONEDA INT, DESCRIPCION_MONEDA TEXT, ACTIVO INTEGER, " +
            "CANT_DECIMALES INT, SIMBOLO TEXT, MONEDA_PLURAL TEXT, MONEDA_SINGULAR TEXT )";
        return this.db.getDataBase().executeSql(CREATE_MONEDAS_TABLE, []).then(function (Moneda) {
            console.log('se creo la table Moneda', Moneda);
            return true;
        }).catch(function (error) {
            console.error('no se creo la tabla Moneda', error);
            return false;
        });
    };
    MonedasBDService.prototype.addMoneda = function (moneda) {
        var addLine = "INSERT INTO " + this.TABLE_MONEDAS + " (ID_MONEDA, DESCRIPCION_MONEDA, ACTIVO, " +
            "CANT_DECIMALES, SIMBOLO, MONEDA_PLURAL, MONEDA_SINGULAR) " +
            "VALUES (?,?,?,?,?,?,?)";
        var params = [moneda.id, moneda.descripcion, moneda.activo, moneda.cant_decimales,
            moneda.simbolo, moneda.plural, moneda.singular];
        return this.db.getDataBase().executeSql(addLine, params).then(function (exito) {
            console.log('se agrego la Moneda', exito);
            return true;
        }).catch(function (error) {
            console.log('no se puede agregar la Moneda', error);
            return false;
        });
    };
    MonedasBDService.prototype.getMonedas = function () {
        var query = "SELECT * FROM " + this.TABLE_MONEDAS;
        var monedas = [];
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            //console.log("DATOS DE MONEDA TRAIDA ", data.rows.length, data.rows.item(0), data)
            if (data.rows.length > 0) {
                for (var index = 0; index < data.rows.length; index++) {
                    monedas.push(new Moneda(data.rows.item(index)));
                }
            }
            return monedas;
        }).catch(function (err) {
            console.log('no pudo obtenerse las monedas localmente', err);
            return err;
        });
    };
    MonedasBDService.prototype.deleteAllMonedas = function () {
        var query = "DELETE FROM " + this.TABLE_MONEDAS;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se eliminaron correctamente los datos de la tabla monedas', data);
            return true;
        }).catch(function (error) {
            console.log('no se eliminaron los datos de la tabla monedas', error);
            return false;
        });
    };
    MonedasBDService.prototype.find = function (id) {
        return this.db.find(this.TABLE_MONEDAS, id, "ID_MONEDA").then(function (data) {
            return data;
        }).catch(function (error) {
            console.error("ERROR TRAYENDO MONEDA ", error);
            return {};
        });
    };
    MonedasBDService.prototype.generateMontoValorizado = function (moneda, cotizacion, monto, cant_decimales) {
        var montoVal = 0;
        if (moneda == 1) {
            montoVal = parseFloat((monto * cotizacion).toFixed(cant_decimales));
        }
        else {
            montoVal = parseFloat((monto / cotizacion).toFixed(cant_decimales));
        }
        return montoVal;
    };
    MonedasBDService.prototype.getMoneda = function (id_moneda) {
        var query = "SELECT * FROM CBV_MONEDAS WHERE ID_MONEDA=?";
        return this.db.getDataBase().executeSql(query, [id_moneda]).then(function (data) {
            if (data.rows.length > 0) {
                return new Moneda(data.rows.item(0));
            }
            else {
                return null;
            }
        }).catch(function (error) {
            console.log("error al traer la moneda por el id ", error);
            return null;
        });
    };
    MonedasBDService.prototype.deleteTableMoneda = function () {
        var query = "DROP TABLE IF EXISTS " + this.TABLE_MONEDAS;
        return this.db.getDataBase().executeSql(query, []).then(function (data) {
            console.log('se elimino la tabla moneda', data);
            return true;
        }).catch(function (error) {
            console.log('no se elimino la tabla moneda', error);
            return false;
        });
    };
    MonedasBDService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [BDManagementService])
    ], MonedasBDService);
    return MonedasBDService;
}());
export { MonedasBDService };
//# sourceMappingURL=monedas-bd.service.js.map