import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
var BDManagementService = /** @class */ (function () {
    function BDManagementService(platform, sqlitedb) {
        var _this = this;
        this.platform = platform;
        this.sqlitedb = sqlitedb;
        this.platform.ready().then(function () {
            _this.sqlitedb.create({
                name: 'cobranzasMovil.db',
                location: "default",
                createFromLocation: 1
            }).then(function (db) {
                console.log("bd traida");
                _this.bd = db;
            }).catch(function (error) {
                console.log('error al crear o traer la base de datos ', error);
            });
        });
    }
    BDManagementService.prototype.getDataBase = function () {
        return this.bd;
    };
    BDManagementService.prototype.generateQueryToInsert = function (table, values) {
        var query = "INSERT INTO " + table + " (";
        var valuesToInsert = "";
        var parameters = [];
        Object.keys(values).forEach(function (key) {
            query = query + key + ",";
            parameters.push(values[key]);
            valuesToInsert = valuesToInsert + "?,";
        });
        query = this.erase_last_element(query) + ")";
        valuesToInsert = " VALUES(" + this.erase_last_element(valuesToInsert) + ")";
        query = query + valuesToInsert;
        console.log(query, parameters);
        return {
            query: query,
            params: parameters
        };
    };
    BDManagementService.prototype.generateQueryToUpdate = function (table, values, conditions) {
        if (conditions === void 0) { conditions = null; }
        var query = "UPDATE " + table + " SET ";
        var parameters = [];
        //Sets
        Object.keys(values).forEach(function (key) {
            query = query + key + "=?,";
            parameters.push(values[key]);
        });
        query = this.erase_last_element(query);
        //Wheres
        if (conditions != null) {
            if (conditions.length > 0) {
                query = query + " WHERE ";
                var n = conditions.length;
                for (var i = 0; i < n; i++) {
                    query = query + conditions[i][0] + " " + conditions[i][1] + " ? ";
                    if (i < n - 1) {
                        query = query + " AND ";
                    }
                    parameters.push(conditions[i][2]);
                }
            }
        }
        console.log(query, parameters);
        return {
            query: query,
            params: parameters
        };
    };
    BDManagementService.prototype.update = function (table, values, conditions) {
        if (conditions === void 0) { conditions = null; }
        var builder = this.generateQueryToUpdate(table, values, conditions);
        return this.getDataBase().executeSql(builder.query, builder.params).then(function (data) {
            console.log("Update ejecutado exitosamente en " + table, data);
            return data;
        }).catch(function (error) {
            console.error("Error en update en " + table, error);
            throw (error);
        });
    };
    BDManagementService.prototype.insert = function (table, values) {
        var builder = this.generateQueryToInsert(table, values);
        return this.getDataBase().executeSql(builder.query, builder.params).then(function (data) {
            console.log("Insert ejecutado exitosamente", data);
            return data;
        }).catch(function (error) {
            console.error("Error en inserción en " + table);
            throw (error);
        });
    };
    BDManagementService.prototype.erase_last_element = function (x) {
        var pos = x.length - 1;
        if (pos > 0) {
            return x.substring(0, pos);
        }
        return "";
    };
    BDManagementService.prototype.find = function (table, id, id_key) {
        if (id_key === void 0) { id_key = "ID"; }
        var query = "SELECT * FROM " + table + " WHERE " + id_key + "=? LIMIT 1";
        return this.getDataBase().executeSql(query, [id]).then(function (data) {
            console.log('Dato encontrado de ', table, data, data.rows.item(0));
            return data.rows.item(0);
        }).catch(function (error) {
            console.log('Dato no pude ser encontrado de ' + table, error);
            return null;
        });
    };
    BDManagementService.prototype.delete = function (table, id, id_key) {
        if (id_key === void 0) { id_key = "ID"; }
        var query = "DELETE FROM " + table + " WHERE " + id_key + "=?";
        var params = [id];
        return this.getDataBase().executeSql(query, [id]).then(function (data) {
            console.log('Dato eliminado de' + table + ' con ' + id_key + " = " + id);
            return true;
        }).catch(function (error) {
            console.log('Dato no pude ser eliminado de ' + table, error);
            return false;
        });
    };
    BDManagementService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SQLite])
    ], BDManagementService);
    return BDManagementService;
}());
export { BDManagementService };
//# sourceMappingURL=bdmanagement.service.js.map