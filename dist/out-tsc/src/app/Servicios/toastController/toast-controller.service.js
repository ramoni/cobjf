import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Toast } from '@ionic-native/toast/ngx';
import { ToastController } from '@ionic/angular';
var ToastControllerService = /** @class */ (function () {
    function ToastControllerService(toast, toastControl) {
        this.toast = toast;
        this.toastControl = toastControl;
    }
    ToastControllerService.prototype.presentToastMedio = function (mensaje) {
        this.toast.show(mensaje, '6000', 'center').subscribe(function (toast) {
            console.log(toast);
        });
    };
    ToastControllerService.prototype.presentToast = function (mensaje) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastControl.create({
                            message: mensaje,
                            duration: 6000,
                            position: 'top',
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ToastControllerService.prototype.presentToastOptions = function (title, texto, buttons) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var t;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastControl.create({
                            header: title,
                            duration: 4000,
                            message: texto,
                            buttons: buttons,
                        })];
                    case 1:
                        t = _a.sent();
                        return [4 /*yield*/, t.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ToastControllerService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Toast,
            ToastController])
    ], ToastControllerService);
    return ToastControllerService;
}());
export { ToastControllerService };
//# sourceMappingURL=toast-controller.service.js.map