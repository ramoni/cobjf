import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { Documentos } from '../../Clases/documentos';
import { NewDocumentResponse } from '../../Clases/new-document-response';
import { MensajeResponse } from '../../Clases/mensaje-response';
import { DocumentData } from '../../Clases/document-data';
import { DocumentWithCheck } from '../../Clases/document-with-check';
import { flatMap, map } from 'rxjs/operators';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
var DocumentosLogicaService = /** @class */ (function () {
    function DocumentosLogicaService(http, geoLogic, documentosBD) {
        this.http = http;
        this.geoLogic = geoLogic;
        this.documentosBD = documentosBD;
    }
    DocumentosLogicaService.prototype.getList = function (cliente, offset, last_result) {
        return this.http.doGet('clientes/' + cliente.id + '/entregas?', { first_result: offset, last_result: last_result }).then(function (res) {
            var data = JSON.parse(res.data);
            var toRet = [];
            console.log("documentos ", data);
            for (var _i = 0, _a = data.lista; _i < _a.length; _i++) {
                var row = _a[_i];
                var toDocument = new Documentos(row);
                if (toDocument.guardado === 1) {
                    toRet.push(new DocumentWithCheck(toDocument, true));
                }
                else {
                    toRet.push(new DocumentWithCheck(toDocument, false));
                }
            }
            return toRet;
        });
    };
    DocumentosLogicaService.prototype.obtenerDocumentosLocal = function (cliente, offset, last_result) {
        return this.documentosBD.getEntregasLocal(Number(cliente.id), offset, last_result).then(function (data) {
            var toRet = [];
            toRet = data.map(function (f) {
                if (f.guardado === 1) {
                    return new DocumentWithCheck(f, true);
                }
                else {
                    return new DocumentWithCheck(f, false);
                }
            });
            console.log('toRet entregas local', toRet);
            return toRet;
        });
    };
    DocumentosLogicaService.prototype.obtenerEntregasDocOnline = function (clientes) {
        return this.http.doGet('offline/entregas?first_result=1&last_result=100000?', {}, { clientes: clientes }).then(function (res) {
            var data = JSON.parse(res.data);
            var toRet = [];
            toRet = data.lista.map(function (ed) {
                return ed;
            });
            console.log('ENTREGAS ONLINE: ', toRet);
            return toRet;
        });
    };
    DocumentosLogicaService.prototype.addOrUpdate = function (entregas) {
        var _this = this;
        var toAdd = new DocumentData(entregas.map(function (f) {
            return {
                id_entrega: f.id_entrega,
                id_cobrador: f.id_cobrador,
                id_cliente: f.id_cliente,
                id_entrega_det: parseInt(f.id_entrega_det),
                id_documento: parseInt(f.id_documento),
                no_documento: f.no_documento,
                id_moneda: f.id_moneda,
                importe: f.importe,
                guardado: f.guardado,
                fecha_documento: f.fecha_documento,
                latitud: f.latitud,
                longitud: f.longitud,
                imei: f.imei,
                habilitacion_caja: JSON.parse(localStorage.getItem('habilitacion'))
            };
        }));
        return this.geoLogic.getPos().pipe(map(function (data) {
            console.log(data);
            if (data !== null) {
                console.log('Entrega de la factura con ubicacion');
                for (var _i = 0, _a = toAdd.entregas; _i < _a.length; _i++) {
                    var row = _a[_i];
                    row.latitud = String(data.coords.latitude);
                    row.longitud = String(data.coords.longitude);
                }
                //console.log(toAdd);
                var documentData = new NewDocumentResponse();
                return documentData;
            }
            else {
                console.log('Entrega del recibo sin ubicacion');
                for (var _b = 0, _c = toAdd.entregas; _b < _c.length; _b++) {
                    var row = _c[_b];
                    row.latitud = "";
                    row.longitud = "";
                }
                //console.log(toAdd);
                var documentData = new NewDocumentResponse();
                //console.log(documentData);
                return documentData;
            }
        }), flatMap(function (req) {
            console.log('enviado para entrega de documentos', toAdd);
            return _this.documentosBD.entregarDocumentos(toAdd)
                //return this.http.doPost('entregas/nuevo?', toAdd)
                .then(function (response) {
                console.log('response entrega de documentos', response);
                req.message = new MensajeResponse(response);
                console.log('documentResponse ', req);
                return req;
            }).catch(function (error) {
                return (error.error);
            });
        }));
    };
    DocumentosLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            GeolocalizacionLogicaService,
            EntregasDocBDService])
    ], DocumentosLogicaService);
    return DocumentosLogicaService;
}());
export { DocumentosLogicaService };
//# sourceMappingURL=documentos-logica.service.js.map