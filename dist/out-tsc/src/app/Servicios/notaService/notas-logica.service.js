import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { map, flatMap } from 'rxjs/operators';
import { NewNotasResponse } from 'src/app/Clases/new-notas-response';
import { MensajeResponse } from 'src/app/Clases/mensaje-response';
import { NuevaNota } from 'src/app/Clases/nueva-nota';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
var NotasLogicaService = /** @class */ (function () {
    function NotasLogicaService(geoLogic, http, notasBDService) {
        this.geoLogic = geoLogic;
        this.http = http;
        this.notasBDService = notasBDService;
    }
    NotasLogicaService.prototype.addOrUpdateNotas = function (data) {
        var _this = this;
        var toAdd = new NuevaNota(data.id, data.id_cliente, data.nota, data.latitud, data.longitud, data.tipo, data.habilitacion_caja, JSON.parse(localStorage.getItem('imei')));
        if (!data.id) {
            return this.geoLogic.getPos().pipe(map(function (data) {
                console.log(data);
                if (data !== null) {
                    console.log('Entrega o visita con ubicacion');
                    toAdd.latitud = String(data.coords.latitude);
                    toAdd.longitud = String(data.coords.longitude);
                    console.log(toAdd);
                    var documentData_1 = new NewNotasResponse();
                    return documentData_1;
                }
                else {
                    console.log('Entrega o visita sin ubicacion');
                    toAdd.latitud = "";
                    toAdd.longitud = "";
                }
                console.log(toAdd);
                var documentData = new NewNotasResponse();
                console.log(documentData);
                return documentData;
            }), flatMap(function (req) {
                //Aca se cambia a offline
                console.log("DATOS PARA AGREGAR NOTA", toAdd);
                return _this.notasBDService.setNotaConMensaje(toAdd).then(function (response) {
                    console.log('response al guardar nota', response);
                    req.message = new MensajeResponse(response);
                    console.log(req);
                    return req;
                });
                /*
                return this.http.doPost('notas/nuevo?', toAdd)
                .then(response => {
                  console.log('response al guardar nota', response)
                  req.message = new MensajeResponse(JSON.parse(response.data));
                  console.log(req);
                  return req;
      
                });*/
            }));
        }
        else {
            return this.geoLogic.getPos().pipe(map(function (data) {
                if (data !== null) {
                    console.log('Entrega o visita con ubicacion');
                    toAdd.latitud = String(data.coords.latitude);
                    toAdd.longitud = String(data.coords.longitude);
                    console.log(toAdd);
                    var documentData_2 = new NewNotasResponse();
                    return documentData_2;
                }
                else {
                    console.log('Entrega o visita sin ubicacion');
                    toAdd.latitud = "";
                    toAdd.longitud = "";
                }
                console.log(toAdd);
                var documentData = new NewNotasResponse();
                console.log(documentData);
                return documentData;
            }), flatMap(function (req) {
                console.log("ACTUALZIAR NOTA ", toAdd);
                return _this.notasBDService.updateNotaConMensaje(toAdd, [['ID', '=', toAdd.id]]).then(function (response) {
                    console.log('response al actualizar nota', response);
                    req.message = new MensajeResponse(response);
                    console.log(req);
                    return req;
                });
                /*
                return from(this.http.doPut('notas/' + data.id + '/editar?', toAdd)
                  .then(response => {
                    console.log('response al editar nota', response)
                    req.message = new MensajeResponse(JSON.parse(response.data));
                    console.log(req);
                    return req;
      
                  })
                )*/
            }));
        }
    };
    /*getNotas(id: number): Notas {
      if (!this.notas)
        this.loadList(id);
      return this.notas;
    }*/
    /*loadList(id: number) {
    
      if (localStorage.getItem('notas')) {
  
        this.notas = JSON.parse(localStorage.getItem('notas'));
      }
      return this.http.doGet('clientes/' + id + '/notas?').then(res => {
        let data = JSON.parse(res.data);
        this.notas = data
  
        localStorage.setItem('notas', JSON.stringify(this.notas));
        return this.notas;
      });
    }*/
    /*loadNotaCobrador(idCliente: number, tipo: string): Promise<Notas> {
      return this.http.doGet('clientes/' + idCliente + '/notas_cobradores?', { tipo: tipo }).then(resp => {
        let data = JSON.parse(resp.data);
        console.log('notas cobrador ', data);
        let toRet;
        if (data.length > 0) {
          toRet = new Notas(data[0]);
          //console.log('mayor a cero')
        } else {
          ///console.log('vacio')
          toRet = new Notas("");
        }
        //console.log()
        return toRet;
  
      })
    }*/
    NotasLogicaService.prototype.loadNotaCobrador = function (idCliente, habilitacion_caja, tipo) {
        return this.notasBDService.getNotaCobrador(idCliente, habilitacion_caja, tipo);
    };
    NotasLogicaService.prototype.getAllNotasOnline = function (habilitacion_caja) {
        var _this = this;
        var first_result = 0;
        var last_result = 1000;
        return this.http.doGet('offline/habilitaciones/' + habilitacion_caja + '/notas?first_result=' + first_result + '&last_result=' + last_result + '?').then(function (resp) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var data, notas;
            return tslib_1.__generator(this, function (_a) {
                data = JSON.parse(resp.data);
                console.log('notas cobrador ', data);
                notas = data.lista;
                return [2 /*return*/, notas];
            });
        }); });
    };
    NotasLogicaService.prototype.guardarNotas = function (notas) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var existeNota, _i, notas_1, nota;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, notas_1 = notas;
                        _a.label = 1;
                    case 1:
                        if (!(_i < notas_1.length)) return [3 /*break*/, 4];
                        nota = notas_1[_i];
                        console.log("Preguntar si existe esta nota para el cliente, la habilitacion y el tipo");
                        return [4 /*yield*/, this.notasBDService.existeNotaLocal(nota.ID_CLIENTE, nota.HABILITACION_CAJA, nota.TIPO)];
                    case 2:
                        //let nota = notas[i];
                        existeNota = _a.sent();
                        if (existeNota == false) {
                            console.log("Es una nota nueva que debemos de insertar");
                            this.notasBDService.addNota(nota);
                        }
                        else {
                            console.log("Ya existe la nota");
                        }
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    NotasLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [GeolocalizacionLogicaService,
            HttpService,
            NotasBDService])
    ], NotasLogicaService);
    return NotasLogicaService;
}());
export { NotasLogicaService };
//# sourceMappingURL=notas-logica.service.js.map