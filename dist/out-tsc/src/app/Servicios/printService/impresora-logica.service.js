import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
var ImpresoraLogicaService = /** @class */ (function () {
    function ImpresoraLogicaService() {
    }
    /**
     * getAllImpresoras
     */
    ImpresoraLogicaService.prototype.getAllPrinters = function () {
        if (!window.plugins.starPrinter) {
            console.log('No encuentra el plugin de la impresora');
            return throwError("No hay impresoras disponibles");
        }
        return Observable.create(function (observer) {
            window.plugins.starPrinter.portDiscovery('Bluetooth', function (error, printerList) {
                console.log("Start printing", arguments);
                if (error) {
                    observer.error(error);
                }
                else {
                    observer.next(printerList);
                    observer.complete();
                }
            });
        });
    };
    ImpresoraLogicaService.prototype.printString = function (printer, text) {
        if (!window.plugins.starPrinter) {
            console.log('No encuentra el plugin de la impresora');
            return throwError("No hay impresoras disponibles");
        }
        return Observable.create(function (observer) {
            window.plugins.starPrinter.printReceipt(printer.name, text, function (error, result) {
                console.log("Finishing printing", arguments);
                if (error) {
                    observer.error("Por favor, verifique la conexion a su impresora y vuelva a intentarlo!");
                }
                else {
                    observer.next(result);
                    observer.complete();
                }
            });
        });
    };
    ImpresoraLogicaService.prototype.printerStatus = function () {
        if (!window.plugins.starPrinter) {
            console.log('No encuentra el plugin de la impresora');
            return throwError("No hay impresoras disponibles");
        }
        return Observable.create(function (observer) {
            window.plugins.starPrinter.checkStatus('Bluetooth', function (error, result) {
                if (error) {
                    observer.error("Por favor, verifique la conexion a su impresora y vuelva a intentarlo!");
                }
                else {
                    if (result.offline) {
                        observer.error('Impresora fuera de linea, verifique la conexion');
                    }
                    else {
                        console.log(result.offline ? "printer is offline" : "printer is online");
                        observer.complete();
                        observer.next(result);
                    }
                }
            });
        });
    };
    ImpresoraLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ImpresoraLogicaService);
    return ImpresoraLogicaService;
}());
export { ImpresoraLogicaService };
//# sourceMappingURL=impresora-logica.service.js.map