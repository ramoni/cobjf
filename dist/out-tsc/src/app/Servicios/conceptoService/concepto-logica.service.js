import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Concepto } from '../../Clases/concepto';
import { HttpService } from '../httpFunctions/http.service';
import { ConceptosBDService } from '../almacenamientoSQLITE/conceptosBD/conceptos-bd.service';
var ConceptoLogicaService = /** @class */ (function () {
    function ConceptoLogicaService(http, conceptosBD) {
        this.http = http;
        this.conceptosBD = conceptosBD;
        this.conceptos = [];
    }
    ConceptoLogicaService.prototype.getConceptosOnline = function () {
        var _this = this;
        return this.http.doGet('conceptos?', { first_result: 0, last_result: 500 })
            .then(function (res) {
            var data = JSON.parse(res.data).lista;
            var toRet = [];
            toRet = data.map(function (c) {
                return new Concepto(c);
            });
            console.log('conceptos', toRet);
            _this.conceptos = toRet;
            return toRet;
        });
    };
    ConceptoLogicaService.prototype.getConceptosBD = function () {
        var _this = this;
        return this.conceptosBD.getConceptos().then(function (conceptos) {
            _this.conceptos = conceptos;
            return conceptos;
        });
    };
    ConceptoLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService, ConceptosBDService])
    ], ConceptoLogicaService);
    return ConceptoLogicaService;
}());
export { ConceptoLogicaService };
//# sourceMappingURL=concepto-logica.service.js.map