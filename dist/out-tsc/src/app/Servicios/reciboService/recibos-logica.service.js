import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from '../httpFunctions/http.service';
import { CreateReceiptData } from 'src/app/Clases/create-receipt-data';
import { GeolocalizacionLogicaService } from '../geolocalizacionService/geolocalizacion-logica.service';
import { ReciboDTO } from 'src/app/Clases/recibo-DTO';
import { Recibo } from 'src/app/Clases/recibo';
import { InvoiceWithACobrar } from 'src/app/Clases/invoice-with-acobrar';
import { NewReceiptResponse } from 'src/app/Clases/new-receipt-response';
import { ImpresoraLogicaService } from 'src/app/Servicios/printService/impresora-logica.service';
import { throwError, from } from 'rxjs';
import { map, flatMap, catchError } from 'rxjs/operators';
import * as moment from 'moment';
import { ValoresLogicaService } from '../valorService/valores-logica.service';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { ValorRecibo } from 'src/app/Clases/valor-recibo';
import { ReciboRequestService } from '../reciboRequestService/recibo-request.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
import { ClientesBDService } from '../almacenamientoSQLITE/clientesBD/clientes-bd.service';
import { MonedasBDService } from '../almacenamientoSQLITE/monedasBD/monedas-bd.service';
import { FacturasBDService } from '../almacenamientoSQLITE/facturasBD/facturas-bd.service';
import { EntidadesBDService } from '../almacenamientoSQLITE/entidadesBD/entidades-bd.service';
import { CuentasBDService } from '../almacenamientoSQLITE/cuentasBD/cuentas-bd.service';
import { ConceptosBDService } from '../almacenamientoSQLITE/conceptosBD/conceptos-bd.service';
import { UtilitariosService } from '../utilitarios/utilitarios.service';
import { WriteFileService } from '../writeFileService/logWrite.service';
var RecibosLogicaService = /** @class */ (function () {
    function RecibosLogicaService(http, geoLogic, printLogic, valorLogic, recibosDBservice, reciboRequestService, userBDService, clienteBD, monedaBD, facturasBD, entidadBD, cuentaBD, conceptoBD, utils, writeFile) {
        this.http = http;
        this.geoLogic = geoLogic;
        this.printLogic = printLogic;
        this.valorLogic = valorLogic;
        this.recibosDBservice = recibosDBservice;
        this.reciboRequestService = reciboRequestService;
        this.userBDService = userBDService;
        this.clienteBD = clienteBD;
        this.monedaBD = monedaBD;
        this.facturasBD = facturasBD;
        this.entidadBD = entidadBD;
        this.cuentaBD = cuentaBD;
        this.conceptoBD = conceptoBD;
        this.utils = utils;
        this.writeFile = writeFile;
    }
    /**
     * Retorna un valor por defecto,
     * a ser usado como primer valor en un
     * nuevo recibo.
     */
    RecibosLogicaService.prototype.getDefaultValor = function (moneda) {
        console.log('Valor por defecto ', this.valorLogic.getDefaultType());
        var valueDefault = new ValorRecibo(this.valorLogic.getDefaultType(), moneda, "0", 0, 0, null, null, null, false);
        console.log('Valor por defecto en el recibo ', valueDefault);
        return valueDefault;
        //return new Value(this.valorLogic.getDefaultType(), moneda, "0", 0, 0, null, null, null);
    };
    RecibosLogicaService.prototype.getDefaultReceipt = function (cliente, facturas) {
        var toRet = new Recibo(cliente, {});
        toRet.nro_recibo = 'A definir';
        toRet.facturas = facturas.map(function (f) {
            return new InvoiceWithACobrar(f, f.saldo_cuota);
        });
        toRet.valores = [];
        toRet.id_moneda = '1';
        toRet.interes = 0;
        toRet.fecha = new Date();
        toRet.total = 0;
        toRet.estado = 'CREADA';
        if (localStorage.getItem('imei')) {
            toRet.version = JSON.parse(localStorage.getItem('version'));
        }
        else {
            toRet.version = 'undefined';
        }
        toRet.imei = JSON.parse(localStorage.getItem('imei'));
        return toRet;
    };
    RecibosLogicaService.prototype.getRecibosPorFactura = function (factura) {
        return this.recibosDBservice.obtenerRecibosFactura(factura.id).then(function (recibos) {
            console.log('recibos de la factura ', recibos);
            var toRet = [];
            for (var _i = 0, recibos_1 = recibos; _i < recibos_1.length; _i++) {
                var row = recibos_1[_i];
                var toAdd = new Recibo(factura.cliente, row);
                toAdd.facturas.push(new InvoiceWithACobrar(factura, 0));
                toRet.push(toAdd);
            }
            return toRet;
        });
    };
    RecibosLogicaService.prototype.anular = function (recibo) {
        recibo.imei = JSON.parse(localStorage.getItem('imei'));
        return this.recibosDBservice.anular(recibo.id).then(function (resp) {
            if (resp) {
                return {
                    "exito": "Recibo anulado con exito"
                };
            }
            return {
                "error": "No pudo ser anulado el recibo"
            };
        }).catch(function (error) {
            console.log('No pudo anularse el recibo', error);
            return {
                "error": "No pudo ser anulado el recibo"
            };
        });
    };
    RecibosLogicaService.prototype._doPrint = function (value) {
        console.log('Sending to printer');
        return this.printLogic.printString(value.printer, value.print).pipe(map(function (response) {
            console.log('the printer return:', response);
            return value;
        }), catchError(function (error) {
            console.log("error al imprimir el recibo en impresora", error);
            return throwError(error);
        }));
    };
    RecibosLogicaService.prototype.print = function (recibo, tipo) {
        var _this = this;
        var data = new CreateReceiptData();
        data.id = parseInt(recibo.id);
        data.data = new NewReceiptResponse({
            ID_RECIBO: parseInt(recibo.id),
            exito: "Recibo impreso con exito"
        });
        return this.printLogic.getAllPrinters().pipe(map(function (printers) {
            data.printer = _this._handlePrinters(printers);
            return data;
        }), catchError(function (err) {
            console.log('error al encontrar impresoras disponibles', err);
            return throwError('Error al encontrar impresoras disponibles');
        }), flatMap(function (res) {
            //console.log(res)
            return from(_this.crearImpresionRecibo(res.id, tipo).then(function (imprimir) {
                if (tipo === '1') {
                    res.print = imprimir.ORIGINAL;
                    return res;
                }
                else {
                    res.print = imprimir.DUPLICADO;
                    return res;
                }
            }).catch(function (error) {
                console.error('no pudo obtenerse la impresion del recibo', error);
                return throwError('No se pudo obtener el recibo para la impresion');
            }));
        }), flatMap(function (res) {
            return _this._doPrint(res).pipe(catchError(function (error) {
                console.log("error al imprimir el recibo en reimpresiones", error);
                return throwError(error);
            }));
        }), catchError(function (error) {
            console.error('Ocurrio un error inesperado al imprimir un recibo en reimpresion', error);
            return throwError(error);
        }));
    };
    RecibosLogicaService.prototype.addOrUpdate = function (recibo) {
        var _this = this;
        console.log('recibo recibido en logica de recibos ', recibo);
        var toAdd = new ReciboDTO(moment(recibo.fecha).format('DD-MM-YYYY'), parseInt(recibo.cliente.id), parseInt(recibo.id_moneda), recibo.cotizacion, recibo.interes, recibo.total, recibo.anticipo, recibo.version, recibo.facturas.map(function (f) {
            return {
                id_factura: parseInt(f.data.id),
                monto: f.a_cobrar,
                cuota: f.data.cuota_minima,
                id_moneda: f.data.id_moneda,
                saldo: f.data.saldo_cuota,
                saldo_total: f.data.saldo,
                tipo: f.data.tipo
            };
        }), recibo.valores.map(function (v) {
            return {
                id_tipo_valor: parseInt(v.tipo.id),
                numero_valor: v.numero,
                id_moneda: parseInt(v.moneda.id),
                monto: v.total,
                id_entidad: v.banco,
                id_cuenta_bancaria: v.cuenta,
                fecha_emision: v.fecha,
                valorizado: v.valorizado
            };
        }), recibo.latitud, recibo.longitud, recibo.marca, parseInt(recibo.concepto), recibo.imei);
        return this.geoLogic.getPos()
            .pipe(map(function (data) {
            if (data !== null) {
                toAdd.latitud = "" + data.coords.latitude;
                toAdd.longitud = "" + data.coords.longitude;
                var receiptData = new CreateReceiptData();
                receiptData.dto = toAdd;
                // console.log('La cabecera del recibo');
                console.log('recibo DTO ', toAdd);
                return receiptData;
            }
            else {
                toAdd.latitud = "";
                toAdd.longitud = "";
                var receiptData = new CreateReceiptData();
                receiptData.dto = toAdd;
                console.log(toAdd);
                return receiptData;
            }
        }), 
        // obtener impresoras
        flatMap(function (req) {
            return _this.printLogic.getAllPrinters().pipe(map(function (printers) {
                console.log("impresoras", printers);
                req.printer = _this._handlePrinters(printers);
                return req;
            }), catchError(function (err) {
                console.log('error al encontrar impresoras disponibles', err);
                return throwError(err);
            }));
        }), 
        // imprimir texto vacio
        flatMap(function (req) {
            return _this.printLogic.printString(req.printer, "").pipe(map(function (res) {
                console.log('imprimiendo texto vacio para prueba');
                return req;
            }), catchError(function (err) {
                console.log('error al imprimir texto vacio', err);
                return throwError(err);
            }));
        }), 
        /*flatMap((req: CreateReceiptData) => {
          return this.printLogic.printerStatus().pipe(map(res => {
            console.log('Se encontro impresoras disponibles', res)
            return req;
          }), catchError(err => {
            console.log('impresoras no disponibles', err)
            return throwError(err);
          }))
        }),*/
        // crear recibo
        flatMap(function (req) {
            console.log('lo que se envia para que se cree el recibo en recibos/nuevo', toAdd);
            return from(_this.addRecibo(toAdd).then(function (res) {
                console.log('respuesta del recibos/nuevo ', res);
                req.data = new NewReceiptResponse(res);
                req.id = Number(req.data.id);
                localStorage.setItem('recibo', JSON.stringify(req));
                localStorage.setItem('reciboGenerado', JSON.stringify(req.id));
                return req;
            }).catch(function (error) {
                console.error('error al crear el recibo/nuevo', error);
                return Promise.reject(error.error);
            }));
        }), flatMap(function (req) {
            return from(_this.crearImpresionRecibo(req.id, '1').then(function (res) {
                req.data.reciboOriginal = res.ORIGINAL;
                return req;
            }).catch(function (error) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                return tslib_1.__generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.recibosDBservice.borrarReciboEnCascada(req.id)];
                        case 1:
                            _a.sent();
                            localStorage.removeItem('recibo');
                            localStorage.removeItem('reciboGenerado');
                            console.error('error en creacion de la impresion del recibo', error);
                            return [2 /*return*/, Promise.reject(error)];
                    }
                });
            }); }));
        }), flatMap(function (req) {
            return _this._doPrintOriginal(req).pipe(catchError(function (error) {
                //this.recibosDBservice.borrarReciboEnCascada(req.id);
                console.error('catchError doPrintOriginal', error);
                return throwError(error);
            }));
        }), catchError(function (error) {
            //Eliminar recibo
            console.error('ocurrio un error inesperado en crear el recibo', error);
            return throwError(error);
        }));
    };
    RecibosLogicaService.prototype.printDuplicado = function () {
        var _this = this;
        var req = new CreateReceiptData();
        req = JSON.parse(localStorage.getItem('recibo'));
        console.log('printTime recibo', req);
        req.id = req.data.id;
        return from(this.crearImpresionRecibo(req.id, '2').then(function (res) {
            req.data.reciboDuplicado = res.DUPLICADO;
            return req;
        }).catch(function (error) {
            console.error('error en el metodo de creacion de la impresion del recibo duplicado', error);
            return throwError(error);
        })).pipe(map(function (data) {
            console.log('Se creo la impresion del recibo ');
            return data;
        }), flatMap(function (req) {
            return _this._doPrintDuplicado(req).pipe(catchError(function (error) {
                console.error('catchError doPrintDuplicado', error);
                return throwError(error);
            }));
        }), catchError(function (error) {
            console.error('ocurrio un error inesperado en la creacion de recibo duplicado', error);
            return throwError(error);
        }));
    };
    RecibosLogicaService.prototype._doPrintOriginal = function (data) {
        console.log('doPrintOriginal');
        data.print = data.data.reciboOriginal;
        return this.printLogic.printString(data.printer, data.print).pipe(map(function (response) {
            console.log('the printer return:', response);
            return data;
        }), catchError(function (error) {
            console.log("error al imprimir el recibo en impresora", error);
            return throwError(error);
        }));
    };
    RecibosLogicaService.prototype._doPrintDuplicado = function (data) {
        console.log('doPrintDuplicado');
        data.print = data.data.reciboDuplicado;
        return this.printLogic.printString(data.printer, data.print).pipe(map(function (response) {
            console.log('the printer return:', response);
            return data;
        }), catchError(function (error) {
            console.log('error al imprimir el duplicado del recibo', error);
            return throwError(error);
        }));
    };
    RecibosLogicaService.prototype._handlePrinters = function (data) {
        data = data.filter(function (d) { return d !== null; });
        if (data.length === 0) {
            throw new Error('No hay impresoras disponibles, verifique la conexion a Bluetooh');
        }
        if (data.length > 1) {
            alert('Muchas impresoras encontradas, usando la primera');
        }
        return data[0];
    };
    RecibosLogicaService.prototype.addOrUpdateFCO = function (recibo) {
        var _this = this;
        var toAdd = new ReciboDTO(moment(recibo.fecha).format('DD-MM-YYYY'), parseInt(recibo.cliente.id), parseInt(recibo.id_moneda), recibo.cotizacion, recibo.interes, recibo.total, recibo.anticipo, recibo.version, recibo.facturas.map(function (f) {
            return {
                id_factura: parseInt(f.data.id),
                monto: f.a_cobrar,
                cuota: f.data.cuota_minima,
                id_moneda: f.data.id_moneda,
                saldo: f.data.saldo,
            };
        }), recibo.valores.map(function (v) {
            return {
                id_tipo_valor: parseInt(v.tipo.id),
                numero_valor: v.numero,
                id_moneda: parseInt(v.moneda.id),
                monto: v.total,
                id_entidad: v.banco,
                id_cuenta_bancaria: v.cuenta,
                fecha_emision: v.fecha,
                valorizado: v.valorizado
            };
        }), recibo.latitud, recibo.longitud, recibo.marca, parseInt(recibo.concepto), recibo.imei);
        return this.geoLogic.getPos()
            .pipe(map(function (data) {
            if (data !== null) {
                toAdd.latitud = "" + data.coords.latitude;
                toAdd.longitud = "" + data.coords.longitude;
                var receiptData = new CreateReceiptData();
                receiptData.dto = toAdd;
                console.log(toAdd);
                return receiptData;
            }
            else {
                toAdd.latitud = "";
                toAdd.longitud = "";
                var receiptData = new CreateReceiptData();
                receiptData.dto = toAdd;
                console.log(toAdd);
                return receiptData;
            }
        }), 
        //hacer el post nomas ya
        flatMap(function (req) {
            return from(_this.addRecibo(toAdd).then(function (res) {
                console.log('respuesta del recibos/nuevo FCO', res);
                req.data = new NewReceiptResponse(res);
                req.id = Number(req.data.id);
                return req;
            }).catch(function (error) {
                console.error('error al crear el recibo FCO', error);
                throw throwError(error);
            }));
        }) //flatmap
        , catchError(function (error) {
            console.log('Error en recibo contado', error);
            return throwError(error);
        }));
    };
    RecibosLogicaService.prototype.getRecibosOnline = function (facturas) {
        return this.http.doGet('offline/recibos?', {}, { facturas: facturas }).then(function (response) {
            var data = JSON.parse(response.data);
            return data.lista;
        });
    };
    RecibosLogicaService.prototype.addRecibo = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var request;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.reciboRequestService.formatData(data)];
                    case 1:
                        request = _a.sent();
                        console.log("DATO PARA AGREGAR RECIBO ", request);
                        return [2 /*return*/, this.recibosDBservice.setRecibo(request).then(function (resp) {
                                console.log("ADDRECIBO EXITOSO");
                                return resp;
                            }).catch(function (error) {
                                console.error("ERROR ADDRECIBO", error);
                                return Promise.reject(error);
                            })];
                }
            });
        });
    };
    RecibosLogicaService.prototype.crearImpresionRecibo = function (id_recibo, type) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var linea, cotizacion, totalRecibo, interes, anticipo, id_cobrador, id_concepto, id_moneda_recibo, nroRecibo, habilitacion, id_cliente, talonario, nombre_cobrador, fecha_recibo, numReciboFinal, razonSocial, rucCliente, simboloCabecera, cantidad_decimales, valoresPagos, detallesPagos, tipoCotizacion, montoCambio, total, totalReciboFCR, totalCabecera, totalFacturaFormateado, totalReciboFormateado, anticipoFormateado, interesFormateado, recibo, user, cli, moneda, credito, _i, detallesPagos_1, det, tipo, _a, detallesPagos_2, detalle, factura, num_factura, monto_formateado, tipo, mon, aux, montoValorizado, simbolo, nombreEntidad, nombreCuenta, montoValorizadoFormateado, _b, valoresPagos_1, valor, montoFormateado, mon, enti, enti, cuen, mon, enti, enti, cuen, monedaCabecera, xcifra, concep, fechaHoraActual;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        console.log('crearImpresionRecibo, id_recibo', id_recibo);
                        linea = "";
                        cotizacion = 0.0;
                        totalRecibo = 0.0;
                        interes = 0.0;
                        anticipo = 0.0;
                        id_cobrador = 0;
                        id_concepto = 0;
                        id_moneda_recibo = 0;
                        nroRecibo = 0;
                        habilitacion = 0;
                        id_cliente = 0;
                        talonario = 0;
                        nombre_cobrador = "";
                        fecha_recibo = "";
                        numReciboFinal = "";
                        razonSocial = "";
                        rucCliente = null;
                        simboloCabecera = "";
                        cantidad_decimales = 0;
                        valoresPagos = [];
                        detallesPagos = [];
                        tipoCotizacion = 0.0;
                        montoCambio = 0.0;
                        total = 0.0;
                        totalReciboFCR = 0.0;
                        totalCabecera = 0.0;
                        totalFacturaFormateado = "";
                        totalReciboFormateado = "";
                        anticipoFormateado = "";
                        interesFormateado = "";
                        return [4 /*yield*/, this.recibosDBservice.obtenerReciboCap(id_recibo)];
                    case 1:
                        recibo = _c.sent();
                        console.log('recibo-cabecera en impresion', recibo);
                        if (!(recibo !== null)) return [3 /*break*/, 54];
                        id_concepto = recibo.ID_CONCEPTO;
                        id_cobrador = recibo.ID_COBRADOR;
                        fecha_recibo = recibo.FECHA;
                        id_moneda_recibo = recibo.ID_MONEDA;
                        cotizacion = recibo.COTIZACION;
                        totalRecibo = recibo.TOTAL_RECIBO;
                        interes = recibo.INTERES;
                        anticipo = recibo.ANTICIPO;
                        nroRecibo = recibo.NRO_RECIBO;
                        habilitacion = recibo.HABILITACION_CAJA;
                        id_cliente = recibo.ID_CLIENTE;
                        console.log('total_recibo', totalRecibo);
                        totalReciboFormateado = this.utils.formatearNumber(totalRecibo);
                        anticipoFormateado = this.utils.formatearNumber(anticipo);
                        interesFormateado = this.utils.formatearNumber(interes);
                        return [4 /*yield*/, this.userBDService.getTalonario(id_cobrador)];
                    case 2:
                        user = _c.sent();
                        if (!(user !== null)) return [3 /*break*/, 52];
                        console.log('encontro el usuario', user);
                        talonario = user.TALONARIO;
                        nombre_cobrador = user.NOMBRE;
                        //numero de recibo final
                        if (talonario == 0) {
                            numReciboFinal = "001-" + " " + "-000" + nroRecibo;
                        }
                        else {
                            numReciboFinal = "001-" + talonario + "-000" + nroRecibo;
                        }
                        return [4 /*yield*/, this.recibosDBservice.getValoresRecibo(id_recibo, id_moneda_recibo, cotizacion)];
                    case 3:
                        valoresPagos = _c.sent();
                        if (!(valoresPagos !== null && valoresPagos.length !== 0)) return [3 /*break*/, 50];
                        return [4 /*yield*/, this.recibosDBservice.getDetallesRecibo(id_recibo)];
                    case 4:
                        detallesPagos = _c.sent();
                        if (!(detallesPagos !== null && detallesPagos.length !== 0)) return [3 /*break*/, 48];
                        return [4 /*yield*/, this.clienteBD.getCliente(id_cliente)];
                    case 5:
                        cli = _c.sent();
                        if (!(cli !== null)) return [3 /*break*/, 46];
                        console.log('encontro el cliente', cli);
                        rucCliente = cli.ruc;
                        razonSocial = cli.razonSocial;
                        return [4 /*yield*/, this.monedaBD.getMoneda(id_moneda_recibo)];
                    case 6:
                        moneda = _c.sent();
                        if (!(moneda !== null)) return [3 /*break*/, 44];
                        console.log('encontro la moneda valoresPagos', moneda);
                        cantidad_decimales = moneda.cant_decimales;
                        simboloCabecera = moneda.simbolo;
                        /////// formateo de decimales falta
                        linea = linea + "J. Fleischman y Cia S.R.L."
                            + "\r\nRuc: 80001490-1\r\nDireccion:Tte.Nicolas Cazenave\r\n"
                            + "y Tte.Victor Valdez\r\nTel.:(021) 238 2200\r\n"
                            + "******************************\r\n"
                            + "Recibo:" + numReciboFinal + "\r\n"
                            + "Fecha:" + fecha_recibo + "\r\n"
                            + "Recibimos de: \r\n"
                            + this.utils.limpiar_caracteres_especiales(razonSocial)
                            + "\r\nR.U.C: " + rucCliente + "\r\n"
                            + "Cotizacion: " + cotizacion + " Gs/USD \r\n"
                            + "______________________________\r\n";
                        return [4 /*yield*/, this.facturasBD.todosCredito(detallesPagos)];
                    case 7:
                        credito = _c.sent();
                        if (!(credito === false)) return [3 /*break*/, 8];
                        // todos son contado
                        totalCabecera = totalRecibo;
                        linea = linea + "\r\nPor Facturas: \r\n"
                            + "NRO    IMPORTE   \r\n";
                        return [3 /*break*/, 13];
                    case 8:
                        _i = 0, detallesPagos_1 = detallesPagos;
                        _c.label = 9;
                    case 9:
                        if (!(_i < detallesPagos_1.length)) return [3 /*break*/, 12];
                        det = detallesPagos_1[_i];
                        return [4 /*yield*/, this.facturasBD.isFCO(det.ID_FACTURA)];
                    case 10:
                        tipo = _c.sent();
                        if (tipo === false) { //no es contado, es a credito
                            if (det.ID_MONEDA !== id_moneda_recibo) {
                                tipoCotizacion = cotizacion;
                                if (det.ID_MONEDA === 1) {
                                    montoCambio = det.MONTO / tipoCotizacion;
                                }
                                else {
                                    montoCambio = det.MONTO * tipoCotizacion;
                                }
                                total = total + montoCambio;
                            }
                            else { //son de la misma moneda
                                total = total + det.MONTO;
                            }
                        }
                        _c.label = 11;
                    case 11:
                        _i++;
                        return [3 /*break*/, 9];
                    case 12:
                        totalFacturaFormateado = this.utils.formatearNumber(Number(total.toFixed(cantidad_decimales)));
                        console.log('total_factura_formateado', totalFacturaFormateado);
                        totalReciboFCR = total;
                        if (interes > 0.0) {
                            totalReciboFCR += interes;
                        }
                        if (anticipo > 0.0) {
                            totalReciboFCR += anticipo;
                        }
                        totalCabecera = totalReciboFCR;
                        totalReciboFormateado = this.utils.formatearNumber(Number(totalReciboFCR.toFixed(cantidad_decimales)));
                        linea = linea + "\r\nPor Facturas: \r\n"
                            + "NRO        IMPORTE        CUOTA \r\n";
                        _c.label = 13;
                    case 13:
                        total = 0.0;
                        _a = 0, detallesPagos_2 = detallesPagos;
                        _c.label = 14;
                    case 14:
                        if (!(_a < detallesPagos_2.length)) return [3 /*break*/, 19];
                        detalle = detallesPagos_2[_a];
                        factura = void 0;
                        num_factura = null;
                        monto_formateado = null;
                        return [4 /*yield*/, this.facturasBD.isFCO(detalle.ID_FACTURA)];
                    case 15:
                        tipo = _c.sent();
                        if (!(tipo == false)) return [3 /*break*/, 18];
                        return [4 /*yield*/, this.facturasBD.detallesFactura(detalle.ID_FACTURA)];
                    case 16:
                        factura = _c.sent();
                        if (!(factura != null)) return [3 /*break*/, 18];
                        console.log('encontro la factura', factura);
                        num_factura = factura.NO_FACTURA;
                        num_factura = num_factura.substring(4);
                        return [4 /*yield*/, this.monedaBD.getMoneda(factura.ID_MONEDA)];
                    case 17:
                        mon = _c.sent();
                        if (mon !== null) {
                            cantidad_decimales = mon.cant_decimales;
                            monto_formateado = this.utils.formatearNumber(Number(detalle.MONTO.toFixed(cantidad_decimales)));
                            //formar el string
                            console.log('monto_formateado del MONTO DEL DETALLE', monto_formateado);
                            linea = linea + num_factura + "   " + monto_formateado + " "
                                + mon.simbolo + " " + detalle.NRO_CUOTA + "/" + factura.MAX_CUOTA + "\r\n";
                            if (factura.ID_MONEDA != id_moneda_recibo) {
                                montoCambio = cotizacion;
                                if (factura.ID_MONEDA == 1) {
                                    montoCambio = detalle.MONTO / montoCambio;
                                }
                                else {
                                    montoCambio = detalle.MONTO * montoCambio;
                                }
                                total += montoCambio;
                            }
                            else {
                                total += detalle.MONTO;
                            }
                        }
                        _c.label = 18;
                    case 18:
                        _a++;
                        return [3 /*break*/, 14];
                    case 19:
                        linea = linea + "\r\n";
                        console.log("Total de facturas en imprimirRecibo " + total);
                        if (anticipo > 0.0) {
                            linea = linea + "Anticipo: " + anticipoFormateado + "  " + simboloCabecera + "\r\n";
                        }
                        if (interes > 0.0) {
                            linea = linea + "Interes: " + interesFormateado + "  " + simboloCabecera + "\r\n";
                        }
                        linea = linea + "Total Importe: " + totalReciboFormateado + "  " + simboloCabecera + "\r\n"
                            + "______________________________\r\n"
                            + "Medios de Pago \r\n";
                        aux = 0.0;
                        montoValorizado = 0.0;
                        simbolo = null;
                        aux = totalCabecera;
                        console.log("Total Cabecera " + aux);
                        nombreEntidad = void 0;
                        nombreCuenta = void 0;
                        montoValorizadoFormateado = void 0;
                        _b = 0, valoresPagos_1 = valoresPagos;
                        _c.label = 20;
                    case 20:
                        if (!(_b < valoresPagos_1.length)) return [3 /*break*/, 41];
                        valor = valoresPagos_1[_b];
                        console.log('valor de valoresPagos', valor);
                        if (!(aux != 0.0)) return [3 /*break*/, 39];
                        montoValorizado = valor.MONTO_VALORIZADO;
                        montoFormateado = null;
                        if (!(montoValorizado <= aux)) return [3 /*break*/, 30];
                        linea = linea + this.utils.limpiar_caracteres_especiales(valor.DESCRIPCION) + "\r\n";
                        console.log("La moneda del valor es " + valor.ID_MONEDA);
                        return [4 /*yield*/, this.monedaBD.getMoneda(valor.ID_MONEDA)];
                    case 21:
                        mon = _c.sent();
                        if (!(mon != null)) return [3 /*break*/, 29];
                        cantidad_decimales = mon.cant_decimales;
                        simbolo = mon.simbolo;
                        if (!(valor.NUMERO_VALOR === "0" && (parseInt(valor.ID_ENTIDAD) == 0 || valor.ID_ENTIDAD === null))) return [3 /*break*/, 22];
                        montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                        linea = linea + "IMPORTE:     " + montoFormateado + "  " + simbolo + "\r\n";
                        return [3 /*break*/, 28];
                    case 22:
                        if (!(!(valor.NUMERO_VALOR === "0") && (parseInt(valor.ID_ENTIDAD) == 0 || valor.ID_ENTIDAD === null))) return [3 /*break*/, 23];
                        montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                        linea = linea + "IMPORTE:     " + montoFormateado + "  " + simbolo + "\r\n";
                        return [3 /*break*/, 28];
                    case 23:
                        console.log("Cuenta bancaria " + valor.ID_CUENTA_BANCARIA);
                        if (!(valor.ID_CUENTA_BANCARIA === "" ||
                            valor.ID_CUENTA_BANCARIA === null)) return [3 /*break*/, 25];
                        console.log("Entra a escribir en caso de que solo tenga entidad");
                        linea = linea + "Ch.NRO:    BANCO    IMPORTE\r\n";
                        montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                        console.log("Nombre de la entidad " + valor.ID_ENTIDAD);
                        return [4 /*yield*/, this.entidadBD.getEntidad(valor.ID_ENTIDAD)];
                    case 24:
                        enti = _c.sent();
                        nombreEntidad = enti.descripcion;
                        linea = linea + valor.NUMERO_VALOR + "     "
                            + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "    "
                            + montoFormateado + " " + simbolo + "\r\n";
                        return [3 /*break*/, 28];
                    case 25:
                        console.log("Entra a escribir en caso de que tenga cuenta bancaria");
                        linea = linea + "Ch.NRO:  BANCO   CUENTA   IMPORTE\r\n";
                        return [4 /*yield*/, this.entidadBD.getEntidad(valor.ID_ENTIDAD)];
                    case 26:
                        enti = _c.sent();
                        nombreEntidad = enti.descripcion;
                        return [4 /*yield*/, this.cuentaBD.getCuenta(valor.ID_CUENTA_BANCARIA)];
                    case 27:
                        cuen = _c.sent();
                        nombreCuenta = cuen.descripcion;
                        montoFormateado = this.utils.formatearNumber(Number(valor.MONTO.toFixed(cantidad_decimales)));
                        linea = linea + valor.NUMERO_VALOR + "   "
                            + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "   "
                            + this.utils.limpiar_caracteres_especiales(nombreCuenta) + "   "
                            + montoFormateado + " " + simbolo + "\r\n";
                        _c.label = 28;
                    case 28:
                        aux = aux + montoValorizado;
                        _c.label = 29;
                    case 29: return [3 /*break*/, 38];
                    case 30:
                        console.log("Monto Valorizado del valor es mayor a total cabecera");
                        if (id_moneda_recibo != valor.ID_MONEDA) {
                            if (id_moneda_recibo == 1) {
                                montoValorizado = this.utils.round(aux / cotizacion, 2);
                            }
                            else {
                                montoValorizado = this.utils.round(aux * cotizacion, 0);
                            }
                        }
                        else {
                            montoValorizado = aux;
                        }
                        console.log("MontoValorizado " + montoValorizado);
                        linea = linea + this.utils.limpiar_caracteres_especiales(valor.DESCRIPCION) + "\r\n";
                        return [4 /*yield*/, this.monedaBD.getMoneda(valor.ID_MONEDA)];
                    case 31:
                        mon = _c.sent();
                        if (!(mon != null)) return [3 /*break*/, 38];
                        simbolo = mon.simbolo;
                        cantidad_decimales = mon.cant_decimales;
                        if (!(valor.NUMERO_VALOR === "0" && (parseInt(valor.ID_ENTIDAD) == 0 || valor.ID_ENTIDAD === null))) return [3 /*break*/, 32];
                        montoValorizadoFormateado = this.utils.formatearNumber(Number(montoValorizado.toFixed(cantidad_decimales)));
                        console.log("MONTO FORMATEADO " + montoValorizadoFormateado);
                        linea = linea + "IMPORTE:     " + montoValorizadoFormateado + "  "
                            + simbolo + "\r\n";
                        return [3 /*break*/, 38];
                    case 32:
                        console.log("Cuenta bancaria " + valor.ID_CUENTA_BANCARIA);
                        if (!(valor.ID_CUENTA_BANCARIA === null || valor.ID_CUENTA_BANCARIA === "")) return [3 /*break*/, 34];
                        linea = linea + "Ch.NRO:    BANCO    IMPORTE\r\n";
                        return [4 /*yield*/, this.entidadBD.getEntidad(valor.ID_ENTIDAD)];
                    case 33:
                        enti = _c.sent();
                        nombreEntidad = enti.descripcion;
                        montoValorizadoFormateado = this.utils.formatearNumber(Number(montoValorizado.toFixed(cantidad_decimales)));
                        linea = linea + valor.NUMERO_VALOR + "     "
                            + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "    "
                            + montoValorizadoFormateado + " " + simbolo + "\r\n";
                        return [3 /*break*/, 37];
                    case 34:
                        linea = linea + "Ch.NRO: BANCO  CUENTA  IMPORTE\r\n";
                        return [4 /*yield*/, this.entidadBD.getEntidad(valor.ID_ENTIDAD)];
                    case 35:
                        enti = _c.sent();
                        nombreEntidad = enti.descripcion;
                        return [4 /*yield*/, this.cuentaBD.getCuenta(valor.ID_CUENTA_BANCARIA)];
                    case 36:
                        cuen = _c.sent();
                        nombreCuenta = cuen.descripcion;
                        montoValorizadoFormateado = this.utils.formatearNumber(Number(montoValorizado.toFixed(cantidad_decimales)));
                        linea = linea + valor.NUMERO_VALOR + "   "
                            + this.utils.limpiar_caracteres_especiales(nombreEntidad) + "   "
                            + this.utils.limpiar_caracteres_especiales(nombreCuenta) + "   "
                            + montoValorizadoFormateado + " " + simbolo + "\r\n";
                        _c.label = 37;
                    case 37:
                        aux = 0.0;
                        _c.label = 38;
                    case 38: return [3 /*break*/, 40];
                    case 39: return [3 /*break*/, 41];
                    case 40:
                        _b++;
                        return [3 /*break*/, 20];
                    case 41:
                        monedaCabecera = moneda;
                        cantidad_decimales = monedaCabecera.cant_decimales;
                        xcifra = this.utils.formatearNumber(parseFloat(totalCabecera.toFixed(cantidad_decimales)));
                        xcifra = xcifra.replace(/\./g, '');
                        simbolo = monedaCabecera.simbolo;
                        linea = linea + "TOTAL PAGO: " + this.utils.formatearNumber(Number(totalCabecera.toFixed(cantidad_decimales))) + " "
                            + simbolo + "\t\r\n"
                            + "______________________________\r\n"
                            + "Total Recibo: " + totalReciboFormateado + "  " + simbolo + "\r\n"
                            + "SON: " + this.utils.numeroALetras(xcifra, {
                            singular: monedaCabecera.singular,
                            plural: monedaCabecera.plural
                        }) + " \t\r\n";
                        if (!(id_concepto !== null && id_concepto !== 0)) return [3 /*break*/, 43];
                        return [4 /*yield*/, this.conceptoBD.getConcepto(id_concepto)];
                    case 42:
                        concep = _c.sent();
                        linea = linea + "CONCEPTO: "
                            + this.utils.limpiar_caracteres_especiales(concep.descripcion)
                            + "\t\n";
                        _c.label = 43;
                    case 43:
                        linea = linea + "******************************\t\n"
                            + "\r\n" + "\r\n" + "\r\n";
                        fechaHoraActual = moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
                        linea = linea + "Cobrador: " + this.utils.limpiar_caracteres_especiales(nombre_cobrador) + "\t\r\n"
                            + "Fecha y Hora:" + fechaHoraActual + "\t\r\n";
                        if (type == '1') {
                            // imprimir que es un recibo original para el cliente
                            linea = linea + "\t\t\t\t" + "ORIGINAL:CLIENTE" + "\r\n";
                            linea = linea + "******************************\t\n";
                            console.log('original ', linea);
                        }
                        else {
                            // imprimir que es un recibo duplicado para el cliente
                            linea = linea + "\t\t" + "DUPLICADO:ARCH.TRIBUTARIO" + "\r\n";
                            linea = linea + "******************************\t\n";
                            console.log('duplicado ', linea);
                        }
                        if (type == '1') {
                            console.log("Recibo original cliente");
                            this.recibosDBservice.guardarReciboOriginal(id_recibo, "1-" + (new Date()).getTime() + "-" + id_cliente);
                            this.writeFile.crearArchivoImpresionRecibo("1-" + (new Date()).getTime() + "-" + id_cliente, linea);
                            return [2 /*return*/, Promise.resolve({
                                    ORIGINAL: linea,
                                })];
                        }
                        else {
                            // recibo duplicado
                            console.log("Recibo duplicado cliente");
                            this.recibosDBservice.guardarReciboDuplicado(id_recibo, "2-" + (new Date()).getTime() + "-" + id_cliente);
                            this.writeFile.crearArchivoImpresionRecibo("2-" + (new Date()).getTime() + "-" + id_cliente, linea);
                            return [2 /*return*/, Promise.resolve({
                                    DUPLICADO: linea
                                })];
                        }
                        return [3 /*break*/, 45];
                    case 44:
                        console.log("No se pudo obtener datos de la moneda utilizada");
                        return [2 /*return*/, Promise.reject("No se pudo obtener datos de la moneda utilizada")];
                    case 45: return [3 /*break*/, 47];
                    case 46:
                        console.log("No se pudo obtener datos del cliente");
                        return [2 /*return*/, Promise.reject("No se pudo obtener datos del cliente")];
                    case 47: return [3 /*break*/, 49];
                    case 48:
                        console.log("No se pudo obtener los detalles del recibo");
                        return [2 /*return*/, Promise.reject("No se pudo obtener los detalles del recibo")];
                    case 49: return [3 /*break*/, 51];
                    case 50:
                        console.log("No se pudo obtener los valores del recibo");
                        return [2 /*return*/, Promise.reject("No se pudo obtener los valores del recibo")];
                    case 51: return [3 /*break*/, 53];
                    case 52:
                        console.log("No se pudo obtener el usuario");
                        return [2 /*return*/, Promise.reject("No se pudo obtener el usuario")];
                    case 53: return [3 /*break*/, 55];
                    case 54:
                        console.log("No se pudo obtener el recibo cabecera");
                        return [2 /*return*/, Promise.reject("No se pudo obtener la cabecera del recibo")];
                    case 55: return [2 /*return*/];
                }
            });
        });
    };
    RecibosLogicaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            GeolocalizacionLogicaService,
            ImpresoraLogicaService,
            ValoresLogicaService,
            RecibosBDService,
            ReciboRequestService,
            UsuariosBDService,
            ClientesBDService,
            MonedasBDService,
            FacturasBDService,
            EntidadesBDService,
            CuentasBDService,
            ConceptosBDService,
            UtilitariosService,
            WriteFileService])
    ], RecibosLogicaService);
    return RecibosLogicaService;
}());
export { RecibosLogicaService };
//# sourceMappingURL=recibos-logica.service.js.map