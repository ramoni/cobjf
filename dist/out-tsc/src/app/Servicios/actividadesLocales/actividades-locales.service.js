import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { RecibosBDService } from '../almacenamientoSQLITE/recibosBD/recibos-bd.service';
import { EntregasDocBDService } from '../almacenamientoSQLITE/entregasDocBD/entregas-doc-bd.service';
import { NotasBDService } from '../almacenamientoSQLITE/notasBD/notas-bd.service';
import { UsuariosBDService } from '../almacenamientoSQLITE/usuariosBD/usuarios-bd.service';
var ActividadesLocalesService = /** @class */ (function () {
    function ActividadesLocalesService(userService, recibosService, entregasService, notasService) {
        this.userService = userService;
        this.recibosService = recibosService;
        this.entregasService = entregasService;
        this.notasService = notasService;
    }
    ActividadesLocalesService.prototype.mostrarActividadesLocales = function (habilitacion) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var datosOnline, recibosSync, entregasSync, notasLocalesSync, totalRecibos, totalEntregas, totalNotasVisitas, totalNotasEntregas, datosActualizados, userData;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.userService.getUserData()];
                    case 1:
                        datosOnline = _a.sent();
                        return [4 /*yield*/, this.recibosService.cantidadRecibosSync(habilitacion)];
                    case 2:
                        recibosSync = _a.sent();
                        return [4 /*yield*/, this.entregasService.cantidadEntregaDocSync(habilitacion)];
                    case 3:
                        entregasSync = _a.sent();
                        return [4 /*yield*/, this.notasService.cantidadNotasSync(habilitacion)];
                    case 4:
                        notasLocalesSync = _a.sent();
                        totalRecibos = 0;
                        totalEntregas = 0;
                        totalNotasVisitas = 0;
                        totalNotasEntregas = 0;
                        datosActualizados = {};
                        userData = datosOnline["USUARIO"];
                        console.log("USERDATA ", userData);
                        totalRecibos = parseInt(userData["RECIBOS"]) + recibosSync;
                        totalEntregas = parseInt(userData["ENTREGAS"]) + entregasSync;
                        totalNotasVisitas = notasLocalesSync["NOTAS_VISITAS"] + parseInt(userData["NOTAS_VISITAS"]);
                        totalNotasEntregas = notasLocalesSync["NOTAS_ENTREGAS"] + parseInt(userData["NOTAS_ENTREGAS"]);
                        datosActualizados["RECIBOS"] = totalRecibos;
                        datosActualizados["ENTREGAS"] = totalEntregas;
                        datosActualizados["NOTAS_VISITAS"] = totalNotasVisitas;
                        datosActualizados["NOTAS_ENTREGAS"] = totalNotasEntregas;
                        console.log("DATOS ACTUALIZADOS ", datosActualizados);
                        return [2 /*return*/, datosActualizados];
                }
            });
        });
    };
    ActividadesLocalesService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [UsuariosBDService,
            RecibosBDService,
            EntregasDocBDService,
            NotasBDService])
    ], ActividadesLocalesService);
    return ActividadesLocalesService;
}());
export { ActividadesLocalesService };
//# sourceMappingURL=actividades-locales.service.js.map