import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
var WriteFileService = /** @class */ (function () {
    function WriteFileService(file) {
        this.file = file;
    }
    WriteFileService.prototype.escribirLog = function (texto) {
        var _this = this;
        texto = texto + "\n";
        //console.log("Texto para el log ", texto)
        this.file.checkFile(this.file.externalApplicationStorageDirectory, 'cobranzas_log.txt').then(function (existe) {
            ///console.log("existe el archivo", existe)
            _this.file.writeFile(_this.file.externalApplicationStorageDirectory, 'cobranzas_log.txt', texto, { append: true, replace: false }).then(function (escribe) {
                //console.log("escribe", escribe)
            }).catch(function (no_escribe) {
                //console.log("no escribe", no_escribe)
            });
        }).catch(function (no_existe) {
            //console.log("no existe el archivo", no_existe)
            _this.file.writeFile(_this.file.externalApplicationStorageDirectory, 'cobranzas_log.txt', texto).then(function (creo) {
            }).catch(function (no_creo) {
                console.log("no creo el archivo", no_creo);
            });
        });
    };
    WriteFileService.prototype.crearArchivoImpresionRecibo = function (nombre_archivo, texto) {
        var _this = this;
        this.file.createDir(this.file.externalApplicationStorageDirectory, 'recibos', false).then(function (data) {
            console.log('se creo el directorio en ', _this.file.externalApplicationStorageDirectory + "/recibos");
            _this.escribirArchivoRecibo(nombre_archivo, texto);
        }).catch(function (error) {
            console.error('El directorio se habia creado y ya existe ', error);
            _this.escribirArchivoRecibo(nombre_archivo, texto);
        });
    };
    WriteFileService.prototype.escribirArchivoRecibo = function (nombre_archivo, texto) {
        console.log("NOMBRE DE ARCHIVO " + nombre_archivo);
        this.file.writeFile(this.file.externalApplicationStorageDirectory + "/recibos", nombre_archivo + '.txt', texto, { replace: true }).then(function (creo) {
            console.log('creo el archivo de recibo', creo);
        }).catch(function (no_creo) {
            console.log("no creo el archivo de recibo", no_creo);
        });
    };
    WriteFileService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [File])
    ], WriteFileService);
    return WriteFileService;
}());
export { WriteFileService };
//# sourceMappingURL=logWrite.service.js.map