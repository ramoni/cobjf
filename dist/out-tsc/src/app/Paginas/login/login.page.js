import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { SesionService } from 'src/app/Servicios/sesionValidation/sesion.service';
import { ConfigService } from 'src/app/Servicios/configuracion/config.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { TablesManageService } from 'src/app/Servicios/almacenamientoSQLITE/tablesManagment/tables-manage.service';
import { UsuarioService } from 'src/app/Servicios/usuarioService/usuario.service';
import { DatosOnlineService } from 'src/app/Servicios/onlineServices/datos-online.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
import * as moment from 'moment';
var LoginPage = /** @class */ (function () {
    function LoginPage(loadingService, writeLog, userService, navControl, validarSesion, config, tablesManage, toast, onlineMethods) {
        this.loadingService = loadingService;
        this.writeLog = writeLog;
        this.userService = userService;
        this.navControl = navControl;
        this.validarSesion = validarSesion;
        this.config = config;
        this.tablesManage = tablesManage;
        this.toast = toast;
        this.onlineMethods = onlineMethods;
        this.tokenExpirado = false;
    }
    LoginPage.prototype.ngOnInit = function () {
        if (localStorage.getItem('version')) {
            this.version = JSON.parse(localStorage.getItem('version'));
        }
    };
    LoginPage.prototype.togglePasswordType = function () {
        this.passwordType = this.passwordType || 'password';
        this.passwordType = (this.passwordType === 'password') ? 'text' : 'password';
    };
    LoginPage.prototype.bajadaDeDatos = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var bajada_1, timerId_1;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.config.IS_OFFLINE) return [3 /*break*/, 2];
                        console.log('Modo online, hacer peticiones de conceptos, entidades, cuentas, etc');
                        localStorage.setItem("bajandoDatosOnline", JSON.stringify(true));
                        return [4 /*yield*/, this.onlineMethods.bajarDatosOnline()];
                    case 1:
                        bajada_1 = _a.sent();
                        if (bajada_1 === false) {
                            this.toast.presentToast('No se pudieron descargar todos los datos, se hara un nuevo intento');
                            timerId_1 = setInterval(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                return tslib_1.__generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, this.onlineMethods.bajarDatosOnline()];
                                        case 1:
                                            bajada_1 = _a.sent();
                                            if (bajada_1) {
                                                console.log('limpiar el intervalo de llamada');
                                                localStorage.setItem("bajandoDatosOnline", JSON.stringify(false));
                                                clearInterval(timerId_1);
                                            }
                                            else {
                                                this.toast.presentToast('No se pudieron descargar todos los datos, verifique su conexion. Se hara un nuevo intento');
                                            }
                                            return [2 /*return*/];
                                    }
                                });
                            }); }, 30000);
                        }
                        else {
                            localStorage.setItem("bajandoDatosOnline", JSON.stringify(false));
                        }
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.guardarIdEIrAMenu = function () {
        this.bajadaDeDatos();
        var w = window;
        w.nav = this.navControl;
        console.log('Guardar e ir al MENU');
        this.navControl.navigateRoot('home');
    };
    LoginPage.prototype.iniciarSesion = function () {
        var _this = this;
        this.fecha_hora = new Date();
        this.writeLog.escribirLog(this.fecha_hora + 'Peticion: Inicio de sesion usuario ' + this.usuario + ' password: ' + this.password + '\n');
        this.userService.iniciarSesion(this.usuario.toLowerCase(), this.password, JSON.parse(localStorage.getItem('version')), JSON.parse(localStorage.getItem('imei')))
            .then(function (data) {
            var usuario = JSON.parse(localStorage.getItem('user'));
            _this.usuarioParse = usuario;
            _this.fecha_hora = new Date();
            _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con exito: ' + JSON.stringify(data) + '\n');
            if (_this.tokenExpirado) {
                _this.userService.existeHabilitacionUsuario(data.HABILITACION, _this.usuario.toLowerCase(), _this.password).then(function (data_resp) {
                    var resp = data_resp;
                    console.log('habilitacion ', resp);
                    if (data_resp === true) { //inserta un nuevo usuario
                        _this.userService.addUsuario(_this.usuario, _this.password, _this.usuarioParse, JSON.parse(localStorage.getItem(_this.config.AUTHORIZATION_STRING)), JSON.parse(localStorage.getItem('expiredToken')))
                            .then(function () {
                            _this.loadingService.dismiss();
                            localStorage.setItem('habilitacion', JSON.stringify(data.HABILITACION));
                            _this.fecha_hora = new Date();
                            _this.writeLog.escribirLog(_this.fecha_hora + ' Respuesta con exito: ' + JSON.stringify(data) + '\n');
                            _this.guardarIdEIrAMenu();
                        }).catch(function (error) {
                            _this.loadingService.dismiss();
                            console.log('no se pudo insertar un nuevo usuario', error);
                        });
                        /*localStorage.setItem('habilitacion', data.totalHabilitacion.toString());
                        this.guardarIdEIrAMenu();
                      this.fecha_hora = new Date();
                      //this.writeLog.escribirLog(this.fecha_hora + ' Respuesta con exito: ' + JSON.stringify(data) + '\n');
                        */
                    }
                    else {
                        //verificar que esta caja tenga datos que no se hayan sincronizado todavia y entonces pasar
                        _this.userService.existenDatosHabilitacion(data.HABILITACION).then(function (resp) {
                            if (resp === true) { //retorno true porque hay datos
                                localStorage.setItem('expiredToken', '');
                                _this.loadingService.dismiss();
                                _this.toast.presentToast("Existen datos que faltan ser sincronizados con la habilitacion anterior, sincronice e intente nuevamente!");
                                return;
                            }
                            else { //no hay datos para sincronizar
                                _this.tablesManage.deleteAllTables().then(function (resp) {
                                    if (resp === true) { //se eliminaron todas las tablas
                                        _this.tablesManage.createTables().then(function () {
                                            _this.userService.addUsuario(_this.usuario, _this.password, _this.usuarioParse, localStorage.getItem(_this.config.AUTHORIZATION_STRING), localStorage.getItem('expiredToken'))
                                                .then(function () {
                                                console.log('se inserto un new user');
                                                _this.loadingService.dismiss();
                                                localStorage.setItem('habilitacion', JSON.stringify(data.HABILITACION));
                                                _this.fecha_hora = new Date();
                                                var fecha = moment(_this.fecha_hora).format('YYYY-MM-DD HH:mm:ss');
                                                _this.writeLog.escribirLog(_this.fecha_hora + ' Respuesta con exito: ' + JSON.stringify(data) + '\n');
                                                _this.guardarIdEIrAMenu();
                                            }).catch(function (error) {
                                                _this.loadingService.dismiss();
                                                console.log('Error al agregar nuevo usuario', error);
                                                _this.toast.presentToast('No se pudo agregar su usuario a la tabla local');
                                            });
                                            /*this.fecha_hora = new Date();
                                              //let fecha = moment(this.fecha_hora).format('YYYY-MM-DD HH:mm:ss');
                                              //this.writeLog.escribirLog(this.fecha_hora + ' Respuesta con exito: ' + JSON.stringify(data) + '\n');
                                              */
                                        }).catch(function (error) {
                                            _this.loadingService.dismiss();
                                            console.log('No se crearon todas las tablas', error);
                                            _this.toast.presentToast('Errores al eliminar las tablas locales');
                                        });
                                    }
                                    else {
                                        _this.loadingService.dismiss();
                                        console.log('No se pudieron eliminar todas las tablas');
                                        _this.toast.presentToast('Errores al eliminar las tablas locales');
                                    }
                                }).catch(function (err) {
                                    _this.loadingService.dismiss();
                                    console.log('error al eliminar todas las tablas ', err);
                                    _this.toast.presentToast('Errores al eliminar las tablas locales');
                                });
                            }
                        }).catch(function (error) {
                            _this.loadingService.dismiss();
                            console.log('No se pudieron obtener los datos de la habilitacion', error);
                            _this.toast.presentToast('No se pudo obtener los datos de la habilitacion de las tablas locales');
                        });
                    }
                }).catch(function (error) {
                    _this.loadingService.dismiss();
                    console.log('No se pudo obtener la habilitacion del usuario', error);
                    _this.toast.presentToast('Errores al eliminar las tablas locales');
                });
            }
            else { //no expiro el token
                /*
                  Falta la sincronizacion
       
                */
                _this.tablesManage.deleteAllTables().then(function (resp) {
                    if (resp === true) { //se eliminaron todas las tablas
                        _this.tablesManage.createTables().then(function () {
                            _this.userService.addUsuario(_this.usuario, _this.password, _this.usuarioParse, localStorage.getItem(_this.config.AUTHORIZATION_STRING), localStorage.getItem('expiredToken'))
                                .then(function () {
                                console.log('se inserto un new user');
                                _this.loadingService.dismiss();
                                localStorage.setItem('habilitacion', JSON.stringify(data.HABILITACION));
                                _this.fecha_hora = new Date();
                                _this.writeLog.escribirLog(_this.fecha_hora + ' Respuesta con exito: ' + JSON.stringify(data) + '\n');
                                _this.guardarIdEIrAMenu();
                            }).catch(function (error) {
                                _this.loadingService.dismiss();
                                console.log('no se pudo insertar un nuevo usuario', error);
                                _this.toast.presentToast('Error al insertar su usuario en la tabla local');
                            });
                        }).catch(function (error) {
                            _this.loadingService.dismiss();
                            console.log('no se pudo crear las tablas', error);
                            _this.toast.presentToast('No se pudo crear las tablas localmente');
                        });
                    }
                    else { //errores al eliminar las tablas
                        _this.loadingService.dismiss();
                        _this.toast.presentToast('No se puede iniciar sesion, error al eliminar las tablas');
                    }
                }).catch(function (err) {
                    _this.loadingService.dismiss();
                    console.error("No se eliminaron las tablas", err);
                    _this.toast.presentToast('Errores al eliminar las tablas locales');
                });
            }
        }).catch(function (error) {
            _this.loadingService.dismiss();
            var messageError = JSON.parse(error.error).error;
            console.log("ocurrio un error en iniciar sesion ", messageError);
            _this.fecha_hora = new Date();
            if (error.status === 401) {
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + JSON.stringify(messageError) + '\n');
                _this.toast.presentToast(messageError);
            }
            else {
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + ' El servidor no se encuentra disponible, verifique su conexion e intente nuevamente' + '\n');
                _this.toast.presentToast(messageError);
            }
        });
        //this.password = '';
    };
    LoginPage.prototype.validarDatosLocales = function () {
        var _this = this;
        if (!this.usuario || !this.password) {
            this.toast.presentToast("Usuario o contraseña con campos vacios");
            return;
        }
        this.loadingService.present("Iniciando Sesion...");
        this.tablesManage.createTables().then(function () {
            _this.userService.existeUsuario(_this.usuario, _this.password).then(function (data) {
                console.log('data de la BD', data);
                if (data !== null) { //encontro un usuario
                    var user = data;
                    console.log('data validarDatosLocales', user);
                    var expired = void 0;
                    if (user.TOKEN_EXPIRED !== null) {
                        expired = user.TOKEN_EXPIRED;
                    }
                    else {
                        expired = null;
                    }
                    if (_this.validarSesion.validarSesion(user.TOKEN, user.TOKEN_EXPIRED)) {
                        localStorage.setItem(_this.config.AUTHORIZATION_STRING, user.TOKEN);
                        localStorage.setItem('expiredToken', user.TOKEN_EXPIRED);
                        _this.datosConsulta = user;
                        var userData = user;
                        _this.config.IS_OFFLINE = true;
                        _this.userService.setDatosUsuario(userData);
                        _this.loadingService.dismiss();
                        _this.fecha_hora = new Date();
                        _this.writeLog.escribirLog(_this.fecha_hora + ' Respuesta con exito: ' + JSON.stringify(data) + '\n');
                        _this.guardarIdEIrAMenu();
                    }
                    else {
                        console.log('Token expirado, LOGIN ONLINE');
                        _this.tokenExpirado = true;
                        _this.config.IS_OFFLINE = false;
                        //aca tambien el mismo control
                        _this.iniciarSesion();
                    }
                }
                else {
                    console.log('No hay datos en la tabla o el usuario ingresado no es correcto!, preguntar en la API, LOGIN ONLINE');
                    //tomar la habilitacion actual si es que la hay y ver si existen datos que tienen que ser subidos con esa habilitacion
                    if (localStorage.getItem('habilitacion')) {
                        var habilitacion = Number(localStorage.getItem('habilitacion'));
                        if (habilitacion !== 0) {
                            console.log('Consultar si es que hay datos con esa habilitacion para ver si se sincronizo todo');
                            _this.userService.existenDatosHabilitacion(JSON.parse(localStorage.getItem('habilitacion'))).then(function (resp) {
                                if (resp === true) {
                                    _this.loadingService.dismiss();
                                    var usuario = JSON.parse(localStorage.getItem('user'));
                                    _this.usuarioParse = usuario;
                                    _this.fecha_hora = new Date();
                                    _this.writeLog.escribirLog(_this.fecha_hora + ' Peticion: Inicio de sesion, existen datos que faltan ser subidos con el cobrador  ' + _this.usuarioParse.USUARIO.NOMBRE + '\n');
                                    _this.toast.presentToast("Existen datos que faltan ser subidos con el cobrador " + _this.usuarioParse.USUARIO.NOMBRE + ", inicie sesión nuevamente y aguarde unos minutos la sincronización!!");
                                    return;
                                }
                                else { //no hay datos que sincronizar
                                    _this.config.IS_OFFLINE = false;
                                    _this.iniciarSesion();
                                }
                            }).catch(function (err) {
                                _this.loadingService.dismiss();
                                _this.toast.presentToast('Problemas para consultar la Base de Datos local');
                                console.log('No se puede consultar si exiten datos para la habilitacion', err);
                            });
                        }
                        else { //debe 
                            _this.config.IS_OFFLINE = false;
                            _this.iniciarSesion();
                        }
                    }
                    else { //no hay guardado de habilitacion
                        _this.config.IS_OFFLINE = false;
                        _this.iniciarSesion();
                    }
                }
            }).catch(function (error) {
                _this.loadingService.dismiss();
                _this.toast.presentToast('Problemas para consultar la Base de Datos local');
                console.log('no se pudo obtener los datos de la bd', error);
            });
        }).catch(function (error) {
            _this.loadingService.dismiss();
            _this.toast.presentToast('Problemas para crear las tablas en la bd local');
            console.log('Problemas para crear las tablas en la bd local', error);
        });
    };
    LoginPage = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingControllerService,
            WriteFileService,
            UsuarioService,
            NavController,
            SesionService,
            ConfigService,
            TablesManageService,
            ToastControllerService,
            DatosOnlineService])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map