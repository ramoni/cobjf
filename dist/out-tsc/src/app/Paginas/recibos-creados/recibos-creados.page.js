import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { RecibosLogicaService } from 'src/app/Servicios/reciboService/recibos-logica.service';
import { ActivatedRoute } from '@angular/router';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
var RecibosCreadosPage = /** @class */ (function () {
    function RecibosCreadosPage(navCtrl, logicaRecibo, loadingService, alertCtrl, plat, toast, parametros) {
        this.navCtrl = navCtrl;
        this.logicaRecibo = logicaRecibo;
        this.loadingService = loadingService;
        this.alertCtrl = alertCtrl;
        this.plat = plat;
        this.toast = toast;
        this.parametros = parametros;
        this.type_one = '1';
        this.type_two = '2';
        this.initializeBackButtonCustomHandler();
    }
    RecibosCreadosPage.prototype.ngOnInit = function () {
        var _this = this;
        this.factura = JSON.parse(this.parametros.snapshot.params.datos).factura;
        this.totalFactura = this.factura.total;
        this.totalSaldo = this.factura.saldo;
        this.loadingService.present('Obteniendo Recibos...');
        this.logicaRecibo.getRecibosPorFactura(this.factura).then(function (data) {
            _this.loadingService.dismiss();
            _this.recibos = data;
            console.log('recibos', _this.recibos);
        }).catch(function (error) {
            _this.loadingService.dismiss();
            console.log('No se pudo obtener los recibos de las facturas', error);
            _this.toast.presentToast('No se pudo obtener los recibos de las facturas');
        });
    };
    RecibosCreadosPage.prototype.loadData = function (refresher) {
        var _this = this;
        if (refresher === void 0) { refresher = null; }
        this.logicaRecibo.getRecibosPorFactura(this.factura).then(function (data) {
            _this.recibos = data;
            refresher.target.complete();
        }).catch(function (error) {
            refresher.target.complete();
            console.log('No se pudo obtener los recibos de las facturas', error);
            _this.toast.presentToast('No se pudo obtener los recibos de las facturas');
        });
        ;
    };
    RecibosCreadosPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.navCtrl.pop();
        });
    };
    RecibosCreadosPage.prototype.anular = function (recibo) {
        var _this = this;
        this.alertCtrl.alertMessageWithHandler(this.title, "Esta seguro que desea anular el recibo?", function (d) {
            _this.loadingService.present("Anulando...");
            _this.logicaRecibo.anular(recibo).then(function (response) {
                recibo.estado = 'ANULADO';
                _this.loadingService.dismiss();
                _this.toast.presentToastMedio(response.exito);
            }).catch(function (error) {
                _this.loadingService.dismiss();
                console.log('No se pudo anular el recibo', error);
                _this.toast.presentToast(error.error);
            });
        }, true);
    };
    RecibosCreadosPage.prototype.imprimir = function (recibo, tipo) {
        var _this = this;
        this.loadingService.present("Imprimiendo...");
        this.logicaRecibo.print(recibo, tipo).subscribe(function (response) {
            _this.loadingService.dismiss();
            _this.toast.presentToastMedio(response.data.message);
        }, function (error) {
            _this.loadingService.dismiss();
            console.log('error en impresion de recibo', error);
            _this.toast.presentToast(error);
        });
    };
    RecibosCreadosPage = tslib_1.__decorate([
        Component({
            selector: 'app-recibos-creados',
            templateUrl: './recibos-creados.page.html',
            styleUrls: ['./recibos-creados.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            RecibosLogicaService,
            LoadingControllerService,
            AlertControllerService,
            Platform,
            ToastControllerService,
            ActivatedRoute])
    ], RecibosCreadosPage);
    return RecibosCreadosPage;
}());
export { RecibosCreadosPage };
//# sourceMappingURL=recibos-creados.page.js.map