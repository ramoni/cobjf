import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RecibosCreadosPage } from './recibos-creados.page';
import { PipesModuleModule } from 'src/app/Pipes/pipes-module.module';
var routes = [
    {
        path: '',
        component: RecibosCreadosPage
    }
];
var RecibosCreadosPageModule = /** @class */ (function () {
    function RecibosCreadosPageModule() {
    }
    RecibosCreadosPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                PipesModuleModule,
            ],
            declarations: [RecibosCreadosPage]
        })
    ], RecibosCreadosPageModule);
    return RecibosCreadosPageModule;
}());
export { RecibosCreadosPageModule };
//# sourceMappingURL=recibos-creados.module.js.map