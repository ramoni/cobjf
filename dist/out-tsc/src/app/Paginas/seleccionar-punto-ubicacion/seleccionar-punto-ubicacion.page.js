import * as tslib_1 from "tslib";
import { Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NavParams, NavController, ModalController, Platform } from '@ionic/angular';
var SeleccionarPuntoUbicacionPage = /** @class */ (function () {
    function SeleccionarPuntoUbicacionPage(navCtrl, navParams, modal, geo, plat, zone) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modal = modal;
        this.geo = geo;
        this.plat = plat;
        this.zone = zone;
        this.autocompleteItems = [];
        this.startPos = this.navParams.data.position;
        if (!this.startPos.lat || !this.startPos.lng || this.startPos.lat === 0 || this.startPos.l === 0)
            this.startPos = null;
        console.log("la ubicacion ", this.startPos);
        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.autocomplete = { input: '' };
        this.autocompleteItems = [];
        this.geocoder = new google.maps.Geocoder;
        this.initializeBackButtonCustomHandler();
    }
    SeleccionarPuntoUbicacionPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.dismiss();
        });
    };
    SeleccionarPuntoUbicacionPage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.startPos) {
            console.log('tiene ubicacion');
            this._buildMap(this.startPos.lat, this.startPos.lng);
            this._showPoint(this.startPos.lat, this.startPos.lng);
        }
        else {
            this.geo.getCurrentPosition().then(function (pos) {
                _this._buildMap(pos.coords.latitude, pos.coords.longitude);
                _this._showPoint(pos.coords.latitude, pos.coords.longitude);
            });
        }
    };
    SeleccionarPuntoUbicacionPage.prototype._buildMap = function (lat, lng) {
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: lat, lng: lng },
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    };
    SeleccionarPuntoUbicacionPage.prototype._showPoint = function (lat, lng) {
        console.log('direccion en showpoint', lat, lng);
        this.marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: {
                lat: lat,
                lng: lng
            },
            draggable: true,
        });
        var infoWindow = new google.maps.InfoWindow({
            content: "Ubicacion!"
        });
        google.maps.event.addListener(this.marker, 'click', function () {
            infoWindow.open(this.map, this.marker);
        });
    };
    SeleccionarPuntoUbicacionPage.prototype.aceptar = function () {
        console.log('el marker position', this.marker.position);
        var lat = this.marker.position.lat();
        var long = this.marker.position.lng();
        var ubicacion = { lat: lat, lng: long };
        this.dismissData(ubicacion);
    };
    SeleccionarPuntoUbicacionPage.prototype.updateSearchResults = function () {
        var _this = this;
        if (this.autocomplete.input == '') {
            this.autocompleteItems = [];
            return;
        }
        this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input }, function (predictions) {
            _this.autocompleteItems = [];
            _this.zone.run(function () {
                predictions.forEach(function (prediction) {
                    _this.autocompleteItems.push(prediction);
                });
            });
        });
    };
    SeleccionarPuntoUbicacionPage.prototype.selectSearchResult = function (item) {
        var _this = this;
        //this.clearMarkers();
        this.geocoder.geocode({ 'placeId': item.place_id }, function (results, status) {
            if (status === 'OK' && results[0]) {
                console.log('results', results);
                var position = {
                    lat: results[0].geometry.location.lat,
                    lng: results[0].geometry.location.lng
                };
                /*let marker = new google.maps.Marker({
                  position: results[0].geometry.location,
                  map: this.map,
                });*/
                //this.markers.push(marker);
                //this.map.setCenter(results[0].geometry.location);
                console.log('position ', position);
                _this.autocomplete.input = '';
                _this.autocompleteItems = [];
                _this.map.setCenter(results[0].geometry.location);
                _this._showPoint(Number(position.lat()), Number(position.lng()));
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    SeleccionarPuntoUbicacionPage.prototype.dismissData = function (ubicacion) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modal.dismiss(ubicacion)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SeleccionarPuntoUbicacionPage.prototype.dismiss = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modal.dismiss()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], SeleccionarPuntoUbicacionPage.prototype, "mapElement", void 0);
    SeleccionarPuntoUbicacionPage = tslib_1.__decorate([
        Component({
            selector: 'app-seleccionar-punto-ubicacion',
            templateUrl: './seleccionar-punto-ubicacion.page.html',
            styleUrls: ['./seleccionar-punto-ubicacion.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            NavParams,
            ModalController,
            Geolocation,
            Platform,
            NgZone])
    ], SeleccionarPuntoUbicacionPage);
    return SeleccionarPuntoUbicacionPage;
}());
export { SeleccionarPuntoUbicacionPage };
//# sourceMappingURL=seleccionar-punto-ubicacion.page.js.map