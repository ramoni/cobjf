import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SeleccionarPuntoUbicacionPage } from './seleccionar-punto-ubicacion.page';
var routes = [
    {
        path: '',
        component: SeleccionarPuntoUbicacionPage
    }
];
var SeleccionarPuntoUbicacionPageModule = /** @class */ (function () {
    function SeleccionarPuntoUbicacionPageModule() {
    }
    SeleccionarPuntoUbicacionPageModule = tslib_1.__decorate([
        NgModule({
            entryComponents: [SeleccionarPuntoUbicacionPage],
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [SeleccionarPuntoUbicacionPage]
        })
    ], SeleccionarPuntoUbicacionPageModule);
    return SeleccionarPuntoUbicacionPageModule;
}());
export { SeleccionarPuntoUbicacionPageModule };
//# sourceMappingURL=seleccionar-punto-ubicacion.module.js.map