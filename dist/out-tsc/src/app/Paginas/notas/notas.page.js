import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { NotasLogicaService } from 'src/app/Servicios/notaService/notas-logica.service';
import { ActivatedRoute } from '@angular/router';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
var NotasPage = /** @class */ (function () {
    function NotasPage(logic, loadingService, alertCtrl, navCtrl, ruta, plat, writeLog) {
        this.logic = logic;
        this.loadingService = loadingService;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.ruta = ruta;
        this.plat = plat;
        this.writeLog = writeLog;
        this.nota = null;
        this.title = '¿Guardar nota?';
        this.initializeBackButtonCustomHandler();
    }
    NotasPage.prototype.ngOnInit = function () {
        var datos = JSON.parse(this.ruta.snapshot.params.datos);
        console.log('nota recibida en notas', datos);
        this.notaCobrador = datos.nota;
        this.marca = datos.marca;
        this.id_cliente = datos.idCliente;
        //console.log(this.notaCobrador);
        if (!this.notaCobrador.id) {
            this.notaCobrador.tipo = this.marca;
            this.notaCobrador.id_cliente = this.id_cliente;
            console.log('Escribir nueva nota');
        }
        else {
            this.notaCobrador.tipo = this.marca;
            this.nota = this.notaCobrador.nota;
            console.log('Editar nota');
        }
    };
    NotasPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.navCtrl.pop();
        });
    };
    NotasPage.prototype.guardarNota = function () {
        var _this = this;
        this.notaCobrador.nota = this.nota;
        this.notaCobrador.habilitacion_caja = parseInt(JSON.parse(localStorage.getItem('habilitacion')));
        console.log(this.notaCobrador);
        this.loadingService.present('Guardando...');
        this.logic.addOrUpdateNotas(this.notaCobrador).subscribe(function (response) {
            _this.writeLog.escribirLog(new Date() + 'Peticion: Creación o edicion de nota \n ' + JSON.stringify(_this.notaCobrador));
            _this.loadingService.dismiss();
            _this.mostrarPopUp(response.message.mensaje, function (data) {
                _this.navCtrl.pop();
            });
        }, function (error) {
            _this.mostrarPopUp(error);
            _this.loadingService.dismiss();
        });
    };
    NotasPage.prototype.mostrarPopUp = function (mensaje, handler, conCancelar) {
        if (handler === void 0) { handler = null; }
        if (conCancelar === void 0) { conCancelar = false; }
        var buttons = [{
                text: 'Aceptar',
                handler: function (data) { if (handler)
                    handler(data); }
            }];
        if (conCancelar) {
            buttons.unshift({
                text: 'Cancelar',
                handler: null
            });
        }
        this.simpleAlert(mensaje, buttons);
    };
    NotasPage.prototype.simpleAlert = function (texto, buttons) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: texto,
                            buttons: buttons,
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NotasPage = tslib_1.__decorate([
        Component({
            selector: 'app-notas',
            templateUrl: './notas.page.html',
            styleUrls: ['./notas.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NotasLogicaService,
            LoadingControllerService,
            AlertController,
            NavController,
            ActivatedRoute,
            Platform,
            WriteFileService])
    ], NotasPage);
    return NotasPage;
}());
export { NotasPage };
//# sourceMappingURL=notas.page.js.map