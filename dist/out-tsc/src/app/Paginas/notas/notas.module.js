import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NotasPage } from './notas.page';
var routes = [
    {
        path: '',
        component: NotasPage
    }
];
var NotasPageModule = /** @class */ (function () {
    function NotasPageModule() {
    }
    NotasPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [NotasPage]
        })
    ], NotasPageModule);
    return NotasPageModule;
}());
export { NotasPageModule };
//# sourceMappingURL=notas.module.js.map