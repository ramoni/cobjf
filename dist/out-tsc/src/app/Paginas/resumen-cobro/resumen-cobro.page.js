import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform, } from '@ionic/angular';
import { RecibosLogicaService } from 'src/app/Servicios/reciboService/recibos-logica.service';
import { CreateReceiptData } from 'src/app/Clases/create-receipt-data';
import { ActivatedRoute } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
var ResumenCobroPage = /** @class */ (function () {
    function ResumenCobroPage(navCtrl, logicaRecibos, alertCtrl, loadingService, plat, writeLog, ruta, toast) {
        this.navCtrl = navCtrl;
        this.logicaRecibos = logicaRecibos;
        this.alertCtrl = alertCtrl;
        this.loadingService = loadingService;
        this.plat = plat;
        this.writeLog = writeLog;
        this.ruta = ruta;
        this.toast = toast;
        this.totalRecibo = 0;
        this.totalFCO = 0;
        this.totalFCR = 0;
        this.fcoCheck = false;
        this.reciboCheck = false;
        this.interesCheck = false;
        this.anticipoCheck = false;
        this.todosFCO = false;
        this.todosFCR = false;
        this.printOK = false;
        this.imprimio = false;
        this.imprimioDuplicado = false;
        this.marcaFin = [];
        this.initializeBackButtonCustomHandler();
    }
    ResumenCobroPage.prototype.ngOnInit = function () {
        this.recibo = JSON.parse(this.ruta.snapshot.params.datos).recibo;
        this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente;
        this.moneda = JSON.parse(this.ruta.snapshot.params.datos).moneda;
        this.valores = JSON.parse(this.ruta.snapshot.params.datos).valores;
        this.bancos = JSON.parse(this.ruta.snapshot.params.datos).bancos;
        this.facturas = this.recibo.facturas;
        console.log(this.moneda);
        this._sumatoriaFacturas(this.facturas);
        var date = new Date();
        var time = date.getTime();
        var hash = Md5.hashStr(String(time));
        console.log('hash', hash);
        this.marcaFin.push(hash);
        this.marca = this.marcaFin[0];
        console.log('marca', this.marca);
    };
    ResumenCobroPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    ResumenCobroPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            (_this.navCtrl.pop()).then(function () {
                _this.navCtrl.pop();
            });
        });
    };
    ResumenCobroPage.prototype._sumatoriaFacturas = function (facturas) {
        for (var _i = 0, _a = this.facturas; _i < _a.length; _i++) {
            var f = _a[_i];
            if (f.data.tipo === 'FCO') {
                this.totalFCO += f.valorizado;
            }
            else {
                this.totalFCR += f.valorizado;
            }
        }
        if (this.totalFCO > 0) {
            this.fcoCheck = true;
            this.totalRecibo += this.recibo.interes + this.totalFCO;
            //this.totalFCO +=this.recibo.interes;
        }
        if (this.totalFCR > 0) {
            this.reciboCheck = true;
            this.totalRecibo += +this.totalFCR;
        }
        if (this.recibo.interes > 0) {
            this.interesCheck = true;
            this.totalRecibo += this.recibo.interes;
        }
        if (this.recibo.anticipo > 0) {
            this.anticipoCheck = true;
            this.totalRecibo += this.recibo.anticipo;
        }
    };
    ResumenCobroPage.prototype.finish = function () {
        var _this = this;
        for (var _i = 0, _a = this.facturas; _i < _a.length; _i++) {
            var f = _a[_i];
            if (f.data.tipo === 'FCO') {
                this.todosFCO = true;
            }
            else {
                this.todosFCO = false;
                break;
            }
        }
        for (var _b = 0, _c = this.facturas; _b < _c.length; _b++) {
            var f = _c[_b];
            if (f.data.tipo === 'FCR') {
                this.todosFCR = true;
            }
            else {
                this.todosFCR = false;
                break;
            }
        }
        if ((this.todosFCO === true && this.todosFCR === true) ||
            (this.todosFCO === false && this.todosFCR === true)) {
            this.alertCtrl.alertMessageWithHandler(this.title, "Esta seguro de guardar e imprimir el recibo?", function (d) {
                _this.loadingService.present('Guardando ...');
                _this.recibo.marca = _this.marca;
                _this.writeLog.escribirLog(new Date() + ' Peticion: Guardado de recibo, facturas FCR e impresion original' + JSON.stringify(_this.recibo) + '\n');
                _this.logicaRecibos.addOrUpdate(_this.recibo).subscribe(function (response) {
                    console.log('retorno de addOrUpdate ', response);
                    _this.loadingService.dismiss();
                    _this.writeLog.escribirLog(new Date() + 'Respuesta con exito: ' + JSON.stringify(response.data) + '\n');
                    _this.toast.presentToastMedio(response.data.message);
                    _this.imprimio = true;
                    _this.printOK = true;
                    localStorage.removeItem('reciboGenerado');
                }, function (error) {
                    _this.imprimio = false;
                    _this.printOK = false;
                    _this.loadingService.dismiss();
                    console.log('error en crear/imprimir el recibo original', error);
                    var recibo = new CreateReceiptData();
                    recibo.id = JSON.parse(localStorage.getItem('reciboGenerado'));
                    //console.log('recibo en error de impresiones ', recibo);
                    if (recibo.id !== null) {
                        _this.verificarRecibo();
                        console.log('Tiene que eliminar el recibo generado');
                        localStorage.removeItem('reciboGenerado');
                    }
                    _this.fecha_hora = new Date();
                    _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + error + '\n');
                    _this.toast.presentToast(error);
                });
            }, true);
        }
        else {
            this.alertCtrl.alertMessageWithHandler(this.title, "Esta seguro que desea guardar el cobro?", function (d) {
                _this.loadingService.present('Guardando ...');
                _this.fecha_hora = new Date();
                _this.writeLog.escribirLog(_this.fecha_hora + 'Peticion: Guardado de recibo, facturas FCO' + JSON.stringify(_this.recibo) + '\n');
                _this.recibo.marca = _this.marca;
                console.log(_this.recibo);
                _this.logicaRecibos.addOrUpdateFCO(_this.recibo).subscribe(function (response) {
                    console.log('retorno de addOrUpdateFCO', response);
                    _this.fecha_hora = new Date();
                    _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con exito: ' + JSON.stringify(response.data) + '\n');
                    _this.loadingService.dismiss();
                    _this.imprimio = true;
                    _this.printOK = true;
                    _this.imprimioDuplicado = true;
                }, function (error) {
                    console.log('ocurrio un error para factura FCO', error);
                    _this.imprimio = false;
                    _this.printOK = false;
                    _this.imprimioDuplicado = false;
                    _this.fecha_hora = new Date();
                    _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + error + '\n');
                    _this.toast.presentToast(error);
                    _this.loadingService.dismiss();
                });
            }, true);
        }
    };
    ResumenCobroPage.prototype.imprimirDuplicado = function () {
        var _this = this;
        this.loadingService.present('Imprimiendo ...');
        setTimeout(function () {
            _this.fecha_hora = new Date();
            _this.writeLog.escribirLog(_this.fecha_hora + 'Peticion: Guardado de recibo, facturas FCR e impresion duplicado' + JSON.stringify(localStorage.getItem('recibo')) + '\n');
            _this.logicaRecibos.printDuplicado().subscribe(function (response) {
                _this.loadingService.dismiss();
                _this.fecha_hora = new Date();
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con exito: ' + JSON.stringify(response) + '\n');
                _this.imprimioDuplicado = true;
                _this.toast.presentToastMedio('Recibo impreso correctamente');
            }, function (error) {
                console.log('error en imprimiendo');
                _this.loadingService.dismiss();
                console.log('error en imprimir el recibo ', error);
                _this.fecha_hora = new Date();
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + error + '\n');
                _this.toast.presentToast(error);
            });
        }, 2000);
    };
    ResumenCobroPage.prototype.verificarRecibo = function () {
        this.imprimio = true;
        this.printOK = true;
    };
    ResumenCobroPage.prototype.volverFacturas = function () {
        this.navCtrl.navigateBack(['facturas',
            this.cliente
        ]);
    };
    ResumenCobroPage.prototype.esCero = function (total) {
        if (total > 0) {
            return false;
        }
        else {
            return true;
        }
    };
    ResumenCobroPage = tslib_1.__decorate([
        Component({
            selector: 'app-resumen-cobro',
            templateUrl: './resumen-cobro.page.html',
            styleUrls: ['./resumen-cobro.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            RecibosLogicaService,
            AlertControllerService,
            LoadingControllerService,
            Platform,
            WriteFileService,
            ActivatedRoute,
            ToastControllerService])
    ], ResumenCobroPage);
    return ResumenCobroPage;
}());
export { ResumenCobroPage };
//# sourceMappingURL=resumen-cobro.page.js.map