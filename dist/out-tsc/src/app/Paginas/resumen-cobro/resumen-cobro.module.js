import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ResumenCobroPage } from './resumen-cobro.page';
import { PipesModuleModule } from 'src/app/Pipes/pipes-module.module';
var routes = [
    {
        path: '',
        component: ResumenCobroPage
    }
];
var ResumenCobroPageModule = /** @class */ (function () {
    function ResumenCobroPageModule() {
    }
    ResumenCobroPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                PipesModuleModule,
            ],
            declarations: [ResumenCobroPage]
        })
    ], ResumenCobroPageModule);
    return ResumenCobroPageModule;
}());
export { ResumenCobroPageModule };
//# sourceMappingURL=resumen-cobro.module.js.map