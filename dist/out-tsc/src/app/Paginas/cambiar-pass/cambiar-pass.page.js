import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { HttpService } from 'src/app/Servicios/httpFunctions/http.service';
import { FormBuilder, Validators } from '@angular/forms';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
var CambiarPassPage = /** @class */ (function () {
    function CambiarPassPage(navCtrl, form, helper, loadingService, plat, toast) {
        this.navCtrl = navCtrl;
        this.form = form;
        this.helper = helper;
        this.loadingService = loadingService;
        this.plat = plat;
        this.toast = toast;
        this.mostrarMensaje = false;
        this.mostrarError = false;
        this.mostrarError1 = false;
        this.passwordForm = this.form.group({
            passwordActual: [null, Validators.required],
            passwordNuevo: [null, [Validators.required, Validators.minLength(6)]],
            passwordNuevo1: [null, [Validators.required, Validators.minLength(6)]]
        });
    }
    CambiarPassPage.prototype.ngOnInit = function () {
    };
    CambiarPassPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribe(function () {
            _this.navCtrl.navigateRoot(['home']);
        });
    };
    CambiarPassPage.prototype.ionViewDidEnter = function () {
        this.initializeBackButtonCustomHandler();
    };
    CambiarPassPage.prototype.guardar = function () {
        if (this.verificarPass() === false) {
            //los passwords no son iguales//
            this.passwordForm.controls['passwordNuevo'].setValue(null);
            this.passwordForm.controls['passwordNuevo1'].setValue(null);
        }
        else {
            this.cambiarContraseña(this.passwordForm.controls['passwordActual'].value, this.passwordForm.controls['passwordNuevo'].value, this.passwordForm.controls['passwordNuevo1'].value);
        }
    };
    CambiarPassPage.prototype.verificarPass = function () {
        if (this.passwordForm.controls['passwordNuevo'].value !== this.passwordForm.controls['passwordNuevo1'].value) {
            this.mostrarMensaje = true;
            return false;
        }
    };
    CambiarPassPage.prototype.cambiarContraseña = function (passActual, passNuevo, passConfir) {
        var _this = this;
        this.loadingService.present('Cambiando contraseña...');
        return this.helper.doPost('cambiar_password?', {
            password_actual: passActual,
            password_nuevo: passNuevo,
            password_nuevo1: passConfir
        }, {}).then(function (response) {
            var data = JSON.parse(response.data);
            console.log('cambio contrasenha ', data);
            _this.toast.presentToastMedio(data.exito);
            _this.loadingService.dismiss();
            _this.navCtrl.navigateRoot(['login']);
        }).catch(function (error) {
            _this.loadingService.dismiss();
            if (error.status === 401) {
                _this.toast.presentToast("La contraseña actual no corresponde a este usuario");
            }
            else if (error.status === 400) {
                _this.toast.presentToast("Debes ingresar la misma contraseña dos veces para confirmarla");
            }
            else {
                _this.toast.presentToast("El servidor no se encuentra disponible, verifique su conexion e intente nuevamente");
            }
        });
    };
    CambiarPassPage.prototype.togglePasswordTypePA = function () {
        this.passwordActual = this.passwordActual || 'password';
        this.passwordActual = (this.passwordActual === 'password') ? 'text' : 'password';
    };
    CambiarPassPage.prototype.togglePasswordTypePN = function () {
        this.passwordNuevo = this.passwordNuevo || 'password';
        this.passwordNuevo = (this.passwordNuevo === 'password') ? 'text' : 'password';
    };
    CambiarPassPage.prototype.togglePasswordTypePN1 = function () {
        this.passwordNuevo1 = this.passwordNuevo1 || 'password';
        this.passwordNuevo1 = (this.passwordNuevo1 === 'password') ? 'text' : 'password';
    };
    CambiarPassPage = tslib_1.__decorate([
        Component({
            selector: 'app-cambiar-pass',
            templateUrl: './cambiar-pass.page.html',
            styleUrls: ['./cambiar-pass.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            FormBuilder,
            HttpService,
            LoadingControllerService,
            Platform,
            ToastControllerService])
    ], CambiarPassPage);
    return CambiarPassPage;
}());
export { CambiarPassPage };
//# sourceMappingURL=cambiar-pass.page.js.map