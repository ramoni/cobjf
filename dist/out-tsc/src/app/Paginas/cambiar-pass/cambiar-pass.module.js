import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CambiarPassPage } from './cambiar-pass.page';
var routes = [
    {
        path: '',
        component: CambiarPassPage
    }
];
var CambiarPassPageModule = /** @class */ (function () {
    function CambiarPassPageModule() {
    }
    CambiarPassPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                FormsModule,
                ReactiveFormsModule,
            ],
            declarations: [CambiarPassPage]
        })
    ], CambiarPassPageModule);
    return CambiarPassPageModule;
}());
export { CambiarPassPageModule };
//# sourceMappingURL=cambiar-pass.module.js.map