import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { GeolocalizacionLogicaService } from 'src/app/Servicios/geolocalizacionService/geolocalizacion-logica.service';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { UsuarioService } from 'src/app/Servicios/usuarioService/usuario.service';
import { DatosOnlineService } from 'src/app/Servicios/onlineServices/datos-online.service';
import { HabilitacionesBDService } from 'src/app/Servicios/AlmacenamientoSQLITE/habilitacionesBD/habilitaciones-bd.service';
import { ActividadesLocalesService } from 'src/app/Servicios/actividadesLocales/actividades-locales.service';
import { SincronizacionService } from 'src/app/Servicios/sincronizacionService/sincronizacion.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, geo, loadingService, userService, plat, writeLog, toastService, alertService, onlineMethods, actividadesLocalesService, habilitacionService, sincronizacionService) {
        this.navCtrl = navCtrl;
        this.geo = geo;
        this.loadingService = loadingService;
        this.userService = userService;
        this.plat = plat;
        this.writeLog = writeLog;
        this.toastService = toastService;
        this.alertService = alertService;
        this.onlineMethods = onlineMethods;
        this.actividadesLocalesService = actividadesLocalesService;
        this.habilitacionService = habilitacionService;
        this.sincronizacionService = sincronizacionService;
        this.datosSubida = 0;
        this.datosBajada = 0;
        this.recibos = 0;
        this.entregas = 0;
        this.notasVisitas = 0;
        this.notasEntregas = 0;
        this.cargando = true;
    }
    HomePage.prototype.ngOnInit = function () {
        this.v = JSON.parse(localStorage.getItem('version'));
        this.userData = this.userService.getDatosUsuario();
        this.habilitacion_caja = JSON.parse(localStorage.getItem('habilitacion'));
        this.getGeo();
        this.bajandoDatosOnline();
    };
    HomePage.prototype.ionViewDidEnter = function () {
        this.initializeBackButtonCustomHandler();
        this.initActividadesLocales();
        this.mostrarUsoDatosApp();
    };
    HomePage.prototype.bajandoDatosOnline = function () {
        var _this = this;
        var intervaloDatosOnline = setInterval(function () {
            var bajandoDatos = JSON.parse(localStorage.getItem("bajandoDatosOnline"));
            console.log("VALOR DE BAJANDO DATOS ONLINE ", bajandoDatos);
            if (bajandoDatos == true) {
                _this.cargando = true;
            }
            else {
                //Se deja de realizar el intervalo
                clearInterval(intervaloDatosOnline);
                _this.cargando = false;
            }
        }, 1000);
    };
    HomePage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.alertService.confirmExitAPP();
        });
    };
    HomePage.prototype.getGeo = function () {
        var _this = this;
        this.geo.findCurrentCiudad().subscribe(function (ciudad) {
            _this.lugar = ciudad;
            console.log(_this.lugar);
        }, function (error) {
            console.log("error en la ubicacion ", error);
            _this.lugar = 'No se puede obtener la ubicacion';
        });
    };
    HomePage.prototype.openDocumentos = function () {
        this.navCtrl.navigateForward(['actividades', {
                datos: JSON.stringify({
                    marca: "documentos"
                })
            }]);
    };
    HomePage.prototype.openCobros = function () {
        this.navCtrl.navigateForward(['actividades', {
                datos: JSON.stringify({
                    marca: "visitas"
                })
            }]);
    };
    HomePage.prototype.cerrarCaja = function (habilitacion) {
        var _this = this;
        this.fecha_hora = new Date();
        this.writeLog.escribirLog(this.fecha_hora + 'Peticion: Cierre de habilitacion  ' + habilitacion + '\n');
        this.alertService.alertMessageWithHandler(this.title, "Esta seguro que desea cerrar su caja?", function (d) {
            _this.loadingService.present('Verificando datos ...');
            //OFFLINE
            _this.userService.existenDatosHabilitacion(JSON.parse(localStorage.getItem('habilitacion'))).then(function (resp) {
                _this.loadingService.dismiss();
                if (resp) {
                    _this.toastService.presentToast("Existen datos que faltan ser subidos, aguarde unos minutos, verifique su conexion a internet e intente nuevamente!");
                    return;
                }
                else {
                    _this.loadingService.present('Cerrando...');
                    _this.userService.cambiarHabilitacion(habilitacion).then(function (response) {
                        _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con exito: ' + JSON.stringify(response) + '\n');
                        _this.toastService.presentToastMedio(response.message);
                        _this.habilitacion_caja = JSON.parse(localStorage.getItem('habilitacion'));
                        _this.datosSubida = 0;
                        _this.datosBajada = 0;
                        _this.writeLog.escribirLog(new Date() + " Petición: Cierre de caja \n");
                        _this.habilitacionService.cerrarHabilitacionLocal(habilitacion).then(function (data) {
                            console.log('Cierre de caja local ', data);
                            _this.loadingService.dismiss();
                            return _this.habilitacion_caja;
                        }).catch(function (error) {
                            _this.toastService.presentToast("Error en cerrar caja localmente");
                            console.error("Error en cierre local", error);
                            _this.writeLog.escribirLog(new Date() + " Cierre de caja con errores \nError: " + JSON.stringify(error) + " \n");
                            _this.loadingService.dismiss();
                        });
                        _this.cantidad_datos = 0;
                    }).catch(function (error) {
                        _this.loadingService.dismiss();
                        _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + error + '\n');
                        _this.toastService.presentToast(JSON.parse(error.error).error);
                    });
                }
            });
        }, true);
    };
    HomePage.prototype.initActividadesLocales = function () {
        var _this = this;
        this.habilitacion_caja = JSON.parse(localStorage.getItem('habilitacion'));
        this.actividadesLocalesService.mostrarActividadesLocales(this.habilitacion_caja).then(function (data) {
            var respuesta = data;
            console.log('La respuesta de actividades locales ', respuesta);
            _this.recibos = respuesta.RECIBOS;
            _this.entregas = respuesta.ENTREGAS;
            _this.notasVisitas = respuesta.NOTAS_VISITAS;
            _this.notasEntregas = respuesta.NOTAS_ENTREGAS;
        });
    };
    HomePage.prototype.mostrarUsoDatosApp = function () {
        var subida = Number(localStorage.getItem('subida'));
        var bajada = Number(localStorage.getItem('bajada'));
        var calculo1 = (subida / 10000).toFixed(1);
        var calculo2 = (bajada / 10000).toFixed(1);
        this.datosSubida = Number(calculo1);
        this.datosBajada = Number(calculo2);
        this.cantidad_datos = Number(calculo1) + Number(calculo2);
        localStorage.setItem('datos', JSON.stringify(subida + bajada));
        console.log('subida: ', subida, 'bajada: ', bajada);
    };
    HomePage.prototype.refrescarHome = function (event) {
        var _this = this;
        this.writeLog.escribirLog(new Date() + ' Peticion: Refrescar Datos App \n');
        this.onlineMethods.bajarDatosOnline().then(function () {
            _this.writeLog.escribirLog(new Date() + 'Respuesta con exito: SE DESCARGARON TODOS LOS DATOS \n');
            _this.writeLog.escribirLog(new Date() + 'Peticion: Actualizar Datos de usuario \n');
            _this.onlineMethods.obtenerDatosInicio().then(function (data) {
                _this.writeLog.escribirLog(new Date() + 'Respuesta con exito: Datos de usuario actualizados \n');
                console.log('data refresh user', data);
                _this.userData = data;
                console.log('user Data', _this.userData);
                localStorage.setItem('habilitacion', JSON.stringify(_this.userData.HABILITACION));
                _this.habilitacion_caja = _this.userData.HABILITACION;
                localStorage.setItem('user', JSON.stringify(data));
                event.target.complete();
                return _this.userData, _this.getGeo(), _this.habilitacion_caja;
            }).catch(function (error) {
                _this.writeLog.escribirLog(new Date() + 'Respuesta con error: Datos de usuario no actualizados \n');
                console.error('error al obtener datos del user', error);
            });
        }).catch(function () {
            _this.writeLog.escribirLog(new Date() + 'Respuesta con error: NO SE PUDIERON DESCARGAR TODOS LOS DATOS \n');
        });
    };
    HomePage.prototype.sincronizarTodoUnavez = function () {
        var habilitacion = this.habilitacion_caja;
        console.log("Sincronizar recibos de habilitacion " + habilitacion);
        this.sincronizacionService.sincronizarTodoUnavez();
    };
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: './home.page.html',
            styleUrls: ['./home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            GeolocalizacionLogicaService,
            LoadingControllerService,
            UsuarioService,
            Platform,
            WriteFileService,
            ToastControllerService,
            AlertControllerService,
            DatosOnlineService,
            ActividadesLocalesService,
            HabilitacionesBDService,
            SincronizacionService])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map