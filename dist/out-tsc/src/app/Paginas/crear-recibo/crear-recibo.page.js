import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { RecibosLogicaService } from 'src/app/Servicios/reciboService/recibos-logica.service';
import { ActivatedRoute } from '@angular/router';
import { ConceptoLogicaService } from 'src/app/Servicios/conceptoService/concepto-logica.service';
import { CuentaLogicaService } from 'src/app/Servicios/cuentaService/cuenta-logica.service';
import { EntidadLogicaService } from 'src/app/Servicios/entidadService/entidad-logica.service';
import { MonedaLogicaService } from 'src/app/Servicios/monedaService/moneda-logica.service';
import { ValoresLogicaService } from 'src/app/Servicios/valorService/valores-logica.service';
import * as moment from 'moment';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
var CrearReciboPage = /** @class */ (function () {
    function CrearReciboPage(navCtrl, logic, monedaLogic, valorLogic, entidadLogic, cuentaLogic, plat, conceptoLogic, ruta, alertService) {
        this.navCtrl = navCtrl;
        this.logic = logic;
        this.monedaLogic = monedaLogic;
        this.valorLogic = valorLogic;
        this.entidadLogic = entidadLogic;
        this.cuentaLogic = cuentaLogic;
        this.plat = plat;
        this.conceptoLogic = conceptoLogic;
        this.ruta = ruta;
        this.alertService = alertService;
        this.totalFacturas = 0;
        this.totalValores = 0;
        this.tab = 'datos';
        this.isChecked = false;
        this.interesAux = 0;
        this.interesAux2 = 0;
        this.construct = true;
        this.totalInteres = 0;
        this.monedaEnable = false;
        this.DECIMAL_SEPARATOR = ".";
        this.GROUP_SEPARATOR = ",";
        this.total_aux = [];
        this.dataSync = false;
        this.facturas = [];
        this.initializeBackButtonCustomHandler();
    }
    ;
    CrearReciboPage.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSync = false;
        this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente;
        this.facturaCobrar = JSON.parse(this.ruta.snapshot.params.datos).facturas;
        this.moneda = JSON.parse(this.ruta.snapshot.params.datos).monedaRecibo;
        this.facturas = this.facturaCobrar;
        this.recibo = null;
        console.log('facturas a cobrar en recibos ', this.facturaCobrar);
        //console.log('recibo en recibos ', this.recibo)
        this.utiles().then(function () {
            _this.dataSync = true;
            if (!_this.recibo) {
                _this.recibo = _this.logic.getDefaultReceipt(_this.cliente, _this.facturas);
                _this.recibo.id_moneda = _this.moneda.id;
                _this.recibo.cotizacion = _this.valorLogic.getCotizacion();
                _this.facturaInteres = _this.facturas;
                _this.addDefaultValor();
                _this.title = 'Carga';
            }
            else {
                _this.moneda = _this.monedaLogic.getByIdOrName(_this.recibo.id_moneda.toString());
                _this.title = 'Edicion';
            }
            console.log(_this.recibo);
            _this.fecha = moment(_this.recibo.fecha).format('YYYY-MM-DD');
            console.log('fecha recibo', _this.fecha);
            _this.recalcularTotalFacturas();
            _this.recalcularTotalValores();
        });
    };
    CrearReciboPage.prototype.utiles = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e;
            return tslib_1.__generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.monedaLogic.getMonedasBD()];
                    case 1:
                        _a.monedas = _f.sent();
                        //console.log('monedas en crear-recibo', this.monedas)
                        _b = this;
                        return [4 /*yield*/, this.valorLogic.getValoresBD()];
                    case 2:
                        //console.log('monedas en crear-recibo', this.monedas)
                        _b.tipoValores = _f.sent();
                        console.log('valores en crear-recibo', this.tipoValores);
                        _c = this;
                        return [4 /*yield*/, this.entidadLogic.getEntidadesBD()];
                    case 3:
                        _c.entidades = _f.sent();
                        //console.log('entidades en crear-recibo', this.entidades)
                        _d = this;
                        return [4 /*yield*/, this.cuentaLogic.getCuentasBD()];
                    case 4:
                        //console.log('entidades en crear-recibo', this.entidades)
                        _d.cuentas = _f.sent();
                        //console.log('cuentas en crear-recibo', this.cuentas)
                        _e = this;
                        return [4 /*yield*/, this.conceptoLogic.getConceptosBD()];
                    case 5:
                        //console.log('cuentas en crear-recibo', this.cuentas)
                        _e.conceptos = _f.sent();
                        //console.log('conceptos en crear-recibo', this.conceptos)
                        return [2 /*return*/, (this.monedas, this.tipoValores, this.entidades, this.cuentas, this.conceptos)];
                }
            });
        });
    };
    CrearReciboPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.navCtrl.pop();
        });
    };
    CrearReciboPage.prototype.addDefaultValor = function () {
        console.log('tipoValores', this.tipoValores[0]);
        var valor = this.tipoValores[0];
        for (var _i = 0, _a = this.monedas; _i < _a.length; _i++) {
            var mon = _a[_i];
            //console.log('mon; ', mon, 'monedas', this.monedas)
            if (mon.id === valor.id_moneda) {
                this.monedaValor = mon;
                break;
            }
        }
        console.log('monedaValor addDefaultValor', this.monedaValor);
        this.recibo.valores.push(this.logic.getDefaultValor(this.monedaValor));
    };
    CrearReciboPage.prototype.removeValor = function (valor) {
        if (this.recibo.valores.length === 1) {
            this.alertService.alertMessageWithHandler('Error', 'Debe haber al menos un valor');
            return;
        }
        this.recibo.valores = this.recibo.valores.filter(function (val) { return val !== valor; });
        this.recalcularTotalValores();
    };
    CrearReciboPage.prototype.onMonedaChange = function () {
        console.log('onMonedaChange');
        this.monedaAux = this.monedaLogic.getByIdOrName(this.moneda.id);
        this.moneda = this.monedaAux;
        this.recibo.id_moneda = this.moneda.id;
        this.recalcularTotalFacturas();
        this.recalcularTotalValores();
    };
    CrearReciboPage.prototype.setearMonedaValor = function (i) {
        //console.log('La moneda cambiada en el valor ', this.recibo.valores[i].moneda);
        console.log('setearMonedaValor');
        this.recibo.valores[i].tipo.id_moneda = this.recibo.valores[i].moneda.id;
        this.recalcularTotalValores();
    };
    CrearReciboPage.prototype.calcularInteres = function (facturas) {
        //this.loadingService.present('Obteniendo interes...')
        for (var i = 0; i < facturas.length; i++) {
            var interes = facturas[i].interes;
            this.sumarInteres(interes, facturas[i].moneda);
        }
        // this.loadingService.dismiss()
    };
    CrearReciboPage.prototype.sumarInteres = function (data, moneda) {
        this.monedaAux = this.monedaLogic.getByIdOrName(moneda);
        if (this.moneda.descripcion === moneda) {
            this.totalInteres += parseFloat(data.toFixed(this.moneda.cant_decimales));
            this.totalFacturas += parseFloat(data.toFixed(this.moneda.cant_decimales));
            console.log('es igual a las monedas ', this.totalInteres, this.totalFacturas);
        }
        else {
            if (data > 0) {
                this.interesAux = 0;
                this.interesAux = this.monedaLogic.convert(data, this.monedaAux, this.moneda, this.recibo.cotizacion);
                console.log('interes auxiliar ', this.interesAux);
                this.totalFacturas += parseFloat(this.interesAux.toFixed(this.moneda.cant_decimales));
                this.totalInteres += parseFloat(this.interesAux.toFixed(this.moneda.cant_decimales));
                console.log('total interes ', this.totalInteres);
                console.log('total facturas', this.totalFacturas);
            }
        }
        this.recibo.interes = this.totalInteres;
        return this.recibo.interes;
    };
    CrearReciboPage.prototype.recalcularTotalFacturas = function () {
        console.log('recalcularTotalFacturas');
        this.totalFacturas = 0;
        for (var _i = 0, _a = this.recibo.facturas; _i < _a.length; _i++) {
            var f = _a[_i];
            if (f.a_cobrar > f.data.saldo) {
                this.title = 'carga';
                //return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'El monto a cobrar no debe superar el saldo de la factura!');
                return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'No puede realizarse mas de un recibo en el dia, a una misma factura!');
            }
            else {
                f.a_cobrar = +f.a_cobrar;
                if (this.moneda.descripcion === f.data.moneda) {
                    f.valorizado = f.a_cobrar;
                }
                else {
                    this.value = this.monedaLogic.convertByDesc(f.a_cobrar, f.data.moneda, this.moneda.descripcion, this.recibo.cotizacion)
                        .toFixed(this.moneda.cant_decimales);
                    f.valorizado = Number(this.value);
                }
                this.totalFacturas += f.valorizado;
            }
        }
        var redondeoFac = this.totalFacturas.toFixed(this.moneda.cant_decimales);
        this.totalFacturas = Number(redondeoFac);
        if (this.isChecked) {
            this.totalInteres = 0;
            this.recibo.interes = 0;
            this.calcularInteres(this.facturaInteres);
        }
        else {
            this.totalInteres = 0;
            this.recibo.interes = 0;
            this.totalFacturas = this.totalFacturas;
        }
        var total = this.totalFacturas.toFixed(this.moneda.cant_decimales);
        this.totalFacturas = Number(total);
    };
    CrearReciboPage.prototype.recalcularTotalValores = function () {
        console.log('recalcularTotalValores');
        this.totalValores = 0;
        for (var _i = 0, _a = this.recibo.valores; _i < _a.length; _i++) {
            var f = _a[_i];
            f.total = +f.total;
            if (this.moneda.id === f.moneda.id) {
                f.valorizado = f.total;
            }
            else {
                f.valorizado = Number(this.monedaLogic.convert(f.total, f.moneda, this.moneda, this.recibo.cotizacion)
                    .toFixed(this.moneda.cant_decimales));
            }
            this.totalValores += f.valorizado;
        }
    };
    CrearReciboPage.prototype.limpiarValor = function (i) {
        console.log('limpiarValores ');
        this.recibo.valores[i].banco = null;
        this.recibo.valores[i].cuenta = null;
        this.recibo.valores[i].numero = "0";
        var valorSeleccionado = this.valorLogic.getByIdOrName(this.recibo.valores[i].tipo.id);
        console.log('valor seleccionado', valorSeleccionado);
        this.recibo.valores[i].tipo.id = valorSeleccionado.id;
        this.recibo.valores[i].tipo.descripcion = valorSeleccionado.descripcion;
        this.recibo.valores[i].tipo.has_banco = valorSeleccionado.has_banco;
        this.recibo.valores[i].tipo.has_cuenta = valorSeleccionado.has_cuenta;
        this.recibo.valores[i].tipo.has_fecha = valorSeleccionado.has_fecha;
        this.recibo.valores[i].tipo.has_number = valorSeleccionado.has_number;
        this.recibo.valores[i].tipo.abreviacion = valorSeleccionado.abreviacion;
        //console.log('ID moned del valor ',valorSeleccionado.id_moneda);
        if (valorSeleccionado.id_moneda === '0') {
            console.log('No tiene moneda por defecto true');
            this.recibo.valores[i].tipo.id_moneda = this.moneda.id;
            this.recibo.valores[i].modificarMoneda = true;
        }
        else {
            console.log('Tiene moneda por defecto false');
            this.recibo.valores[i].tipo.id_moneda = valorSeleccionado.id_moneda;
            this.recibo.valores[i].modificarMoneda = false;
            //this.monedaEnable = false;
        }
        if (this.recibo.valores[i].tipo.has_fecha === 1) {
            console.log('fecha de recibo', this.recibo.fecha);
            this.recibo.valores[i].fecha = moment(this.recibo.fecha).format('YYYY-MM-DD');
            console.log('fecha de recibo valor moment', this.recibo.valores[i].fecha);
        }
        else {
            this.recibo.valores[i].fecha = null;
        }
        if (this.recibo.valores[i].tipo.has_cuenta === 1) {
            console.log('maneja cuenta');
        }
        for (var _i = 0, _a = this.monedas; _i < _a.length; _i++) {
            var moneda = _a[_i];
            if (moneda.id === this.recibo.valores[i].tipo.id_moneda) {
                this.monedaEnable = false;
                console.log('Encuentra la moneda del valor');
                this.recibo.valores[i].moneda = moneda;
            }
        }
        /*if (band === fal
    
    
    
        /*this.recibo.valores[i].banco = null;
        this.recibo.valores[i].cuenta = null;
        this.recibo.valores[i].numero = "0";
        if (this.recibo.valores[i].tipo.has_fecha === 1) {
          this.recibo.valores[i].fecha = moment(this.recibo.fecha).format('YYYY-MM-DD');
        } else {
          this.recibo.valores[i].fecha = null;
        }
        let band: boolean = false;
        console.log('recibo valores tipo id_moneda ', this.recibo.valores[i].tipo.id_moneda)
        for (let moneda of this.monedas) {
          if (moneda.id === this.recibo.valores[i].tipo.id_moneda) {
            this.monedaEnable = false;
            this.recibo.valores[i].moneda = moneda;
            band = true;
          }
    
        }
        console.log('band', band)
        if (band === false) {
          //preguntar aca si es retencion y acreditacion en cuenta que no este desactivado para cambiar la moneda eso nomas//
          console.log('id valor ', this.recibo.valores[i].tipo.id);
          if (this.recibo.valores[i].tipo.id === '12' || this.recibo.valores[i].tipo.id === '10') {
            this.monedaEnable = true;
          } else {
            this.monedaEnable = false;
          }
          this.recibo.valores[i].moneda = this.moneda;
        }*/
    };
    CrearReciboPage.prototype.changeCuenta = function (i) {
        for (var _i = 0, _a = this.cuentas; _i < _a.length; _i++) {
            var cuenta = _a[_i];
            if (this.recibo.valores[i].cuenta === cuenta.id) {
                for (var _b = 0, _c = this.monedas; _b < _c.length; _b++) {
                    var moneda = _c[_b];
                    if (moneda.id === cuenta.id_moneda) {
                        this.recibo.valores[i].moneda = moneda;
                        break;
                    }
                }
            }
        }
    };
    CrearReciboPage.prototype.finish = function () {
        //para controlar valores con numeros que sean requeridos el numero de cheque, fecha y banco
        for (var i = 0; i < this.recibo.valores.length; i++) {
            if (this.recibo.valores[i].tipo.has_number === 1) {
                if (this.recibo.valores[i].numero === "0" || this.recibo.valores[i].numero === "") {
                    return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese el numero de cheque/ cuenta o retencion!');
                }
            }
            if (this.recibo.valores[i].tipo.has_fecha === 1) {
                if (this.recibo.valores[i].fecha === null || this.recibo.valores[i].fecha === "") {
                    return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Seleccione la fecha del cheque/ cuenta o retencion!');
                }
            }
            if (this.recibo.valores[i].tipo.has_banco === 1) {
                if (this.recibo.valores[i].banco === null) {
                    return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Seleccione el banco correspondiente!');
                }
            }
            //para acreditacion en cuenta//
            if (this.recibo.valores[i].tipo.has_cuenta === 1) {
                if (this.recibo.valores[i].cuenta === null) {
                    return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Seleccione una cuenta!');
                }
            }
        }
        if (this.totalFacturas <= 0) {
            return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese al menos un monto para una factura mayor a 0');
        }
        if (this.recibo.cotizacion <= 0) {
            return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese una cotizacion valida, al menos la cotizacion del dia');
        }
        if (this.totalValores <= 0) {
            return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'Ingrese al menos un valor');
        }
        if (!this.isChecked) {
            this.recibo.interes = 0;
        }
        if (this.totalFacturas - this.totalValores > 0.0001) {
            return this.alertService.alertMessageWithHandler('Error en la ' + this.title, 'El total de los valores es menor al total a cobrar');
        }
        else {
            this.recibo.anticipo = Number((this.totalValores - this.totalFacturas).toFixed(this.moneda.cant_decimales));
        }
        this.recibo.total = this.totalValores;
        this.navCtrl.navigateForward(['resumen-cobro', {
                datos: JSON.stringify({
                    recibo: this.recibo,
                    cliente: this.cliente,
                    decimalesMoneda: this.moneda.cant_decimales,
                    moneda: this.moneda,
                    bancos: this.entidades,
                    valores: this.tipoValores
                })
            }]);
    };
    CrearReciboPage = tslib_1.__decorate([
        Component({
            selector: 'app-crear-recibo',
            templateUrl: './crear-recibo.page.html',
            styleUrls: ['./crear-recibo.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            RecibosLogicaService,
            MonedaLogicaService,
            ValoresLogicaService,
            EntidadLogicaService,
            CuentaLogicaService,
            Platform,
            ConceptoLogicaService,
            ActivatedRoute,
            AlertControllerService])
    ], CrearReciboPage);
    return CrearReciboPage;
}());
export { CrearReciboPage };
//# sourceMappingURL=crear-recibo.page.js.map