import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CrearReciboPage } from './crear-recibo.page';
import { PipesModuleModule } from 'src/app/Pipes/pipes-module.module';
var routes = [
    {
        path: '',
        component: CrearReciboPage
    }
];
var CrearReciboPageModule = /** @class */ (function () {
    function CrearReciboPageModule() {
    }
    CrearReciboPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                PipesModuleModule,
            ],
            declarations: [CrearReciboPage]
        })
    ], CrearReciboPageModule);
    return CrearReciboPageModule;
}());
export { CrearReciboPageModule };
//# sourceMappingURL=crear-recibo.module.js.map