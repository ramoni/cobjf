import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ActividadesConClientesPage } from './actividades-con-clientes.page';
var routes = [
    {
        path: '',
        component: ActividadesConClientesPage
    }
];
var ActividadesConClientesPageModule = /** @class */ (function () {
    function ActividadesConClientesPageModule() {
    }
    ActividadesConClientesPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ActividadesConClientesPage]
        })
    ], ActividadesConClientesPageModule);
    return ActividadesConClientesPageModule;
}());
export { ActividadesConClientesPageModule };
//# sourceMappingURL=actividades-con-clientes.module.js.map