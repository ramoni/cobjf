import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { SeleccionarPuntoUbicacionPage } from '../seleccionar-punto-ubicacion/seleccionar-punto-ubicacion.page';
import { ClientesLogicaService } from 'src/app/Servicios/clienteService/clientes-logica.service';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ClientesBDService } from 'src/app/Servicios/almacenamientoSQLITE/clientesBD/clientes-bd.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
var ActividadesConClientesPage = /** @class */ (function () {
    function ActividadesConClientesPage(navCtrl, modalController, platform, logicCliente, loadingService, toast, plat, ruta, clientesBDService) {
        this.navCtrl = navCtrl;
        this.modalController = modalController;
        this.platform = platform;
        this.logicCliente = logicCliente;
        this.loadingService = loadingService;
        this.toast = toast;
        this.plat = plat;
        this.ruta = ruta;
        this.clientesBDService = clientesBDService;
        this.nextOffset = 0;
        this.limit = 10;
        this.noMoreData = false;
        this.clientes = [];
        this.infinite = true;
        this.cliente = null;
        this.opciones = null;
        this.tipoFiltro = null;
        this.opciones = JSON.parse(this.ruta.snapshot.params.datos).marca;
        console.log("llego a clientes la marca= ", this.opciones);
    }
    ActividadesConClientesPage.prototype.ngOnInit = function () {
        ///////////////////////////////////////////
        var hosts = document.querySelectorAll('.is-list-condensed ion-item-option');
        Array.from(hosts).forEach(function (host) {
            var style = document.createElement('style');
            style.textContent = "\n    button.button-native {\n      font-weight: var(--is-button-native-font-weight);\n    }";
            host.shadowRoot.appendChild(style);
        });
        ///////////////////////////////////////////
        this.nextOffset = 0;
        this.noMoreData = false;
        this._doLoad(false);
    };
    ActividadesConClientesPage.prototype.buscarVisitas = function (cliente) {
        console.log("cliente ", cliente);
        this.nextOffset = 0;
        this.noMoreData = false;
        this.limit = 10;
        this._doLoad(false);
    };
    ActividadesConClientesPage.prototype.cancelarBusqueda = function () {
        this.cliente = null;
        this.nextOffset = 0;
        this.noMoreData = false;
        this.limit = 10;
        this._doLoad(false);
    };
    ActividadesConClientesPage.prototype.ionViewDidEnter = function () {
        this.initializeBackButtonCustomHandler();
    };
    ActividadesConClientesPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.navCtrl.navigateRoot(['home']);
        });
    };
    ActividadesConClientesPage.prototype.cargarFacturas = function (cliente) {
        this.navCtrl.navigateForward(['facturas', {
                datos: JSON.stringify({ cliente: cliente })
            }]);
    };
    ActividadesConClientesPage.prototype.cargarDocumentos = function (cliente) {
        this.navCtrl.navigateForward(['documentos', {
                datos: JSON.stringify({ cliente: cliente })
            }]);
    };
    ActividadesConClientesPage.prototype.llamar = function (item) {
        window.open('tel:2125551212', '_system');
    };
    ActividadesConClientesPage.prototype.abrirDir = function (item) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toEdit, modal;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        toEdit = false;
                        if (!item.latitud || !item.longitud || item.latitud === 0 || item.longitud === 0) {
                            this.toast.presentToastOptions('Necesita Ubicacion', 'No hay ubicacion, por favor elija una y presione guardar', ['Aceptar']);
                            toEdit = true;
                        }
                        return [4 /*yield*/, this.modalController.create({
                                component: SeleccionarPuntoUbicacionPage,
                                componentProps: {
                                    position: {
                                        lat: Number(item.latitud),
                                        lng: Number(item.longitud)
                                    },
                                    startEdit: toEdit
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (ubicacion) {
                            if (ubicacion !== null && ubicacion.data !== undefined) {
                                console.log('The result data:', ubicacion);
                                item.latitud = parseFloat(ubicacion.data.lat);
                                item.longitud = parseFloat(ubicacion.data.lng);
                                console.log('item', item);
                                _this.loadingService.present('Actualizando Ubicacion del cliente...');
                                _this.logicCliente.actualizarCliente(item).then(function (resp) {
                                    _this.clientesBDService.updateCliente(item);
                                    console.log('actualizado ', resp);
                                    _this.loadingService.dismiss();
                                    _this.toast.presentToastMedio("Ubicacion actualizada");
                                }).catch(function (error) {
                                    _this.loadingService.dismiss();
                                    console.log('error al actualizar la lat y long del cliente', error);
                                    _this.toast.presentToast("No pudimos actualizar la ubicacion del cliente");
                                });
                            }
                            else {
                                _this.toast.presentToast("Ubicacion no seleccionada");
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ActividadesConClientesPage.prototype.loadData = function (refresher) {
        var _this = this;
        this.noMoreData = false;
        //this.logicCliente.removeCache();
        this.nextOffset = 0;
        this.limit = 10;
        this.clientes = [];
        this.infinite = true;
        //this._doLoad(false).subscribe(null, null, () => {
        this._doLoad(false).then(function () {
            refresher.target.complete();
        }).catch(function (error) {
            refresher.target.complete();
            console.log('error en refresher ', error);
            _this.toast.presentToast(JSON.parse(error.error).error);
            //this.presentToastSimple("El servidor no esta disponible, intente mas tarde");
        });
        ;
    };
    ActividadesConClientesPage.prototype.doInfinite = function (event) {
        var _this = this;
        this._doLoad(true).then(function () {
            event.target.complete();
            if (_this.noMoreData) {
                event.target.disabled = true;
                _this.infinite = false;
                if (_this.opciones === "visitas") {
                    _this.toast.presentToast("No hay mas visitas");
                }
                if (_this.opciones === "documentos") {
                    _this.toast.presentToast('No hay mas entregas');
                }
                if (_this.opciones === "todos") {
                    _this.toast.presentToast("No hay mas visitas y entregas");
                }
            }
        }).catch(function (error) {
            event.target.complete();
            _this.toast.presentToast(JSON.parse(error.error).error);
            //this.presentToastSimple("El servidor no se encuentra disponible, verifique su conexion e intente nuevamente")
        });
    };
    ActividadesConClientesPage.prototype._doLoad = function (append) {
        var _this = this;
        if (append === void 0) { append = false; }
        var mensaje = null;
        if (this.opciones === "visitas") {
            this.tipoFiltro = 'F';
            mensaje = 'Obteniendo visitas';
        }
        if (this.opciones === "documentos") {
            this.tipoFiltro = 'E';
            mensaje = 'Obteniendo entregas';
        }
        if (this.opciones === "todos") {
            this.tipoFiltro = 'T';
            mensaje = "Obteniendo visitas y entregas";
        }
        //console.log(this.tipoFiltro);
        this.loadingService.present(mensaje);
        var ob = this._buildPromise();
        ob.then(function (value) {
            _this.loadingService.dismiss();
            //console.log("respuesta _doLoad ", value)
            if (append) {
                _this.clientes = _this.clientes.concat(value);
            }
            else {
                _this.clientes = value;
            }
            _this.noMoreData = value.length < 10;
            _this.nextOffset += value.length;
            if (_this.nextOffset <= 10) {
                _this.nextOffset += 1;
            }
            _this.limit += value.length;
        }).catch(function (error) {
            _this.loadingService.dismiss();
            _this.toast.presentToast(JSON.parse(error.error).error);
            //this.presentToastSimple("El servidor no esta disponible, intente mas tarde");
        });
        return ob;
    };
    ActividadesConClientesPage.prototype._buildPromise = function () {
        return this.clientesBDService.getListLocal(this.nextOffset, this.limit, this.cliente, this.tipoFiltro);
        //return this.logicCliente.getList(this.nextOffset, this.limit, this.cliente, this.tipoFiltro);
    };
    ActividadesConClientesPage.prototype.cambiarEvento = function (select) {
        this.nextOffset = 0;
        this.noMoreData = false;
        this.limit = 10;
        this.clientes = [];
        this._doLoad(false);
        this.infinite = true;
    };
    ActividadesConClientesPage = tslib_1.__decorate([
        Component({
            selector: 'app-actividades-con-clientes',
            templateUrl: './actividades-con-clientes.page.html',
            styleUrls: ['./actividades-con-clientes.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            ModalController,
            Platform,
            ClientesLogicaService,
            LoadingControllerService,
            ToastControllerService,
            Platform,
            ActivatedRoute,
            ClientesBDService])
    ], ActividadesConClientesPage);
    return ActividadesConClientesPage;
}());
export { ActividadesConClientesPage };
//# sourceMappingURL=actividades-con-clientes.page.js.map