import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { ClientesLogicaService } from 'src/app/Servicios/clienteService/clientes-logica.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
var AsignacionPage = /** @class */ (function () {
    function AsignacionPage(toast, loadingService, logic, plat, navCtrl, alertCtrl, writeLog) {
        this.toast = toast;
        this.loadingService = loadingService;
        this.logic = logic;
        this.plat = plat;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.writeLog = writeLog;
        this.noMoreData = false;
        this.infinite = true;
        this.limit = 10;
        this.clientes = [];
        this.cliente = null;
        this.buttonCondition = false;
    }
    AsignacionPage.prototype.ngOnInit = function () {
        this.tipoFiltro = 'N';
        this.habilitacion = JSON.parse(localStorage.getItem('habilitacion'));
        if (this.habilitacion > 0) {
            this.buttonCondition = false;
        }
        else {
            this.buttonCondition = true;
        }
        this.nextOffset = 0;
        this.noMoreData = false;
        this._doLoad(false);
    };
    AsignacionPage.prototype.ionViewDidEnter = function () {
        this.initializeBackButtonCustomHandler();
    };
    AsignacionPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.navCtrl.navigateRoot(['home']);
        });
    };
    AsignacionPage.prototype.loadData = function (refresher) {
        if (refresher === void 0) { refresher = null; }
        this.noMoreData = false;
        this.nextOffset = 0;
        this.limit = 10;
        this.clientes = [];
        this.infinite = true;
        this._doLoad(false).then(function () {
            refresher.target.complete();
        }).catch(function (err) {
            refresher.target.complete();
            console.log('error en refresher ');
        });
    };
    AsignacionPage.prototype.doInfinite = function (event) {
        var _this = this;
        this._doLoad(true).then(function () {
            event.target.complete();
            if (_this.noMoreData) {
                _this.infinite = false;
                event.target.disabled = true;
                _this.toast.presentToast("No hay mas clientes");
            }
        }).catch(function (err) {
            console.log('error en infinite scroll', err);
            event.target.complete();
            _this.toast.presentToast(JSON.parse(err.error).error);
        });
    };
    AsignacionPage.prototype._doLoad = function (append) {
        var _this = this;
        if (append === void 0) { append = false; }
        this.loadingService.present("Obteniendo clientes");
        var ob = this._buildPromise();
        ob.then(function (value) {
            _this.loadingService.dismiss();
            if (append) {
                _this.clientes = _this.clientes.concat(value);
            }
            else {
                _this.clientes = value;
            }
            _this.noMoreData = value.length < 10;
            _this.nextOffset += value.length;
            if (_this.nextOffset <= 10) {
                _this.nextOffset += 1;
            }
            _this.limit += value.length;
        }).catch(function (error) {
            _this.loadingService.dismiss();
            _this.toast.presentToast(JSON.parse(error.error).error);
            //this.presentToastSimple("El servidor no esta disponible, intente mas tarde");
        });
        return ob;
    };
    AsignacionPage.prototype._buildPromise = function () {
        return this.logic.getVisitasOnline(this.nextOffset, this.limit, this.cliente, this.tipoFiltro);
    };
    AsignacionPage.prototype.buscarClientes = function () {
        this.nextOffset = 0;
        this.noMoreData = false;
        this.limit = 10;
        this._doLoad(false);
    };
    AsignacionPage.prototype.cancelarBusqueda = function () {
        this.cliente = null;
        this.nextOffset = 0;
        this.noMoreData = false;
        this.limit = 10;
        this._doLoad(false);
    };
    AsignacionPage.prototype.asignarCliente = function (cliente) {
        var _this = this;
        var mensaje = 'Desea asignarse el cliente ' + cliente.razonSocial + ' ?';
        this.alertCtrl.alertMessageWithHandler("Asignacion de Cliente", mensaje, function (d) {
            _this.fecha_hora = new Date();
            _this.writeLog.escribirLog(_this.fecha_hora + 'Peticion: Asignacion de Cliente ' + cliente.razonSocial + '\n');
            _this.loadingService.present('Asignando ...');
            _this.logic.guardarCliente(cliente).then(function (response) {
                _this.fecha_hora = new Date();
                _this.loadingService.dismiss();
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con exito: ' + JSON.stringify(response) + '\n');
                _this.toast.presentToastMedio(response.exito);
            }).catch(function (error) {
                _this.fecha_hora = new Date();
                _this.loadingService.dismiss();
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + JSON.stringify(error) + '\n');
                _this.toast.presentToast(JSON.parse(error.error).error);
            });
        }, true);
    };
    AsignacionPage = tslib_1.__decorate([
        Component({
            selector: 'app-asignacion',
            templateUrl: './asignacion.page.html',
            styleUrls: ['./asignacion.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ToastControllerService,
            LoadingControllerService,
            ClientesLogicaService,
            Platform,
            NavController,
            AlertControllerService,
            WriteFileService])
    ], AsignacionPage);
    return AsignacionPage;
}());
export { AsignacionPage };
//# sourceMappingURL=asignacion.page.js.map