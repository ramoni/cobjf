import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AsignacionPage } from './asignacion.page';
var routes = [
    {
        path: '',
        component: AsignacionPage
    }
];
var AsignacionPageModule = /** @class */ (function () {
    function AsignacionPageModule() {
    }
    AsignacionPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AsignacionPage]
        })
    ], AsignacionPageModule);
    return AsignacionPageModule;
}());
export { AsignacionPageModule };
//# sourceMappingURL=asignacion.module.js.map