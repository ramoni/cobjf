import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FacturasPage } from './facturas.page';
import { PipesModuleModule } from 'src/app/Pipes/pipes-module.module';
var routes = [
    {
        path: '',
        component: FacturasPage
    }
];
var FacturasPageModule = /** @class */ (function () {
    function FacturasPageModule() {
    }
    FacturasPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                PipesModuleModule,
            ],
            declarations: [FacturasPage]
        })
    ], FacturasPageModule);
    return FacturasPageModule;
}());
export { FacturasPageModule };
//# sourceMappingURL=facturas.module.js.map