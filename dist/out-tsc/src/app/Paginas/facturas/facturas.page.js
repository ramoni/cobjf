import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { FacturasLogicaService } from 'src/app/Servicios/facturasService/facturas-logica.service';
import { ActivatedRoute } from '@angular/router';
import { NotasLogicaService } from 'src/app/Servicios/notaService/notas-logica.service';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { MonedaLogicaService } from 'src/app/Servicios/monedaService/moneda-logica.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
var FacturasPage = /** @class */ (function () {
    function FacturasPage(navCtrl, logic, alertService, plat, ruta, notasLogica, loadingService, toastControl, monedaLogic) {
        this.navCtrl = navCtrl;
        this.logic = logic;
        this.alertService = alertService;
        this.plat = plat;
        this.ruta = ruta;
        this.notasLogica = notasLogica;
        this.loadingService = loadingService;
        this.toastControl = toastControl;
        this.monedaLogic = monedaLogic;
        this.mensajeNotas = "No hay notas para mostrar";
        this.mensajeNotasCobrador = "No hay notas del cobrador para mostrar";
        this.nextOffset = 0;
        this.facturas = [];
        this.noMoreData = false;
        this.limit = 10;
        this.limitChecks = 10;
        this.countChecks = 0;
        this.buttonCondition = false;
        this.infinite = true;
        this.orden = 'nroFactura';
    }
    FacturasPage.prototype.ngOnInit = function () {
        this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente;
        console.log("cliente en facturas ", this.cliente);
        this.user = JSON.parse(localStorage.getItem('user'));
        this.habilitacion = JSON.parse(localStorage.getItem('habilitacion'));
        if (this.habilitacion > 0) {
            this.buttonCondition = false;
        }
        else {
            this.buttonCondition = true;
        }
        this.cargarNotasGenerales();
    };
    FacturasPage.prototype.ionViewDidEnter = function () {
        this.initializeBackButtonCustomHandler();
        this.nextOffset = 0;
        this.noMoreData = false;
        /////
        this.infinite = true;
        //////
        this.limit = 10;
        this._doLoad(false);
    };
    FacturasPage.prototype.cargarNotasGenerales = function () {
        if (this.cliente.notas === "null") {
            this.mensajeNotas = "No hay notas para mostrar";
        }
        else {
            this.mensajeNotas = this.cliente.notas;
        }
        this.notasVisitasCobrador();
    };
    FacturasPage.prototype.abrirDir = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var noTieneUbicacion, end;
            return tslib_1.__generator(this, function (_a) {
                noTieneUbicacion = false;
                if (!this.cliente.latitud || !this.cliente.longitud) {
                    //this.presentToast('No hay ubicacion, por favor elija una y presione guardar')
                    console.log('no tiene ubicacion el cliente');
                    noTieneUbicacion = true;
                }
                if (noTieneUbicacion) {
                    end = this.cliente.dir;
                }
                else {
                    end = [this.cliente.latitud, this.cliente.longitud];
                }
                launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function (isAvailable) {
                    var app;
                    //console.log('esta disponible',isAvailable)
                    if (isAvailable) {
                        console.log('esta disponible', isAvailable);
                        app = launchnavigator.APP.GOOGLE_MAPS;
                    }
                    else {
                        console.log("Google Maps not available - falling back to user selection");
                        app = launchnavigator.APP.USER_SELECT;
                    }
                    launchnavigator.navigate(end, {
                        app: app
                    });
                });
                return [2 /*return*/];
            });
        });
    };
    FacturasPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribeWithPriority(0, function () {
            _this.navCtrl.pop();
        });
    };
    FacturasPage.prototype.cambiarFiltro = function () {
        console.log(this.orden);
        this.nextOffset = 0;
        this.noMoreData = false;
        this._doLoad(false);
    };
    FacturasPage.prototype.cargarRecibos = function (fact) {
        this.navCtrl.navigateForward(['recibos-creados',
            { datos: JSON.stringify({ factura: fact }) }]);
    };
    FacturasPage.prototype.cobrar = function () {
        var aCobrar;
        aCobrar = this.facturas
            .filter(function (Factura) { return Factura.check; })
            .map(function (Factura) { return Factura.data; });
        if (aCobrar.length < 1) {
            this.toastControl.presentToast('Seleccione facturas');
            return;
        }
        var factura_fisrt = aCobrar[0].moneda;
        this.moneda = this.monedaLogic.getByIdOrName(factura_fisrt);
        this.navCtrl.navigateForward(['crear-recibo',
            { datos: JSON.stringify({
                    facturas: aCobrar,
                    cliente: this.cliente,
                    monedaRecibo: this.moneda
                }) }]);
    };
    FacturasPage.prototype.loadData = function (refresher) {
        var _this = this;
        if (refresher === void 0) { refresher = null; }
        this.noMoreData = false;
        this.nextOffset = 0;
        this.limit = 10;
        this.facturas = [];
        this.infinite = true;
        this.cargarNotasGenerales();
        this._doLoad(false).then(function () {
            refresher.target.complete();
        }).catch(function (error) {
            refresher.target.complete();
            _this.toastControl.presentToast(JSON.parse(error.error).error);
        });
    };
    FacturasPage.prototype._doLoad = function (append) {
        var _this = this;
        if (append === void 0) { append = false; }
        this.loadingService.present('Obteniendo facturas...');
        var ob = this._buildPromise();
        ob.then(function (value) {
            _this.loadingService.dismiss();
            if (append) {
                _this.facturas = _this.facturas.concat(value);
            }
            else {
                _this.facturas = value;
            }
            _this.noMoreData = value.length < 10;
            _this.nextOffset += value.length;
            if (_this.nextOffset <= 10) {
                _this.nextOffset += 1;
            }
            _this.limit += value.length;
        }).catch(function (error) {
            _this.loadingService.dismiss();
            console.log('No se pudo obtener localmente las facturas', error);
            _this.toastControl.presentToast('No se pudo obtener las facturas de la tabla local');
        });
        return ob;
    };
    FacturasPage.prototype._buildPromise = function () {
        return this.logic.obtenerFacturasLocal(this.cliente, this.nextOffset, this.limit, this.orden);
        //return this.logic.getList(this.cliente, this.nextOffset, this.limit, this.orden);
    };
    FacturasPage.prototype.ver = function (Factura) {
        this.cargarRecibos(Factura.data);
    };
    FacturasPage.prototype.doInfinite = function (event) {
        var _this = this;
        this._doLoad(true).then(function () {
            event.target.complete();
            if (_this.noMoreData) {
                _this.infinite = false;
                event.target.disabled = true;
                _this.toastControl.presentToast("No hay mas facturas");
            }
        }).catch(function (error) {
            event.target.complete();
            console.error('no pudo obtenerse las facturas del cliente', error);
            _this.toastControl.presentToast('Error al obtener las facturas del cliente');
        });
    };
    FacturasPage.prototype.notasVisitasCobrador = function () {
        var _this = this;
        this.notasLogica.loadNotaCobrador(Number(this.cliente.id), Number(JSON.parse(localStorage.getItem('habilitacion'))), 'F').then(function (notasCob) {
            _this.notasCobrador = notasCob;
            console.log('notas Cobrador facturas', _this.notasCobrador);
            if (_this.notasCobrador.id !== null && _this.notasCobrador.nota !== null) {
                _this.mensajeNotasCobrador = "";
                _this.mensajeNotasCobrador = _this.mensajeNotasCobrador.concat(_this.notasCobrador.nota);
            }
            else {
                _this.mensajeNotasCobrador = "No hay notas de cobrador que mostrar";
            }
        });
    };
    FacturasPage.prototype.mostrarNotas = function (opcion) {
        var _this = this;
        var buttons;
        var mensaje;
        if (opcion === '1') {
            mensaje = this.mensajeNotas;
            buttons =
                [
                    {
                        text: 'Aceptar',
                        handler: null
                    }
                ];
        }
        else {
            mensaje = this.mensajeNotasCobrador;
            buttons =
                [{
                        text: 'Editar',
                        handler: function () {
                            _this.navCtrl.navigateForward(['notas', {
                                    datos: JSON.stringify({
                                        idCliente: _this.cliente.id,
                                        nota: _this.notasCobrador,
                                        marca: 'F',
                                    })
                                }]);
                        }
                    },
                    {
                        text: 'Aceptar',
                        handler: null
                    }];
        }
        this.alertService.simpleAlert('Notas', mensaje, buttons);
    };
    FacturasPage = tslib_1.__decorate([
        Component({
            selector: 'app-facturas',
            templateUrl: './facturas.page.html',
            styleUrls: ['./facturas.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            FacturasLogicaService,
            AlertControllerService,
            Platform,
            ActivatedRoute,
            NotasLogicaService,
            LoadingControllerService,
            ToastControllerService,
            MonedaLogicaService])
    ], FacturasPage);
    return FacturasPage;
}());
export { FacturasPage };
//# sourceMappingURL=facturas.page.js.map