import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { DocumentosLogicaService } from 'src/app/Servicios/documentosService/documentos-logica.service';
import { ActivatedRoute } from '@angular/router';
import { NotasLogicaService } from 'src/app/Servicios/notaService/notas-logica.service';
import { LoadingControllerService } from 'src/app/Servicios/loaddingController/loading-controller.service';
import { ToastControllerService } from 'src/app/Servicios/toastController/toast-controller.service';
import { AlertControllerService } from 'src/app/Servicios/alertController/alert-controller.service';
import { WriteFileService } from 'src/app/Servicios/writeFileService/logWrite.service';
var DocumentosPage = /** @class */ (function () {
    function DocumentosPage(navCtrl, loadingService, platform, logic, plat, writeLog, ruta, logicaNotas, toast, alert) {
        this.navCtrl = navCtrl;
        this.loadingService = loadingService;
        this.platform = platform;
        this.logic = logic;
        this.plat = plat;
        this.writeLog = writeLog;
        this.ruta = ruta;
        this.logicaNotas = logicaNotas;
        this.toast = toast;
        this.alert = alert;
        this.nextOffset = 0;
        this.limit = 10;
        this.noMoreData = false;
        this.buttonCondition = false;
        this.mensajeNotasCobrador = "No hay notas del cobrador para mostrar";
        this.documents = [];
    }
    DocumentosPage.prototype.ngOnInit = function () {
        this.cliente = JSON.parse(this.ruta.snapshot.params.datos).cliente;
        this.user = JSON.parse(localStorage.getItem('user'));
        this.habilitacion = JSON.parse(localStorage.getItem('habilitacion'));
        if (this.habilitacion > 0) {
            this.buttonCondition = false;
        }
        else {
            this.buttonCondition = true;
        }
        this.nextOffset = 0;
        this.noMoreData = false;
        this._doLoad(false);
        this.notasDocumentosCobrador();
    };
    DocumentosPage.prototype.abrirDir = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var noTieneUbicacion, end;
            return tslib_1.__generator(this, function (_a) {
                noTieneUbicacion = false;
                if (!this.cliente.latitud || !this.cliente.longitud) {
                    //this.presentToast('No hay ubicacion, por favor elija una y presione guardar')
                    console.log('no tiene ubicacion el cliente');
                    noTieneUbicacion = true;
                }
                if (noTieneUbicacion) {
                    end = this.cliente.dir;
                }
                else {
                    end = [this.cliente.latitud, this.cliente.longitud];
                }
                launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function (isAvailable) {
                    var app;
                    //console.log('esta disponible',isAvailable)
                    if (isAvailable) {
                        console.log('esta disponible', isAvailable);
                        app = launchnavigator.APP.GOOGLE_MAPS;
                    }
                    else {
                        console.log("Google Maps not available - falling back to user selection");
                        app = launchnavigator.APP.USER_SELECT;
                    }
                    launchnavigator.navigate(end, {
                        app: app
                    });
                });
                return [2 /*return*/];
            });
        });
    };
    DocumentosPage.prototype.ionViewDidEnter = function () {
        this.initializeBackButtonCustomHandler();
    };
    DocumentosPage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.plat.backButton.subscribe(function () {
            _this.navCtrl.pop();
        });
    };
    DocumentosPage.prototype.loadData = function (refresher) {
        var _this = this;
        this.noMoreData = false;
        this.notasDocumentosCobrador();
        this._doLoad(true).then(function () {
            refresher.target.complete();
        }).catch(function (error) {
            console.error('error en obtener los datos de entregas localmente', error);
            refresher.target.complete();
            _this.toast.presentToast('No pudo obtenerse los documentos del cliente');
        });
    };
    DocumentosPage.prototype.doInfinite = function (event) {
        var _this = this;
        this._doLoad(true).then(function () {
            event.target.complete();
            if (_this.noMoreData) {
                event.target.disabled = true;
                _this.toast.presentToast('No hay mas documentos');
            }
        }).catch(function (error) {
            event.target.complete();
            console.error('no puede obtenerse las entregas del cliente', error);
            _this.toast.presentToast("El servidor no esta disponible, intente mas tarde");
        });
    };
    DocumentosPage.prototype._doLoad = function (append) {
        var _this = this;
        if (append === void 0) { append = false; }
        this.loadingService.present('Obteniendo documentos...');
        var ob = this._buildPromise();
        ob.then(function (value) {
            _this.loadingService.dismiss();
            if (append) {
                _this.documents = _this.documents.concat(value);
            }
            else {
                _this.documents = value;
            }
            _this.noMoreData = value.length < 10;
            _this.nextOffset += value.length;
            if (_this.nextOffset <= 10) {
                _this.nextOffset += 1;
            }
            _this.limit += value.length;
        }).catch(function (error) {
            console.log('error al cargar documentos ', error);
            _this.loadingService.dismiss();
            _this.toast.presentToast('No pudo obtenerse los documentos del cliente');
        });
        return ob;
    };
    DocumentosPage.prototype._buildPromise = function () {
        return this.logic.obtenerDocumentosLocal(this.cliente, this.nextOffset, this.limit);
    };
    DocumentosPage.prototype.guardar = function () {
        var _this = this;
        var aGuardar = [];
        var aGuardar1 = [];
        for (var _i = 0, _a = this.documents; _i < _a.length; _i++) {
            var row = _a[_i];
            //let toDocument = new Documentos(row.data);
            //console.log('los documentos ', toDocument);
            if (row.check === true) {
                //console.log('Marcado con 1');
                row.data.guardado = 1;
                row.data.imei = JSON.parse(localStorage.getItem('imei'));
                //console.log('se va a guardar ', row.data);
                aGuardar1.push(row.data);
            }
            else {
                //console.log('Marcado con 0');
                row.data.guardado = 0;
                row.data.imei = JSON.parse(localStorage.getItem('imei'));
                //console.log('no se va a guardar ', row.data);
            }
            aGuardar.push(row.data);
        }
        console.log('aGuardar ', aGuardar);
        /*if (aGuardar1.length < 1) {
          this.toast.presentToast('Seleccione Documentos')
          return;
        }*/
        this.fecha_hora = new Date();
        this.writeLog.escribirLog(this.fecha_hora + 'Peticion: Guardado de documentos ' + JSON.stringify(aGuardar) + '\n');
        this.alert.alertMessageWithHandler(this.title, "Esta seguro que desea entregar los documentos?", function (d) {
            _this.loadingService.present('Guardando...');
            _this.logic.addOrUpdate(aGuardar).subscribe(function (response) {
                console.log('el mensaje documentos', response.message);
                _this.fecha_hora = new Date();
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con exito: ' + JSON.stringify(response.message) + '\n');
                _this.loadingService.dismiss();
                _this.toast.presentToastMedio(response.message.mensaje);
            }, function (error) {
                _this.fecha_hora = new Date();
                _this.writeLog.escribirLog(_this.fecha_hora + 'Respuesta con error: ' + JSON.stringify(error) + '\n');
                _this.toast.presentToast(error);
                _this.loadingService.dismiss();
            });
        }, true);
    };
    DocumentosPage.prototype.notasDocumentosCobrador = function () {
        var _this = this;
        this.logicaNotas.loadNotaCobrador(Number(this.cliente.id), Number(JSON.parse(localStorage.getItem('habilitacion'))), 'E')
            .then(function (notasCob) {
            _this.notasCobrador = notasCob;
            console.log('notas Cobrador entregas', _this.notasCobrador);
            if (_this.notasCobrador.id !== null && _this.notasCobrador.nota !== null) {
                _this.mensajeNotasCobrador = "";
                _this.mensajeNotasCobrador = _this.mensajeNotasCobrador.concat(_this.notasCobrador.nota);
            }
            else {
                _this.mensajeNotasCobrador = "No hay notas de cobrador que mostrar";
            }
        });
    };
    DocumentosPage.prototype.mostrarNotas = function () {
        var _this = this;
        var mensaje = this.mensajeNotasCobrador;
        var buttons = [{
                text: 'Editar',
                handler: function () {
                    _this.navCtrl.navigateForward(['notas', {
                            datos: JSON.stringify({
                                idCliente: _this.cliente.id,
                                nota: _this.notasCobrador,
                                marca: 'E',
                            })
                        }]);
                }
            },
            {
                text: 'Aceptar',
                handler: null
            }];
        this.alert.simpleAlert('Notas', mensaje, buttons);
    };
    DocumentosPage = tslib_1.__decorate([
        Component({
            selector: 'app-documentos',
            templateUrl: './documentos.page.html',
            styleUrls: ['./documentos.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            LoadingControllerService,
            Platform,
            DocumentosLogicaService,
            Platform,
            WriteFileService,
            ActivatedRoute,
            NotasLogicaService,
            ToastControllerService,
            AlertControllerService])
    ], DocumentosPage);
    return DocumentosPage;
}());
export { DocumentosPage };
//# sourceMappingURL=documentos.page.js.map