import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DocumentosPage } from './documentos.page';
var routes = [
    {
        path: '',
        component: DocumentosPage
    }
];
var DocumentosPageModule = /** @class */ (function () {
    function DocumentosPageModule() {
    }
    DocumentosPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [DocumentosPage]
        })
    ], DocumentosPageModule);
    return DocumentosPageModule;
}());
export { DocumentosPageModule };
//# sourceMappingURL=documentos.module.js.map