var ValorRecibo = /** @class */ (function () {
    function ValorRecibo(tipo, moneda, numero, total, valorizado, banco, cuenta, fecha, modificarMoneda) {
        this.tipo = tipo;
        this.moneda = moneda;
        this.numero = numero;
        this.total = total;
        this.valorizado = valorizado;
        this.banco = banco;
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.modificarMoneda = modificarMoneda;
    }
    return ValorRecibo;
}());
export { ValorRecibo };
//# sourceMappingURL=valor-recibo.js.map