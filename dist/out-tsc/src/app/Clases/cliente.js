var Cliente = /** @class */ (function () {
    function Cliente(data) {
        this.id = data.ID_CLIENTE;
        this.razonSocial = data.RAZON_SOCIAL;
        this.ruc = data.RUC_CLIENTE;
        this.dir = data.DIR_CLIENTE;
        this.entregas = parseInt(data.ENTREGAS);
        this.facturas = parseInt(data.FACTURAS);
        this.latitud = data.LATITUD ? parseFloat(data.LATITUD) : 0;
        this.longitud = data.LONGITUD ? parseFloat(data.LONGITUD) : 0;
        this.entregas_doc = parseInt(data.ENTREGAS_DOC);
        this.recibos_fac = parseInt(data.RECIBOS_FAC);
        this.notas_fac = parseInt(data.NOTAS_FACTURAS);
        this.notas_ent = parseInt(data.NOTAS_ENTREGAS);
        this.notas = data.NOTAS;
    }
    return Cliente;
}());
export { Cliente };
//# sourceMappingURL=cliente.js.map