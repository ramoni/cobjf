var NumHabilitacion = /** @class */ (function () {
    function NumHabilitacion(habilitacion, datos, imei) {
        this.habilitacion = habilitacion;
        this.datos = datos;
        this.imei = imei;
        this.logs = null;
    }
    return NumHabilitacion;
}());
export { NumHabilitacion };
//# sourceMappingURL=num-habilitacion.js.map