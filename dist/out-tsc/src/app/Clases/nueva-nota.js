var NuevaNota = /** @class */ (function () {
    function NuevaNota(id, id_cliente, nota, latitud, longitud, tipo, habilitacion_caja, imei) {
        this.id = id;
        this.id_cliente = id_cliente;
        this.nota = nota;
        this.latitud = latitud;
        this.longitud = longitud;
        this.tipo = tipo;
        this.habilitacion_caja = habilitacion_caja;
        this.imei = imei;
    }
    return NuevaNota;
}());
export { NuevaNota };
//# sourceMappingURL=nueva-nota.js.map