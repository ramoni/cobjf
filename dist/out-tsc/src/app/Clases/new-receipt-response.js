var NewReceiptResponse = /** @class */ (function () {
    function NewReceiptResponse(data) {
        this.message = data.exito;
        this.id = Number(data.ID_RECIBO);
        this.totalHabilitacion = data.totalHabilitacion;
        this.reciboOriginal = data.RECIBO_ORIGINAL;
        this.reciboDuplicado = data.RECIBO_DUPLICADO;
    }
    return NewReceiptResponse;
}());
export { NewReceiptResponse };
//# sourceMappingURL=new-receipt-response.js.map