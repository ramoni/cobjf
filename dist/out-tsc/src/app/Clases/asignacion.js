var Asignacion = /** @class */ (function () {
    function Asignacion(data) {
        this.visitas = data.VISITAS;
        this.entregas = data.ENTREGAS;
        this.id_cliente = data.ID_CLIENTE;
        this.imei = JSON.parse(localStorage.getItem('imei'));
    }
    return Asignacion;
}());
export { Asignacion };
//# sourceMappingURL=asignacion.js.map