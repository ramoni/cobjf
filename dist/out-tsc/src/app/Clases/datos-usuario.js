var DatosUsuario = /** @class */ (function () {
    function DatosUsuario(data) {
        this.CAJA = data.CAJA;
        this.DOCUMENTOS = parseInt(data.DOCUMENTOS);
        this.EXPIRED = data.EXPIRED;
        this.FACTURAS = parseInt(data.FACTURAS);
        this.HABILITACION = parseInt(data.HABILITACION);
        this.DATOS_SINCRONIZADOS.RECIBOS = data.DATOS_SINCRONIZADOS.RECIBOS;
        this.DATOS_SINCRONIZADOS.ENTREGAS = data.DATOS_SINCRONIZADOS.ENTREGAS;
        this.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS = data.DATOS_SINCRONIZADOS.NOTAS_ENTREGAS;
        this.DATOS_SINCRONIZADOS.NOTAS_VISITAS = data.DATOS_SINCRONIZADOS.NOTAS_VISITAS;
    }
    return DatosUsuario;
}());
export { DatosUsuario };
//# sourceMappingURL=datos-usuario.js.map