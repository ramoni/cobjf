var Value = /** @class */ (function () {
    function Value(tipo, moneda, numero, total, valorizado, banco, cuenta, fecha) {
        this.tipo = tipo;
        this.moneda = moneda;
        this.numero = numero;
        this.total = total;
        this.valorizado = valorizado;
        this.banco = banco;
        this.cuenta = cuenta;
        this.fecha = fecha;
    }
    return Value;
}());
export { Value };
//# sourceMappingURL=value.js.map