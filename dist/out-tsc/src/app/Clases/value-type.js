var ValueType = /** @class */ (function () {
    function ValueType(data) {
        this.id = data.ID_VALOR.toString();
        this.descripcion = data.DESCRIPCION;
        this.abreviacion = data.ABREVIACION;
        this.has_number = parseInt(data.IND_NUMERO);
        this.has_cuenta = parseInt(data.IND_CUENTA);
        this.has_banco = parseInt(data.IND_BANCO);
        this.has_fecha = parseInt(data.IND_FECHA);
        this.id_moneda = data.ID_MONEDA;
    }
    return ValueType;
}());
export { ValueType };
//# sourceMappingURL=value-type.js.map