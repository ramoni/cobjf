var Entidad = /** @class */ (function () {
    function Entidad(data) {
        this.id = data.ID_ENTIDAD.toString();
        this.descripcion = data.DESCRIPCION;
    }
    return Entidad;
}());
export { Entidad };
//# sourceMappingURL=entidad.js.map