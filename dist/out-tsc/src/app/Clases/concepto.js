var Concepto = /** @class */ (function () {
    function Concepto(data) {
        this.id_concepto = data.ID_CONCEPTO.toString();
        this.descripcion = data.DESCRIPCION;
    }
    return Concepto;
}());
export { Concepto };
//# sourceMappingURL=concepto.js.map