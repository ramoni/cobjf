var Cuenta = /** @class */ (function () {
    function Cuenta(data) {
        this.id = data.ID_CUENTA.toString();
        this.descripcion = data.DESCRIPCION;
        this.id_entidad = data.ID_ENTIDAD;
        this.id_moneda = data.ID_MONEDA;
    }
    return Cuenta;
}());
export { Cuenta };
//# sourceMappingURL=cuenta.js.map