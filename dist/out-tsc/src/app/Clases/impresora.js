var Impresora = /** @class */ (function () {
    function Impresora(macAddress, name) {
        this.macAddress = macAddress;
        this.name = name;
    }
    return Impresora;
}());
export { Impresora };
//# sourceMappingURL=impresora.js.map