var ReciboDTO = /** @class */ (function () {
    function ReciboDTO(fecha, id_cliente, id_moneda, cotizacion, interes, total_recibo, anticipo, version, facturas, valores, latitud, longitud, marca, id_concepto, imei) {
        this.fecha = fecha;
        this.id_cliente = id_cliente;
        this.id_moneda = id_moneda;
        this.cotizacion = cotizacion;
        this.interes = interes;
        this.total_recibo = total_recibo;
        this.anticipo = anticipo;
        this.version = version;
        this.facturas = facturas;
        this.valores = valores;
        this.latitud = latitud;
        this.longitud = longitud;
        this.marca = marca;
        this.id_concepto = id_concepto;
        this.imei = imei;
    }
    return ReciboDTO;
}());
export { ReciboDTO };
//# sourceMappingURL=recibo-DTO.js.map