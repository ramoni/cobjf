var InvoiceWithACobrar = /** @class */ (function () {
    function InvoiceWithACobrar(data, valorizado) {
        this.data = data;
        this.valorizado = valorizado;
        this.a_cobrar = valorizado;
    }
    return InvoiceWithACobrar;
}());
export { InvoiceWithACobrar };
//# sourceMappingURL=invoice-with-acobrar.js.map