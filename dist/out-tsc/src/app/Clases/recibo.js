import { InvoiceWithACobrar } from './invoice-with-acobrar';
import { Factura } from './factura';
import * as moment from 'moment';
import { ValorRecibo } from './valor-recibo';
var Recibo = /** @class */ (function () {
    function Recibo(cliente, data) {
        this.cliente = cliente;
        this.id = data.ID_RECIBO_CAB;
        this.nro_recibo = data.NRO_RECIBO;
        this.id_moneda = data.ID_MONEDA;
        this.fecha = moment(data.FEC_RECIBO, 'DD-MM-YYYY').toDate();
        this.total = parseFloat(data.TOT_RECIBO);
        this.estado = data.ESTADO;
        this.anticipo = data.ANTICIPO;
        this.cotizacion = data.COTIZACION;
        this.interes = data.INTERES;
        this.version = data.VERSION;
        this.latitud = data.LATITUD;
        this.longitud = data.LONGITUD;
        this.marca = data.MARCA;
        this.concepto = data.CONCEPTO;
        this.imei = data.IMEI;
        this.facturas = [];
        this.valores = null;
        this.sincronizacion = data.REQUIERE_SINCRONIZACION;
    }
    Recibo.prototype.loadData = function (config, data, vLogic, monedaLogic) {
        console.log('data de recibos ', data);
        var datos = data.CABECERA;
        this.id = datos.ID_RECIBO_CAB;
        this.fecha = datos.FEC_RECIBO ? moment(datos.FEC_RECIBO, config.DEFAULT_DATE_FORMAT).toDate() : null;
        this.estado = datos.ESTADO;
        this.id_moneda = datos.ID_MONEDA || '1';
        this.cotizacion = parseFloat(datos.COTIZACION || 0);
        this.total = parseFloat(datos.TOTAL_RECIBO || 0);
        this.interes = parseFloat(datos.INTERES || 0);
        this.anticipo = parseFloat(datos.ANTICIPO || 0);
        this.latitud = datos.LATITUD;
        this.longitud = datos.LONGITUD;
        this.facturas = [];
        if (data.DETALLE && data.DETALLE.FACTURAS) {
            for (var _i = 0, _a = data.DETALLE.FACTURAS; _i < _a.length; _i++) {
                var det = _a[_i];
                var fa = new InvoiceWithACobrar(new Factura(det, {}), parseFloat(det.MONTO_PAGADO));
                console.log(parseFloat(det.MONTO_PAGADO), det.MONTO_PAGADO, det);
                this.facturas.push(fa);
            }
        }
        this.valores = [];
        if (data.VALORES && data.VALORES.VALORES) {
            for (var _b = 0, _c = data.VALORES.VALORES; _b < _c.length; _b++) {
                var det = _c[_b];
                var va = new ValorRecibo(vLogic.getByIdOrName(det.ID_TIPO_VALOR), monedaLogic.getByIdOrName(det.ID_MONEDA), det.NUMERO_VALOR, parseFloat(det.MONTO), 0, det.BANCO, det.CUENTA, moment(det.fecha, config.DEFAULT_DATE_FORMAT).format('YYYY-MM-DD'), false);
                va.id = det.ID_VALOR;
                this.valores.push(va);
            }
        }
        return this;
    };
    return Recibo;
}());
export { Recibo };
//# sourceMappingURL=recibo.js.map