var DocumentWithCheck = /** @class */ (function () {
    function DocumentWithCheck(data, check) {
        this.data = data;
        this.check = check;
    }
    return DocumentWithCheck;
}());
export { DocumentWithCheck };
//# sourceMappingURL=document-with-check.js.map