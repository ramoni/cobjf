var Notas = /** @class */ (function () {
    function Notas(data) {
        if (data === void 0) { data = null; }
        if (data != null) {
            this.id = parseInt(data.ID);
            this.nota = data.NOTAS;
            this.fecha = data.FEC_VISITA;
            this.id_cliente = data.ID_CLIENTE;
            this.latitud = data.LATITUD;
            this.longitud = data.LONGITUD;
            this.tipo = data.TIPO;
            this.habilitacion_caja = data.HABILITACION;
            this.sincronizacion = data.REQUIERE_SINCRONIZACION;
        }
        else {
            this.id = null;
            this.nota = null;
            this.fecha = null;
            this.id_cliente = null;
            this.latitud = null;
            this.longitud = null;
            this.tipo = null;
            this.habilitacion_caja = null;
            this.sincronizacion = null;
        }
    }
    return Notas;
}());
export { Notas };
//# sourceMappingURL=notas.js.map