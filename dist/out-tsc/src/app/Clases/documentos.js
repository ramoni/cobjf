var Documentos = /** @class */ (function () {
    function Documentos(data) {
        this.id_entrega = parseInt(data.ID_ENTREGA);
        this.id_entrega_det = data.ID_ENTREGA_DET;
        this.id_cobrador = data.ID_COBRADOR;
        this.tipo_documento = data.ID_TIPO;
        this.id_documento = data.ID_DOCUMENTO;
        this.no_documento = data.NO_DOCUMENTO;
        this.fecha_documento = data.FECHA_DOCUMENTO;
        this.id_moneda = data.ID_MONEDA;
        this.simbolo = data.SIMBOLO;
        this.importe = parseFloat(data.IMPORTE);
        this.guardado = parseInt(data.GUARDADO);
        this.tipo = data.TIPO;
        this.id_cliente = data.ID_CLIENTE;
        this.latitud = data.LATITUD;
        this.longitud = data.LONGITUD;
        this.imei = data.IMEI;
    }
    return Documentos;
}());
export { Documentos };
//# sourceMappingURL=documentos.js.map