var Moneda = /** @class */ (function () {
    function Moneda(data) {
        this.id = data.ID_MONEDA.toString();
        this.descripcion = data.DESCRIPCION_MONEDA;
        this.activo = data.ACTIVO.toString();
        this.cant_decimales = parseInt(data.CANT_DECIMALES);
        this.simbolo = data.SIMBOLO;
        this.plural = data.MONEDA_PLURAL;
        this.singular = data.MONEDA_SINGULAR;
    }
    return Moneda;
}());
export { Moneda };
//# sourceMappingURL=moneda.js.map