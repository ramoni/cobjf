var NewCobroRequest = /** @class */ (function () {
    function NewCobroRequest(idCliente, notas, latitud, longitud) {
        this.id_cliente = idCliente;
        this.notas = notas;
        this.latitud = latitud;
        this.longitud = longitud;
    }
    return NewCobroRequest;
}());
//# sourceMappingURL=new-cobro-request.js.map