import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './Guards/auth-guard.service';
import { NotAuthGuardService } from './Guards/notAuthGuard/not-auth-guard.service';
var routes = [
    //Por defecto enviar al login
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    //Rutas agrupadas con un guard que controla que no esten logueados
    {
        path: '',
        canActivate: [NotAuthGuardService],
        children: [
            //Ruta para loguearse
            { path: 'login', loadChildren: './Paginas/login/login.module#LoginPageModule' },
        ]
    },
    //Rutas agrupadas y controladas por un guard que controla que esten logueados
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            { path: 'home', loadChildren: './Paginas/home/home.module#HomePageModule' },
            { path: 'documentos', loadChildren: './Paginas/documentos/documentos.module#DocumentosPageModule' },
            { path: 'actividades', loadChildren: './Paginas/actividades-con-clientes/actividades-con-clientes.module#ActividadesConClientesPageModule' },
            { path: 'notas', loadChildren: './Paginas/notas/notas.module#NotasPageModule' },
            { path: 'facturas', loadChildren: './Paginas/facturas/facturas.module#FacturasPageModule' },
            { path: 'seleccionar-punto-ubicacion', loadChildren: './Paginas/seleccionar-punto-ubicacion/seleccionar-punto-ubicacion.module#SeleccionarPuntoUbicacionPageModule' },
            { path: 'cambiar-pass', loadChildren: './Paginas/cambiar-pass/cambiar-pass.module#CambiarPassPageModule' },
            { path: 'asignacion', loadChildren: './Paginas/asignacion/asignacion.module#AsignacionPageModule' },
            { path: 'resumen-cobro', loadChildren: './Paginas/resumen-cobro/resumen-cobro.module#ResumenCobroPageModule' },
            { path: 'crear-recibo', loadChildren: './Paginas/crear-recibo/crear-recibo.module#CrearReciboPageModule' },
            { path: 'recibos-creados', loadChildren: './Paginas/recibos-creados/recibos-creados.module#RecibosCreadosPageModule' },
        ]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
                FormsModule,
                ReactiveFormsModule,
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map