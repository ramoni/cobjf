import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import * as moment from 'moment';
var MomentPipe = /** @class */ (function () {
    function MomentPipe() {
    }
    MomentPipe.prototype.transform = function (value, format) {
        if (format === void 0) { format = 'DD/MM/YYYY'; }
        if (!value)
            return '';
        return moment(value, 'DD-MMM-YYYY').format(format);
    };
    MomentPipe = tslib_1.__decorate([
        Pipe({
            name: 'moment'
        })
    ], MomentPipe);
    return MomentPipe;
}());
export { MomentPipe };
//# sourceMappingURL=moment.pipe.js.map