import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonedaPipe } from './moneda/moneda.pipe';
import { MomentPipe } from './moment/moment.pipe';
import { FilterPipe } from './filtrado/filter.pipe';
var PipesModuleModule = /** @class */ (function () {
    function PipesModuleModule() {
    }
    PipesModuleModule = tslib_1.__decorate([
        NgModule({
            declarations: [MonedaPipe, MomentPipe, FilterPipe],
            imports: [
                CommonModule
            ],
            exports: [MonedaPipe, MomentPipe, FilterPipe],
        })
    ], PipesModuleModule);
    return PipesModuleModule;
}());
export { PipesModuleModule };
//# sourceMappingURL=pipes-module.module.js.map