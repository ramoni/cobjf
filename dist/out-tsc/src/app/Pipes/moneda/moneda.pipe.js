import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { MonedaLogicaService } from 'src/app/Servicios/monedaService/moneda-logica.service';
var MonedaPipe = /** @class */ (function () {
    function MonedaPipe(logicaMoneda) {
        this.logicaMoneda = logicaMoneda;
    }
    MonedaPipe.prototype.transform = function (value, currencySign, decimalLength, chunkDelimiter, decimalDelimiter, chunkLength) {
        if (currencySign === void 0) { currencySign = ''; }
        if (decimalLength === void 0) { decimalLength = 0; }
        if (chunkDelimiter === void 0) { chunkDelimiter = '.'; }
        if (decimalDelimiter === void 0) { decimalDelimiter = ','; }
        if (chunkLength === void 0) { chunkLength = 3; }
        if (!currencySign)
            currencySign = 'GUARANIES';
        var moneda = this.logicaMoneda.getByIdOrName(currencySign);
        if (moneda) {
            currencySign = moneda.simbolo;
            decimalLength = moneda.cant_decimales;
        }
        else {
            if (currencySign === 'DOLARES') {
                currencySign = 'USD. ';
                decimalLength = 2;
            }
            else {
                currencySign = 'Gs. ';
                decimalLength = 0;
            }
        }
        var result = '\\d(?=(\\d{' + chunkLength + '})+' + (decimalLength > 0 ? '\\D' : '$') + ')';
        var num = value.toFixed(Math.max(0, ~~decimalLength));
        return (decimalDelimiter ? num.replace('.', decimalDelimiter) : num).replace(new RegExp(result, 'g'), '$&' + chunkDelimiter) + ' ' + currencySign;
    };
    MonedaPipe = tslib_1.__decorate([
        Pipe({
            name: 'moneda'
        }),
        tslib_1.__metadata("design:paramtypes", [MonedaLogicaService])
    ], MonedaPipe);
    return MonedaPipe;
}());
export { MonedaPipe };
//# sourceMappingURL=moneda.pipe.js.map