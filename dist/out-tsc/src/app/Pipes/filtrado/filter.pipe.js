import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    /*transform(items: any[], field: string, value: string): any[] {
      if (!items) return [];
      return items.filter(entidad => entidad[field] == value);
    }*/
    FilterPipe.prototype.transform = function (items, value1, value2) {
        var filter = { id_entidad: value1, id_moneda: value2 };
        //let filter={id_entidad:value1};
        console.log('El filtro es ', filter);
        console.log('Los items son ', items);
        if (!items)
            return [];
        return items.filter(function (item) {
            var notMatchingField = Object.keys(filter)
                .find(function (key) { return item[key] !== filter[key]; });
            //console.log('notMatchingField ', notMatchingField);
            return !notMatchingField; // true if matches all field
        });
    };
    FilterPipe = tslib_1.__decorate([
        Pipe({
            name: 'filter'
        })
    ], FilterPipe);
    return FilterPipe;
}());
export { FilterPipe };
//# sourceMappingURL=filter.pipe.js.map