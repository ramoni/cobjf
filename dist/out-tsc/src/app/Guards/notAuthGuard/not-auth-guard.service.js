import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AuthService } from '../../Servicios/autenticacionService/auth.service';
import { Router } from '@angular/router';
var NotAuthGuardService = /** @class */ (function () {
    function NotAuthGuardService(_authService, _router) {
        this._authService = _authService;
        this._router = _router;
    }
    NotAuthGuardService.prototype.canActivate = function (next, state) {
        if (!this._authService.isAuthenticated()) {
            return true;
        }
        // navigate to login page
        this._router.navigate(['/home']);
        // you can save redirect url so after authing we can move them back to the page they requested
        return false;
    };
    NotAuthGuardService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [AuthService, Router])
    ], NotAuthGuardService);
    return NotAuthGuardService;
}());
export { NotAuthGuardService };
//# sourceMappingURL=not-auth-guard.service.js.map