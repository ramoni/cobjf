import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Toast } from '@ionic-native/toast/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { File } from '@ionic-native/file/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Uid } from '@ionic-native/uid/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpModule } from '@angular/http';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SeleccionarPuntoUbicacionPageModule } from './Paginas/seleccionar-punto-ubicacion/seleccionar-punto-ubicacion.module';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { Network } from '@ionic-native/network/ngx';
import { AuthGuard } from './Guards/auth-guard.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [AppComponent],
            entryComponents: [],
            imports: [
                BrowserModule,
                IonicModule.forRoot(),
                AppRoutingModule,
                HttpModule,
                SeleccionarPuntoUbicacionPageModule,
            ],
            providers: [
                StatusBar,
                SplashScreen,
                Toast,
                AppVersion,
                AndroidPermissions,
                Geolocation,
                Uid,
                HTTP,
                GoogleMaps,
                LocationAccuracy,
                SQLite,
                Network,
                AuthGuard,
                File,
                ScreenOrientation,
                { provide: NavParams, useClass: /** @class */ (function () {
                        function class_1() {
                            this.NavParams = jasmine.createSpy("NavParams");
                        }
                        return class_1;
                    }()) },
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
            ],
            exports: [],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map