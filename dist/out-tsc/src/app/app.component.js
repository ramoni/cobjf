import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Uid } from '@ionic-native/uid/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { AlertControllerService } from './Servicios/alertController/alert-controller.service';
import { SincronizacionService } from './Servicios/sincronizacionService/sincronizacion.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, navControl, gpsRequest, androidPermissions, appVersion, toast, uid, alertService, syncro, screenOrientation) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.navControl = navControl;
        this.gpsRequest = gpsRequest;
        this.androidPermissions = androidPermissions;
        this.appVersion = appVersion;
        this.toast = toast;
        this.uid = uid;
        this.alertService = alertService;
        this.syncro = syncro;
        this.screenOrientation = screenOrientation;
        this.appPages = [
            {
                title: 'Inicio',
                url: '/home',
                icon: 'ios-home'
            },
            {
                title: 'Actividades',
                url: '/actividades',
                icon: 'ios-navigate'
            },
            {
                title: 'Asignar',
                url: '/asignacion',
                icon: 'ios-clipboard'
            },
            {
                title: 'Contraseña',
                url: '/cambiar-pass',
                icon: 'key'
            },
        ];
        this.initializeApp();
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.overlaysWebView(false);
            _this.statusBar.backgroundColorByHexString('#C9113C');
            _this.statusBar.show();
            _this.splashScreen.hide();
            /*(<any>window).plugins.preventscreenshot.disable((a) =>
            this.successCallback(a),
            (b) => this.errorCallback(b));*/
            _this.versionImei();
            _this.syncro.sincronizarTodo();
            _this.screenOrientation.lock(_this.screenOrientation.ORIENTATIONS.PORTRAIT);
        });
    };
    /*successCallback(result) {
      console.log(result); // true - enabled, false - disabled
    }
  
    errorCallback(error) {
      console.log(error);
    }*/
    AppComponent.prototype.openActividades = function () {
        this.navControl.navigateForward(['actividades', { datos: JSON.stringify({ marca: "todos" }) }]);
    };
    AppComponent.prototype.logout = function () {
        this.alertService.mostrarConfirmacionLogout();
    };
    AppComponent.prototype.checkGPSPermission = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
                            if (result.hasPermission) {
                                //If having permission show 'Turn On GPS' dialogue
                                _this.encenderGPS();
                            }
                            else {
                                //If not having permission ask for permission
                                _this.requestGPSPermission();
                            }
                        }, function (err) {
                            alert(err);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppComponent.prototype.encenderGPS = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.gpsRequest.request(this.gpsRequest.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
                            // When GPS Turned ON call method to get Accurate location coordinates
                            console.log('Encendio el gps');
                        }, 
                        //error => alert('Error al solicitar encendido de GPS ' + JSON.stringify(error))
                        function (error) { return console.log('Error al solicitar encendido de GPS ' + JSON.stringify(error)); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppComponent.prototype.requestGPSPermission = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.gpsRequest.canRequest().then(function (canRequest) {
                            if (canRequest) {
                                console.log("4");
                            }
                            else {
                                //Show 'GPS Permission Request' dialogue
                                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                                    .then(function () {
                                    // call method to turn on GPS
                                    _this.encenderGPS();
                                }, function (error) {
                                    //Show alert if user click on 'No Thanks'
                                    //alert('Error al preguntar los permisos de acceso a la ubicacion ' + error)
                                    console.log('Error al preguntar los permisos de acceso a la ubicacion ' + error);
                                });
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppComponent.prototype.versionImei = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.appVersion.getVersionNumber().then(function (dat) {
                            localStorage.setItem('version', JSON.stringify(dat));
                        }).catch(function (err) {
                            console.log('error al obtener la version de la App ', err);
                        })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.getIMEI().then(function (resp) {
                                console.log("imei del telefono en app-compo getImei", resp);
                                localStorage.setItem('imei', JSON.stringify(resp));
                            }).catch(function (err) {
                                console.log('No se obtine el imei del telefono ', err);
                            })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppComponent.prototype.getIMEI = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var hasPermission, result;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE)];
                    case 1:
                        hasPermission = (_a.sent()).hasPermission;
                        if (!!hasPermission) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE)];
                    case 2:
                        result = _a.sent();
                        if (!result.hasPermission) {
                            console.error('Se necesita permisos');
                            throw new Error('Se necesita permisos ');
                        }
                        console.log('se dio los permisos');
                        this.toast.show('En el proximo reinicio obtendremos el imei', '5000', 'center').subscribe(function (toast) {
                            console.log(toast);
                        });
                        //this.checkGPSPermission();
                        setTimeout(function () {
                            navigator['app'].exitApp();
                        }, 3000);
                        // ok, a user gave us permission, we can get him identifiers after restart app
                        return [2 /*return*/];
                    case 3:
                        this.checkGPSPermission();
                        return [2 /*return*/, this.uid.IMEI];
                }
            });
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar,
            NavController,
            LocationAccuracy,
            AndroidPermissions,
            AppVersion,
            Toast,
            Uid,
            AlertControllerService,
            SincronizacionService,
            ScreenOrientation])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map